#python3
import glob
#import root_numpy
import uproot
import pandas as pd
from pandas import HDFStore
from datetime import datetime
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import math
import csv
import argparse
import pickle
import os
import multiprocessing as mp

def plot_hist(sample):
    print(sample+'\n')
    #labels=[str(i*10+220) for i in range(0,178)]
    #labels=['0','200','450','700','950','1200','1450','1700','1950']
    labels=['-5.6','-5.0','-4.4','-3.8','-3.2','-2.6','-2.0','-1.4','1.0']
    for sys in ['Nominal','ExptSys','TheoSys']:
        i=0
        for term in dict(sorted(hists_f[sample][sys].items())).keys():
            print('plotting ' + str(term))
            fig1,ax = plt.subplots(figsize=(12,9))
            if sys != 'Nominal':
                print(sample)
                #print(type(hists_f[sample]['Nominal']['Nom_hist'].values()))
                #print(hists_f[sample]['Nominal']['Nom_hist'].values())
                plt.plot(range(0,len(hists_f[sample][sys][term].values())), hists_f[sample][sys][term].values()/(hists_f[sample]['Nominal']['Nom_hist'].values()+10e-10), label=sample+': #'+str(i)+': '+str(term.split(';')[0]),linestyle='solid',**kwargs)
            else:
                plt.plot(range(0,len(hists_f[sample][sys][term].values())), hists_f[sample][sys][term].values(), label=str(i)+': '+sample+': '+str(term),linestyle='solid',**kwargs)
            plt.legend(loc='upper right',fontsize =15)
            plt.ylabel("Cross-section distribution", fontsize=25)
            #ax.set_xlabel(r'$m_{4\ell}$ [GeV]', fontsize=25)
            ax.set_xlabel('MCFM_MELA_ggZZ', fontsize=25)
            plt.xticks(fontsize=25)
            plt.yticks(fontsize=25)
            #print(hists_f[sample][sys][term].values())
            #print(len(hists_f[sample][sys][term].values()))
            ax.set_xticklabels(labels)
            #plt.yscale('symlog', linthresh=1)
            if sys != 'Nominal':
                plt.ylim(0.7, 1.3)
                plt.ylabel("Ratio to Nominal", fontsize=25)
                plt.axhline(y=1, color='black', linestyle='--')
            outdir=OutFolder+"/"+sample+'/'+sys+'/'
            os.system('mkdir -p '+outdir)
            plt.savefig(outdir+'/'+str(i)+'_'+str(term.split(';')[0])+".png",dpi=200)
            plt.close()
            i+=1


parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--inpath',  type=str, default='', help='Add path to the sample directory (only) each campaign accesed separately')
parser.add_argument('--outpath', type=str, default='', help='Add path to output')

args = parser.parse_args()
OutFolder = args.outpath
input_file_loc = args.inpath

OutFolder = 'outputs_04June2022/outputs_04June2022_plot_MCFM/'
#input_file_loc = 'outputs_04June2022/Merged_code1_Final_WS/'
input_file_loc = 'outputs_04June2022/Merged_code1_Final_WS_MCFM/'

os.system('mkdir -p '+ OutFolder)
hists_f = {}
hists_f['qqZZ']    = uproot.open(input_file_loc+"/qqZZ_hists.root")
hists_f['ggZZ']    = uproot.open(input_file_loc+"/ggZZ_hists.root")
hists_f['OtpSM']   = uproot.open(input_file_loc+"/OtpSM_hists.root")
hists_f['OpgSqrd'] = uproot.open(input_file_loc+"/OpgSqrd_hists.root")
hists_f['OtpSqrd'] = uproot.open(input_file_loc+"/OtpSqrd_hists.root")

#print(hists_f['ggZZ']['ExptSys'].keys())
#print(hists_f['ggZZ']['ExptSys'].values())
kwargs = dict(linewidth=3)


pool = mp.Pool(mp.cpu_count())

results = pool.map(plot_hist, ['ggZZ','qqZZ','OtpSM','OpgSqrd','OtpSqrd'])

pool.close()
