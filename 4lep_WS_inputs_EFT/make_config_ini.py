#python3 needed
import numpy as np
import os

print(f'generating config file... (If SyntaxError, please use python3)')

#range_x = [-2.0,1.7]
#range_x = [-5.0,1.0]
#range_x = [-5,1]
range_x_SR = [-5.0,1.0]
range_x_CR = [180.0,220.0]

#binning = 14 #total bins
#binning = 10 #total bins
#binning = 178
#binning = np.linspace(range_x[0],range_x[1],binning+1)

#binning = [-2.0, -1.72, -1.43, -1.15, -0.86, -0.58, -0.29, -0.01, 0.28, 0.56, 1.7]

binning_CR = [180.0,190.0,200.0,210.0,220.0]
binning_SR = [-5.0, -4.4, -3.8, -3.2, -2.6, -2.0, -1.4, -0.8, -0.2, 1.0]

config_filename='WS_inputs_config/07Aug2022_EFT_4l_config_file_wCR_wSys_noMC_MCFM_rebinlast2bin.ini'
f=open(config_filename,'w')

f.write('[main]\n\n')
f.write('fileDir = ./WS_inputs_07Aug2022_Final_WS_MCFM\n\n')
f.write('NPlist = nuisance.txt\n\n')
f.write('data = /afs/cern.ch/user/w/weiy/offshell/data/CROnlyData/dataAll_blinded_220.root\n\n')
obs_CR='m4l_fsr'
obs_SR='MCFM_MELA_ggZZ'
#obs='ME_dis'
#obs='ggFNN_MELA'

f.write('categories = ')
for i in range(len(binning_CR)-1):
    f.write(f'{obs_CR}_CR_4l_Incl_bin_{i+1}_13TeV,')
for i in range(len(binning_SR)-1):
    if i != (len(binning_SR)-2):
        f.write(f'{obs_SR}_SR_4l_Incl_bin_{i+1}_13TeV,')
    else:
        f.write(f'{obs_SR}_SR_4l_Incl_bin_{i+1}_13TeV\n\n')

#f.write('mcsets = OpgSM,OtpSM,OptSM,OpgSqrd,OtpSqrd,OptSqrd,OpgOtp,OptOtp,qqZZ,ggZZ')
f.write('mcsets = OpgSM,OtpSM,OpgSqrd,OtpSqrd,OpgOtp,qqZZ,ggZZ,VBF,ttV_All\n\n')

f.write('\n[cuts]\n\n')

for i in range(len(binning_CR)-1):
    f.write(f'{obs_CR}_CR_4l_Incl_bin_{i+1}_13TeV = (((180<=m4l_fsr)&&(m4l_fsr<=220))&&(({binning_CR[i]:.2f}<{obs_CR})&&({obs_CR}<{binning_CR[i+1]:.2f})))\n')
for i in range(len(binning_SR)-1):
    f.write(f'{obs_SR}_SR_4l_Incl_bin_{i+1}_13TeV = (((220<=m4l_fsr)&&(m4l_fsr<=2000))&&(({binning_SR[i]:.2f}<{obs_SR})&&({obs_SR}<{binning_SR[i+1]:.2f})))\n')

f.write('\n\n[observables]\n\n')
for i in range(len(binning_CR)-1):
    f.write(f'{obs_CR}_CR_4l_Incl_bin_{i+1}_13TeV =  {obs_CR}:{obs_CR},1,{range_x_CR[0]},{range_x_CR[1]}\n')
for i in range(len(binning_SR)-1):
    f.write(f'{obs_SR}_SR_4l_Incl_bin_{i+1}_13TeV =  {obs_SR}:{obs_SR},1,{range_x_SR[0]},{range_x_SR[1]}\n')

f.write('\n\n[coefficients]\n\n')

Procs={'OpgSM':'cpG',
       'OtpSM':'ctp',
       'OpgSqrd':'cpGsqrd*cpG*cpG',
       'OtpSqrd':'ctpsqrd*ctp*ctp',
       'OpgOtp':'cpGctp*cpG*ctp',
       'ggZZ':'',
       'qqZZ':'mu_qqZZ',
       'VBF':'',
       'ttV_All':''}

for Proc in Procs:
    poi=''
    if Procs[Proc]:
        poi='poi:'+Procs[Proc]+'; '
    if 'mu' not in Procs[Proc]:
        f.write(Proc+' = '+poi+'factors:n_'+Proc+',bgyields.txt; sys:'+Proc+'_sys.txt ; global:ATLAS_LUMI(139.0/0.983/1.017)\n')
    else:
        f.write(Proc+' = '+poi+'factors:n_'+Proc+',bgyields139.txt; sys:'+Proc+'_sys.txt\n')
        
f.write('\n\n[')
for i in range(len(binning_CR)-1):
    f.write(f'{obs_CR}_CR_4l_Incl_bin_{i+1}_13TeV,')
for i in range(len(binning_SR)-1):
    if i != (len(binning_SR)-2):
        f.write(f'{obs_SR}_SR_4l_Incl_bin_{i+1}_13TeV,')
    else:
        f.write(f'{obs_SR}_SR_4l_Incl_bin_{i+1}_13TeV]\n\n')

for Proc in Procs:
    SB = 'Bkg'
    if 'O' in Proc:
        SB = 'Signal'
    f.write(Proc+' = SampleCount : ATLAS_'+SB+'_'+Proc+'\n')

f.write('\n\n[asimov: asimovData]\n\n')

f.write('cpG = 0.0\n')
f.write('ctp = 0.0\n')
f.write('cpGsqrd = 0.0\n')
f.write('ctpsqrd = 0.0\n')
f.write('cpGctp = 0.0\n')
f.write('mu_qqZZ = 1.0\n')

print('Finished!!\nSee '+config_filename)
