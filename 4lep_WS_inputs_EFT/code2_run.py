import os

# outname="Final_WS_MCFM"
outname="Final_WS_NN_SM"
date="23Jan2023"

inPath="./outputs_"+date+"/Merged_code1_"+outname
outputPath="./outputs_"+date+"/Merged_code2_"+outname

os.system('mkdir -p ' + outputPath)
os.system('rm -rf ' + outputPath+'/*')

#ProcNames = ["qqZZ","ggZZ",
#             "OtpSM","OpgSM","OptSM",
#             "OpgSqrd","OtpSqrd","OptSqrd",
#             "OpgOtp","OptOtp"]

ProcNames = ["qqZZ_0jet","qqZZ_1jet","qqZZ_2jet",
             "ggZZ","VBF","ttV_All",
             "OtpSM","OpgSM",
             "OpgSqrd","OtpSqrd",
             "OpgOtp"]

#ProcNames = ["VBF","ttV_All"]
# ProcNames = ["qqZZ_0jet"]

for ProcName in ProcNames:
    os.system('python make_sys_hists_code2.py --inProc '+ProcName+' --inpath '+inPath+' --outpath '+outputPath)
    print('-----------------------')
