#python smooth.py --inpath WSInputs/WS_inputs_Final_WS_22_09_05
import os
from collections import OrderedDict
from glob import glob
import argparse
import json
import yaml
import jsbeautifier

parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--inpath',  type=str, default='./', help='Add path to merged histograms')

args = parser.parse_args()
inpath=args.inpath

catgs=["CR_1","CR_2","CR_3","SR_ggF","SR_Mxd","SR_VBF"]

# catgs=["SR_Mxd"]

def floatls(ls):
    return [float(item) for item in ls]

def get_catg_bin(line):
    return int(line.split("_bin_")[1].split("_")[0])-1

def get_catg(line):
    category_tmp=line.replace("[","").replace("]","")
    for catg in catgs:
        if catg in category_tmp:
            return catg
    print("Something wrong here. No catg return")
    print("line input:"+line)
    exit()

def get_rebinvar_sub(variations,weights):
    sum_weighted_var=0
    sum_weights=0
    print("")
    print("variations, weights: ",variations,weights)
    for var, w in zip(variations,weights):
        if var > 1.5 or var < 0.5:
            var=1.0
        sum_weighted_var+=var*w
        sum_weights+=w
    print("sum_weighted_var,sum_weights: ",sum_weighted_var,sum_weights)
    if abs(sum_weights) > 10e-9:
        sum_weighted_var = sum_weighted_var/sum_weights
    else:
        sum_weighted_var=1
    print("rebinvar: ",sum_weighted_var)

    return [float(format(sum_weighted_var,"0.4f"))]*len(weights)
    # return [1.0000]*len(weights)


def get_rebinvar(sys,catg,sys_smooth,ch):
    up_var_rebin = sys["up"][:]
    dn_var_rebin = sys["dn"][:]

    # bin_edges={"SR_Mixed":[0,1,2,4]}

    #over
    # bin_edges={"SR_ggF":[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],
    #            "SR_Mixed":[0,1,2,4],
    #            "SR_VBF":[0,1,2,3,4]}

    # over
    # bin_edges={"SR_ggF":[0,1,2,3,4,5,6,7,8,10,12,15],
    #            "SR_Mixed":[0,1,2,4],
    #            "SR_VBF":[0,1,2,3,4]}


    bin_edges={"SR_ggF":[0,1,2,3,4,5,6,9,13],
               "SR_Mxd":[0,3],
               "SR_VBF":[0,2,4]}
    sys_specs=['EffectiveNP_7','EffectiveNP_10']
    sys_specs2=['JET_JER_EffectiveNP_4','JET_JER_EffectiveNP_2','JET_JER_EffectiveNP_3','JET_JER_EffectiveNP_8','JET_JER_EffectiveNP_9']
    # for sys_spec in sys_specs:
    #     if sys_spec in sys_smooth:
    #         bin_edges['SR_ggF']=[0,15]
    # if "Modelling" in sys_smooth:
    #     bin_edges={"SR_ggF":[0,1,2,3,4,5,6,8,15],
    #                "SR_Mixed":[0,1,2,4],
    #                "SR_VBF":[0,1,2,4]}
    for i in range(len(bin_edges[catg][:-1])):
        m=bin_edges[catg][i]
        n=bin_edges[catg][i+1]
        up_var_rebin[m:n]=get_rebinvar_sub(sys["up"][m:n],sys["norm"][m:n])
        dn_var_rebin[m:n]=get_rebinvar_sub(sys["dn"][m:n],sys["norm"][m:n])
        # if ch in ["tot_emu_bkg","Zjets_bkg","Other_bkg"]:
        #     up_var_rebin[m:n] = [1.0000]*(n-m)
        #     dn_var_rebin[m:n] = [1.0000]*(n-m)
    # if (catg == "SR_Mixed" or catg == "SR_VBF") and sys_smooth in sys_specs2:
    #     up_var_rebin=[1.0]*4
    #     dn_var_rebin=[1.0]*4

    return up_var_rebin, dn_var_rebin


yields=OrderedDict()
for line in open(inpath+"/bgyields.txt"):
    line=line.rstrip()
    if "#" not in line and line != "" and "SR" not in line:
        line=[i.replace(" ","") for i in line.split("&")]
        ch=line[0].replace("n_","")
        yields[ch]=OrderedDict()
        yields[ch]["CR_1"]=floatls(line[1:5])
        yields[ch]["CR_2"]=floatls(line[5:7])
        yields[ch]["CR_3"]=floatls(line[7:8])
        yields[ch]["SR_ggF"]=floatls(line[8:22])
        yields[ch]["SR_Mxd"]=floatls(line[22:25])
        yields[ch]["SR_VBF"]=floatls(line[25:29])

print(json.dumps(yields,sort_keys=False, indent=4))

sys_smooths=[]
sys_smooths+=['EG_SCALE_AF2',
              'EG_SCALE_ALL',
              'EG_RESOLUTION_ALL']

sys_smooths+=['JET_BJES_Response','PRW_DATASF']

sys_smooths+=['JET_EffectiveNP_Detector1',
              'JET_EffectiveNP_Detector2',
              'JET_EffectiveNP_Mixed1',
              'JET_EffectiveNP_Mixed2',
              'JET_EffectiveNP_Mixed3',
              'JET_EffectiveNP_Modelling1',
              'JET_EffectiveNP_Modelling2',
              'JET_EffectiveNP_Modelling3',
              'JET_EffectiveNP_Modelling4',
              'JET_EffectiveNP_Statistical1',
              'JET_EffectiveNP_Statistical2',
              'JET_EffectiveNP_Statistical3',
              'JET_EffectiveNP_Statistical4',
              'JET_EffectiveNP_Statistical5',
              'JET_EffectiveNP_Statistical6']

sys_smooths+=['JET_EtaIntercalibration_Modelling',
              'JET_EtaIntercalibration_NonClosure_2018data',
              'JET_EtaIntercalibration_NonClosure_highE',
              'JET_EtaIntercalibration_NonClosure_negEta',
              'JET_EtaIntercalibration_NonClosure_posEta',
              'JET_EtaIntercalibration_TotalStat']

sys_smooths+=['JET_Flavor_Composition',
              'JET_Flavor_Composition_VBF',
              'JET_Flavor_Composition_gg',
              'JET_Flavor_Composition_qq',
              'JET_Flavor_Response',
              'JET_Flavor_Response_VBF',
              'JET_Flavor_Response_gg',
              'JET_Flavor_Response_qq']

sys_smooths+=['JET_JER_DataVsMC_MC16',
              'JET_JER_EffectiveNP_1',
              'JET_JER_EffectiveNP_10',
              'JET_JER_EffectiveNP_11',
              'JET_JER_EffectiveNP_12restTerm',
              'JET_JER_EffectiveNP_2',
              'JET_JER_EffectiveNP_3',
              'JET_JER_EffectiveNP_4',
              'JET_JER_EffectiveNP_5',
              'JET_JER_EffectiveNP_6',
              'JET_JER_EffectiveNP_7',
              'JET_JER_EffectiveNP_8',
              'JET_JER_EffectiveNP_9']

sys_smooths+=['JET_JvtEfficiency',
              'JET_Pileup_OffsetMu',
              'JET_Pileup_OffsetNPV',
              'JET_Pileup_PtTerm',
              'JET_Pileup_RhoTopology',
              'JET_PunchThrough_MC16',
              'JET_SingleParticle_HighPt',
              'JET_fJvtEfficiency',
              'MET_SoftTrk_ResoPara',
              'MET_SoftTrk_ResoPerp',
              'MET_SoftTrk_Scale']

sys_smooths+=['MUON_SAGITTA_RHO',
              'MUON_ID',
              'MUON_MS',
              'MUON_SAGITTA_RESBIAS',
              'MUON_SCALE']

sys_smooths+=['H4l_Shower_UEPS_Sherpa_ggHM_QSF_shape',
              'H4l_Shower_UEPS_Sherpa_ggHM_CKKW_shape',
              'H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_shape',
              'H4l_Shower_UEPS_Sherpa_HM_QSF_shape',
              'H4l_Shower_UEPS_Sherpa_HM_CKKW_shape',
              'H4l_Shower_UEPS_Sherpa_HM_CSSKIN_shape',
              'H4l_Shower_UEPS_VBF_OffShell_isrmuRfac_0p5_fsrmuRfac_0p5',
              'H4l_Shower_UEPS_VBF_OffShell_isrmuRfac_2p0_fsrmuRfac_2p0',
              'H4l_Shower_UEPS_VBF_OffShell_isrmuRfac_0p5_fsrmuRfac_2p0',
              'H4l_Shower_UEPS_Sherpa_ggHM_CKKW_norm',
              'H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_norm',
              'H4l_Shower_UEPS_Sherpa_HM_CKKW_norm',
              'HOQCD_syst_VBF',
              'redBkgNorm']

# sys_smooths_sym=['MUON_SAGITTA_RHO','MUON_SAGITTA_RESBIAS','JET_JER_EffectiveNP_7','JET_JER_EffectiveNP_10']
sys_smooths_sym=sys_smooths

sys_smooths+=['HOQCD_syst_0Jet','HOQCD_syst_1Jet','HOQCD_syst_2Jet','ggZZNLO_QCD_syst']

# sys_smooths+=['JET_EffectiveNP_Detector1','JET_EffectiveNP_Detector2','JET_EffectiveNP_Mixed1','JET_EffectiveNP_Mixed2','JET_EffectiveNP_Mixed3','JET_EtaIntercalibration_NonClosure_2018data','JET_EtaIntercalibration_NonClosure_highE','JET_EtaIntercalibration_NonClosure_negEta','JET_EtaIntercalibration_NonClosure_posEta','JET_EtaIntercalibration_TotalStat','JET_JER_EffectiveNP_1','JET_JER_EffectiveNP_5','JET_JER_EffectiveNP_11','JET_JER_EffectiveNP_12restTerm','JET_JER_EffectiveNP_8','JET_JER_EffectiveNP_9','JET_JER_DataVsMC_MC16','JET_Pileup_PtTerm','JET_PunchThrough_MC16','JET_SingleParticle_HighPt','MET_SoftTrk_ResoPara','MET_SoftTrk_ResoPerp','MET_SoftTrk_Scale']

# sys_smooths+=['JET_EffectiveNP_Modelling2','JET_EffectiveNP_Modelling3','JET_EffectiveNP_Modelling4','JET_EffectiveNP_Statistical1','JET_EffectiveNP_Statistical2','JET_EffectiveNP_Statistical3','JET_EffectiveNP_Statistical4','JET_EffectiveNP_Statistical5','JET_EffectiveNP_Statistical6','JET_EtaIntercalibration_Modelling']


# sys_smooths_sym=[]


sys=OrderedDict()
# channels=['ggBNLOB','ggBNLOI','ggSBINLOI','ggSNLOI','ggSNLOS',
#           'qqZZ_0Jet','qqZZ_1Jet','qqZZ_2Jet',
#           'VBFB','VBFSBI','VBFSBI10',
#           'ttV_All','redBkg','OpgSM','OpgSqrd','OtpSM','OtpSqrd','OpgOtp']

channels=['OpgSM','OpgSqrd','OtpSM','OtpSqrd','OpgOtp']

for ch in channels:
    sys_f=inpath+"/"+ch+"_sys.txt"
    # if "qqZZ_1_Part0" not in sys_f:
    #     continue
    print(sys_f)
    #ch=os.path.basename(sys_f).replace("_sys.txt","")
    sys[ch]=OrderedDict()
    # channels.append(ch)
    for catg in catgs:
        sys[ch][catg]=OrderedDict()
        for sys_smooth in sys_smooths:
            sys[ch][catg][sys_smooth]=OrderedDict()
            sys[ch][catg][sys_smooth]["up"]=[]
            sys[ch][catg][sys_smooth]["dn"]=[]

    for line in open(sys_f):
        line=line.rstrip()
        if "[" in line:
            catg=get_catg(line)
        line=line.split()
        for sys_smooth in sys_smooths:
            if sys_smooth == line[0]:
                sys[ch][catg][sys_smooth]["up"].append(float(line[2]))
                sys[ch][catg][sys_smooth]["dn"].append(float(line[3]))

    for catg in ['SR_ggF','SR_Mxd','SR_VBF']:
        if ch == 'redBkg':
            continue
        #for catg in ['SR_Mixed']:
        for sys_smooth in sys_smooths:
            sys[ch][catg][sys_smooth]["norm"]=yields[ch][catg]
            print(sys[ch][catg][sys_smooth]["up"])
            up_rebin, dn_rebin = get_rebinvar(sys[ch][catg][sys_smooth],catg,sys_smooth,ch)
            sys[ch][catg][sys_smooth]["up_rebin"] = up_rebin
            sys[ch][catg][sys_smooth]["dn_rebin"] = dn_rebin


print(channels)
print("")
# print(json.dumps(sys['qqZZ_1_Part0']['SR_ggF'],sort_keys=False, indent=4))
options = jsbeautifier.default_options()
options.indent_size=2
print(jsbeautifier.beautify(json.dumps(sys),options))
# print(json.dumps(sys,sort_keys=False, indent=4, separators=(',',':')))
# print(yaml.dump(sys, default_flow_style=False))

outpath=inpath+"_smooth"
os.system("mkdir -p "+outpath)
os.system("cp -r "+inpath+"/* "+outpath+"/")
# os.system("rm "+outpath+"/*orig*")
# os.system("sed -i -e 's/WS_22_09_05/WS_22_09_05_smooth/g' "+outpath+"/config_22_09_05/config_file_3SRs_Final_WS_22_09_05.ini")

# for sys_f in glob(inpath+"/*_sys.txt"):
for ch in channels:
    sys_f=inpath+"/"+ch+"_sys.txt"
    f_out=open(outpath+"/"+os.path.basename(sys_f),"w")
    # ch=os.path.basename(sys_f).replace("_sys.txt","")
    # print(ch)
    for line in open(sys_f):
        if '#' in line:
            continue
        if "[" in line:
            # print(line)
            catg=get_catg(line)
            catg_bin=get_catg_bin(line)
            f_out.write(line)
            continue
        line_split=line.split()
        sys_smooth_if=False
        # if "qqZZ_CSSKIN_" in line:
        #     # continue
        #     line=line.rstrip().split()
        #     dn=float(line[3])
        #     up=2-dn
        #     f_out.write(line[0]+" = " + str(up) + " " + str(dn) + "\n")
        #     continue
        if catg not in ['SR_ggF','SR_Mixed','SR_VBF'] or ch == 'redBkg':
            #if catg not in ['SR_Mixed']:
            line_tmp=line.rstrip()
            line=line.rstrip().split()
            #print("weiy: line"+str(line))
            if float(line[2]) > 1.5 or float(line[2]) < 0.5:
                print("before file+catg+var: "+ch+","+catg+"_bin_"+str(catg_bin)+", "+line_tmp)
                line[2]=str(1.0000)
            if float(line[3]) > 1.5 or float(line[3]) < 0.5:
                print("before file+catg+var: "+ch+","+catg+"_bin_"+str(catg_bin)+", "+line_tmp)
                line[3]=str(1.0000)
            for sys_smooth_sym in sys_smooths_sym:
                if sys_smooth_sym in line_tmp:
                    line[3]=format(2-float(line[2]),"0.4f")
            f_out.write(line[0]+" = "+line[2]+" "+line[3]+"\n")
            continue
        for sys_smooth in sys_smooths:
            if sys_smooth == line_split[0]:
                sys_smooth_if=True
                # print(sys_smooth)
                var_rebin_up=sys[ch][catg][sys_smooth]["up_rebin"][catg_bin]
                var_rebin_dn=sys[ch][catg][sys_smooth]["dn_rebin"][catg_bin]
                if sys_smooth in sys_smooths_sym:
                    # var_rebin_up=1+(var_rebin_up-var_rebin_dn)/2
                    # var_rebin_up=1+(var_rebin_up-var_rebin_dn)/2
                    var_rebin_dn=2-var_rebin_up
                var_rebin_up=format(var_rebin_up,"0.4f")
                var_rebin_dn=format(var_rebin_dn,"0.4f")
                f_out.write(sys_smooth+" = "+str(var_rebin_up)+" "+str(var_rebin_dn)+"\n")
        if not sys_smooth_if:
            f_out.write(line)
