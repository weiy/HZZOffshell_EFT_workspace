#outdir="outputs_ws_EFT_4l_MCFM"
#outdir="outputs_ws_EFT_4l_MCFM_rebinlastbin"
#outdir="outputs_ws_EFT_4l_MCFM_rebinlast2bin_wsys"
outdir="outputs_ws_EFT_4l_MCFM_rebinlastbin_wosys"
mkdir -p $outdir

#datetag="04Jun2022_wSys_wMCSTAT_EFT_4l_MCFM_rebinlast2bin"
datetag="02Mar2022_woSys_wMCSTAT_EFT_4l_MCFM_rebinlastbin"

#mainCombiner  04June2022_EFT_4l_config_file_noCR_MCFM_rebinlast2bin.ini ${outdir}/WS_OffShell_${datetag}_noCR.root 2>&1 | tee ${outdir}/log_offshell_${datetag}_noCR.log

mainCombiner 25Mar2022_EFT_4l_config_file_noCR.ini ${outdir}/WS_OffShell_${datetag}_noCR.root 2>&1 | tee ${outdir}/log_offshell_${datetag}_noCR.log
