export PATH=/afs/cern.ch/user/w/weiy/HZZWorkspace/source/HZZWorkspace/scripts:${PATH}
export HZZWSCODEDIR=/afs/cern.ch/user/w/weiy/HZZWorkspace/source/HZZWorkspace/
cd /afs/cern.ch/user/w/weiy/HZZWorkspace/build
source command.txt
cd -

check_ws.py -w combined -d asimovData -s simPdf --poi_name cpGctp --xMin -2 --xMax 2 --lumi 139 --fixVar cpG:1.0 --fixVar cpGsqrd:1.0 --fixVar ctp:0 --fixVar ctpsqrd:0 WS_OffShell_04Jun2022_wSys_wMCSTAT_EFT_4l_MCFM_rebinlast2bin_linq_cpG0p04_noCR.root
