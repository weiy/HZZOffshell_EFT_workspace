#python3 needed
import uproot3 as uproot
import numpy as np
import os

#outname='Merged_code1_4l_EFT_WS_MCFM_rebinlastbin'
#outname='Merged_code1_4l_EFT_WS_MCFM_rebinlast2bin'
#outname='Merged_code1_4l_EFT_WS_NN_SM_100bins'
#outname='Merged_code1_4l_EFT_WS_MCFM_100bins'
outname='Merged_code1_4l_EFT_WS_m4l_10GeVbinwidth'
merged_dir = './outputs_25Mar2022/'+outname

samples = ['OpgSM_hists.root',
           'OtpSM_hists.root',
           'OptSM_hists.root',
           'OpgSqrd_hists.root',
           'OtpSqrd_hists.root',
           'OptSqrd_hists.root',
           'OpgOtp_hists.root',
           'OptOtp_hists.root',
           'qqZZ_hists.root',
           'ggZZ_hists.root'
           ]

#range_x = [-2.0,1.7]
range_x = [220,2000]
#range_x = [-5,1]

#binning = 14 #total bins
#binning = 10 #total bins
binning = 178
binning = np.linspace(range_x[0],range_x[1],binning+1)

#binning = [-2.0, -1.72, -1.43, -1.15, -0.86, -0.58, -0.29, -0.01, 0.28, 0.56, 1.7]

binning = [-5.0, -4.4, -3.8, -3.2, -2.6, -2.0, -1.4, -0.8, 1.0]

for i in range(len(binning)-1):
    if i != (len(binning)-2):
        print(f'MCFM_MELA_ggZZ_SR_Incl_bin_{i+1}_13TeV & ', end='')
    else:
        print(f'MCFM_MELA_ggZZ_SR_Incl_bin_{i+1}_13TeV')

'''
for sample in samples:
    f_merged = uproot.open(merged_dir + '/'+ sample)
    Nom_hist = f_merged['Nominal']['Nom_hist']
    print('n_'+sample.replace('_hists.root',''),end='')
    for i in range(1, len(Nom_hist)-1):
        print(' & ' + "%.6f" % (Nom_hist[i]/139.0), end='')
    print()
'''
print()


for i in range(len(binning)-1):
    print(f'MCFM_MELA_ggZZ_SR_Incl_bin_{i+1}_13TeV = (((220<=m4l_fsr)&&(m4l_fsr<=2000))&&(({binning[i]:.2f}<MCFM_MELA_ggZZ)&&(MCFM_MELA_ggZZ<{binning[i+1]:.2f})))')

print('')    
print(binning)
print('')    

for i in range(len(binning)-1):
    print(f'MCFM_MELA_ggZZ_SR_Incl_bin_{i+1}_13TeV =  MCFM_MELA_ggZZ:MCFM_MELA_ggZZ,1,{range_x[0]},{range_x[1]}')

print('')

for i in range(len(binning)-1):
    if i != (len(binning)-2):
        print(f'MCFM_MELA_ggZZ_SR_Incl_bin_{i+1}_13TeV,', end='')
    else:
        print(f'MCFM_MELA_ggZZ_SR_Incl_bin_{i+1}_13TeV')

