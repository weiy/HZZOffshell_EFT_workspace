from ROOT import *
import sys
sys.dont_write_bytecode = True
sys.path.append('./Procs_and_Syst/')
import os
from glob import glob
import math
import argparse
import List_Sys
import pickle
import yaml
from collections import OrderedDict
from operator import itemgetter

gROOT.SetBatch(kTRUE)
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--inpath',  type=str, default='./Merged_hists_code2_binnedCR', help='Add path to merged histograms')
parser.add_argument('--outpath', type=str, default='./Workspace_inputs_binnedCR', help='Add path to output histograms')
parser.add_argument('--inProc',  type=str, default='ggSig', help='Add the process name you want')
parser.add_argument('--Threshold_HSOB', type=float, default=0.0, help='Add the threshold for high S/B bins to consider the systematic')
parser.add_argument('--Threshold_LSOB', type=float, default=0.0, help='Add the threshold for low  S/B bins to consider the systematic')

#parser.add_argument('--HighSOB_bins', nargs='+', help='Bins with high S/B to perform pruning', required=True)
parser.add_argument('--RemoveqqZZNorm', type=bool, default=True, help='remove qqZZ norm uncern.')

#Reading in the arguments
args = parser.parse_args()
lumi = 139.0
HistPath = args.inpath
ProcName = args.inProc
outFolder = args.outpath
qqZZNoNorm = args.RemoveqqZZNorm
HighSOB_threshold = args.Threshold_HSOB
LowSOB_threshold = args.Threshold_LSOB

High_S_over_B = [20,21,24,28]
#High_S_over_B = [1,2,3,4,5,6,7,8,9]
#High_S_over_B = [6,8]
#High_S_over_B = [10,11]

#Getting the list of regions with binning/ observables - output of code 1
file_regions = open('regions_of_interest.yaml','r')
regions_list = yaml.load(file_regions)
file_regions.close()
#print(regions_list)

#Name of the outputput text files
outputFile = open(outFolder+"/"+ProcName+"_sys.txt", "w")

#Looping over the bin regions to get the bin edges/to break down concatenated histograms
bin_regions =[] #upper edges of the regions
bin_regions_names=[] #names (and observable)
region_edges=[]#lower and upper limits
for regions in regions_list.keys():
    #print(regions)
    bin_regions.append(len(regions_list[regions][2])-1)
    #Needs to be changed depending on what the observable is called
    if "ncl" in (regions_list[regions][1]):
        obs = "ggFNN_MELA"
    elif "VBF" in (regions_list[regions][1]):
        obs = "VBFNN_MELA"
    elif "MCFM" in (regions_list[regions][1]):
        obs = "MCFM_MELA_ggZZ"
        #obs = "ME_dis"
    else:
        obs = regions_list[regions][1]

    print(regions)
    rgn_name = regions.split("_")
    if "CR" in rgn_name[0]:
        bin_regions_names.append((obs+"_"+rgn_name[0]+"_"+str(int(rgn_name[2].replace('jet',''))+1)))
    else:
        bin_regions_names.append((obs+"_"+rgn_name[0]+"_"+rgn_name[2]))
region_edges.append(1)
#print(bin_regions_names)
#print("bin_regions_names")
#print(bin_regions)

for i in range(0, len(bin_regions)):
    region_edges.append(sum(bin_regions[0:i+1])+1)
#print(region_edges)

#Write out the list of NP - lots of overlap - does it sample by sample
NP_names_outFile = ("./NP_names.txt")
if os.path.isfile(NP_names_outFile) == True:
    with open (NP_names_outFile, 'rb') as NP:
        NP_names = pickle.load(NP)
else:
    NP_names= []

#Define the bkgyields file
bkgyields = outFolder+"/"+"bgyields.txt"
bkgyields139 = outFolder+"/"+"bgyields139.txt"

#Check if file exists, if yes then just append, otherwise then start with
if os.path.isfile(bkgyields)==False:
    bkgyields_file = open(bkgyields,"a+")
    bkgyields139_file = open(bkgyields139,"a+")
    bgyields =""
    for rgn_i in range(0,len(bin_regions_names)):
        for bins in range(1, bin_regions[rgn_i]+1):
            bgyields= bgyields + (bin_regions_names[rgn_i]+"_Incl_bin_"+str(bins)+"_13TeV & ")
    bkgyields_file.write(bgyields[:-2])
    bkgyields_file.write("\nn_"+ProcName+"")
    bkgyields139_file.write(bgyields[:-2])
    bkgyields139_file.write("\nn_"+ProcName+"")

else:
    bkgyields_file = open(bkgyields,"a+")
    bkgyields_file.write("\nn_"+ProcName+" ")
    bkgyields139_file = open(bkgyields139,"a+")
    bkgyields139_file.write("\nn_"+ProcName+" ")

def write_WS_inputs(bin_regions_names,region_edges,nominalhist,var_hist,outputfile,ProcName):
    print("writing workspace...")
    print("bin_regions_names: "+str(bin_regions_names)) #['m4l_fsr_CR_4l', 'MCFM_MELA_ggZZ_SR_4l']
    print("region_edges:"+str(region_edges)) #[1, 5, 10]
    """
    Writes out the WS input text files
    """
    print(nominalhist.GetNbinsX())
    bin_i = 1
    for rgn_i in range(0,len(bin_regions_names)):
        for bins in range(1, bin_regions[rgn_i]+1):
            output_vars = []
            #print(bins+region_edges[rgn_i]-1)
            #print(bin_i)
            outputFile.write("["+bin_regions_names[rgn_i]+"_Incl_bin_"+str(bins)+"_13TeV]\n")
            #print("rgn_i:"+str(rgn_i))
            #print("integral: "+str(nominalhist.Integral(region_edges[rgn_i],region_edges[rgn_i+1])))
            #print("bin_i content: "+str(nominalhist.GetBinContent(bins+region_edges[rgn_i]-1)))
            outputFile.write("####bin_value/all_bins: "+str(format(nominalhist.GetBinContent(bins+region_edges[rgn_i]-1)/nominalhist.Integral(region_edges[rgn_i],region_edges[rgn_i+1])*100,'.2f'))+"%\n")
            if bin_i in High_S_over_B:
                if ('VBF' not in ProcName) and ('ttV' not in ProcName):
                    if nominalhist.GetBinError(bin_i) !=0:
                        upMCSTAT  = 1+ (nominalhist.GetBinError(bin_i)/(nominalhist.GetBinContent(bin_i)))
                        dnMCSTAT  = 1- (nominalhist.GetBinError(bin_i)/(nominalhist.GetBinContent(bin_i)))
                        output_vars.append("MCSTAT_"+ProcName+"_"+bin_regions_names[rgn_i]+"_Incl_bin_"+str(bins)+"_13TeV = "+str(upMCSTAT)+" "+str(dnMCSTAT)+"\n")
                        NP_names.append("MCSTAT_"+ProcName+"_"+bin_regions_names[rgn_i]+"_Incl_bin_"+str(bins)+"_13TeV")

            names=[]
            for sys in var_hist:
                names.append((sys[0].GetName().replace('__1up','')).replace('weight_var_',''))
            #print("weiy: max: "+str(len(max(names, key=len))))
            max_length=len(max(names, key=len))

            for sys in var_hist:
                name = (sys[0].GetName().replace('__1up','')).replace('weight_var_','')
                if nominalhist.GetBinContent(bin_i)!=0:
                    if "PDF_sys" in sys[0].GetName():
                        upVar = (sys[0].GetBinContent(bin_i))/nominalhist.GetBinContent(bin_i)
                        dnVar = (sys[1].GetBinContent(bin_i))/nominalhist.GetBinContent(bin_i)

                    if "JER" in sys[0].GetName():
                        if "JET_JER_EffectiveNP_12restTerm" not in sys[0].GetName():
                            #sys[0]: 1up
                            #sys[1]: PD_1up
                            #sys[2]: 1down
                            #sys[3]: PD_1down
                            upVar = 1+(sys[0].GetBinContent(bin_i)-sys[1].GetBinContent(bin_i))/nominalhist.GetBinContent(bin_i)
                            dnVar = 1+(sys[2].GetBinContent(bin_i)-sys[3].GetBinContent(bin_i))/nominalhist.GetBinContent(bin_i)
                            if "JET_JER_EffectiveNP_4" in sys[0].GetName():
                                if bin_i ==9:
                                    upVar_val_1 = (sys[0].GetBinContent(bin_i+1)-sys[1].GetBinContent(bin_i+1)+nominalhist.GetBinContent(bin_i+1))/nominalhist.GetBinContent(bin_i+1)
                                    upVar_val_2 = (sys[0].GetBinContent(bin_i-1)-sys[1].GetBinContent(bin_i-1)+nominalhist.GetBinContent(bin_i-1))/nominalhist.GetBinContent(bin_i-1)
                                    dnVar_val_1 = (sys[2].GetBinContent(bin_i+1)-sys[3].GetBinContent(bin_i+1)+nominalhist.GetBinContent(bin_i+1))/nominalhist.GetBinContent(bin_i+1)
                                    dnVar_val_2 = (sys[2].GetBinContent(bin_i-1)-sys[3].GetBinContent(bin_i-1)+nominalhist.GetBinContent(bin_i-1))/nominalhist.GetBinContent(bin_i-1)

                                    upVar = (0.5*(upVar_val_1+upVar_val_2))
                                    dnVar = (0.5*(dnVar_val_1+dnVar_val_2))

                    elif "JET_Flavor" in name:
                        if "qq" in ProcName:
                            name = name+"_qq"
                        elif "gg" in ProcName:
                            name = name+"_gg"
                        elif "VBF" in ProcName:
                            name = name+"_VBF"
                        else:
                            print("bin:"+str(bin_i)+": JET Flavor for bkg processes: "+ProcName)

                    elif "_Shower_UEPS" in sys[0].GetName():
                        print("Shower_UEPS:name "+name)
                        if "qq" not in ProcName:
                            norm_up = sys[0].Integral()/nominalhist.Integral()
                            norm_dn = sys[1].Integral()/nominalhist.Integral()
                            #output_vars.append(name+"_norm = "+norm_up+" "+norm_dn+"\n")
                            output_vars.append(('{:'+str(max_length)+'s}').format(name+"_norm")+" = "+('{:.4f}').format(norm_up)+" "+('{:.4f}').format(norm_dn)+"\n")
                            up_norm_hist = sys[0].Clone()
                            dn_norm_hist = sys[1].Clone()
                            up_norm_hist.Scale(nominalhist.Integral()/sys[0].Integral())
                            dn_norm_hist.Scale(nominalhist.Integral()/sys[1].Integral())
                            name = name+"_shape"
                            upVar = up_norm_hist.GetBinContent(bin_i)/nominalhist.GetBinContent(bin_i)
                            dnVar = dn_norm_hist.GetBinContent(bin_i)/nominalhist.GetBinContent(bin_i)
                        else:
                            ##due to no EW corrections in qqZZ systematic samples
                            norm_up = 1 + abs(sys[0].Integral(1,sys[0].GetNbinsX()+1)-sys[1].Integral(1,sys[1].GetNbinsX()+1))/(2*nominalhist.Integral(1,nominalhist.GetNbinsX()+1))
                            norm_dn = 1 - abs(sys[0].Integral(1,sys[0].GetNbinsX()+1)-sys[1].Integral(1,sys[1].GetNbinsX()+1))/(2*nominalhist.Integral(1,nominalhist.GetNbinsX()+1))
                            output_vars.append(('{:'+str(max_length)+'s}').format(name+"_norm")+" = "+('{:.4f}').format(norm_up)+" "+('{:.4f}').format(norm_dn)+"\n")
                            up_norm_hist = sys[0].Clone()
                            dn_norm_hist = sys[1].Clone()
                            up_norm_hist.Scale(nominalhist.Integral(1,nominalhist.GetNbinsX()+1)/sys[0].Integral(1,sys[0].GetNbinsX()+1))
                            dn_norm_hist.Scale(nominalhist.Integral(1,nominalhist.GetNbinsX()+1)/sys[1].Integral(1,sys[1].GetNbinsX()+1))
                            name = name+"_shape"
                            upVar = 1 + (abs(up_norm_hist.GetBinContent(bin_i)-dn_norm_hist.GetBinContent(bin_i)))/(2*nominalhist.GetBinContent(bin_i))
                            dnVar = 1 - (abs(up_norm_hist.GetBinContent(bin_i)-dn_norm_hist.GetBinContent(bin_i)))/(2*nominalhist.GetBinContent(bin_i))
                    else:
                        upVar = (sys[0].GetBinContent(bin_i))/nominalhist.GetBinContent(bin_i)
                        dnVar = (sys[1].GetBinContent(bin_i))/nominalhist.GetBinContent(bin_i)
                else:
                    upVar = 1.0
                    dnVar = 1.0

                output_vars.append(('{:'+str(max_length)+'s}').format(name)+" = "+('{:.4f}').format(float(upVar))+" "+('{:.4f}').format(float(dnVar))+"\n")
                '''
                maxVar = max(abs(1-upVar),abs(1-dnVar))
                if upVar>1:
                    output_vars.append(('{:'+str(max_length)+'s}').format(name)+" = "+('{:.6f}').format((1+maxVar))+" "+('{:.6f}').format((1-maxVar))+"\n")
                    #outputFile.write(name+" = "+str((1+maxVar))+" "+str((1-maxVar))+"\n")
                else:
                    output_vars.append(('{:'+str(max_length)+'s}').format(name)+" = "+('{:.6f}').format((1-maxVar))+" "+('{:.6f}').format((1+maxVar))+"\n")
                    #outputFile.write(name+" = "+str((1-maxVar))+" "+str((1+maxVar))+"\n")
                #or use the following one, but will not change too much.
                #output_vars.append(('{:'+str(max_length)+'s}').format(name)+" = "+('{:.6f}').format(dnVar)+" "+('{:.6f}').format(upVar)+"\n")
                '''
            #sort var by importance
            output_vars_dict=OrderedDict()
            for sys in output_vars:
                output_vars_dict[sys]=max(abs(1-float(sys.split()[2])), abs(1-float(sys.split()[3])))
            d=OrderedDict(sorted(output_vars_dict.items(), key=itemgetter(1),reverse=True))
            for i in d.keys():
                #print(i)
                outputFile.write(i)
            bin_i = bin_i+1

def prune_sys(High_S_over_B,HighSOB_threshold,nom_hist,hist_list):
    #print(hist_list[0].GetName())
    var_int_HSOB=0
    nom_int_HSOB=0
    var_int_LSOB=0
    nom_int_LSOB=0
    WriteSys = False

    for i in range(1, hist_list[0].GetNbinsX()+1):
        if i in High_S_over_B:
            nom_int_HSOB = nom_int_HSOB+nom_hist.GetBinContent(i)
            if "JER" in hist_list[0].GetName():
                var_int_HSOB = var_int_HSOB+hist_list[0].GetBinContent(i)-hist_list[1].GetBinContent(i)+nom_hist.GetBinContent(i)
            else:
                var_int_HSOB = var_int_HSOB+hist_list[0].GetBinContent(i)
        else:
            nom_int_LSOB = nom_int_LSOB+nom_hist.GetBinContent(i)
            if "JER" in hist_list[0].GetName():
                var_int_LSOB = var_int_LSOB+hist_list[0].GetBinContent(i)-hist_list[1].GetBinContent(i)+nom_hist.GetBinContent(i)
            else:
                var_int_LSOB = var_int_LSOB+hist_list[0].GetBinContent(i)

    if nom_int_HSOB  != 0:
        RelRatio_HSOB = abs((var_int_HSOB-nom_int_HSOB)*100/nom_int_HSOB)
    else:
        RelRatio_HSOB = 0

    if nom_int_LSOB  != 0:
        RelRatio_LSOB = abs((var_int_LSOB-nom_int_LSOB)*100/nom_int_LSOB)
    else:
        RelRatio_LSOB = 0
    #print(RelRatio_HSOB)
    #print(RelRatio_LSOB)
    if (RelRatio_HSOB>=HighSOB_threshold):
        WriteSys=True
    else:
        if(RelRatio_LSOB>=LowSOB_threshold):
            WriteSys=True
    #print("weiy:"+str(WriteSys))
    return WriteSys

def main():
    list_var_hists=[]
    list_var_names=[]

    try:
        HistFile = TFile(os.path.join(HistPath,ProcName+"_hists.root"))
    except:
        print("file "+HistPath+'/'+ProcName+"_hists.root doen't exist.")
        exit()
    print("doing "+ProcName+" Process")

    for dirs in ["Nominal","TheoSys","ExptSys"]:# loop over directories
        print(dirs)
        directory = HistFile.GetDirectory(dirs)
        HistFile.cd(dirs)
        for hist in (gDirectory.GetListOfKeys()):
            if "Nom" not in (hist.GetName()) and "PD_" not in (hist.GetName()):
                list_var_names.append(((hist.GetName()).replace("__1up",'')).replace("__1down",'')) #get all systematic names (without variation)

    list_var_names = list(set(list_var_names)) #remove duplicates
    #print(list_var_names)
    for dirs in ["Nominal","TheoSys","ExptSys"]:
        directory = HistFile.GetDirectory(dirs)
        HistFile.cd(dirs)
        if "Nom" in dirs:
            Nomhist = gDirectory.Get("Nom_hist")
            #use nominal histogram to fill bgyields file scaled by
            #lumi and bgyields139 file thats not scaled by lumi
            print("Nominal Histogram Integral: "+str(Nomhist.Integral()))
            for rgn_i in range(0,len(bin_regions_names)):
                for bins in range(region_edges[rgn_i], region_edges[rgn_i+1]):
                    bkgyields139_file.write(" & "+str(format(Nomhist.GetBinContent(bins),'0.6f')))
                    binVal = Nomhist.GetBinContent(bins)/lumi
                    bkgyields_file.write(" & "+str(format(binVal,'0.6f')))
        else:
            print("Looking in "+ dirs)
            for sys in list_var_names:
                sys_hist_files=[] #renew for each iteration
                #print(sys)
                for var in ["1up", "1down"]:
                    sys_vars = sys +'__'+var
                    Sys_hist = gDirectory.Get(sys_vars)
                    if not Sys_hist:
                        continue
                    sys_hist_files.append(Sys_hist)
                    if "JER" in sys_vars:
                        sys_hist_files.append(gDirectory.Get("PD_"+sys_vars))
                #print("weiy: sys_hist_files: "+str(sys_hist_files))

                if Sys_hist:
                    writeSys = prune_sys(High_S_over_B,HighSOB_threshold,Nomhist,sys_hist_files)
                    if not writeSys:
                        print("weiy writeSys: "+str(writeSys))
                    if writeSys==True:
                        #append "up" and "down" for each time
                        list_var_hists.append(sys_hist_files)
                        NP_names.append(sys)
    '''
    for sys in list_var_hists:
        print("sys: "+str(sys[0].GetName())) #0 is up
        print("sys: "+str(sys[1].GetName())) #1 is down
    '''
    write_WS_inputs(bin_regions_names,region_edges,Nomhist,list_var_hists,outputFile,ProcName)
    #print("write out the pruned NP list")
    #print(NP_names)
    with open(NP_names_outFile, 'wb') as NP:
        pickle.dump(NP_names, NP)

if '__main__' in __name__:
    main()
