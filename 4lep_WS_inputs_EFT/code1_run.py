import os
from glob import glob
import sys
sys.path.append('./Procs_and_Syst/')
sys.dont_write_bytecode = True
import List_Procs

mc_campaigns = ['mc16a','mc16d','mc16e']
# mc_campaigns = ['mc16a']

#outname = '4l_EFT_WS_v1'
#outname = '4l_EFT_WS_m4l'
#outname = '4l_EFT_WS_m4l_18bins'
# outname = '4l_EFT_WS_MCFM'
outname = '4l_EFT_WS_NN_SM'
#outname = '4l_EFT_WS_MCFM_rebinlastbin'
#outname = '4l_EFT_WS_MCFM_rebinlast2bin'
#outname = '4l_EFT_WS_NN_SM_100bins'
#outname = '4l_EFT_WS_MCFM_100bins'
#outname = '4l_EFT_WS_m4l_10GeVbinwidth'
#outname = '4l_EFT_WS_binwidth1GeV'
# date = '07Aug2022'
date = '23Jan2023'
outputPath = './outputs_'+date+'/hists_code1_'+outname
outPlotsPath = './outputs_'+date+'/Plots/'
logPath = './txt_code1_files/'+outname+'/'

pathToFiles='/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v26/AntiKt4EMPFlow/'

os.system('mkdir -p outputs_'+date + ' '+outputPath+' '+outPlotsPath)
for mc_campaign in mc_campaigns:
    os.system('mkdir -p '+logPath+'/'+mc_campaign)

EFT_procs = ["OtpSM","OpgSM","OptSM",
             "OpgSqrd","OtpSqrd","OptSqrd",
             "OpgOtp","OptOtp"]

SM_procs_1 = ["qqZZ_1_0jet","qqZZ_1_1jet","qqZZ_1_2jet",
              "qqZZ_2_0jet","qqZZ_2_1jet","qqZZ_2_2jet",
              "qqZZ_3_0jet","qqZZ_3_1jet","qqZZ_3_2jet"]

SM_procs_2 = ["ggZZ","VBF"]+['ttV_0{0:1}'.format(j) for j in range(1,9+1)]

#all_procs = ["ggZZ"]
#all_procs = ["qqZZ_1"]
#all_procs = ["OtpSM"]
#all_procs = ["OpgSqrd"]
#all_procs = EFT_procs + SM_procs
#all_procs = ["VBF"]+['ttV_0{0:1}'.format(j) for j in range(1,9+1)]
#all_procs = ["VBF"]
# all_procs = ['ttV_07']

all_procs = SM_procs_2

all_procs = EFT_procs

# all_procs = ['qqZZ_1_0jet']

for mc_campaign in mc_campaigns:
    for proc in all_procs:
        sample = os.path.basename(glob(pathToFiles + mc_campaign + '/Nominal/*'+ List_Procs.procs[proc].DSID+'*')[0])
        print('Proceed ' + pathToFiles + mc_campaign + '/Nominal/'+ sample)
        os.system('python make_all_hists_code1.py --inpath '+pathToFiles+' --sample '+sample+' --outpath '+outputPath+' --proc '+proc+' --tree tree_incl_all -c ' + mc_campaign + ' &> '+ logPath + '/'+ mc_campaign + '/'+ sample.replace('.root','__Proc_'+proc+'.txt')+  ' &')
        #os.system('python make_all_hists_code1.py --inpath '+pathToFiles+' --sample '+sample+' --outpath '+outputPath+' --proc '+proc+' --tree tree_incl_all -c ' + mc_campaign)
        print('')

print('all jobs have been running in the background. Please ps to check!')
print('For output files, please go to folder '+ logPath +' to check!')
