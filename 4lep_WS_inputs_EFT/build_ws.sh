#run this script by: . build_ws.sh
if [ -z $SOURCEHZZ ];
then
    export PATH=/afs/cern.ch/user/w/weiy/offshell/HZZWorkspace/source/HZZWorkspace/scripts:${PATH}
    export HZZWSCODEDIR=/afs/cern.ch/user/w/weiy/offshell/HZZWorkspace/source/HZZWorkspace/
    cd /afs/cern.ch/user/w/weiy/offshell/HZZWorkspace/build
    source command.txt
    cd -
    SOURCEHZZ=1
fi

outdir="WS_outputs_EFT_4l"
mkdir -p ${outdir}

wstags=(
#"04June2022_EFT_4l_config_file_noCR_MCFM_rebinlast2bin_cpG0p09",
#"04June2022_EFT_4l_config_file_noCR_MCFM_rebinlast2bin_cpG1p0",
#"04June2022_EFT_4l_config_file_noCR_MCFM_rebinlast2bin_ctp1p0",
#"04June2022_EFT_4l_config_file_noCR_MCFM_rebinlast2bin_ctp21p529",
#"04June2022_EFT_4l_config_file_noCR_MCFM_rebinlast2bin_ctp21p5",
#"04June2022_EFT_4l_config_file_noCR_MCFM_rebinlast2bin_ctpsqrd1p0",
#"04June2022_EFT_4l_config_file_noCR_MCFM_rebinlast2bin",
#"04June2022_EFT_4l_config_file_noCR_MCFM_rebinlast2bin_linq_cpG0p04",
#"04June2022_EFT_4l_config_file_noCR_MCFM_rebinlast2bin_linq_ctp20p425",
#"04June2022_EFT_4l_config_file_noCR_MCFM_rebinlast2bin_linq_ctpm11p04"
# "07Aug2022_EFT_4l_config_file_wCR_wSys_noMC_MCFM_rebinlast2bin"
# "07Aug2022_EFT_4l_config_file_wCR_wSys_noMC_MCFM_rebinlast2bin_cpGlin0p09"
# "07Aug2022_EFT_4l_config_file_wCR_wSys_noMC_MCFM_rebinlast2bin_CROnly_wData"
# "07Aug2022_EFT_4l_config_file_wCR_wSys_noMC_MCFM_rebinlast2bin_cpGlinq0p04"
# "07Aug2022_EFT_4l_config_file_wCR_wSys_noMC_MCFM_rebinlast2bin_ctplin22p5"
# "07Aug2022_EFT_4l_config_file_wCR_wSys_noMC_MCFM_rebinlast2bin_ctplinq21p7"
"23Jan2023_EFT_4l_config_file_wCR_wSys_wMC_NN_SM_data"
"23Jan2023_EFT_4l_config_file_wCR_wSys_wMC_NN_SM_data_cpGlinm0p03"
"23Jan2023_EFT_4l_config_file_wCR_wSys_wMC_NN_SM_data_cpGlinqm0p01"
"23Jan2023_EFT_4l_config_file_wCR_wSys_wMC_NN_SM_data_ctplin6p4"
"23Jan2023_EFT_4l_config_file_wCR_wSys_wMC_NN_SM_data_ctplinq2p9"
"23Jan2023_EFT_4l_config_file_wCR_wSys_wMC_NN_SM_data_cpGlin0p2"
)

for wstag in "${wstags[@]}"
do
    mainCombiner  WS_inputs_config/${wstag}.ini ${outdir}/WS_OffShell_${wstag}.root 2>&1 | tee ${outdir}/log_offshell_${wstag}.log
    cp --preserve=timestamps ${outdir}/WS_OffShell_${wstag}.root /afs/cern.ch/user/w/weiy/offshell/offshellWS_fit/ws/4l/EFT/
done

bash /afs/cern.ch/user/w/weiy/offshell/offshellWS_fit/ws/upload_to_ox.sh
echo "copy workspace to fit directory done"
