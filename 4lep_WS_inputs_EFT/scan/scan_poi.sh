outdir="outputs_ws_EFT_4l2l2"
mkdir -p $outdir 
datetag="28Mar2022_noSys_wMCSTAT_EFT_4l2l2v"

## scan poi
tag=$datetag

#input="${outdir}/WS_OffShell_${tag}.root"
input="WS_combined_OffShell_EFT_2l2vOn16Mar2022_4lOn25Mar2022_noCR.root"

wsName="combined"
mcName="ModelConfig"
#dataName="obsData"
#dataName="asimovData_0_0"
dataName="asimovData"

POIName="cpG"

#out_name="${outdir}/${POIName}_scan_${tag}.root"
out_name="${outdir}/${POIName}_scan_${tag}_noCR.root"

scan_poi  ${input} ${out_name} ${wsName} ${POIName} ${dataName} ${POIName}:300:-0.3:0.3 cpGsqrd:0.0,ctp:0.0,ctpsqrd:0.0,cpt:0.0,cptsqrd:0.0,cpGctp:0.0,cptctp:0.0 2>&1 | tee ${outdir}/log_scan_offshell_${POIName}_${tag}
