class Process:
    def __init__(self,name,DSID,weight):
        self.name = name
        self.DSID = DSID
        self.weight = weight

HNNLO_kfactor=1.1

procs={
    ##EFT
    "OtpSM"  : Process(name="OtpSM",DSID="507656",weight="weight"),
    "OpgSM"  : Process("OpgSM"  ,"507656","weight*weight_OpgSM"),
    "OptSM"  : Process("OptSM"  ,"507650","weight*(weight>-0.01)*(weight<0.01)"),
    "OpgSqrd": Process("OpgSqrd","507599","weight"),
    "OpgOtp" : Process("OpgOtp" ,"507599","weight*weight_OpgOtp"),
    "OtpSqrd": Process("OtpSqrd","507652","weight"),
    "OptSqrd": Process("OptSqrd","507654","weight"),
    "OptOtp" : Process("OptOtp" ,"507648","weight"),

    ##SM
    "qqZZ_1_0jet": Process("qqZZ_1_0jet" ,"364250","weight*(n_jets==0)"),
    "qqZZ_1_1jet": Process("qqZZ_1_1jet" ,"364250","weight*(n_jets==1)"),
    "qqZZ_1_2jet": Process("qqZZ_1_2jet" ,"364250","weight*(n_jets>=2)"),
    "qqZZ_2_0jet": Process("qqZZ_2_0jet" ,"346899","weight*(n_jets==0)"),
    "qqZZ_2_1jet": Process("qqZZ_2_1jet" ,"346899","weight*(n_jets==1)"),
    "qqZZ_2_2jet": Process("qqZZ_2_2jet" ,"346899","weight*(n_jets>=2)"),
    "qqZZ_3_0jet": Process("qqZZ_3_0jet" ,"364252","weight*(n_jets==0)"),
    "qqZZ_3_1jet": Process("qqZZ_3_1jet" ,"364252","weight*(n_jets==1)"),
    "qqZZ_3_2jet": Process("qqZZ_3_2jet" ,"364252","weight*(n_jets>=2)"),
    "ggZZ"  : Process("ggZZ"   ,"345706","weight*w_ggZZkSBI*"+str(HNNLO_kfactor)),
    "VBF"   : Process("VBF"    ,"500372","weight"),
    "ttV_01": Process("ttV_01" ,"345937","weight"),
    "ttV_02": Process("ttV_02" ,"345938","weight"),
    "ttV_03": Process("ttV_03" ,"346554","weight"),
    "ttV_04": Process("ttV_04" ,"364243","weight"),
    "ttV_05": Process("ttV_05" ,"364245","weight"),
    "ttV_06": Process("ttV_06" ,"364248","weight"),
    "ttV_07": Process("ttV_07" ,"410142","weight"),
    "ttV_08": Process("ttV_08" ,"410218","weight"),
    "ttV_09": Process("ttV_09" ,"410219","weight"),
}



#README
# (1) OptSM : the mc16a DAOD has abnormal weights for 2l2v only
# (2) qqZZ_3: very small, can be neglected.

'''
samples

ggZZ = ['mc16_13TeV.345706.Sherpa_222_NNPDF30NNLO_ggllll_130M4l.root']

qqZZ = ['mc16_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.root',
        'mc16_13TeV.346899.Sherpa_222_NNPDF30NNLO_llll_m4l100_300_filt100_170.root',
        'mc16_13TeV.364252.Sherpa_222_NNPDF30NNLO_llll_m4l300.root']

#OpgSM = ['mc16_13TeV.507656.MGPy8EG_N30NLOA14_SMEFTNLO_gg4l_OtpSM_OpgSM.root']
OtpSM = ['mc16_13TeV.507656.MGPy8EG_N30NLOA14_SMEFTNLO_gg4l_OtpSM_OpgSM.root']
OptSM = ['mc16_13TeV.507650.MGPy8EG_N30NLOA14_SMEFTNLO_gg4l_OptSM.root']
OpgSqrd = ['mc16_13TeV.507599.MGPy8EG_N30NLOA14_SMEFTNLO_gg4l_OpgSq_OpgOtpfix.root']
OtpSqrd = ['mc16_13TeV.507652.MGPy8EG_N30NLOA14_SMEFTNLO_gg4l_OtpSquared.root']
OptSqrd = ['mc16_13TeV.507654.MGPy8EG_N30NLOA14_SMEFTNLO_gg4l_OptSquared.root']
#OpgOtp = ['mc16_13TeV.507599.MGPy8EG_N30NLOA14_SMEFTNLO_gg4l_OpgSq_OpgOtpfix.root']
OptOtp = ['mc16_13TeV.507648.MGPy8EG_N30NLOA14_SMEFTNLO_gg4l_OptOtp_Squared.root']
'''
