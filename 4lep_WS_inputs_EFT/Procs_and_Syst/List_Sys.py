import sys
import os

var_suffix = ["__1up","__1down"]
def updown(sys_list):
    return [ i+j for i in sys_list for j in var_suffix]

################Theory uncertainties###############
#ggZZ
ggF_KfactorNLO_sys = updown(['weight_var_ggZZNLOkB_QCD_syst',
                             'weight_var_ggZZNLOkSBI_QCD_syst',
                             'weight_var_ggZZNLOkS_QCD_syst'])

ggF_KfactorNLO_sys = updown(['weight_var_ggZZNLOkB_QCD_syst',
                             'weight_var_ggZZNLOkSBI_QCD_syst',
                             'weight_var_ggZZNLOkS_QCD_syst'])

ggZZ_Shower_sys = updown(['weight_var_H4l_Shower_UEPS_Sherpa_ggHM_CKKW',
                          'weight_var_H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN',
                          'weight_var_H4l_Shower_UEPS_Sherpa_ggHM_QSF'])

#qqZZ
qqZZ_scale_factors = ['weight_var_th_MUR0p5_MUF0p5_PDF261000',
                      'weight_var_th_MUR0p5_MUF1_PDF261000',
                      'weight_var_th_MUR1_MUF0p5_PDF261000',
                      'weight_var_th_MUR1_MUF1_PDF261000',
                      'weight_var_th_MUR1_MUF2_PDF261000',
                      'weight_var_th_MUR2_MUF1_PDF261000',
                      'weight_var_th_MUR2_MUF2_PDF261000']

qqZZ_NLO_corr = updown(['weight_var_HOEW_QCD_syst',
                        'weight_var_HOEW_syst'])

qqZZ_Shower_sys = updown(['weight_var_H4l_Shower_UEPS_Sherpa_HM_CKKW',
                          'weight_var_H4l_Shower_UEPS_Sherpa_CSSKIN',
                          'weight_var_H4l_Shower_UEPS_Sherpa_HM_QSF'])

#weight_var_th_MUR1_MUF1_PDF261001-100
Sherpa_samples_PDF_sys=["weight_var_th_MUR1_MUF1_PDF261{0:03}".format(j) for j in range(0,100+1)]


#VBF
VBF_scale_factors = ["weight_var_th_MUR0p5_MUF0p5_PDF260000",
                     "weight_var_th_MUR0p5_MUF1p0_PDF260000",
                     "weight_var_th_MUR0p5_MUF2p0_PDF260000",
                     "weight_var_th_MUR1p0_MUF0p5_PDF260000",
                     "weight_var_th_MUR1p0_MUF1p0_PDF260000",
                     "weight_var_th_MUR1p0_MUF2p0_PDF260000",
                     "weight_var_th_MUR2p0_MUF0p5_PDF260000",
                     "weight_var_th_MUR2p0_MUF1p0_PDF260000",
                     "weight_var_th_MUR2p0_MUF2p0_PDF260000"]

VBF_samples_PDF_sys=["weight_var_th_MUR1p0_MUF1p0_PDF260{0:03}".format(j) for j in range(0,100+1)]

VBF_PS_sys = ["weight_var_H4l_Shower_UEPS_VBF_OffShell_MUR1p0_MUF0p5_PDF260000__1up",
              "weight_var_H4l_Shower_UEPS_VBF_OffShell_MUR2p0_MUF2p0_PDF260000__1up",
              "weight_var_H4l_Shower_UEPS_VBF_OffShell_isrmuRfac_0p5_fsrmuRfac_0p5__1up",
              "weight_var_H4l_Shower_UEPS_VBF_OffShell_isrmuRfac_0p5_fsrmuRfac_2p0__1up",
              "weight_var_H4l_Shower_UEPS_VBF_OffShell_isrmuRfac_2p0_fsrmuRfac_2p0__1up"]

#EFT
EFT_scale_factors_MUF_MUR = [#MUF and MUR
                     "weight_var_th_MUR05_MUF05_PDF261000",
                     "weight_var_th_MUR05_MUF10_PDF261000",
                     "weight_var_th_MUR05_MUF20_PDF261000",
                     "weight_var_th_MUR10_MUF05_PDF261000",
                     "weight_var_th_MUR10_MUF10_PDF261000",
                     "weight_var_th_MUR10_MUF20_PDF261000",
                     "weight_var_th_MUR20_MUF05_PDF261000",
                     "weight_var_th_MUR20_MUF10_PDF261000",
                     "weight_var_th_MUR20_MUF20_PDF261000",
                     ]

EFT_scale_factors_PDF_var = [#PDFset variations
                     "weight_var_th_MUR10_MUF10_PDF13000",#CT14nnlo
                     "weight_var_th_MUR10_MUF10_PDF25300",#MMHT2014nnlo68cl
                     "weight_var_th_MUR10_MUF10_PDF91400",#PDF4LHC15_nnlo_30_pdfas
                     ]

EFT_scale_factors_alphaS = [#alpha_S
                     "weight_var_th_MUR10_MUF10_PDF269000",#NNPDF30_nnlo_as_0117
                     "weight_var_th_MUR10_MUF10_PDF270000",#NNPDF30_nnlo_as_0119
                     ]

#EFT
EFT_scale_factors = EFT_scale_factors_MUF_MUR + EFT_scale_factors_PDF_var + EFT_scale_factors_alphaS



#weight_var_th_MUR10_MUF10_PDF261001-100
Madgraph_samples_PDF_sys=["weight_var_th_MUR10_MUF10_PDF261{0:03}".format(j) for j in range(0,100+1)]

EFT_Shower_sys_muf_muR = ["weight_var_th_isrmuRfac05_fsrmuRfac05",
                          "weight_var_th_isrmuRfac05_fsrmuRfac10",
                          "weight_var_th_isrmuRfac05_fsrmuRfac20",
                          "weight_var_th_isrmuRfac0625_fsrmuRfac10",
                          "weight_var_th_isrmuRfac075_fsrmuRfac10",
                          "weight_var_th_isrmuRfac0875_fsrmuRfac10",
                          "weight_var_th_isrmuRfac10_fsrmuRfac05",
                          "weight_var_th_isrmuRfac10_fsrmuRfac0625",
                          "weight_var_th_isrmuRfac10_fsrmuRfac075",
                          "weight_var_th_isrmuRfac10_fsrmuRfac0875",
                          "weight_var_th_isrmuRfac10_fsrmuRfac125",
                          "weight_var_th_isrmuRfac10_fsrmuRfac15",
                          "weight_var_th_isrmuRfac10_fsrmuRfac175",
                          "weight_var_th_isrmuRfac10_fsrmuRfac20",
                          "weight_var_th_isrmuRfac125_fsrmuRfac10",
                          "weight_var_th_isrmuRfac15_fsrmuRfac10",
                          "weight_var_th_isrmuRfac175_fsrmuRfac10",
                          "weight_var_th_isrmuRfac20_fsrmuRfac05",
                          "weight_var_th_isrmuRfac20_fsrmuRfac10",
                          "weight_var_th_isrmuRfac20_fsrmuRfac20"]

EFT_Shower_sys_others = ["weight_var_th_Var3cDown",
                         "weight_var_th_Var3cUp",
                         "weight_var_th_hardHi",
                         "weight_var_th_hardLo",
                         "weight_var_th_isrPDFminus",
                         "weight_var_th_isrPDFplus"]

EFT_Shower_sys = EFT_Shower_sys_others + EFT_Shower_sys_muf_muR

####################################################

#############Experimental uncertainties#############
NormExp_sys = updown(["weight_var_EL_EFF_ID_CorrUncertaintyNP"+str(j) for j in range(0,15+1)] \
                   + ["weight_var_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP"+str(j) for j in range(0,17+1)] \
                   + ["weight_var_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR",
                      "weight_var_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR",
                      "weight_var_MUON_EFF_ISO_STAT",
                      "weight_var_MUON_EFF_ISO_SYS",
                      "weight_var_MUON_EFF_RECO_STAT_LOWPT",
                      "weight_var_MUON_EFF_RECO_STAT",
                      "weight_var_MUON_EFF_RECO_SYS_LOWPT",
                      "weight_var_MUON_EFF_RECO_SYS",
                      "weight_var_MUON_EFF_TTVA_STAT",
                      "weight_var_MUON_EFF_TTVA_SYS",
                      "weight_var_JET_fJvtEfficiency",
                      "weight_var_PRW_DATASF"])


ExtraExp_sys = updown(["EG_RESOLUTION_ALL",
                       "EG_SCALE_AF2",
                       "EG_SCALE_ALL",
                       "JET_BJES_Response",
                       "JET_EffectiveNP_Detector1",
                       "JET_EffectiveNP_Detector2",
                       "JET_EffectiveNP_Mixed1",
                       "JET_EffectiveNP_Mixed2",
                       "JET_EffectiveNP_Mixed3",
                       "JET_EffectiveNP_Modelling1",
                       "JET_EffectiveNP_Modelling2",
                       "JET_EffectiveNP_Modelling3",
                       "JET_EffectiveNP_Modelling4",
                       "JET_EffectiveNP_Statistical1",
                       "JET_EffectiveNP_Statistical2",
                       "JET_EffectiveNP_Statistical3",
                       "JET_EffectiveNP_Statistical4",
                       "JET_EffectiveNP_Statistical5",
                       "JET_EffectiveNP_Statistical6",
                       "JET_EtaIntercalibration_Modelling",
                       "JET_EtaIntercalibration_NonClosure_2018data",
                       "JET_EtaIntercalibration_NonClosure_highE",
                       "JET_EtaIntercalibration_NonClosure_negEta",
                       "JET_EtaIntercalibration_NonClosure_posEta",
                       "JET_EtaIntercalibration_TotalStat",
                       "JET_Flavor_Composition",
                       "JET_Flavor_Response",
                       "JET_JER_DataVsMC_MC16",
                       "JET_JER_EffectiveNP_10",
                       "JET_JER_EffectiveNP_11",
                       "JET_JER_EffectiveNP_1",
                       "JET_JER_EffectiveNP_12restTerm",
                       "JET_JER_EffectiveNP_2",
                       "JET_JER_EffectiveNP_3",
                       "JET_JER_EffectiveNP_4",
                       "JET_JER_EffectiveNP_5",
                       "JET_JER_EffectiveNP_6",
                       "JET_JER_EffectiveNP_7",
                       "JET_JER_EffectiveNP_8",
                       "JET_JER_EffectiveNP_9",
                       "JET_Pileup_OffsetMu",
                       "JET_Pileup_OffsetNPV",
                       "JET_Pileup_PtTerm",
                       "JET_Pileup_RhoTopology",
                       "JET_PunchThrough_MC16",
                       "JET_SingleParticle_HighPt",
                       "MUON_ID",
                       "MUON_MS",
                       "MUON_SAGITTA_RESBIAS",
                       "MUON_SAGITTA_RHO",
                       "MUON_SCALE",
                       "PD_JET_JER_DataVsMC_MC16",
                       "PD_JET_JER_EffectiveNP_10",
                       "PD_JET_JER_EffectiveNP_11",
                       "PD_JET_JER_EffectiveNP_1",
                       "PD_JET_JER_EffectiveNP_12restTerm",
                       "PD_JET_JER_EffectiveNP_2",
                       "PD_JET_JER_EffectiveNP_3",
                       "PD_JET_JER_EffectiveNP_4",
                       "PD_JET_JER_EffectiveNP_5",
                       "PD_JET_JER_EffectiveNP_6",
                       "PD_JET_JER_EffectiveNP_7",
                       "PD_JET_JER_EffectiveNP_8",
                       "PD_JET_JER_EffectiveNP_9"])
                       # "MET_SoftTrk_Scale",


#ExtraExp_sys.append("MET_SoftTrk_ResoPara")
#ExtraExp_sys.append("MET_SoftTrk_ResoPerp")
#do not consider the MET in 4l analysis

#######################################################

Systematics_list=["NormSystematic"]+ExtraExp_sys

#should change to theoretical uncertainties
TheoSyst_dict = {'ggZZ': ggF_KfactorNLO_sys + Sherpa_samples_PDF_sys + ggZZ_Shower_sys,
                 'VBF' : VBF_samples_PDF_sys + VBF_scale_factors + VBF_PS_sys,
                 'ttV_01':[],
                 'ttV_02':[],
                 'ttV_03':[],
                 'ttV_04':[],
                 'ttV_05':[],
                 'ttV_06':[],
                 'ttV_07':[],
                 'ttV_08':[],
                 'ttV_09':[],
                 'ttV_All':[]}


for proc in ['qqZZ_1_0jet','qqZZ_1_1jet','qqZZ_1_2jet',
             'qqZZ_2_0jet','qqZZ_2_1jet','qqZZ_2_2jet',
             'qqZZ_3_0jet','qqZZ_3_1jet','qqZZ_3_2jet',
             'qqZZ_all','qqZZ_0jet','qqZZ_1jet','qqZZ_2jet']:
    TheoSyst_dict[proc]= qqZZ_scale_factors + qqZZ_NLO_corr + Sherpa_samples_PDF_sys + qqZZ_Shower_sys

for proc in ['OtpSM','OpgSM','OptSM',
             'OpgSqrd','OtpSqrd','OptSqrd',
             'OpgOtp','OptOtp']:
    TheoSyst_dict[proc]= EFT_scale_factors + Madgraph_samples_PDF_sys + EFT_Shower_sys
