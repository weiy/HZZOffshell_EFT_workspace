InSample mc16_13TeV.507650.MGPy8EG_N30NLOA14_SMEFTNLO_gg4l_OptSM.root
['OptSM']
['weight*(weight>-0.01)*(weight<0.01)']
1
!!python/object/apply:collections.OrderedDict
- - - SR_4l_NN_MELA_0p1ggF_0p9qqZZ
    - - ((220<=m4l_fsr)&&(m4l_fsr<=2000))
      - log10(NN_MELA_incl_ggH/(0.1*NN_MELA_incl_ggZZ+0.9*NN_MELA_incl_qqZZ))
      - - -2.0
        - -1.72
        - -1.43
        - -1.15
        - -0.86
        - -0.58
        - -0.29
        - -0.01
        - 0.28
        - 0.56
        - 1.7

doing Campaign mc16d...
Filling nominal histograms OptSM ...
Info in <TCanvas::MakeDefCanvas>:  created default TCanvas with name c1
Writing...Name: Nom_hist Title: Nom_hist NbinsX: 10
-0.100773631628
