InSample mc16_13TeV.507650.MGPy8EG_N30NLOA14_SMEFTNLO_gg4l_OptSM.root
['OptSM']
['weight*(weight>-0.01)*(weight<0.01)']
1
!!python/object/apply:collections.OrderedDict
- - - SR_4l_m4l_18bins_m4l_fsr
    - - ((220<=m4l_fsr)&&(m4l_fsr<=2000))
      - m4l_fsr
      - - 220.0
        - 318.8888888888889
        - 417.77777777777777
        - 516.6666666666666
        - 615.5555555555555
        - 714.4444444444445
        - 813.3333333333333
        - 912.2222222222222
        - 1011.1111111111111
        - 1110.0
        - 1208.888888888889
        - 1307.7777777777778
        - 1406.6666666666665
        - 1505.5555555555554
        - 1604.4444444444443
        - 1703.3333333333333
        - 1802.2222222222222
        - 1901.111111111111
        - 2000.0

doing Campaign mc16d...
Filling nominal histograms OptSM ...
Info in <TCanvas::MakeDefCanvas>:  created default TCanvas with name c1
Writing...Name: Nom_hist Title: Nom_hist NbinsX: 18
-0.100773574623
