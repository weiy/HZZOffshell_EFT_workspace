InSample mc16_13TeV.364252.Sherpa_222_NNPDF30NNLO_llll_m4l300.root
['qqBkg_3']
['weight']
1
!!python/object/apply:collections.OrderedDict
- - - SR_4l_MCFM_MCFM_MELA_ggZZ
    - - ((220<=m4l_fsr)&&(m4l_fsr<=2000))
      - MCFM_MELA_ggZZ
      - - -5.0
        - -4.4
        - -3.8
        - -3.2
        - -2.6
        - -2.0
        - -1.4
        - -0.8
        - 1.0

doing Campaign mc16e...
Filling nominal histograms qqBkg_3 ...
Info in <TCanvas::MakeDefCanvas>:  created default TCanvas with name c1
Writing...Name: Nom_hist Title: Nom_hist NbinsX: 8
323.324072629
