InSample mc16_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.root
['qqBkg_1']
['weight']
1
!!python/object/apply:collections.OrderedDict
- - - SR_4l_MCFM_MCFM_MELA_ggZZ
    - - ((220<=m4l_fsr)&&(m4l_fsr<=2000))
      - MCFM_MELA_ggZZ
      - - -5.0
        - -4.4
        - -3.8
        - -3.2
        - -2.6
        - -2.0
        - -1.4
        - -0.8
        - -0.2
        - 1.0

doing Campaign mc16a...
Filling nominal histograms qqBkg_1 ...
Info in <TCanvas::MakeDefCanvas>:  created default TCanvas with name c1
Writing...Name: Nom_hist Title: Nom_hist NbinsX: 9
305.77585033
