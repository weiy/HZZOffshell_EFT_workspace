#!/bin/bash
# outname="Final_WS_MCFM"
outname="Final_WS_NN_SM"
#outname="Final_WS"
date="23Jan2023"

filelist="OpgSM_hists.root OptSqrd_hists.root OpgSqrd_hists.root OtpSM_hists.root ggZZ_hists.root OptOtp_hists.root OtpSqrd_hists.root OpgOtp_hists.root OptSM_hists.root VBF_hists.root"

outputfileName="./outputs_"$date"/Merged_code1_"$outname

rm -rf $outputfileName
mkdir $outputfileName


# inputfileName="./outputs_"$date"/hists_code1_4l_EFT_WS_m4l_18bins/"
# inputfileName="./outputs_"$date"/hists_code1_4l_EFT_WS_MCFM/"
inputfileName="./outputs_"$date"/hists_code1_4l_EFT_WS_NN_SM/"
yield_file="./outputs_"$date"/yields.txt"
outPlotsPath="./outputs_"$date"/Plots/"

if [ ! -d ${outPlotsPath} ]; then mkdir ${outPlotsPath}; fi

for proc in ${filelist};
do hadd -f $outputfileName/$proc $inputfileName/mc16*/$proc;
done;

#Merge all ttV files
hadd $outputfileName/ttV_All_hists.root $inputfileName/mc16*/ttV*

#Merge the 3 qqZZ files
hadd -f $outputfileName/qqZZ_0jet_hists.root $inputfileName/mc16*/qqZZ_*_0jet_hists.root
hadd -f $outputfileName/qqZZ_1jet_hists.root $inputfileName/mc16*/qqZZ_*_1jet_hists.root
hadd -f $outputfileName/qqZZ_2jet_hists.root $inputfileName/mc16*/qqZZ_*_2jet_hists.root

hadd $outputfileName/qqZZ_all_hists.root $outputfileName/qqZZ_0jet_hists.root $outputfileName/qqZZ_1jet_hists.root $outputfileName/qqZZ_2jet_hists.root

# hadd -f $inputfileName/mc16a/qqZZ_hists.root $inputfileName/mc16a/qqZZ_*_hists.root
# hadd -f $inputfileName/mc16d/qqZZ_hists.root $inputfileName/mc16d/qqZZ_*_hists.root
# hadd -f $inputfileName/mc16e/qqZZ_hists.root $inputfileName/mc16e/qqZZ_*_hists.root

# python qqZZ_norm_overflow_3SR.py --inpath $outputfileName > norm_qqZZ.txt #####this is important but we ignore this as we will use SM inputs for qqZZ

#python3 plotter_code.py --inpath $outputfileName --outpath $outPlotsPath 
#python3 nice_plots.py --inpath $outputfileName --outpath $outPlotsPath
