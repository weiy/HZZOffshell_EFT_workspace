import os
from collections import OrderedDict
from glob import glob
import argparse
import json
import yaml
import jsbeautifier

inpath='WS_inputs_23Jan2023_Final_WS_NN_SM_final_smooth'
catgs=["m4l_fsr_CR","SR_ggF","SR_Mxd","SR_VBF"]

def get_catg_bin(line):
    return int(line.split("_bin_")[1].split("_")[0])


def get_catg(line):
    category_tmp=line.replace("[","").replace("]","")
    for catg in catgs:
        if catg in category_tmp:
            return catg
    print("Something wrong here. No catg return")
    print("line input:"+line)
    exit()


channels=['OpgSM','OpgSqrd','OtpSM','OtpSqrd','OpgOtp']

for ch in channels:
    sys_f=inpath+"/"+ch+"_sys.txt"
    # f_out=open(outpath+"/"+os.path.basename(sys_f),"w")
    # ch=os.path.basename(sys_f).replace("_sys.txt","")
    # print(ch)
    for line in open(sys_f):
        if "[" in line:
            # print(line)
            catg=get_catg(line)
            catg_bin=get_catg_bin(line)
            # f_out.write(line)
            continue
        line_split=line.split()
        sys_smooth_if=False
        # if "qqZZ_CSSKIN_" in line:
        #     # continue
        #     line=line.rstrip().split()
        #     dn=float(line[3])
        #     up=2-dn
        #     f_out.write(line[0]+" = " + str(up) + " " + str(dn) + "\n")
        #     continue
        #if catg not in ['SR_ggF','SR_Mixed','SR_VBF']:
        line_tmp=line.rstrip()
        # print(line)
        line=line.rstrip().split()
        if len(line) != 4:
            print("ERROR here:" + line_tmp)
            continue
        # print(line)
        if (float(line[2])-1)*(float(line[3])-1) > 0:
            print("ERROR: asymmetry found in "+ch+","+catg+"_bin_"+str(catg_bin)+", "+line_tmp)
        if float(line[2]) > 1.15 or float(line[2]) < 0.85 or float(line[3]) > 1.15 or float(line[3]) < 0.85:
            print("before file+catg+var: "+ch+","+catg+"_bin_"+str(catg_bin)+", "+line_tmp)
        # if line[0] in sys_smooths and ch in ['Zjets_bkg','tot_emu_bkg','Other_bkg']:
        #     line[2]=str(1.0000)
        #     line[3]=str(1.0000)
