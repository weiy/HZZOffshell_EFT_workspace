import os

outname="Final_WS_NN_SM"
date="23Jan2023"

inPath="./outputs_"+date+"/Merged_code2_"+outname

print(inPath)
outputPath="WS_inputs_"+date+"_"+outname

os.system('mkdir -p ' + outputPath)
os.system('rm -rf ' + outputPath)
os.system('rm -rf NP_names.txt')
os.system('mkdir -p ' + outputPath)

#HighSoverB=4,11,2
HSOB_Threshold=0
LSOB_Threshold=0


ProcNames = ["qqZZ_0jet","qqZZ_1jet","qqZZ_2jet",
             "ggZZ","VBF","ttV_All",
             "OtpSM","OpgSM",
             "OpgSqrd","OtpSqrd",
             "OpgOtp"]

#ProcNames = ["qqZZ"]

for ProcName in ProcNames:
    os.system('python make_WSInputs_code3.py --inProc '+ProcName+' --inpath '+inPath+' --outpath '+outputPath)

os.system('python make_NPfile.py --outpath '+outputPath)
