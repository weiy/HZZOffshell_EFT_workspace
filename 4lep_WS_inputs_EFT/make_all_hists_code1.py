from ROOT import TFile, gROOT, gStyle, TH1, Form, TChain, TH1F
import sys
sys.path.append('./Procs_and_Syst/')
sys.dont_write_bytecode = True
import os
from array import array
import argparse
import List_Sys
import List_Procs
from array import array
import numpy as np
from math import sqrt
import yaml
from collections import OrderedDict

gROOT.SetBatch(1)
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--tree',    type=str, default='tree_incl_all', help='Name of tree')
parser.add_argument('--inpath',  type=str, default='', help='Add path to the sample directory (only) each campaign accesed separately')
parser.add_argument('--outpath', type=str, default='', help='Add path to output')
parser.add_argument('--sample',  type=str, default='', help='Add a sample you want to read')
parser.add_argument('--proc',  type=str, default='', help='which process to deal with')
parser.add_argument('-c','--campaign',type=str,  nargs='+', default =['mc16a','mc16d','mc16e'],  help='Campaigns needed to run over, example -c mc16a ')
parser.add_argument('--doTheoSys', type=bool, default=True, help='Make Theory Systematic hists')
parser.add_argument('--doNormExptSys', type=bool, default=True, help='Make Norm Experimental Systematic hists')
parser.add_argument('--doExtraExptSys', type=bool, default=True, help='Make Extra Experimental Systematic hists')

args = parser.parse_args()

def region_of_interest(list_regions,ROI_names,all_regions):
    #ROI: region of interest
    """
    function defines the new regions of interest
    """
    for ROI_name in ROI_names:
        if ROI_name in all_regions:
            ROI = all_regions[ROI_name]
            Cuts, Obs, Obs_name, isVarBin, binning = ROI[0], ROI[1], ROI[2], ROI[4], ROI[5]
            name = ROI_name+"_"+str(Obs_name)
            if isVarBin == True:
                bins = binning
            else:
                bins = np.linspace(binning[1], binning[2], num=binning[0]+1).tolist()
        else:
            print("ERROR: region "+ROI_name+" is not defined!")
        list_regions.update({name:[Cuts, Obs, bins]})

def concat_hists(hist_list, histName):
    """
    Concatenate the historgams from all the regions to make one-long histogram
    easier when removing normalization
    """
    binsContent=[] #list to append bin contents
    binsError  =[] #list to append bin errors
    binsEntry  =0  #gets the entries in the histogram
    outhist_len = 0 #len of the concatenated histogram
    for hist in hist_list:
        outhist_len = outhist_len + hist.GetNbinsX()  #concatenates histograms to get final histogram length
    #Define the output concatenated histogram
    outhist = TH1F(histName, histName, outhist_len, 0, outhist_len)
    #Loop over the histograms, each one corresponding to each SR/CR region
    for hist in hist_list:
        binsEntry = binsEntry+(hist.GetEntries()) #get the entries
        for i in range(1,hist.GetNbinsX()+1): #Only the bins, not under and overflow, add overflow to last and underflow to fist bin
            if i ==  1: #add underflow to first bin, do sum in quadrature for the errors
                binsContent.append(hist.GetBinContent(i)+hist.GetBinContent(0))
                binsError.append(sqrt((hist.GetBinError(i))**2+(hist.GetBinError(0))**2))
            elif i == hist.GetNbinsX(): #add overflow to last coloumn and sum in quadrature for error
                binsContent.append(hist.GetBinContent(i)+hist.GetBinContent(hist.GetNbinsX()+1))
                binsError.append(sqrt((hist.GetBinError(i))**2+(hist.GetBinError(hist.GetNbinsX()+1))**2))
            else: #just get the bin content for other bins
                binsContent.append(hist.GetBinContent(i))
                binsError.append(hist.GetBinError(i))

    #set the information in the concatenated histogram
    outhist.SetEntries(binsEntry)
    for i in range(1,len(binsContent)+1):
        #print( binsContent[i-1])
        outhist.SetBinContent(i, binsContent[i-1])
        outhist.SetBinError(i, binsError[i-1])
    #return the concatenated hist
    #print(sum(binsContent))
    return outhist

def write_hist(list_regions,dictHists,Output_Dir,process,camp,myTree):
    """
    function defines the histograms to be written out
    depending on the systematics/ processes etc.
    use the concat_hists function to write out the histogram that has been concatenated in the various regions
    """

    OutFolder = args.outpath #path where hist is stored
    listHists = [] #list of histograms to write out
    #Fill nominal histograms

    for histToFill in dictHists.keys():
        i = 0
        hist_Rgns =[]
        for region in list_regions.keys():
            histName = region+"_"+histToFill #histogram named as Region_variation
            print("weiy reginos+histToFill: "+histName)
            bin_edges = np.array(list_regions[region][2], dtype='float64') #Get the binning/edges
            hist_Rgns.append(TH1F(histName, histName, len(bin_edges)-1, bin_edges)) #append the histograms to a list
            
            if "abs(jet_eta[0]" in list_regions[region][0]:
                divided_wgts = list_regions[region][0].split("||")
                #print(divided_wgts)
                for part, part_wgts in enumerate(divided_wgts):
                    if part == 0:
                        fillHist = Form('%s>>%s'%(list_regions[region][1],histName)) #Fill thehistogram with the appropriate variable
                        print("weiy: fillHist = "+str(fillHist))
                        fillWeight = Form('%s*%s' % (dictHists[histToFill],part_wgts))#use the appropriate weights
                        print("weiy: fillWeight = "+str(fillWeight))
                        myTree.Draw(fillHist, fillWeight)# draw the histogram
                        #print("----------------------Integral------------------")
                        #print(hist_Rgns[i].Integral())
                    else:
                        #print("here")
                        fillHist = Form('%s>>+%s'%(list_regions[region][1],histName)) #Fill the histogram with the appropriate variable
                        print("weiy: fillHist = "+str(fillHist))
                        fillWeight = Form('%s*%s' % (dictHists[histToFill],part_wgts))#use the appropriate weights
                        print("weiy: fillWeight = "+str(fillWeight))
                        myTree.Draw(fillHist, fillWeight)# draw the histogram
                        #print("----------------------Integral------------------")
                        #print(hist_Rgns[i].Integral())
            else:

                fillHist = Form('%s>>%s'%(list_regions[region][1],histName)) #Fill the histogram with the appropriate variable
                fillWeight = Form('%s*%s' % (dictHists[histToFill],list_regions[region][0]))#use the appropriate weights
                print("weiy: fillHist = "+str(fillHist))
                print("weiy: fillWeight = "+str(fillWeight))
                myTree.Draw(fillHist, fillWeight)# draw the histogram
            i += 1
        #append the concatenated histogram
        listHists.append(concat_hists(hist_Rgns, histToFill))
    print("weiy listHists:" + str(listHists))
    OutDirCamp = os.path.join(OutFolder,camp)
    OutSample  = process+"_hists.root"

    os.system("mkdir -p " + OutDirCamp)
    OutFile = os.path.join(OutDirCamp,OutSample)
    if 'Nom_hist' in dictHists.keys():
        os.system("rm -f " + OutFile) # remove older file
    fOut_FromTree = TFile(OutFile, "UPDATE")

    directory = fOut_FromTree.GetDirectory(Output_Dir)
    if not (directory):
        directory = fOut_FromTree.mkdir(Output_Dir)
    directory.cd()
    for hist in listHists:
        print("Writing..." + str(hist))
        print(hist.Integral())
        hist.Write()
        hist.Print("All")
        hist.SetDirectory(0)

    fOut_FromTree.Write("",TFile.kOverwrite)
    fOut_FromTree.Close()

    os.system("mkdir -p " + OutDirCamp+'/ratio/')
    OutFile_ratio = os.path.join(OutDirCamp+'/ratio/',OutSample.replace('.root','_ratio.root'))
    if 'Nom_hist' in dictHists.keys():
        os.system("rm -f " + OutFile_ratio) # remove older file
    fOut_FromTree_ratio = TFile(OutFile_ratio, "UPDATE")

    directory = fOut_FromTree_ratio.GetDirectory(Output_Dir)
    if not (directory):
        directory = fOut_FromTree_ratio.mkdir(Output_Dir)
    directory.cd()
    print("weiy: dict: "+str(dictHists.keys()))
    if 'Nom_hist' not in dictHists.keys():
        Nom_hist=fOut_FromTree_ratio.Get('Nominal/Nom_hist')
    
    print(listHists)
    for hist in listHists:
        print("Writing..." + str(hist))
        print(hist.Integral())
        if 'Nom_hist' not in dictHists.keys():
            hist.Divide(Nom_hist)
        hist.Write()
        hist.SetDirectory(0)

    fOut_FromTree_ratio.Write("",TFile.kOverwrite)
    fOut_FromTree_ratio.Close()

def GetLumi(camp):
    if   "a" in camp: lumi = 36.207
    elif "d" in camp: lumi = 44.307
    elif "e" in camp: lumi = 58.250
    else:             lumi = 0
    return lumi

def main():
    """
    Main function.
    """
    #gStyle.SetOptStat(0)
    TH1.SetDefaultSumw2()

    EosDir = args.inpath
    OutDir = args.outpath
    TreeName = args.tree
    InSample = args.sample
    #The Sample being processed
    print("InSample " + InSample)

    campaigns = args.campaign #whichever campaigns need to be run

    ##############
    # Get weight #
    ##############
    process = args.proc
    weight = List_Procs.procs[args.proc].weight
    print(weight)
    print(process)
    ###########
    # Regions #
    ###########

    list_regions = OrderedDict()
    file_all_regions = open('all_regions.yaml','r')
    yaml_all_regions = yaml.safe_load(file_all_regions)

    # list of region of interest
    ROI_list = ['CR_4l_0jet_m4l','CR_4l_1jet_m4l','CR_4l_2jet_m4l','SR_4l_ggF_NN_SM','SR_4l_Mxd_NN_SM','SR_4l_VBF_NN_SM']
    #ROI_list = ['SR_4l_NN_SM_100bins']
    #ROI_list = ['SR_4l_MCFM_100bins']
    #ROI_list = ['SR_4l_m4l_10GeVbinwidth']
    region_of_interest(list_regions,ROI_list,yaml_all_regions)

    file_region_interest = open('regions_of_interest.yaml','w')
    yaml.dump(list_regions, file_region_interest, default_flow_style=False)
    file_region_interest.close()
    #print(list_regions)
    print(yaml.dump(list_regions, default_flow_style=False))

    #############################################
    # Accessing file and Writing the histograms #
    #############################################

    Nominal_tree = args.tree

    th_dir_Nominal = 'Nominal'
    th_dir_TheoSys = 'TheoSys'
    th_dir_ExptSys = 'ExptSys'

    dictNominalhists=OrderedDict()
    dictNominalhists.update({"Nom_hist":Form('(%s)'%(weight))})
    print("dictNominalhists: "+str(dictNominalhists))

    dictTheoryhists=OrderedDict()
    for var_name in List_Sys.TheoSyst_dict[process]:
        print("dictTheoryhists var_names: "+str(var_name))
        dictTheoryhists.update({var_name:Form('(%s)*(%s)'%(weight,var_name))})
    print("dictTheoryhists: "+str(dictTheoryhists))

    dictNormExphists=OrderedDict() #Get Norm Exp sys names
    for NormExp_sys in List_Sys.NormExp_sys:
        print("dictNormExphists sys_vars: "+str(NormExp_sys))
        dictNormExphists.update({NormExp_sys:Form('(%s)*(%s)'%(weight,NormExp_sys))})
    print("dictNormExphists: "+str(dictNormExphists))

    for camp in campaigns:
        print("doing Campaign " +str(camp)+"...")
        if List_Sys.Systematics_list[0] != 'NormSystematic':
            print('make sure NormSystematics is firstly processed as we need '
                  'get the division of others to Nom_hist. The ratio is more useful.')
            exit()
        for sys in List_Sys.Systematics_list:
            print("weiy: sys="+str(sys))
            myFile_path=os.path.join(EosDir,camp,'Systematics',sys,InSample)
            try:
                myFile = TFile(myFile_path)
            except:
                print('Failed to open ' + myFile_path)
                continue
            myTree = myFile.Get(TreeName)
            total = myTree.GetEntries()
            if sys == 'NormSystematic':
                write_hist(list_regions,dictNominalhists,th_dir_Nominal,process,camp,myTree)
                if args.doTheoSys == True:
                    write_hist(list_regions,dictTheoryhists,th_dir_TheoSys,process,camp,myTree)
                if args.doNormExptSys == True:
                    write_hist(list_regions,dictNormExphists,th_dir_ExptSys,process,camp,myTree)
                print("Now filling other systematics...")
            else:
                if args.doExtraExptSys == True:
                    dictExtraExphists=OrderedDict()
                    dictExtraExphists.update({sys:Form('(%s)'%(weight))})
                    #print(total)
                    write_hist(list_regions,dictExtraExphists,th_dir_ExptSys,process,camp,myTree)

if '__main__' in __name__:
    main()
