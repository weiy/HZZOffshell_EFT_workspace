import numpy as np

f_wleight=open('/afs/cern.ch/work/w/wleight/public/offshell/offshellWS/WS_inputs_0801_Final_WS/bgyields139.txt')
f_weiy=open('WS_inputs_07Aug2022_Final_WS_MCFM/bgyields139.txt')
samples_wleight=['n_qqZZ_0Jet','n_qqZZ_1Jet','n_qqZZ_2Jet','n_ggSBINLOI','n_VBFSBI','n_ttV_All']
samples_weiy=['n_qqZZ','n_ggZZ','n_VBF','n_ttV_All']

def add_lists(lists):
    added_lists = [0]*len(lists[0])
    for j in range(len(lists[0])):
        for list_i in lists:
            added_lists[j] += list_i[j]
    return added_lists

def get_CR_wleight(list0):
    return sum(list0[0:7])
def get_CR_weiy(list0):
    return sum(list0[0:4])

def get_SR_wleight(list0):
    return sum(list0[7:])
def get_SR_weiy(list0):
    return sum(list0[4:])

n_wleight = {}
for line in f_wleight:
    line=line.rstrip()
    for sample in samples_wleight:
        if sample == line.split()[0]:
            n_wleight[sample]=[float(a) for a in line.split('&')[1:]]
            print(sample+": "+str(n_wleight[sample]))

n_weiy = {}
for line in f_weiy:
    line=line.rstrip()
    for sample in samples_weiy:
        if sample == line.split()[0]:
            n_weiy[sample]=[float(a) for a in line.split('&')[1:]]
            print(sample+": "+str(n_weiy[sample]))

print('')

n_wleight['n_qqZZ']=add_lists([n_wleight['n_qqZZ_0Jet'],
                               n_wleight['n_qqZZ_1Jet'],
                               n_wleight['n_qqZZ_2Jet']])

n_wleight['n_ggZZ']=n_wleight['n_ggSBINLOI']
n_wleight['n_VBF']=n_wleight['n_VBFSBI']



#qqZZ is done!!
print('\nn_qqZZ')
print(get_CR_wleight(n_wleight['n_qqZZ']))
print(get_CR_weiy(n_weiy['n_qqZZ']))
print(get_SR_wleight(n_wleight['n_qqZZ']))
print(get_SR_weiy(n_weiy['n_qqZZ']))


#ggZZ is done!!
print('\nn_ggZZ')
print(get_CR_wleight(n_wleight['n_ggZZ']))
print(get_CR_weiy(n_weiy['n_ggZZ']))
print(get_SR_wleight(n_wleight['n_ggZZ']))
print(get_SR_weiy(n_weiy['n_ggZZ']))

#VBF is done!!
print('\nn_VBF')
print(get_CR_wleight(n_wleight['n_VBF']))
print(get_CR_weiy(n_weiy['n_VBF']))
print(get_SR_wleight(n_wleight['n_VBF']))
print(get_SR_weiy(n_weiy['n_VBF']))

#n_ttV_All is done!!
print('\nn_ttV_All')
print(get_CR_wleight(n_wleight['n_ttV_All']))
print(get_CR_weiy(n_weiy['n_ttV_All']))
print(get_SR_wleight(n_wleight['n_ttV_All']))
print(get_SR_weiy(n_weiy['n_ttV_All']))

#WZ_3lep is done!!
#print(n['n_WZ_3lep'])
#print('WZ_3lep')
#print( get_CR(n['n_WZ_3lep']) )
#print( get_SR(n['n_WZ_3lep']) )


#Zjets is done!!
#print('Z+jets')
#print( get_CR(n['n_Zjets_bkg']) )
#print( get_SR(n['n_Zjets_bkg']) )

#n_tot_emu_bkg is done!!
#print('n_tot_emu_bkg')
#print( get_CR(n['n_tot_emu_bkg']) )
#print( get_SR(n['n_tot_emu_bkg']) )

#n_others_bkg is done!!
#print('n_Other_bkg')
#print( get_CR(n['n_Other_bkg']) )
#print( get_SR(n['n_Other_bkg']) )
