outname="Test1_2l2nu_hists"

if [ ! -d Merged_code2_$outname ]; then mkdir Merged_code2_$outname; fi

inPath="./outputs_2604/Merged_code1_"$outname
outputPath="./outputs_2604/Merged_code2_"$outname



python make_sys_hists_code2.py --inProc  ggSBINLOI --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc  ggBNLO --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc  ggSNLO --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"

python make_sys_hists_code2.py --inProc  VBFSBI --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc  VBFB --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc  VBFSBI5 --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc  VBFSBI10 --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"


python make_sys_hists_code2.py --inProc  Zjets_bkg --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc  tot_emu_bkg --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc  qqZZ_bkg --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc  WZ_3lep_bkg --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
#python make_sys_hists_code2.py --inProc  WW_emu_bkg_hists --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_sys_hists_code2.py --inProc  ttbar_emu_bkg_hists --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_sys_hists_code2.py --inProc  qqBkg_1_hists --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_sys_hists_code2.py --inProc  qqBkg_2_hists --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
