from numpy.lib.recfunctions import stack_arrays
#from root_numpy import root2array, root2rec
import glob
#import root_numpy
import uproot
import pandas as pd
from pandas import HDFStore
from datetime import datetime
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import math
import csv
import argparse
import pickle


parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--inpath',  type=str, default='', help='Add path to the sample directory (only) each campaign accesed separately')
parser.add_argument('--outpath', type=str, default='', help='Add path to output')

args = parser.parse_args()
OutFolder = args.outpath
input_file_loc = args.inpath

#file_regions = open('./regions_of_interest.txt','r')
#regions_list = pickle.load(file_regions)
#file_regions.close()
#print(regions_list)

ggSBI_File    = uproot.open(input_file_loc+"/ggSBINLOI_hists.root")
ggSig_File    = uproot.open(input_file_loc+"/ggSNLO_hists.root")
ggBkg_File    = uproot.open(input_file_loc+"/ggBNLO_hists.root")

VBFSBI_File    = uproot.open(input_file_loc+"/VBFSBI_hists.root")
VBFBkg_File    = uproot.open(input_file_loc+"/VBFB_hists.root")
VBFSBI5_File    = uproot.open(input_file_loc+"/VBFSBI5_hists.root")
VBFBkg10_File    = uproot.open(input_file_loc+"/VBFSBI10_hists.root")

WZ3lep_File    = uproot.open(input_file_loc+"/WZ_3lep_bkg_hists.root")
WWemu_File    = uproot.open(input_file_loc+"/WW_emu_bkg_hists.root")
ttbaremu_File    = uproot.open(input_file_loc+"/ttbar_emu_bkg_hists.root")
totemu_File    = uproot.open(input_file_loc+"/tot_emu_bkg_hists.root")

qqZZ_File    = uproot.open(input_file_loc+"/qqZZ_bkg_hists.root")
Zjets_File    = uproot.open(input_file_loc+"/Zjets_bkg_hists.root")


xtick_names=['']
x = np.arange(len(ggSBI_File["Nominal"]["Nom_hist"].values))
kwargs = dict(width=1, fill=False,linewidth=3)

fig1 = plt.figure(figsize=(12,9))



plt.bar(x+1, WZ3lep_File["Nominal"]["Nom_hist"].values, label="WZ_3lep", edgecolor='goldenrod',linestyle='solid',**kwargs)

plt.bar(x+1, totemu_File["Nominal"]["Nom_hist"].values, label="tot_emu", edgecolor='darkgreen',linestyle='solid',**kwargs)

plt.bar(x+1, qqZZ_File["Nominal"]["Nom_hist"].values, label="qqZZ", edgecolor='Maroon',linestyle='solid',**kwargs)

plt.bar(x+1, Zjets_File["Nominal"]["Nom_hist"].values, label="ZJets", edgecolor='deeppink',linestyle='solid',**kwargs)

plt.bar(x+1, VBFSBI_File["Nominal"]["Nom_hist"].values, label="VBFSBI", edgecolor='orangered',linestyle='solid',**kwargs)
plt.bar(x+1, VBFBkg_File["Nominal"]["Nom_hist"].values, label="VBFBkg", edgecolor='orangered',linestyle='dotted',**kwargs)
plt.bar(x+1, VBFSBI5_File["Nominal"]["Nom_hist"].values, label="VBFSBI5", edgecolor='orangered',linestyle='solid',**kwargs)
plt.bar(x+1, VBFBkg10_File["Nominal"]["Nom_hist"].values, label="VBFSBI10", edgecolor='orangered',linestyle='solid',**kwargs)


plt.bar(x+1, ggSBI_File["Nominal"]["Nom_hist"].values, label="ggSBI", edgecolor='Teal',linestyle='dashed',**kwargs)
plt.bar(x+1, ggSig_File["Nominal"]["Nom_hist"].values, label="ggSig", edgecolor='Teal',linestyle='solid',**kwargs)
plt.bar(x+1, ggBkg_File["Nominal"]["Nom_hist"].values, label="ggBkg", edgecolor='Teal',linestyle='dotted',**kwargs)

for i in range(0,len(ggSBI_File["Nominal"]["Nom_hist"].values+1)):
    xtick_names.append("B "+str(i+1))

plt.legend(loc='upper right', fontsize =12)
plt.ylabel("Entries", fontsize=20)
plt.xticks(fontsize=2)
plt.yticks(fontsize=16)
plt.yscale('symlog', linthresh=0.5)
plt.axhline(y=0, color='black', linestyle='--')
plt.ylim(0,5*10**5)

plt.xticks(range(len(xtick_names)), xtick_names, size='small', rotation='vertical')
plt.axvline(x=1.5, linewidth=2, color='black')
plt.axvline(x=2.5, linewidth=2, color='black',linestyle='dotted')
plt.axvline(x=3.5, linewidth=2, color='black')
plt.axvline(x=4.5, linewidth=2, color='black',linestyle='dotted')
plt.axvline(x=5.5, linewidth=2, color='black')
plt.axvline(x=10.5, linewidth=2, color='black',linestyle='dotted')
#plt.axvline(x=15.5, linewidth=2, color='black')

plt.savefig(OutFolder+"Nom_regions_dist_all.png",dpi=200,bbox_inches = 'tight')


xtick_names=['']
x = np.arange(len(ggSBI_File["Nominal"]["Nom_hist"].values))
kwargs = dict(width=0.9,fill=True, linewidth=3,linestyle='solid')

fig1 = plt.figure(figsize=(12,9))
plt.bar(x+1, ggSBI_File["Nominal"]["Nom_hist"].values,  label="ggSBI",   color='Teal',     **kwargs )

plt.bar(x+1, VBFSBI_File["Nominal"]["Nom_hist"].values, label="VBFSBI",  color='orangered',**kwargs, bottom = ggSBI_File["Nominal"]["Nom_hist"].values)

plt.bar(x+1, Zjets_File["Nominal"]["Nom_hist"].values,  label="ZJets",   color='deeppink', **kwargs, bottom = np.array(VBFSBI_File["Nominal"]["Nom_hist"].values)+np.array(ggSBI_File["Nominal"]["Nom_hist"].values ))

plt.bar(x+1, qqZZ_File["Nominal"]["Nom_hist"].values,   label="qqZZ",    color='Maroon',   **kwargs, bottom = np.array(Zjets_File["Nominal"]["Nom_hist"].values)+np.array(VBFSBI_File["Nominal"]["Nom_hist"].values)+np.array(ggSBI_File["Nominal"]["Nom_hist"].values))

plt.bar(x+1, totemu_File["Nominal"]["Nom_hist"].values, label="tot_emu", color='darkgreen',**kwargs, bottom = np.array(qqZZ_File["Nominal"]["Nom_hist"].values)+np.array(Zjets_File["Nominal"]["Nom_hist"].values)+np.array(VBFSBI_File["Nominal"]["Nom_hist"].values)+np.array(ggSBI_File["Nominal"]["Nom_hist"].values))

plt.bar(x+1, WZ3lep_File["Nominal"]["Nom_hist"].values, label="WZ_3lep", color='goldenrod',**kwargs, bottom = np.array(totemu_File["Nominal"]["Nom_hist"].values)+np.array(qqZZ_File["Nominal"]["Nom_hist"].values)+np.array(Zjets_File["Nominal"]["Nom_hist"].values)+np.array(VBFSBI_File["Nominal"]["Nom_hist"].values)+np.array(ggSBI_File["Nominal"]["Nom_hist"].values))

#plt.bar(x+1, VBFSBI_File["Nominal"]["Nom_hist"].values, label="VBF  SBI ", color='Orangered',**kwargs)

for i in range(0,len(ggSBI_File["Nominal"]["Nom_hist"].values+1)):
    xtick_names.append("B "+str(i+1))

leg = plt.legend(loc='upper right', fontsize =20, framealpha=1, frameon=True)

plt.ylabel("Entries", fontsize=20)
plt.xticks(fontsize=10)
plt.yticks(fontsize=16)
plt.yscale('log')
plt.ylim(1e-2,1e6)
plt.xticks(range(len(xtick_names)), xtick_names, size='small', rotation='vertical')
plt.axvline(x=1.5, linewidth=2, color='black')
plt.axvline(x=2.5, linewidth=2, color='black',linestyle='dotted')
plt.axvline(x=3.5, linewidth=2, color='black')
plt.axvline(x=4.5, linewidth=2, color='black',linestyle='dotted')
plt.axvline(x=5.5, linewidth=2, color='black')
plt.axvline(x=10.5, linewidth=2, color='black',linestyle='dotted')

t = plt.text(0.5, 100000,('$\mathit{ATLAS}$ Work in Progress'), fontsize=22)
t.set_bbox(dict(facecolor='white', alpha=1, color='white'))
t = plt.text(0.5, 50000,('139 fb'+ r'$^{-1}$'), fontsize=22)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
##plt.axvline(x=17.5, linewidth=2, color='black',linestyle='dashed')
plt.savefig(OutFolder+"Nom_regions_dist_stacked.png",dpi=200,bbox_inches = 'tight')
#print("Here")
