outname="Test1_2l2nu_hists"

outputPath=./outputs_2604/hists_code1_$outname
outPlotsPath=./outputs_2604/Plots/$outname

if [ ! -d ${outputPath} ]; then mkdir ${outputPath}; fi
if [ ! -d ${outPlotsPath} ]; then mkdir ${outPlotsPath}; fi

pathToFiles=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hllvv/Run2/minitrees_rel139/
echo "--------------------------------------------------------------------------"
##ggF Processes
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_HIGG2D1.e6213_s3126_r9364_p4222.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu 
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.345725.Sherpa_222_NNPDF30NNLO_ggllvvZZNoHiggs.deriv.DAOD_HIGG2D1.e6213_s3126_r9364_p4222.root --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.345726.Sherpa_222_NNPDF30NNLO_ggllvvZZOnlyHiggs.deriv.DAOD_HIGG2D1.e6213_s3126_r9364_p4222.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu 

##VBF Processes
echo "--------------------------------------------------------------------------"
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.500379.MGPy8EG_A14_NNPDF23LO_VBFH125_bkg_2l2v_MET80.deriv.DAOD_HIGG2D1.e8065_s3126_r9364_p4222.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu 
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.500378.MGPy8EG_A14_NNPDF23LO_VBFH125_sbi_2l2v_MET80.deriv.DAOD_HIGG2D1.e8065_s3126_r9364_p4222.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu 
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.500380.MGPy8EG_A14_NNPDF23LO_VBFH125_sbi5_2l2v_MET80.deriv.DAOD_HIGG2D1.e8065_s3126_r9364_p4222.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.500381.MGPy8EG_A14_NNPDF23LO_VBFH125_sbi10_2l2v_MET80.deriv.DAOD_HIGG2D1.e8065_s3126_r9364_p4222.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu



#pathToFiles=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hllvv/Run2/minitrees_rel75/
pathToFiles=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hllvv/Run2/minitrees_rel139/
echo "--------------------------------------------------------------------------"
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364253.Sherpa_222_NNPDF30NNLO_lllv.deriv.DAOD_HIGG2D1.e5916_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu 
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.361600.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvlv.deriv.DAOD_HIGG2D1.e4616_s3126_r9364_p4220.root --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu 
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_HIGG2D1.e6348_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu 

echo "--------------------------------------------------------------------------"
##qqZZ
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.deriv.DAOD_HIGG2D1.e5894_s3126_r9364_p4191.root  --outpath $outputPath --tree tree_PFLOW --doNormExptSys True --doNormTheoSys True -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_HIGG2D1.e6240_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW --doNormExptSys True --doNormTheoSys True -c mc16a --channel 2l2nu


echo "--------------------------------------------------------------------------"
##Zjets
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364100.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_HIGG2D1.e5271_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364101.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_HIGG2D1.e5271_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364102.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_BFilter.deriv.DAOD_HIGG2D1.e5271_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_HIGG2D1.e5271_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_HIGG2D1.e5271_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364105.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_BFilter.deriv.DAOD_HIGG2D1.e5271_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364106.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_HIGG2D1.e5271_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364107.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_HIGG2D1.e5271_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364108.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_BFilter.deriv.DAOD_HIGG2D1.e5271_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364109.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_HIGG2D1.e5271_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_HIGG2D1.e5271_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364111.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_BFilter.deriv.DAOD_HIGG2D1.e5271_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364112.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV500_1000.deriv.DAOD_HIGG2D1.e5271_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364113.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV1000_E_CMS.deriv.DAOD_HIGG2D1.e5271_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu




python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364114.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_HIGG2D1.e5299_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364115.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_HIGG2D1.e5299_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364116.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_BFilter.deriv.DAOD_HIGG2D1.e5299_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364117.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_HIGG2D1.e5299_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364118.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_HIGG2D1.e5299_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364119.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_BFilter.deriv.DAOD_HIGG2D1.e5299_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364120.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_HIGG2D1.e5299_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364121.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_HIGG2D1.e5299_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364122.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_BFilter.deriv.DAOD_HIGG2D1.e5299_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364123.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_HIGG2D1.e5299_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364124.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_HIGG2D1.e5299_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364125.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_BFilter.deriv.DAOD_HIGG2D1.e5299_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364126.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV500_1000.deriv.DAOD_HIGG2D1.e5299_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364127.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV1000_E_CMS.deriv.DAOD_HIGG2D1.e5299_s3126_r9364_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16a --channel 2l2nu

