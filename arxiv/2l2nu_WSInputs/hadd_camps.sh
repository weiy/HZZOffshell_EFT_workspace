#!/bin/bash

outname="Test1_2l2nu_hists"
outputfileName="./outputs_2604/Merged_code1_"$outname

rm -rf $outputfileName
mkdir $outputfileName

#inputfileName="hists_code1"
inputfileName="./outputs_2604/hists_code1_"$outname
yield_file="./outputs_2604/yields.txt"
outPlotsPath="./outputs_2604/Plots/"$outname"/"
mkdir $outPlotsPath

filelist="ggSBINLOI_hists.root ggBNLO_hists.root ggSNLO_hists.root WZ_3lep_bkg_hists.root WW_emu_bkg_hists.root ttbar_emu_bkg_hists.root qqBkg_1_hists.root qqBkg_2_hists.root VBFB_hists.root VBFSBI_hists.root VBFSBI5_hists.root VBFSBI10_hists.root"

for proc in ${filelist};
do hadd $outputfileName/$proc $inputfileName/mc16*/$proc;
done;

hadd $outputfileName/Zjets_bkg_hists.root $inputfileName/mc16*/Zjets_bkg364*
hadd $outputfileName/tot_emu_bkg_hists.root $outputfileName/*emu_bkg_hists.root
hadd $outputfileName/qqZZ_bkg_hists.root $outputfileName/qqBkg*

#python qqZZ_norm_overflow.py --inpath $outputfileName
python3 plotter_code.py --inpath $outputfileName --outpath $outPlotsPath
#python check_yields_mu.py --inpath $outputfileName >> $yield_file
