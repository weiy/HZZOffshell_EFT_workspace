from ROOT import *
import sys
sys.path.append('./Systematic_names/')
import os
import os.path
from os import path
from glob import glob
from array import array
import math
import argparse
import List_Sys
from array import array
import numpy as np
import pickle
from numpy import linalg as LA

gROOT.SetBatch(kTRUE)
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--inpath',  type=str, default='./Merged_hists_code2_binnedCR', help='Add path to merged histograms')
parser.add_argument('--outpath', type=str, default='./Workspace_inputs_binnedCR', help='Add path to output histograms')
parser.add_argument('--inProc',  type=str, default='ggSig', help='Add the process name you want')
parser.add_argument('--Threshold_HSOB', type=float, default=0.0, help='Add the threshold for high S/B bins to consider the systematic')
parser.add_argument('--Threshold_LSOB', type=float, default=0.0, help='Add the threshold for low  S/B bins to consider the systematic')

#parser.add_argument('--HighSOB_bins', nargs='+', help='Bins with high S/B to perform pruning', required=True)
parser.add_argument('--RemoveqqZZNorm', type=bool, default=True, help='remove qqZZ norm uncern.')

#Reading in the arguments
args = parser.parse_args()
lumi = 139.0
HistPath = args.inpath
ProcName = args.inProc
outFolder = args.outpath
qqZZNoNorm = args.RemoveqqZZNorm
HighSOB_threshold = args.Threshold_HSOB
LowSOB_threshold = args.Threshold_LSOB

#High_S_over_B = args.HighSOB_bins
#High_S_over_B = [12,13,15,18]
#High_S_over_B = [29,30,31,32,40,41,42,53,54]

#High_S_over_B =  [20,21,29,30,31,35]

High_S_over_B =  [8,9,10,13,14,15]
#High_S_over_B =  [18,19,24,27]


#Directories in the root files
variations = ["1up", "1down"]
List_dirs=["Nominal", "Theosys","Exptsys",]

#Getting the list of regions with binning/ observables - output of code 1
file_regions = open('regions_of_interest.txt','r')
regions_list = pickle.load(file_regions)
file_regions.close()
#print(regions_list)

#Name of the outputput text files
outputFile = open(outFolder+"/"+ProcName+"_sys.txt", "w")  

#Looping over the bin regions to get the bin edges/to break down concatenated histograms
bin_regions =[] #upper edges of the regions 
bin_regions_names=[] #names (and observable) 
region_edges=[]#lower and upper limits
for regions in regions_list.keys():
    #print(regions_list[regions][1])
    bin_regions.append(len(regions_list[regions][2])-1)
    #Needs to be changed depending on what the observable is called
    if "ncl" in (regions_list[regions][1]):
        obs = "ggFNN_MELA"
    elif "VBF" in (regions_list[regions][1]):
        obs = "VBFNN_MELA"
    elif "MCFM" in (regions_list[regions][1]):
        obs = "ME_disc"
    else:
        obs = (regions_list[regions][1])

    print(regions)
    print(obs)

    rgn_name = regions.split("_")
    if "bb" in rgn_name[1]:
        bin_regions_names.append((obs+"_"+rgn_name[0]+"_"+rgn_name[2]))
    else:
        bin_regions_names.append((obs+"_"+rgn_name[0]+"_"+rgn_name[1]))
region_edges.append(1)
print(bin_regions_names)
print("bin_regions_names")
print(bin_regions)

for i in range(0, len(bin_regions)):
    region_edges.append(sum(bin_regions[0:i+1])+1)
#print(region_edges)

#Write out the list of NP - lots of overlap - does it sample by sample
NP_names_outFile = ("./NP_names.txt")
if os.path.isfile(NP_names_outFile) == True:
    with open (NP_names_outFile, 'rb') as NP:
        NP_names = pickle.load(NP)
else:
    NP_names= []

#Define the bkgyields file
bkgyields = outFolder+"/"+"bgyields.txt"
bkgyields139 = outFolder+"/"+"bgyields139.txt"

#Check if file exists, if yes then just append, otherwise then start with 
if os.path.isfile(bkgyields)==False:
    bkgyields_file = open(bkgyields,"a+")
    bkgyields139_file = open(bkgyields139,"a+")
    bgyields =""
    for rgn_i in range(0,len(bin_regions_names)):
        for bins in range(1, bin_regions[rgn_i]+1):
            bgyields= bgyields + (bin_regions_names[rgn_i]+"Incl_bin_"+str(bins)+"_13TeV & ")        
    bkgyields_file.write(bgyields[:-2])
    bkgyields_file.write("\nn_"+ProcName+" ") 
    bkgyields139_file.write(bgyields[:-2])
    bkgyields139_file.write("\nn_"+ProcName+" ") 

else:
    bkgyields_file = open(bkgyields,"a+")
    bkgyields_file.write("\nn_"+ProcName+" ")
    bkgyields139_file = open(bkgyields139,"a+")
    bkgyields139_file.write("\nn_"+ProcName+" ")

def write_WS_inputs(bin_regions_names,region_edges,nominalhist,var_hist,outputfile,ProcName):
    """
    Writes out the WS input text files
    """
    print(nominalhist.GetNbinsX())
    bin_i = 1
    for rgn_i in range(0,len(bin_regions_names)):
        for bins in range(1, bin_regions[rgn_i]+1):
            #print(bins)
            #print(bin_i)
            outputFile.write("["+bin_regions_names[rgn_i]+"Incl_bin_"+str(bins)+"_13TeV]\n")
            #print(bin_i)
            #if str(bin_i) in High_S_over_B:
            if bin_i in High_S_over_B:
                if 'VBF' not in ProcName:
                    if nominalhist.GetBinError(bin_i) !=0:
                        upMCSTAT  = 1+ (nominalhist.GetBinError(bin_i)/(nominalhist.GetBinContent(bin_i)))
                        dnMCSTAT  = 1- (nominalhist.GetBinError(bin_i)/(nominalhist.GetBinContent(bin_i)))
                        #outputFile.write("MCSTAT_"+ProcName+"_"+bin_regions_names[rgn_i]+"Incl_bin_"+str(bins)+"_13TeV = "+str(upMCSTAT)+" "+str(dnMCSTAT)+"\n")
                        #NP_names.append("MCSTAT_"+ProcName+"_"+bin_regions_names[rgn_i]+"Incl_bin_"+str(bins)+"_13TeV")
                        #print(((nominalhist.GetBinContent(bin_i))/nominalhist.GetBinError(bin_i))**2)
                        #print("["+bin_regions_names[rgn_i]+"Incl_bin_"+str(bins)+"_13TeV]")
            for sys in var_hist:
                name = (sys[0].GetName().replace('__1up','')).replace('weight_var_','')
                if nominalhist.GetBinContent(bin_i)!=0:
                    upVar=str(format((sys[0].GetBinContent(bin_i))/nominalhist.GetBinContent(bin_i), '.4f'))
                    dnVar=str(format((sys[1].GetBinContent(bin_i))/nominalhist.GetBinContent(bin_i), '.4f'))                    
                else:
                    upVar=str(1.00)
                    dnVar=str(1.00)            
                outputFile.write(name+" = "+(upVar)+" "+(dnVar)+"\n")
            bin_i = bin_i+1

def prune_sys(High_S_over_B,HighSOB_threshold,nom_hist,hist_list):
    #print(hist_list[0].GetName())
    var_int_HSOB=0
    nom_int_HSOB=0
    var_int_LSOB=0
    nom_int_LSOB=0
    WriteSys = False

    for i in range(1, hist_list[0].GetNbinsX()+1):
        if i in High_S_over_B:
            var_int_HSOB = var_int_HSOB+hist_list[0].GetBinContent(i)
            nom_int_HSOB = nom_int_HSOB+nom_hist.GetBinContent(i)
        else:
            var_int_LSOB = var_int_LSOB+hist_list[0].GetBinContent(i)
            nom_int_LSOB = nom_int_LSOB+nom_hist.GetBinContent(i)
            
    if nom_int_HSOB  != 0:
        RelRatio_HSOB = abs((var_int_HSOB-nom_int_HSOB)*100/nom_int_HSOB)
    else:
        RelRatio_HSOB = 0

    if nom_int_LSOB  != 0:
        RelRatio_LSOB = abs((var_int_LSOB-nom_int_LSOB)*100/nom_int_LSOB)
    else:
        RelRatio_LSOB = 0 
    #print(RelRatio_HSOB)
    #print(RelRatio_LSOB)
    if (RelRatio_HSOB>=HighSOB_threshold):
        WriteSys=True
    else:
        if(RelRatio_LSOB>=LowSOB_threshold):
            WriteSys=True
    #print(WriteSys)
    return WriteSys
   
def main():
    list_variations=[]
    list_systematic_hists=[]
    if not path.isfile(os.path.join(HistPath,ProcName+"_hists.root")):
        print("File "+ProcName+"_hists.root does not exist")
    else:
        HistFile = TFile(os.path.join(HistPath,ProcName+"_hists.root")) #get the histograms
        print("doing "+ProcName+" Process")
        for dirs in List_dirs:# loop over directories
            directory = HistFile.GetDirectory(dirs)
            if not (directory):
                print(dirs+" does not exist.")
                continue
            HistFile.cd(dirs)
            for hist in (gDirectory.GetListOfKeys()):
                if "Nom" not in (hist.GetName()):
                    list_systematic_hists.append(((hist.GetName()).replace("__1up",'')).replace("__1down",'')) #get all systematic names (without variation)
        
        list_systematic_hists = list_systematic_hists[::2]
        #print("-------------------------------------------")
        #print(list_systematic_hists)
        #print("-------------------------------------------")
        for dirs in List_dirs:
            directory = HistFile.GetDirectory(dirs)
            if not (directory):
                print(dirs+" does not exist.")
                continue
            HistFile.cd(dirs)
            if "Nom" in dirs:
                Nomhist = gDirectory.Get("Nom_hist")
                if Nomhist: #use nominal histogram to fill bgyields file scaled by lumi and bgyields139 file thats not scaled by lumi
                    print("Nominal Histogram Integral: "+str(Nomhist.Integral()))
                    for rgn_i in range(0,len(bin_regions_names)):
                        for bins in range(region_edges[rgn_i], region_edges[rgn_i+1]):
                            bkgyields139_file.write(" & "+str(format(Nomhist.GetBinContent(bins),'0.6f')))
                            binVal = Nomhist.GetBinContent(bins)/lumi
                            bkgyields_file.write(" & "+str(format(binVal,'0.6f')))
                else:
                    print(Nomhist+" does not exist.")            

            else:
                for sys in list_systematic_hists:
                    sys_hist_files=[]
                    for var in variations:
                        #print(sys+'__'+var)
                        sys_vars = sys +'__'+var
                        #print(sys_vars)
                        Sys_hist = gDirectory.Get(sys_vars)
                        if not (Sys_hist):
                            #print(str(Sys_hist)+" does not exist.")
                            continue
                        sys_hist_files.append(Sys_hist)
                    if (Sys_hist):
                        writeSys = prune_sys(High_S_over_B,HighSOB_threshold,Nomhist,sys_hist_files)
                        if writeSys==True:
                            list_variations.append(sys_hist_files)
                            NP_names.append(sys)
    
        write_WS_inputs(bin_regions_names,region_edges,Nomhist,list_variations,outputFile,ProcName)
        #print("write out the pruned NP list")
        #print(NP_names)
        with open(NP_names_outFile, 'wb') as NP:
            pickle.dump(NP_names, NP)

if '__main__' in __name__:
    main()


"""
    NP_names_list_cond=set(NP_names_list)
    print(NP_names_list)
    with open(NP_names_outFile, 'wb') as NP:
        pickle.dump(NP_names_list_cond, NP)
"""
