
outname="Test1_2l2nu_hists"
rm -rf WS_inputs_$outname
rm -rf NP_names.txt
mkdir WS_inputs_$outname

#HighSoverB=4,11,2
HSOB_Threshold=0.5
LSOB_Threshold=3

inPath="./outputs_2604/Merged_code2_"$outname

echo $inPath
outputPath=WS_inputs_$outname

python make_WSInputs_code3.py --inProc  ggSBINLOI --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc  ggBNLO --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc  ggSNLO --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"

python make_WSInputs_code3.py --inProc  VBFSBI --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc  VBFB --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc  VBFSBI5 --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc  VBFSBI10 --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"


python make_WSInputs_code3.py --inProc  Zjets_bkg --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc  tot_emu_bkg --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc  qqZZ_bkg --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc  WZ_3lep_bkg --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"

echo "Writing NP list"
python make_NPfile.py --outpath $outputPath
echo "Writing Config file"
python make_config_file.py --FileName config_file_3SRs_$outname.ini --inpath /afs/cern.ch/user/s/skrishna/work/HZZ/offshellstatistics/2l2nuMakeWSInputs/$outputPath
