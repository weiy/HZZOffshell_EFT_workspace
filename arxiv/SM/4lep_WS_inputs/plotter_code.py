from numpy.lib.recfunctions import stack_arrays
#from root_numpy import root2array, root2rec
import glob
#import root_numpy
import uproot
import pandas as pd
from pandas import HDFStore
from datetime import datetime
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import math
import csv
import argparse
import pickle


parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--inpath',  type=str, default='', help='Add path to the sample directory (only) each campaign accesed separately')
parser.add_argument('--outpath', type=str, default='', help='Add path to output')

args = parser.parse_args()
OutFolder = args.outpath
input_file_loc = args.inpath

#file_regions = open('./regions_of_interest.txt','r')
#regions_list = pickle.load(file_regions)
#file_regions.close()
#print(regions_list)

ggSig_File = uproot.open(input_file_loc+"/ggSNLOS_hists.root")
ggBkg_File = uproot.open(input_file_loc+"/ggBNLOB_hists.root")
ggInf_File = uproot.open(input_file_loc+"/ggINLO_hists.root")

qqBkg_File = uproot.open(input_file_loc+"/qqZZ_all_hists.root")

VBFS_File  = uproot.open(input_file_loc+"/VBFS_hists.root")
VBFB_File  = uproot.open(input_file_loc+"/VBFB_hists.root")
VBFI_File  = uproot.open(input_file_loc+"/VBFI_hists.root")

#print(ggBkg_File.keys())

xtick_names=['']
x = np.arange(len(ggBkg_File["Nominal"]["Nom_hist"].values))
kwargs = dict(width=1, fill=False,linewidth=3)

N_val_ggF = ggSig_File["Nominal"]["Nom_hist"].values+ggBkg_File["Nominal"]["Nom_hist"].values+ggInf_File["Nominal"]["Nom_hist"].values

N_over_s_ggF =[]
s_err_val =(ggSig_File["Nominal"]["Nom_hist"].variances)
b_err_val =(ggBkg_File["Nominal"]["Nom_hist"].variances)
i_err_val =(ggInf_File["Nominal"]["Nom_hist"].variances)

#print(N_val_ggF)
#print("---------")
for i in range(0, len(s_err_val)):
    s_val = max(s_err_val[i],b_err_val[i],i_err_val[i])
    #print(s_err_val[i],b_err_val[i],i_err_val[i])
    #print("----")
    if N_val_ggF[i] == 0:
        N_over_s_ggF.append(0)
    else:
        N_over_s_ggF.append(abs(N_val_ggF[i])/math.sqrt(s_val))
    

N_val_VBF = VBFS_File["Nominal"]["Nom_hist"].values+VBFB_File["Nominal"]["Nom_hist"].values+VBFI_File["Nominal"]["Nom_hist"].values

N_over_s_VBF =[]
s_err_val =(VBFS_File["Nominal"]["Nom_hist"].variances)
b_err_val =(VBFB_File["Nominal"]["Nom_hist"].variances)
i_err_val =(VBFI_File["Nominal"]["Nom_hist"].variances)
#print(N_val)
for i in range(0, len(s_err_val)):
    s_val = max(s_err_val[i],b_err_val[i],i_err_val[i])
    N_over_s_VBF.append(abs(N_val_VBF[i])/math.sqrt(s_val))

S_over_B = (VBFS_File["Nominal"]["Nom_hist"].values+ggSig_File["Nominal"]["Nom_hist"].values)/(ggBkg_File["Nominal"]["Nom_hist"].values+qqBkg_File["Nominal"]["Nom_hist"].values+VBFB_File["Nominal"]["Nom_hist"].values)
S_over_B_ggZZ = (ggSig_File["Nominal"]["Nom_hist"].values)/(ggBkg_File["Nominal"]["Nom_hist"].values+qqBkg_File["Nominal"]["Nom_hist"].values)

print("Bins with S/B greater than 0.3")
for i in range(0,len(ggSig_File["Nominal"]["Nom_hist"].values)):
    if ggSig_File["Nominal"]["Nom_hist"].values[i]>1e-3:
        if VBFS_File["Nominal"]["Nom_hist"].values[i]>1e-3:
            if (S_over_B[i]>3e-1):
                print (i+1)

#print("Plot made in folder /Plots")

fig1 = plt.figure(figsize=(12,9))
plt.bar(x+1, qqBkg_File["Nominal"]["Nom_hist"].values, label="qqZZ Bkg ", edgecolor='Maroon',linestyle='solid',**kwargs)
plt.bar(x+1, ggSig_File["Nominal"]["Nom_hist"].values, label="ggF Sig ", edgecolor='Teal',linestyle='solid',**kwargs)
plt.bar(x+1, ggBkg_File["Nominal"]["Nom_hist"].values, label="ggF Bkg ", edgecolor='Teal',linestyle='dashed',**kwargs)
plt.bar(x+1, ggInf_File["Nominal"]["Nom_hist"].values, label="ggF Inf.", edgecolor='Teal',linestyle='dotted',**kwargs)
plt.bar(x+1, VBFS_File["Nominal"]["Nom_hist"].values,  label="VBF Sig ", edgecolor='Orangered',linestyle='solid',**kwargs)
plt.bar(x+1, VBFB_File["Nominal"]["Nom_hist"].values,  label="VBF Bkg ", edgecolor='Orangered',linestyle='dashed',**kwargs)
plt.bar(x+1, VBFI_File["Nominal"]["Nom_hist"].values,  label="VBF Inf.", edgecolor='Orangered',linestyle='dotted',**kwargs)


for i in range(0,len(ggSig_File["Nominal"]["Nom_hist"].values+1)):
    xtick_names.append("B "+str(i+1))

plt.legend(loc='upper right', fontsize =12)
plt.ylabel("Entries", fontsize=20)
plt.xticks(fontsize=2)
plt.yticks(fontsize=16)
plt.yscale('symlog', linthresh=1)
plt.axhline(y=0, color='black', linestyle='--')
plt.ylim(-10**1, 10**4)

plt.xticks(range(len(xtick_names)), xtick_names, size='small', rotation='vertical')
plt.axvline(x=4.5, linewidth=2, color='black')
plt.axvline(x=6.5, linewidth=2, color='black')
plt.axvline(x=7.5, linewidth=2, color='black')
plt.axvline(x=21.5, linewidth=2, color='black')
plt.axvline(x=24.5, linewidth=2, color='black')

##plt.axvline(x=17.5, linewidth=2, color='black',linestyle='dashed')
plt.savefig(OutFolder+"Nom_regions_dist.png",dpi=200,bbox_inches = 'tight')

for i in range(0,len(ggSig_File["Nominal"]["Nom_hist"].values+1)):
    y_values=[qqBkg_File["Nominal"]["Nom_hist"].values[i], VBFB_File["Nominal"]["Nom_hist"].values[i],VBFS_File["Nominal"]["Nom_hist"].values[i],ggSig_File["Nominal"]["Nom_hist"].values[i],ggBkg_File["Nominal"]["Nom_hist"].values[i]]
    max_y = max(y_values)
    if S_over_B_ggZZ[i]>0.2:
        plt.text(x[i]+0.7, max_y*(1.7),str(format(S_over_B_ggZZ[i],'0.2f')),size='small')
        plt.text(x[i]+0.7, max_y*(2.5),str(format(S_over_B[i],'0.2f')),size='small')

plt.savefig(OutFolder+"Nom_regions_dist_withSoverB.png",dpi=200,bbox_inches = 'tight')
plt.close(fig1)


fig2 = plt.figure(figsize=(12,9))
xtick_names=[""]
x = np.arange(len(ggBkg_File["Nominal"]["Nom_hist"].values))
plt.bar(x+1, ggSig_File["Nominal"]["Nom_hist"].values, label="ggHZZ Sig1 Nominal distribution", edgecolor='Teal',linestyle='solid',**kwargs)
plt.bar(x+1, ggBkg_File["Nominal"]["Nom_hist"].values, label="ggZZ Bkg1 Nominal distribution", edgecolor='Teal',linestyle='dashed',**kwargs)
plt.bar(x+1, ggInf_File["Nominal"]["Nom_hist"].values, label="ggZZ Interf. Nominal distribution", edgecolor='Teal',linestyle='dotted',**kwargs)
#print(x)
#print(N_over_s)
for i in range(0,len(ggSig_File["Nominal"]["Nom_hist"].values)):
    xtick_names.append("B "+str(i+1))
    y_values=[ggBkg_File["Nominal"]["Nom_hist"].values[i], ggSig_File["Nominal"]["Nom_hist"].values[i]]
    max_y = max(y_values)
    if len(str(format(N_over_s_ggF[i],'0.0f')))<2:
        shift = 1
    if len(str(format(N_over_s_ggF[i],'0.0f')))==2:
        shift = 0.8
    if len(str(format(N_over_s_ggF[i],'0.0f')))==3:
        shift = 0.6
    if len(str(format(N_over_s_ggF[i],'0.0f')))==4:
        shift = 0.5
    
    plt.text(x[i]+shift, max_y*(1.7),str(format(N_over_s_ggF[i],'0.0f')), fontsize=10)

t = plt.text(0, -10**1.5, "Number indicates N/(max(s_err, b_err, i_err)", fontsize=12)
t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='black'))

leg = plt.legend(loc='upper right', fontsize =16, framealpha=0.8)
#print(len(xtick_names))
plt.ylabel("Entries", fontsize=20)
plt.xticks(fontsize=2)
plt.yticks(fontsize=16)
#plt.ylim(1e-2,1e5)
plt.yscale('symlog', linthresh=1)
plt.axhline(y=0, color='black', linestyle='--')
plt.ylim(-10**2, 10**4)

plt.xticks(range(len(xtick_names)), xtick_names, size='small', rotation='vertical')

plt.savefig(OutFolder+"NoverS_regions_dist_ggF.png",dpi=200,bbox_inches = 'tight')
plt.close(fig2)


fig3 = plt.figure(figsize=(12,9))
xtick_names=[]
x = np.arange(len(VBFB_File["Nominal"]["Nom_hist"].values))
plt.bar(x+1, VBFS_File["Nominal"]["Nom_hist"].values, label="VBF Sig Nominal distribution", edgecolor='orangered',linestyle='solid',**kwargs)
plt.bar(x+1, VBFB_File["Nominal"]["Nom_hist"].values, label="VBF Bkg Nominal distribution", edgecolor='orangered',linestyle='dashed',**kwargs)
plt.bar(x+1, VBFI_File["Nominal"]["Nom_hist"].values, label="VBF Interf. Nominal distribution", edgecolor='orangered',linestyle='dotted',**kwargs)
#print(x)
#print(N_over_s_VBF)
for i in range(0,len(VBFB_File["Nominal"]["Nom_hist"].values+1)):
    xtick_names.append("B "+str(i+1))
    y_values=[VBFB_File["Nominal"]["Nom_hist"].values[i], VBFS_File["Nominal"]["Nom_hist"].values[i]]
    max_y = max(y_values)
    if len(str(format(N_over_s_VBF[i],'0.0f')))<2:
        shift = 1
    if len(str(format(N_over_s_VBF[i],'0.0f')))==2:
        shift = 0.8
    if len(str(format(N_over_s_VBF[i],'0.0f')))==3:
        shift = 0.6
    if len(str(format(N_over_s_VBF[i],'0.0f')))==4:
        shift = 0.5
    
    plt.text(x[i]+shift, max_y*(1.7),str(format(N_over_s_VBF[i],'0.0f')), fontsize=10)

t = plt.text(0, -10**1.5, "Number indicates N/(max(s_err, b_err, i_err)", fontsize=12)
t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='black'))

leg = plt.legend(loc='upper right', fontsize =16, framealpha=0.8)

plt.ylabel("Entries", fontsize=20)
plt.xticks(fontsize=2)
plt.yticks(fontsize=16)
#plt.ylim(1e-2,1e5)
plt.yscale('symlog', linthresh=1)
plt.axhline(y=0, color='black', linestyle='--')
plt.ylim(-10**1, 10**2)

plt.xticks(range(len(xtick_names)), xtick_names, size='small', rotation='vertical')
plt.axvline(x=4.5, linewidth=2, color='black')
plt.axvline(x=6.5, linewidth=2, color='black')
plt.axvline(x=7.5, linewidth=2, color='black')
plt.axvline(x=21.5, linewidth=2, color='black')
plt.axvline(x=24.5, linewidth=2, color='black')

plt.savefig(OutFolder+"NoverS_regions_dist_VBF.png",dpi=200,bbox_inches = 'tight')
plt.close(fig2)

