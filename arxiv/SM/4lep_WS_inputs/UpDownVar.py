from numpy.lib.recfunctions import stack_arrays
#from root_numpy import root2array, root2rec
import glob
#import root_numpy
import uproot
import pandas as pd
from pandas import HDFStore
from datetime import datetime
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import math
import csv
import argparse
import pickle


parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--inpath',  type=str, default='Merged_code1_SysTrees', help='Add path to the sample directory (only) each campaign accesed separately')
parser.add_argument('--outpath', type=str, default='./Plots/', help='Add path to output')

args = parser.parse_args()
OutFolder = args.outpath
input_file_loc = args.inpath

qqBkg_File_all = uproot.open(input_file_loc+"/qqZZ_all_hists.root")
qqBkg_File_Pt1 = uproot.open(input_file_loc+"/qqZZ_Part1_hists.root")
qqBkg_File_Pt2 = uproot.open(input_file_loc+"/qqZZ_Part2_hists.root")
qqBkg_File_Pt3 = uproot.open(input_file_loc+"/qqZZ_Part3_hists.root")

xtick_names=['']
x = np.arange(len(qqBkg_File_all["Nominal"]["Nom_hist"].values))

kwargs = dict(width=0.9,linewidth=2,linestyle='solid',fill=False)
fig1 = plt.figure(figsize=(12,9))

plt.bar(x+1, qqBkg_File_all["Nominal"]["Nom_hist"].values,  label="qqZZ Nominal", edgecolor='Orangered', **kwargs, )
plt.bar(x+1, qqBkg_File_all["Theosys"]["weight_var_H4l_Shower_UEPS_Sherpa_CKKW__1up"].values,  label="CKKW 1 up", edgecolor='Black', **kwargs, )
plt.bar(x+1, qqBkg_File_all["Theosys"]["weight_var_H4l_Shower_UEPS_Sherpa_CKKW__1down"].values,  label="CKKW 1 dn ", edgecolor='Blue', **kwargs, )

#plt.bar(x+1, VBFSBI_File["Nominal"]["Nom_hist"].values, label="VBF  SBI ", color='Orangered',**kwargs)

for i in range(0,len(qqBkg_File_all["Nominal"]["Nom_hist"].values+1)):
    xtick_names.append("B "+str(i+1))

leg = plt.legend(loc='upper right', fontsize =20, framealpha=1, frameon=True)

UpVar = (qqBkg_File_all["Theosys"]["weight_var_H4l_Shower_UEPS_Sherpa_CKKW__1up"].values)/(qqBkg_File_all["Nominal"]["Nom_hist"].values)
DnVar = (qqBkg_File_all["Theosys"]["weight_var_H4l_Shower_UEPS_Sherpa_CKKW__1down"].values)/(qqBkg_File_all["Nominal"]["Nom_hist"].values)

for i in range(0,len(qqBkg_File_all["Nominal"]["Nom_hist"].values+1)):
    y_values=[qqBkg_File_all["Nominal"]["Nom_hist"].values[i]]
    max_y = max(y_values)
    plt.text(x[i]+0.7, max_y*(1.7),str(format(UpVar[i],'0.2f')),size='small')
    plt.text(x[i]+0.7, max_y*(2.5),str(format(DnVar[i],'0.2f')),size='small')


plt.ylabel("Entries", fontsize=20)
plt.xticks(fontsize=10)
plt.yticks(fontsize=16)
plt.yscale('log')
plt.ylim(1e0,1e5)
plt.xticks(range(len(xtick_names)), xtick_names, size='small', rotation='vertical')
plt.axvline(x=4.5, linewidth=2, color='black')
plt.axvline(x=6.5, linewidth=2, color='black')
plt.axvline(x=7.5, linewidth=2, color='black')
plt.axvline(x=21.5, linewidth=2, color='black')
plt.axvline(x=26.5, linewidth=2, color='black')

t = plt.text(1, 10000,('$\mathit{ATLAS}$ Work in Progress'), fontsize=22)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
t = plt.text(1, 5000,('139 fb'+ r'$^{-1}$'), fontsize=22)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
##plt.axvline(x=17.5, linewidth=2, color='black',linestyle='dashed')
plt.savefig(OutFolder+"UpDown_CKKW_regions_dist.png",dpi=200,bbox_inches = 'tight')


xtick_names=['']
x = np.arange(len(qqBkg_File_all["Nominal"]["Nom_hist"].values))

kwargs = dict(width=0.9,linewidth=2,linestyle='solid',fill=False)
fig1 = plt.figure(figsize=(12,9))

plt.bar(x+1, qqBkg_File_all["Nominal"]["Nom_hist"].values,  label="qqZZ Nominal", edgecolor='Orangered', **kwargs, )
plt.bar(x+1, qqBkg_File_all["Theosys"]["weight_var_H4l_Shower_UEPS_Sherpa_QSF__1up"].values,  label="QSF 1 up", edgecolor='Black', **kwargs, )
plt.bar(x+1, qqBkg_File_all["Theosys"]["weight_var_H4l_Shower_UEPS_Sherpa_QSF__1down"].values,  label="QSF 1 dn ", edgecolor='Blue', **kwargs, )

#plt.bar(x+1, VBFSBI_File["Nominal"]["Nom_hist"].values, label="VBF  SBI ", color='Orangered',**kwargs)

for i in range(0,len(qqBkg_File_all["Nominal"]["Nom_hist"].values+1)):
    xtick_names.append("B "+str(i+1))


UpVar = (qqBkg_File_all["Theosys"]["weight_var_H4l_Shower_UEPS_Sherpa_QSF__1up"].values)/(qqBkg_File_all["Nominal"]["Nom_hist"].values)
DnVar = (qqBkg_File_all["Theosys"]["weight_var_H4l_Shower_UEPS_Sherpa_QSF__1down"].values)/(qqBkg_File_all["Nominal"]["Nom_hist"].values)

for i in range(0,len(qqBkg_File_all["Nominal"]["Nom_hist"].values+1)):
    y_values=[qqBkg_File_all["Nominal"]["Nom_hist"].values[i]]
    max_y = max(y_values)
    plt.text(x[i]+0.7, max_y*(1.7),str(format(UpVar[i],'0.2f')),size='small')
    plt.text(x[i]+0.7, max_y*(2.5),str(format(DnVar[i],'0.2f')),size='small')


leg = plt.legend(loc='upper right', fontsize =20, framealpha=1, frameon=True)

plt.ylabel("Entries", fontsize=20)
plt.xticks(fontsize=10)
plt.yticks(fontsize=16)
plt.yscale('log')
plt.ylim(1e0,1e5)
plt.xticks(range(len(xtick_names)), xtick_names, size='small', rotation='vertical')
plt.axvline(x=4.5, linewidth=2, color='black')
plt.axvline(x=6.5, linewidth=2, color='black')
plt.axvline(x=7.5, linewidth=2, color='black')
plt.axvline(x=21.5, linewidth=2, color='black')
plt.axvline(x=26.5, linewidth=2, color='black')

t = plt.text(1, 10000,('$\mathit{ATLAS}$ Work in Progress'), fontsize=22)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
t = plt.text(1, 5000,('139 fb'+ r'$^{-1}$'), fontsize=22)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
##plt.axvline(x=17.5, linewidth=2, color='black',linestyle='dashed')
plt.savefig(OutFolder+"UpDown_QSF_regions_dist.png",dpi=200,bbox_inches = 'tight')


xtick_names=['']
x = np.arange(len(qqBkg_File_all["Nominal"]["Nom_hist"].values))

kwargs = dict(width=0.9,linewidth=2,linestyle='solid',fill=False)
fig1 = plt.figure(figsize=(12,9))

plt.bar(x+1, qqBkg_File_all["Nominal"]["Nom_hist"].values,  label="qqZZ Nominal", edgecolor='Orangered', **kwargs, )
plt.bar(x+1, qqBkg_File_all["Theosys"]["weight_var_HOEW_QCD_syst__1up"].values,  label="HOQCD 1 up", edgecolor='Black', **kwargs, )
plt.bar(x+1, qqBkg_File_all["Theosys"]["weight_var_HOEW_QCD_syst__1down"].values,  label="HOQCD 1 dn ", edgecolor='Blue', **kwargs, )

#plt.bar(x+1, VBFSBI_File["Nominal"]["Nom_hist"].values, label="VBF  SBI ", color='Orangered',**kwargs)

for i in range(0,len(qqBkg_File_all["Nominal"]["Nom_hist"].values+1)):
    xtick_names.append("B "+str(i+1))

leg = plt.legend(loc='upper right', fontsize =20, framealpha=1, frameon=True)

plt.ylabel("Entries", fontsize=20)
plt.xticks(fontsize=10)
plt.yticks(fontsize=16)
plt.yscale('log')
plt.ylim(1e0,1e5)
plt.xticks(range(len(xtick_names)), xtick_names, size='small', rotation='vertical')
plt.axvline(x=4.5, linewidth=2, color='black')
plt.axvline(x=6.5, linewidth=2, color='black')
plt.axvline(x=7.5, linewidth=2, color='black')
plt.axvline(x=21.5, linewidth=2, color='black')
plt.axvline(x=26.5, linewidth=2, color='black')

t = plt.text(1, 10000,('$\mathit{ATLAS}$ Work in Progress'), fontsize=22)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
t = plt.text(1, 5000,('139 fb'+ r'$^{-1}$'), fontsize=22)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
##plt.axvline(x=17.5, linewidth=2, color='black',linestyle='dashed')
plt.savefig(OutFolder+"UpDown_HOQCD_regions_dist.png",dpi=200,bbox_inches = 'tight')

"""
xtick_names=['']
x = np.arange(len(qqBkg_File_all["Nominal"]["Nom_hist"].values[:7]))
kwargs = dict(width=0.95,fill=True, linewidth=3,linestyle='solid')

fig2 = plt.figure(figsize=(12,9))

plt.bar(x+1, qqBkg_File_all["Nominal"]["Nom_hist"].values,  label="qqZZ Nominal", edgecolor='Orangered', **kwargs, )
plt.bar(x+1, qqBkg_File_all["Theosys"]["weight_var_H4l_Shower_UEPS_Sherpa_QSF__1up"].values,  label="QSF 1 up", edgecolor='Black', **kwargs, )
plt.bar(x+1, qqBkg_File_all["Theosys"]["weight_var_H4l_Shower_UEPS_Sherpa_QSF__1down"].values,  label="QSF 1 dn ", edgecolor='Blue', **kwargs, )


for i in range(0,len(ggSBI_File["Nominal"]["Nom_hist"].values[:7]+1)):
    xtick_names.append("B "+str(i+1))

print(xtick_names)
leg = plt.legend(loc='upper right', fontsize =20, framealpha=1, frameon=True)

plt.ylabel("Entries", fontsize=20)
plt.xticks(fontsize=10)
plt.yticks(fontsize=16)
plt.yscale('log')
plt.ylim(1e-1,1e5)
plt.xticks(range(len(xtick_names)), xtick_names, size='small', rotation='vertical')
plt.axvline(x=4.5, linewidth=2, color='black')
plt.axvline(x=6.5, linewidth=2, color='black')
#plt.axvline(x=7.5, linewidth=2, color='black')
#plt.axvline(x=21.5, linewidth=2, color='black')
#plt.axvline(x=25.5, linewidth=2, color='black')

t = plt.text(0.5, 10000,('$\mathit{ATLAS}$ Work in Progress'), fontsize=22)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
t = plt.text(0.5, 5000,('139 fb'+ r'$^{-1}$'), fontsize=22)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
##plt.axvline(x=17.5, linewidth=2, color='black',linestyle='dashed')
plt.savefig(OutFolder+"Nom_regions_dist_stacked_CR1.png",dpi=200,bbox_inches = 'tight')
#print("Here")



xtick_names=['']
x = np.arange(len(ggSBI_File["Nominal"]["Nom_hist"].values[8:22]))
kwargs = dict(width=0.95,fill=True, linewidth=3,linestyle='solid')

fig2 = plt.figure(figsize=(12,9))
plt.bar(x+1, VBFSBI_File["Nominal"]["Nom_hist"].values[7:21],  label="VBF  SBI ", color='Orangered', **kwargs, )
plt.bar(x+1, ggSBI_File["Nominal"]["Nom_hist"].values[7:21],   label="ggF  SBI ", color='Teal',      **kwargs, bottom=VBFSBI_File["Nominal"]["Nom_hist"].values[7:21] )
plt.bar(x+1, qqBkg_File["Nominal"]["Nom_hist"].values[7:21],   label="qqZZ Bkg ", color='Maroon',    **kwargs, bottom=np.array(ggSBI_File["Nominal"]["Nom_hist"].values[7:21])+np.array(VBFSBI_File["Nominal"]["Nom_hist"].values[7:21]))

for i in range(0,len(ggSBI_File["Nominal"]["Nom_hist"].values[7:21]+1)):
    xtick_names.append("B "+str(i+1))

print(xtick_names)
leg = plt.legend(loc='upper right', fontsize =20, framealpha=1, frameon=True)

plt.ylabel("Entries", fontsize=20)
plt.xticks(fontsize=10)
plt.yticks(fontsize=16)
plt.yscale('log')
plt.ylim(1e-1,1e5)
plt.xticks(range(len(xtick_names)), xtick_names, size='small', rotation='vertical')
#plt.axvline(x=4.5, linewidth=2, color='black')
#plt.axvline(x=6.5, linewidth=2, color='black')
#plt.axvline(x=7.5, linewidth=2, color='black')
#plt.axvline(x=21.5, linewidth=2, color='black')
#plt.axvline(x=25.5, linewidth=2, color='black')

t = plt.text(0.5, 10000,('$\mathit{ATLAS}$ Work in Progress'), fontsize=22)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
t = plt.text(0.5, 5000,('139 fb'+ r'$^{-1}$'), fontsize=22)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
##plt.axvline(x=17.5, linewidth=2, color='black',linestyle='dashed')
plt.savefig(OutFolder+"Nom_regions_dist_stacked_SR1.png",dpi=200,bbox_inches = 'tight')
#print("Here")


xtick_names=['']
x = np.arange(len(ggSBI_File["Nominal"]["Nom_hist"].values[21:26]))
kwargs = dict(width=0.95,fill=True, linewidth=3,linestyle='solid')

fig2 = plt.figure(figsize=(12,9))
plt.bar(x+1, VBFSBI_File["Nominal"]["Nom_hist"].values[21:26],  label="VBF  SBI ", color='Orangered', **kwargs, )
plt.bar(x+1, ggSBI_File["Nominal"]["Nom_hist"].values[21:26],   label="ggF  SBI ", color='Teal',      **kwargs, bottom=VBFSBI_File["Nominal"]["Nom_hist"].values[21:26] )
plt.bar(x+1, qqBkg_File["Nominal"]["Nom_hist"].values[21:26],   label="qqZZ Bkg ", color='Maroon',    **kwargs, bottom=np.array(ggSBI_File["Nominal"]["Nom_hist"].values[21:26])+np.array(VBFSBI_File["Nominal"]["Nom_hist"].values[21:26]))

for i in range(0,len(ggSBI_File["Nominal"]["Nom_hist"].values[21:26]+1)):
    xtick_names.append("B "+str(i+1))

print(xtick_names)
leg = plt.legend(loc='upper right', fontsize =20, framealpha=1, frameon=True)

plt.ylabel("Entries", fontsize=20)
plt.xticks(fontsize=10)
plt.yticks(fontsize=16)
plt.yscale('log')
plt.ylim(1e-1,1e5)
plt.xticks(range(len(xtick_names)), xtick_names, size='small', rotation='vertical')
#plt.axvline(x=4.5, linewidth=2, color='black')
#plt.axvline(x=6.5, linewidth=2, color='black')
#plt.axvline(x=7.5, linewidth=2, color='black')
#plt.axvline(x=21.5, linewidth=2, color='black')
#plt.axvline(x=25.5, linewidth=2, color='black')

t = plt.text(0.5, 10000,('$\mathit{ATLAS}$ Work in Progress'), fontsize=22)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
t = plt.text(0.5, 5000,('139 fb'+ r'$^{-1}$'), fontsize=22)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
##plt.axvline(x=17.5, linewidth=2, color='black',linestyle='dashed')
plt.savefig(OutFolder+"Nom_regions_dist_stacked_SR2.png",dpi=200,bbox_inches = 'tight')


xtick_names=['']
x = np.arange(len(ggSBI_File["Nominal"]["Nom_hist"].values[26:30]))
kwargs = dict(width=0.95,fill=True, linewidth=3,linestyle='solid')

fig2 = plt.figure(figsize=(12,9))
plt.bar(x+1, VBFSBI_File["Nominal"]["Nom_hist"].values[26:30],  label="VBF  SBI ", color='Orangered', **kwargs, )
plt.bar(x+1, ggSBI_File["Nominal"]["Nom_hist"].values[26:30],   label="ggF  SBI ", color='Teal',      **kwargs, bottom=VBFSBI_File["Nominal"]["Nom_hist"].values[26:30] )
plt.bar(x+1, qqBkg_File["Nominal"]["Nom_hist"].values[26:30],   label="qqZZ Bkg ", color='Maroon',    **kwargs, bottom=np.array(ggSBI_File["Nominal"]["Nom_hist"].values[26:30])+np.array(VBFSBI_File["Nominal"]["Nom_hist"].values[26:30]))

for i in range(0,len(ggSBI_File["Nominal"]["Nom_hist"].values[26:30]+1)):
    xtick_names.append("B "+str(i+1))

print(xtick_names)
leg = plt.legend(loc='upper right', fontsize =20, framealpha=1, frameon=True)

plt.ylabel("Entries", fontsize=20)
plt.xticks(fontsize=10)
plt.yticks(fontsize=16)
plt.yscale('log')
plt.ylim(1e-1,1e5)
plt.xticks(range(len(xtick_names)), xtick_names, size='small', rotation='vertical')
#plt.axvline(x=4.5, linewidth=2, color='black')
#plt.axvline(x=6.5, linewidth=2, color='black')
#plt.axvline(x=7.5, linewidth=2, color='black')
#plt.axvline(x=21.5, linewidth=2, color='black')
#plt.axvline(x=25.5, linewidth=2, color='black')

t = plt.text(0.5, 10000,('$\mathit{ATLAS}$ Work in Progress'), fontsize=22)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
t = plt.text(0.5, 5000,('139 fb'+ r'$^{-1}$'), fontsize=22)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
##plt.axvline(x=17.5, linewidth=2, color='black',linestyle='dashed')
plt.savefig(OutFolder+"Nom_regions_dist_stacked_SR3.png",dpi=200,bbox_inches = 'tight')
"""
