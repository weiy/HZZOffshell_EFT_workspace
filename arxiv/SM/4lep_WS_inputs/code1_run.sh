outname="Final_WS_EFT_4l_v1"
date="01_Mar_2022"

pathToFiles=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v25/AntiKt4EMPFlow/
outputPath=./outputs_$date/hists_code1_$outname
outPlotsPath=./outputs_$date/Plots/

if [ ! -d outputs_${date} ]; then mkdir outputs_${date}; fi
if [ ! -d ${outputPath} ]; then mkdir ${outputPath}; fi
if [ ! -d ${outPlotsPath} ]; then mkdir ${outPlotsPath}; fi


pathToFiles=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v25/AntiKt4EMPFlow/

#ggBkg
#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.345709.Sherpa_222_NNPDF30NNLO_ggllllNoHiggs_130M4l.root  --outpath $outputPath --tree tree_incl_all --doNormExptSys True --doNormTheoSys True 
echo "--------------------------------------------------------------------------"

#ggSBI
#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.345706.Sherpa_222_NNPDF30NNLO_ggllll_130M4l.root  --outpath $outputPath --tree tree_incl_all --doNormExptSys True --doNormTheoSys True 
echo "--------------------------------------------------------------------------"

#ggSignal
#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.345712.Sherpa_222_NNPDF30NNLO_ggllllOnlyHiggs_130M4l.root  --outpath $outputPath --tree tree_incl_all --doNormExptSys True --doNormTheoSys True
echo "--------------------------------------------------------------------------"

#qqZZBkg
#python make_all_hists_code1.py --inpath $pathToFiles  --sample  mc16_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.root  --outpath $outputPath --tree tree_incl_all --doNormExptSys True --doNormTheoSys True #-c mc16e

echo "--------------------------------------------------------------------------"

#python make_all_hists_code1.py --inpath $pathToFiles  --sample  mc16_13TeV.346899.Sherpa_222_NNPDF30NNLO_llll_m4l100_300_filt100_170.root --outpath $outputPath --tree tree_incl_all --doNormExptSys True --doNormTheoSys True #-c mc16e
echo "--------------------------------------------------------------------------"

python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364252.Sherpa_222_NNPDF30NNLO_llll_m4l300.root  --outpath $outputPath --tree tree_incl_all --doNormExptSys True --doNormTheoSys True -c mc16e
echo "--------------------------------------------------------------------------"


VBFFiles=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v25/AntiKt4EMPFlow/

#VBFBkg
#python make_all_hists_code1.py --inpath $VBFFiles  --sample mc16_13TeV.500373.MGPy8EG_A14_NNPDF23LO_VBFH125_bkg_4l_m4l100.root  --outpath $outputPath --tree tree_incl_all --doNormExptSys True --doNormTheoSys True
echo "--------------------------------------------------------------------------"

#VBFSBI
#python make_all_hists_code1.py --inpath $VBFFiles  --sample mc16_13TeV.500372.MGPy8EG_A14_NNPDF23LO_VBFH125_sbi_4l_m4l130.root --outpath $outputPath --tree tree_incl_all --doNormExptSys True --doNormTheoSys True
echo "--------------------------------------------------------------------------"

#VBFSBI5
#python make_all_hists_code1.py --inpath $VBFFiles  --sample mc16_13TeV.500374.MGPy8EG_A14_NNPDF23LO_VBFH125_sbi5_4l_m4l130.root  --outpath $outputPath --tree tree_incl_all --doNormExptSys True --doNormTheoSys True
echo "--------------------------------------------------------------------------"




#VBFSBI10
#python make_all_hists_code1.py --inpath $VBFFiles  --sample mc16_13TeV.500375.MGPy8EG_A14_NNPDF23LO_VBFH125_sbi10_4l_m4l130.root  --outpath $outputPath --tree tree_incl_all --doNormExptSys True --doNormTheoSys True 
echo "--------------------------------------------------------------------------"

