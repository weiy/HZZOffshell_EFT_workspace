

####qqZZ NORM IS NOT BEING REMOVED#####

outname="Final_WS_v1"
date="0815"

inPath="./outputs_"$date"/Merged_code1_"$outname
outputPath="./outputs_"$date"/Merged_code2_"$outname

if [ ! -d ${outputPath} ]; then mkdir ${outputPath}; fi


python make_sys_hists_code2.py --inProc ggSNLOS --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc ggSNLOI --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc ggINLO --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc ggSBINLOI --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc ggBNLO --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc ggBNLOB --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc ggBNLOI --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"

python make_sys_hists_code2.py --inProc qqZZ_Part1 --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc qqZZ_Part2 --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc qqZZ_Part3 --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"

python make_sys_hists_code2.py --inProc VBFB --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc VBFSBI --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc VBFSBI5 --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"


#python make_sys_hists_code2.py --inProc VBFI --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_sys_hists_code2.py --inProc VBFS --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_sys_hists_code2.py --inProc VBFSBI10 --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_sys_hists_code2.py --inProc VBFSBIS --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_sys_hists_code2.py --inProc VBFSBI5S --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_sys_hists_code2.py --inProc VBFSBI10S --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_sys_hists_code2.py --inProc VBFSBII --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_sys_hists_code2.py --inProc VBFSBI5I --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_sys_hists_code2.py --inProc VBFSBI10I --inpath $inPath --outpath $outputPath
#echo "Code to determine high S/B bins for pruning"
#python3 plotter_code.py
