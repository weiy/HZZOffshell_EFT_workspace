outname="Final_WS_v1"
date="0815"

inPath="./outputs_"$date"/Merged_code2_"$outname

echo $inPath
outputPath=WS_inputs_${date}_${outname}_sym

if [ ! -d ${outputPath} ]; then mkdir ${outputPath}; fi

rm -rf WS_inputs_${date}_${outname}_sym
rm -rf NP_names.txt
mkdir WS_inputs_${date}_${outname}_sym

#HighSoverB=4,11,2
HSOB_Threshold=0
LSOB_Threshold=0

python make_WSInputs_code3.py --inProc ggSNLOS  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc ggSNLOI  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc ggSBINLOI  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc ggBNLOB  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc ggBNLOI  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"

python make_WSInputs_code3.py --inProc qqZZ_Part1  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc qqZZ_Part2  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc qqZZ_Part3  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"

python make_WSInputs_code3.py --inProc VBFB  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc VBFSBI  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc VBFSBI5  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"


#python make_WSInputs_code3.py --inProc VBFI  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_WSInputs_code3.py --inProc VBFS  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_WSInputs_code3.py --inProc VBFSBI10  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_WSInputs_code3.py --inProc VBFSBIS  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_WSInputs_code3.py --inProc VBFSBI5S  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_WSInputs_code3.py --inProc VBFSBI10S  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_WSInputs_code3.py --inProc VBFSBII  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_WSInputs_code3.py --inProc VBFSBI5I  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
#echo "--------------------------------------------------------------------------"
#python make_WSInputs_code3.py --inProc VBFSBI10I  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath

echo "Writing NP list"
python make_NPfile.py --outpath $outputPath
echo "Writing Config file"
python make_config_file.py --FileName config_file_4lep_${date}_${outname}.ini --inpath $PWD/$outputPath
