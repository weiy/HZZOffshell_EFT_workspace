#!/bin/bash

outname="Final_WS_v1"
date="0815"

filelist="ggBNLO_hists.root ggBNLOB_hists.root ggBNLOI_hists.root ggSNLO_hists.root ggSNLOS_hists.root ggSNLOI_hists.root ggSBINLOI_hists.root VBFB_hists.root VBFSBII_hists.root VBFSBIS_hists.root VBFSBI_hists.root VBFSBI5S_hists.root VBFSBI5I_hists.root VBFSBI5_hists.root"

outputfileName="./outputs_"$date"/Merged_code1_"$outname

rm -rf $outputfileName
mkdir $outputfileName


inputfileName="./outputs_"$date"/hists_code1_"$outname
yield_file="./outputs_"$date"/yields.txt"
outPlotsPath="./outputs_"$date"/Plots/"

if [ ! -d ${outPlotsPath} ]; then mkdir ${outPlotsPath}; fi

for proc in ${filelist}; 
do hadd $outputfileName/$proc $inputfileName/mc16*/$proc; 
done;

#Merge the 3 qqZZ files
hadd $outputfileName/qqZZ_Part1_hists.root $inputfileName/mc16*/*Part1*
hadd $outputfileName/qqZZ_Part2_hists.root $inputfileName/mc16*/*Part2*
hadd $outputfileName/qqZZ_Part3_hists.root $inputfileName/mc16*/*Part3*
hadd $outputfileName/qqZZ_all_hists.root $outputfileName/qqZZ_Part1_hists.root $outputfileName/qqZZ_Part2_hists.root $outputfileName/qqZZ_Part3_hists.root


#Merge the files to make interference
#hadd $outputfileName/ggINLO_hists.root  $outputfileName/ggSBINLOI_hists.root   $outputfileName/ggSNLOI_hists.root  $outputfileName/ggBNLOI_hists.root
#hadd $outputfileName/VBFS_hists.root  $outputfileName/VBFSBIS_hists.root   $outputfileName/VBFSBI5S_hists.root  $outputfileName/VBFSBI10S_hists.root
#hadd $outputfileName/VBFI_hists.root  $outputfileName/VBFSBII_hists.root   $outputfileName/VBFSBI5I_hists.root  $outputfileName/VBFSBI10I_hists.root

#python qqZZ_norm_overflow_3SR.py --inpath $outputfileName > norm_qqZZ.txt
#python3 plotter_code.py --inpath $outputfileName --outpath $outPlotsPath 
#python3 nice_plots.py --inpath $outputfileName --outpath $outPlotsPath

