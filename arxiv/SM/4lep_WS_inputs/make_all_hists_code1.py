from ROOT import *
import sys
sys.path.append('./Systematic_names/')
import os
import os.path
from os import path
from glob import glob
from array import array
import math
import argparse
import List_Sys
from array import array
import numpy as np
from math import sqrt
from math import log10
#import json
import pickle
from collections import OrderedDict

gROOT.SetBatch(kTRUE)
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--tree',    type=str, default='tree_incl_all', help='Name of tree')
parser.add_argument('--inpath',  type=str, default='', help='Add path to the sample directory (only) each campaign accesed separately')
parser.add_argument('--outpath', type=str, default='', help='Add path to output')
parser.add_argument('--sample',  type=str, default='', help='Add a sample you want to read')
parser.add_argument('-c','--campaign',type=str,  nargs='+', default =['mc16a','mc16d','mc16e'],  help='Campaigns needed to run over, example -c mc16a ')
#parser.add_argument('-c','--campaign',type=str,  nargs='+', default =['mc16d'],  help='Campaigns needed to run over, example -c mc16a ')
parser.add_argument('--doNormTheoSys', type=bool, default=True, help='Make Norm Theory system. hists' )
parser.add_argument('--doNormExptSys', type=bool, default=True, help='Make Norm Exptem. system. hists' )

args = parser.parse_args()

def region_of_interest(list_regions,name,Cuts,Obs,Obs_name,binning,isSR,isVariableBinning):
    """
    function defines the new regions of interest
    """
    name = name+"_"+str(Obs_name)  
    if isVariableBinning == True:
        bins = binning
        #print(binning)
    else:
        bins = np.linspace(binning[1], binning[2], num=binning[0]+1)
    list_regions.update({name:[Cuts, Obs, bins]})
    
def concat_hists(hist_list, histName):
    """
    Concatenate the historgams from all the regions to make one-long histogram
    easier when removing normalization
    """
    binsContent=[] #list to append bin contents
    binsError  =[] #list to append bin errors
    binsEntry  =0  #gets the entries in the histogram
    outhist_len = 0 #len of the concatenated histogram
    for hist in hist_list:
        outhist_len = outhist_len + hist.GetNbinsX()  #concatenates histograms to get final histogram length      
    #Define the output concatenated histogram
    outhist = TH1F(histName, histName, outhist_len, 0, outhist_len) 
    #Loop over the histograms, each one corresponding to each SR/CR region
    for hist in hist_list:
        binsEntry = binsEntry+(hist.GetEntries()) #get the entries
        for i in range(1,hist.GetNbinsX()+1): #Only the bins, not under and overflow, add overflow to last and underflow to fist bin
            if i ==  1: #add underflow to first bin, do  sum in quadrature for the errors
                binsContent.append(hist.GetBinContent(i)+hist.GetBinContent(0))
                binsError.append(sqrt((hist.GetBinError(i))**2+(hist.GetBinError(0))**2))
            elif i == hist.GetNbinsX(): #add overflow to last coloumn and sum in quadrature for error
                binsContent.append(hist.GetBinContent(i)+hist.GetBinContent(hist.GetNbinsX()+1))
                binsError.append(sqrt((hist.GetBinError(i))**2+(hist.GetBinError(hist.GetNbinsX()+1))**2))
            else: #just get the bin content for other bins
                binsContent.append(hist.GetBinContent(i))
                binsError.append(hist.GetBinError(i))

    #set the information in the concatenated histogram
    outhist.SetEntries(binsEntry)
    for i in range(1,len(binsContent)+1):
        #print( binsContent[i-1])
        outhist.SetBinContent(i, binsContent[i-1])
        outhist.SetBinError(i, binsError[i-1])
    #return the concatenated hist
    #print(sum(binsContent))
    return outhist        

def write_hist(list_regions,weight_Indx,dictHists,Output_Dir,Process,camp,myTree):
    """
    function defines the histograms to be written out
    depending on the systematics/ processes etc.
    use the concat_hists function to write out the histogram that has been concatenated in the various regions
    """

    OutFolder = args.outpath #path where hist is stored
    listHists = [] #hist of histograms to write out
    #Fill nominal histograms

    for histToFill in dictHists.keys():
        i =0
        #print(histToFill)
        hist_Rgns =[]
        for regions in list_regions.keys():           
            #print(regions)
            histName = regions+"_"+histToFill #histogram named as Region_variable
            #print "Defining...", histName
            bin_edges = np.array(list_regions[regions][2], dtype='float64') #Get the binning/edges
            #print("bin edges")
            #print(bin_edges)
            hist_Rgns.append(TH1F(histName, histName, len(bin_edges)-1, bin_edges)) #append the histograms to a list 
            
            if "abs(jet_eta[0]" in list_regions[regions][0]:
                divided_wgts = list_regions[regions][0].split("||")
                #print(divided_wgts)
                for part, part_wgts in enumerate(divided_wgts):
                    if part == 0:
                        fillHist = Form('%s>>%s'%(list_regions[regions][1],histName)) #Fill the histogram with the appropriate variable
                        #print(fillHist)
                        fillWeight = Form('%s*%s' % (dictHists[histToFill][weight_Indx],part_wgts))#use the appropriate weights
                        #print(fillWeight)
                        myTree.Draw(fillHist, fillWeight)# draw the histogram
                        #print("----------------------Integral------------------")
                        #print(hist_Rgns[i].Integral())
                    else:
                        #print("here")
                        fillHist = Form('%s>>+%s'%(list_regions[regions][1],histName)) #Fill the histogram with the appropriate variable
                        #print(fillHist)
                        fillWeight = Form('%s*%s' % (dictHists[histToFill][weight_Indx],part_wgts))#use the appropriate weights
                        #print(fillWeight)
                        myTree.Draw(fillHist, fillWeight)# draw the histogram
                        #print("----------------------Integral------------------")
                        #print(hist_Rgns[i].Integral())

            else:
                fillHist = Form('%s>>%s'%(list_regions[regions][1],histName)) #Fill the histogram with the appropriate variable
                #print(fillHist)
                fillWeight = Form('%s*%s' % (dictHists[histToFill][weight_Indx],list_regions[regions][0]))#use the appropriate weights
                #print(fillWeight)
                myTree.Draw(fillHist, fillWeight)# draw the histogram
                #print("Integral: "+str(hist_Rgns[i].Integral()))
            
            i =i+1
        #append the concatenated histogram
        listHists.append(concat_hists(hist_Rgns, histToFill))

    #print(listHists)
    OutDirCamp = os.path.join(OutFolder,camp)
    OutSample  = Process+"_hists.root"
        
    if path.isdir(OutDirCamp): #check if the directory exists 
        if path.isfile(os.path.join(OutDirCamp,OutSample)):#check if file exists
            fOut_FromTree = TFile(os.path.join(OutDirCamp,OutSample), "UPDATE")#update the root file with the histograms
            print('root file exits, updating it ...')
        else:#if file doesnt exist then write out a new one
            fOut_FromTree = TFile(os.path.join(OutDirCamp,OutSample), "CREATE")
            print('root file doesnt exit, creating it ..')
    else:#if directory doesnt exist then write out a new one
        print('making a new directory and root file ...')
        os.mkdir(OutDirCamp)
        fOut_FromTree = TFile(os.path.join(OutDirCamp,OutSample), "CREATE")

    directory = fOut_FromTree.GetDirectory(Output_Dir)
    if not (directory):
        #print ">>> created " + Output_Dir 
        directory = fOut_FromTree.mkdir(Output_Dir)
    directory.cd()
    #write out all the histograms
    for hist in listHists:
        #print "Writing...", hist
        #print(hist.Integral())
        hist.Write()
        hist.SetDirectory(0)

    fOut_FromTree.Write("",TFile.kOverwrite)
    fOut_FromTree.Close()


def main():
    """
    Main function.
    """    
    #gStyle.SetOptStat(0)
    TH1.SetDefaultSumw2()
    #reading the arguments provided
    EosDir = args.inpath
    OutDir = args.outpath
    TreeName = args.tree
    InSample = args.sample
    
    #The Sample being processed
    print "InSample ", InSample
    
    splitSample = False # Automatically set to False, changed inside the sample
    weight_scl_1 = 1 # set to 1 as default, using %2==0 and %2==1 to split the samples that need splitting 
    weight_scl_2 = 1
    
    Idenf = "" #to keep track of sample names
    Idenf_scl_1 = ""
    Idenf_scl_2 = ""

    campaigns = args.campaign #whichever campaigns need to be run
    variations = ["1up", "1down"]

    ####################
    # Input Root Files #
    ####################
    
    #Need a DSID List of all possible input samples for the analysis
    if "data" in InSample:
        Process = "data"
        isData  = True
    
    #ggF Procs
    if "345712" in InSample:
        Process = "ggSNLO"
        splitSample = True
        weight_scl    = "weight*w_ggZZkS"
        Idenf         = "" 
        weight_scl_1  = "weight*w_ggZZkS*(2)*(event%2==0)"
        Idenf_scl_1   = "S"
        weight_scl_2  = "weight*w_ggZZkS*(2)*(event%2==1)*(-1)"
        Idenf_scl_2   = "I"

    if "345706" in InSample:
        Process       = "ggSBINLOI"
        weight_scl    = "weight*w_ggZZkSBI"

    if "345709" in InSample:
        Process = "ggBNLO"
        splitSample   = True
        weight_scl    = "weight*w_ggZZkB"
        Idenf         = ""
        weight_scl_1  = "weight*w_ggZZkB*(2)*(event%2==0)"
        Idenf_scl_1   = "B"
        weight_scl_2  = "weight*w_ggZZkB*(2)*(event%2==1)*(-1)"
        Idenf_scl_2   = "I"

    #VBF Procs
    const = "1/(5-(9*sqrt(5))+(4*sqrt(10)))"
    #background is taken directly from bkg file, signal and interference are built from VBFSBI VBFSBI5 and VBFSBI19 
    #Signal, interf and background are build statistically independently

    if "500373" in InSample: 
        Process       = "VBFB"
        weight_scl    = "weight"

    if "500372" in InSample:
        Process       = "VBFSBI"
        splitSample   = True
        weight_scl    = "weight"
        Idenf         = ""
        param_S       = "(sqrt(5)-sqrt(10))*"+const
        weight_scl_1  = "weight*(2)*(event%2==0)*"+param_S
        Idenf_scl_1   = "S"
        param_I       = "(5)*"+const
        weight_scl_2  = "weight*(2)*(event%2==1)*"+param_I
        Idenf_scl_2   = "I"

    if "500374" in InSample:
        Process       = "VBFSBI5"
        splitSample   = True
        weight_scl    = "weight"
        Idenf         = ""
        param_S       = "(sqrt(10)-1)*"+const
        weight_scl_1  = "weight*(2)*(event%2==0)*"+param_S
        Idenf_scl_1   = "S"
        param_I       = "(-9)*"+const
        weight_scl_2  = "weight*(2)*(event%2==1)*"+param_I
        Idenf_scl_2   = "I"

    if "500375" in InSample:
        Process       = "VBFSBI10"
        weight_scl    = "weight"
        splitSample   = True
        weight_scl    = "weight"
        Idenf         = ""
        param_S       = "(1-sqrt(5))*"+const
        weight_scl_1  = "weight*(2)*(event%2==0)*"+param_S
        Idenf_scl_1   = "S"
        param_I       = "(4)*"+const
        weight_scl_2  = "weight*(2)*(event%2==1)*"+param_I
        Idenf_scl_2   = "I"

    #qqZZ Procs
    CR_split_num = 3 #3default
    #sample is split in 3 parts due to 3 CR and already produced in 3 parts x 3 campaigns so there are 27 histograms here
    #bkg1 bkg2 and bkg3 are the 3 minitrees, part1 part2 and part3 are the parts in which we split due to our CR definitions
    if "364250" in InSample:
        Process       = "qqBkg_1"
        splitSample   = True
        if CR_split_num == 2:
            weight_scl    = "weight*(n_jets<=1)"
            Idenf         = "_Part1"
            weight_scl_1  = "weight*(n_jets>=2)"
            Idenf_scl_1   = "_Part2"
            weight_scl_2  = "weight"
            Idenf_scl_2   = ""

        elif CR_split_num == 3:
            weight_scl    = "weight*(n_jets==0)"
            Idenf         = "_Part1"
            weight_scl_1  = "weight*(n_jets==1)"
            Idenf_scl_1   = "_Part2"
            weight_scl_2  = "weight*(n_jets>=2)"
            Idenf_scl_2   = "_Part3"

        else:
            weight_scl    = "weight"
            #mc16_13TeV.346899.Sherpa_222_NNPDF30NNLO_llll_m4l100_300_filt100_170.root
    if "346899" in InSample:
    #if "364251" in InSample:
        Process       = "qqBkg_2"
        splitSample   = True
        if CR_split_num == 2:
            weight_scl    = "weight*(n_jets<=1)"
            Idenf         = "_Part1"
            weight_scl_1  = "weight*(n_jets>=2)"
            Idenf_scl_1   = "_Part2"
            weight_scl_2  = "weight"
            Idenf_scl_2   = ""

        elif CR_split_num == 3:
            weight_scl    = "weight*(n_jets==0)"
            Idenf         = "_Part1"
            weight_scl_1  = "weight*(n_jets==1)"
            Idenf_scl_1   = "_Part2"
            weight_scl_2  = "weight*(n_jets>=2)"
            Idenf_scl_2   = "_Part3"

        else:
            weight_scl    = "weight"

    if "364252" in InSample:
        Process       = "qqBkg_3"
        splitSample   = True
        if CR_split_num == 2:
            weight_scl    = "weight*(n_jets<=1)"
            Idenf         = "_Part1"
            weight_scl_1  = "weight*(n_jets>=2)"
            Idenf_scl_1   = "_Part2"
            weight_scl_2  = "weight"
            Idenf_scl_2   = ""

        elif CR_split_num == 3:
            weight_scl    = "weight*(n_jets==0)"
            Idenf         = "_Part1"
            weight_scl_1  = "weight*(n_jets==1)"
            Idenf_scl_1   = "_Part2"
            weight_scl_2  = "weight*(n_jets>=2)"
            Idenf_scl_2   = "_Part3"
        else:
            weight_scl    = "weight"
        
    ################
    # Output Files #
    ################
    
    Dir_Nominal   = "Nominal"
    Dir_TheoSys   = "Theosys"
    Dir_ExptSys   = "Exptsys"
    
    ###########
    # Regions #
    ###########

    list_regions=OrderedDict()
    
    #Validation region
    ValRgn_name = "VR_qq_1"
    ValRgn_cuts = "((180<=m4l_fsr)&&(m4l_fsr<=220)&&(n_jets==0))" # to be changed when 3 VRs are used
    ValRgn_bins = [6,-2,0] # number of bins, minbin, max bin or Variable bin widths
    ValRgn_Obs  = "log10(tree_ggFNN.NN_MELA_incl_ggH/(0.1*tree_ggFNN.NN_MELA_incl_ggZZ + 0.9*tree_ggFNN.NN_MELA_incl_qqZZ))"
    ValRgn_Obs_name = "NN_MELA_incl"
    #ValRgn_bins = [6,0,60] # number of bins, minbin, max bin or Variable bin widths
    #ValRgn_Obs  = "pt4l_fsr"
    #ValRgn_Obs_name = "pt4l_fsr"
    isSR = False
    isVarBinning = False

    #region_of_interest(list_regions,ValRgn_name,ValRgn_cuts,ValRgn_Obs,ValRgn_Obs_name,ValRgn_bins,isSR,isVarBinning)

    ValRgn_name = "VR_qq_2"
    ValRgn_cuts = "((180<=m4l_fsr)&&(m4l_fsr<=220)&&(n_jets==1))"
    ValRgn_bins = [3,-2,0] # number of bins, minbin, max bin or Variable bin widths
    ValRgn_Obs  = "log10(tree_ggFNN.NN_MELA_incl_ggH/(0.1*tree_ggFNN.NN_MELA_incl_ggZZ + 0.9*tree_ggFNN.NN_MELA_incl_qqZZ))"
    ValRgn_Obs_name = "NN_MELA_incl"
    #ValRgn_bins = [3,0,60] # number of bins, minbin, max bin or Variable bin widths
    #ValRgn_Obs  = "pt4l_fsr"
    #ValRgn_Obs_name = "pt4l_fsr"
    isSR = False
    isVarBinning = False

    #region_of_interest(list_regions,ValRgn_name,ValRgn_cuts,ValRgn_Obs,ValRgn_Obs_name,ValRgn_bins,isSR,isVarBinning)

    ValRgn_name = "VR_qq_3"
    ValRgn_cuts = "((180<=m4l_fsr)&&(m4l_fsr<=220)&&(n_jets>=2))"
    ValRgn_bins = [1,-2,0] # number of bins, minbin, max bin or Variable bin widths
    ValRgn_Obs  = "log10(tree_ggFNN.NN_MELA_incl_ggH/(0.1*tree_ggFNN.NN_MELA_incl_ggZZ + 0.9*tree_ggFNN.NN_MELA_incl_qqZZ))"
    ValRgn_Obs_name = "NN_MELA_incl"
    #ValRgn_bins = [1,0,200] # number of bins, minbin, max bin or Variable bin widths
    #ValRgn_Obs  = "pt4l_fsr"
    #ValRgn_Obs_name = "pt4l_fsr"
    isSR = False
    isVarBinning = False

    #region_of_interest(list_regions,ValRgn_name,ValRgn_cuts,ValRgn_Obs,ValRgn_Obs_name,ValRgn_bins,isSR,isVarBinning)


    #This is fixed
    SigRgn = "(220<=m4l_fsr)&&(m4l_fsr<=2000)" 

    #Control #region 
    CtrRgn_name = "CR_qq_1"
    CtrRgn_cuts = "((180<=m4l_fsr)&&(m4l_fsr<=220)&&(n_jets==0))" # to be changed when 3 CRs are used 
    #CtrRgn_cuts = "((180<=m4l_fsr)&&(m4l_fsr<=220)&&(n_jets<=1))" # to be changed when 3 CRs are used 
    CtrRgn_bins = [4,180,220] # number of bins, minbin, max bin or Variable bin widths
    CtrRgn_Obs  = "m4l_fsr"
    CtrRgn_Obs_name = "m4l_fsr"
    isSR = False
    isVarBinning = False
    
    region_of_interest(list_regions,CtrRgn_name,CtrRgn_cuts,CtrRgn_Obs,CtrRgn_Obs_name,CtrRgn_bins,isSR,isVarBinning)

    CtrRgn_name = "CR_qq_2"
    CtrRgn_cuts = "((180<=m4l_fsr)&&(m4l_fsr<=220)&&(n_jets==1))" 
    CtrRgn_bins = [2,180,220] # number of bins, minbin, max bin or Variable bin widths
    CtrRgn_Obs  = "m4l_fsr"
    CtrRgn_Obs_name = "m4l_fsr"
    isSR = False
    isVarBinning = False
    
    # not included since working on 2 CRs
    region_of_interest(list_regions,CtrRgn_name,CtrRgn_cuts,CtrRgn_Obs,CtrRgn_Obs_name,CtrRgn_bins,isSR,isVarBinning)
    
    CtrRgn_name = "CR_qq_3"
    CtrRgn_cuts = "((180<=m4l_fsr)&&(m4l_fsr<=220)&&(n_jets>=2))" 
    CtrRgn_bins = [1,180,220] # number of bins, minbin, max bin or Variable bin widths
    CtrRgn_Obs  = "m4l_fsr"
    CtrRgn_Obs_name = "m4l_fsr"
    isSR = False
    isVarBinning = False

    region_of_interest(list_regions,CtrRgn_name,CtrRgn_cuts,CtrRgn_Obs,CtrRgn_Obs_name,CtrRgn_bins,isSR,isVarBinning)

    #Signal #Regions
    ggFRgn_name = "SR_ggF"
    ggFRgn_cuts = Form("((n_jets==0)&&%s)||((n_jets==1 && (abs(jet_eta[0])<2.2))&&%s)||((n_jets>=2 && dijet_deltaeta<4)&&%s)" % (SigRgn, SigRgn, SigRgn))
    ggFRgn_Obs  = "log10(tree_ggFNN.NN_MELA_incl_ggH/(0.1*tree_ggFNN.NN_MELA_incl_ggZZ + 0.9*tree_ggFNN.NN_MELA_incl_qqZZ))"
    ggFRgn_Obs_name = "NN_MELA_incl"
    ggFRgn_bins = [14, -1.34, 1.6]
    isSR = True
    isVarBinning = False

    region_of_interest(list_regions,ggFRgn_name,ggFRgn_cuts,ggFRgn_Obs,ggFRgn_Obs_name,ggFRgn_bins,isSR,isVarBinning)
    
    #Mixed #Region
    MxdRgn_name = "SR_Mxd"
    MxdRgn_cuts = Form("((n_jets==1 && (abs(jet_eta[0])>=2.2))&& %s)" % (SigRgn))
    #MxdRgn_bins = [3, -0.3, 1.5] # number of bins, minbin, max bin or Variable bin widths
    #MxdRgn_Obs  = "log10(tree_ggFNN.NN_MELA_incl_ggH/(0.1*tree_ggFNN.NN_MELA_incl_ggZZ + 0.9*tree_ggFNN.NN_MELA_incl_qqZZ))"
    #MxdRgn_Obs_name = "NN_MELA_incl"
    
    MxdRgn_Obs  = "MCFM_MELA_ggZZ"
    MxdRgn_Obs_name = "MCFM_MELA_ggZZ"
    MxdRgn_bins = [3, -4, 0.5] # number of bins, minbin, max bin or Variable bin widths
    isSR = True
    isVarBinning = False

    region_of_interest(list_regions,MxdRgn_name,MxdRgn_cuts,MxdRgn_Obs,MxdRgn_Obs_name,MxdRgn_bins,isSR,isVarBinning)

    #VBF #Region
    VBFRgn_name = "SR_VBF"
    VBFRgn_cuts = Form("((n_jets>=2 && dijet_deltaeta>=4)&&%s)" % (SigRgn))
    VBFRgn_Obs  = "log10(tree_VBFNN.NN_MELA_VBF_sig/(0.1*tree_VBFNN.NN_MELA_VBF_bkg + 0.9*tree_VBFNN.NN_MELA_VBF_qqZZ))"
    VBFRgn_bins = [4, -1.5, 1.5] # number of bins, minbin, max bin or Variable bin widths
    VBFRgn_Obs_name  = "NN_MELA_VBF_disc"
    isSR = True
    isVarBinning = False

    region_of_interest(list_regions,VBFRgn_name,VBFRgn_cuts,VBFRgn_Obs,VBFRgn_Obs_name,VBFRgn_bins,isSR,isVarBinning)

    file_output = open("regions_of_interest.txt",'w')
    pickle.dump(list_regions,file_output)
    file_output.close()

    ####################################
    # Defining dict for the Histograms #
    ####################################

    print(Process)

    dictTheoryhists={}
    for systematics in List_Sys.sys_to_process[Process]:
       #print(systematics)
        for sys in systematics.keys():
            for var_names in systematics[sys]:
                if "PDF" in var_names:
                    dictTheoryhists.update({var_names:[Form('(%s)*(%s)'%(weight_scl, var_names)),Form('(%s)*(%s)'%(weight_scl_1,var_names)),Form('(%s)*(%s)'%(weight_scl_2,var_names))],})
                else:
                    for var in variations:
                        sys_vars = var_names+'__'+var
                        dictTheoryhists.update({sys_vars:[Form('(%s)*(%s)'%(weight_scl, sys_vars)),Form('(%s)*(%s)'%(weight_scl_1,sys_vars)),Form('(%s)*(%s)'%(weight_scl_2,sys_vars))],})
                
    dictNominalhists={}
    dictNominalhists.update({"Nom_hist":[Form('(%s)'%(weight_scl)),Form('(%s)'%(weight_scl_1)),Form('(%s)'%(weight_scl_2))]})
    
    dictNormExphists ={}#Get Norm Exp sys names
    for Exp_norm_sys in List_Sys.Experimental_sys_Norm['Exp_Norm_sys']:
       for var in variations:
           sys_vars = Exp_norm_sys+'__'+var
           dictNormExphists.update({sys_vars:[Form('(%s)*(%s)'%(weight_scl,sys_vars)),Form('(%s)*(%s)'%(weight_scl_1,sys_vars)),Form('(%s)*(%s)'%(weight_scl_2,sys_vars))]})
                
    #############################################
    # Accessing file and Writing the histograms #
    #############################################

    #ggF_Friend="/eos/user/r/rcoelhol/ggFNN/"
    #VBF_Friend="/eos/user/r/rcoelhol/VBFNN/"
    ggF_Friend="/afs/cern.ch/user/s/skrishna/eos/ggFNN"
    VBF_Friend="/afs/cern.ch/user/s/skrishna/eos/VBFNN"
    '''
    for camp in campaigns:
        print("doing Campaign " +str(camp)+"...")
        for sys in List_Sys.Systematics_dict['sys']:               
            #print(sys)
            #print("Filling experimental systematics")
            if sys == 'NormSystematic':                
                myFriend_ggF = TFile(os.path.join(ggF_Friend,camp,'Systematics',sys,InSample))
                myFriend_VBF = TFile(os.path.join(VBF_Friend,camp,'Systematics',sys,InSample))
        
                myFile = TFile(os.path.join(EosDir,camp,'Systematics',sys,InSample))
                if not myFile:
                    print(InSample+" Does not exist...")
                    continue
                #print(myFile)
                myTree = myFile.Get(TreeName)
                
                myTree.AddFriend("tree_ggFNN = tree_incl_all",myFriend_ggF)
                myTree.AddFriend("tree_VBFNN = tree_incl_all",myFriend_VBF)
                total = myTree.GetEntries()
                
                #Save the histograms
                #weight_Indx,dictExphists,Output_File,Output_Dir,Process,camp,myTree
                if splitSample==True:
                    print("splitting sample into parts...")
                    weight_Indx=1
                    print "Filling histogram part 1"
                    write_hist(list_regions,weight_Indx,dictNominalhists,Dir_Nominal,Process+Idenf_scl_1,camp,myTree)
                    if args.doNormTheoSys == True:
                        write_hist(list_regions,weight_Indx,dictTheoryhists, Dir_TheoSys,Process+Idenf_scl_1,camp,myTree)
                    if args.doNormExptSys == True:
                        write_hist(list_regions,weight_Indx,dictNormExphists,Dir_ExptSys,Process+Idenf_scl_1,camp,myTree)
                
                    weight_Indx=2
                    print "Filling histogram part 2"
                    write_hist(list_regions,weight_Indx,dictNominalhists,Dir_Nominal,Process+Idenf_scl_2,camp,myTree)
                    if args.doNormTheoSys == True:
                        write_hist(list_regions,weight_Indx,dictTheoryhists, Dir_TheoSys,Process+Idenf_scl_2,camp,myTree) 
                    if args.doNormExptSys == True:
                        write_hist(list_regions,weight_Indx,dictNormExphists,Dir_ExptSys,Process+Idenf_scl_2,camp,myTree)                       

                weight_Indx=0
                if "qqBkg" in Process:
                    print "Filling nominal histogram part 3"
                else:
                    print "Filling nominal histograms..."
                write_hist(list_regions,weight_Indx,dictNominalhists,Dir_Nominal,Process+Idenf,camp,myTree)
                if args.doNormTheoSys == True:
                    write_hist(list_regions,weight_Indx,dictTheoryhists, Dir_TheoSys,Process+Idenf,camp,myTree)
                if args.doNormExptSys == True:
                    write_hist(list_regions,weight_Indx,dictNormExphists,Dir_ExptSys,Process+Idenf,camp,myTree)
                print("Now filling other systematics...")

            else:
                #print("Doing "+sys+" Systematic....")
                if args.doNormExptSys == True:
                    if "ResoP" in sys:
                        dictExphists={}
                        dictExphists.update({sys:[Form('(%s)'%(weight_scl)),Form('(%s)'%(weight_scl_1)),Form('(%s)'%(weight_scl_2))]})
                        myFriend_ggF = TFile(os.path.join(ggF_Friend,camp,'Systematics',sys,InSample))
                        myFriend_VBF = TFile(os.path.join(VBF_Friend,camp,'Systematics',sys,InSample))
                        myFile = TFile(os.path.join(EosDir,camp,'Systematics',sys,InSample))
                        if not myFile:
                            print(InSample+" Does not exist...")
                            continue
                        myTree = myFile.Get(TreeName)
                        total = myTree.GetEntries()
                        myTree.AddFriend("tree_ggFNN = tree_incl_all",myFriend_ggF)
                        myTree.AddFriend("tree_VBFNN = tree_incl_all",myFriend_VBF)
                        #Save the histograms
                        if splitSample==True:
                            #print("splitting sample into two parts...")
                            weight_Indx=1
                            #print "Filling Experimental Sys: "+sys+  "Histogram Part 1"
                            write_hist(list_regions,weight_Indx,dictExphists,Dir_ExptSys,Process+Idenf_scl_1,camp,myTree)
                            weight_Indx=2
                            #print "Filling Experimental Sys: "+sys+  " Histogram Part 2"
                            write_hist(list_regions,weight_Indx,dictExphists,Dir_ExptSys,Process+Idenf_scl_2,camp,myTree)                        
                            
                        weight_Indx=0
                        #print "Filling Experimental Sys: "+sys+  " histograms..."
                        write_hist(list_regions,weight_Indx,dictExphists,Dir_ExptSys,Process+Idenf,camp,myTree)

                    else:
                        for var in variations:
                            dictExphists={}
                            Expt_sys = sys+'__'+var
                            dictExphists.update({Expt_sys:[Form('(%s)'%(weight_scl)),Form('(%s)'%(weight_scl_1)),Form('(%s)'%(weight_scl_2))]})
                            myFile = TFile(os.path.join(EosDir,camp,'Systematics',Expt_sys ,InSample))
                            myFriend_ggF = TFile(os.path.join(ggF_Friend,camp,'Systematics',Expt_sys,InSample))
                            myFriend_VBF = TFile(os.path.join(VBF_Friend,camp,'Systematics',Expt_sys,InSample))
                            #print(myFile)
                            if not myFile:
                                print(InSample+" Does not exist...")
                                continue
                            myTree = myFile.Get(TreeName)
                            myTree.AddFriend("tree_ggFNN = tree_incl_all",myFriend_ggF)
                            myTree.AddFriend("tree_VBFNN = tree_incl_all",myFriend_VBF)
                            total = myTree.GetEntries()
                            #print(total)                            
                            #Save the histograms
                            if splitSample==True:
                                #print("splitting sample into two parts...")
                                weight_Indx=1
                                #print "Filling Experimental Sys: "+Expt_sys +  " Histogram Part 1"
                                write_hist(list_regions,weight_Indx,dictExphists,Dir_ExptSys,Process+Idenf_scl_1,camp,myTree)
                                weight_Indx=2
                                #print "Filling Experimental Sys: "+Expt_sys +  " Histogram Part 2"
                                write_hist(list_regions,weight_Indx,dictExphists,Dir_ExptSys,Process+Idenf_scl_2,camp,myTree)
                            weight_Indx=0
                            #print "Filling Experimental Sys: "+Expt_sys +  " histograms..."
                            write_hist(list_regions,weight_Indx,dictExphists,Dir_ExptSys,Process+Idenf,camp,myTree)

       '''         
if '__main__' in __name__:
    main()

