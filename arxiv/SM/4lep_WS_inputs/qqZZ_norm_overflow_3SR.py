from ROOT import *
import sys
sys.path.append('./Systematic_names/')
import os
import os.path
from os import path
from glob import glob
from array import array
import math
import argparse
import List_Sys
from array import array
import numpy as np
import pickle
from numpy import linalg as LA

gROOT.SetBatch(kTRUE)
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--inpath',  type=str, default='./Merged_hists_code1_binnedCR', help='Add path to merged histograms')
parser.add_argument('--inProc',  type=str, default='qqZZ_all', help='Add the process name you want')
parser.add_argument('-c','--OtherFiles', type=str,nargs='+', default =['qqZZ_Part1','qqZZ_Part2','qqZZ_Part3'],  help='This is to make sure that the normalization on qqZZ is removed properly with all parts considered ')

#Reading in the arguments
args = parser.parse_args()
HistPath = args.inpath
ProcName = args.inProc
otherFiles = args.OtherFiles

#Directories in the root files
variations = ["1up", "1down"]
List_dirs=["Nominal", "Theosys", "Exptsys"]

#Getting the list of regions with binning/ observables - output of code 1
file_regions = open('regions_of_interest.txt','r')
regions_list = pickle.load(file_regions)
file_regions.close()

def update_hist(Hist, HistFile):
    Hist.Fill(1,0)
    HistFile.Write("",TFile.kOverwrite)
    #HistFile.Close()

def update_histList(listHists, HistFile , gDirectory):
    for hist in listHists:
        #print "Writing...", hist
        #print(hist.Integral())
        hist.SetDirectory(gDirectory)
        hist.Write()
        #hist.Fill(1,0)
    HistFile.Write("",TFile.kOverwrite)
    #HistFile.Close()

#def add_overflow(histName,gDir_tot,gDir_part1,HistFile_other1,gDir_part2,HistFile_other2,isNom):
def add_overflow(histName,gDir_tot,gDir_part1,HistFile_other1,gDir_part2,HistFile_other2,gDir_part3,HistFile_other3,isNom):
    print("Doing: "+ histName)
    TotHist = gDir_tot.Get(histName)
    if TotHist:
        Int_TotHist = TotHist.Integral()
        print("Total Histogram Integral: "+str(Int_TotHist))
        Part1Hist = gDir_part1.Get(histName)
        Part2Hist = gDir_part2.Get(histName)
        Part3Hist = gDir_part3.Get(histName)

        print("Part 1 Integral: "+str(Part1Hist.Integral()))
        Part1Hist.SetBinContent(Part1Hist.GetNbinsX()+1, Part2Hist.Integral()+Part3Hist.Integral())
        print("Now set at: "+str(Part1Hist.Integral(0,Part1Hist.GetNbinsX()+1)))

        print("Part 2 Integral: "+str(Part2Hist.Integral()))
        Part2Hist.SetBinContent(Part2Hist.GetNbinsX()+1, Part1Hist.Integral()+Part3Hist.Integral())
        print("Now set at: "+str(Part2Hist.Integral(0,Part2Hist.GetNbinsX()+1)))
        
        print("Part 3 Integral: "+str(Part3Hist.Integral()))
        Part3Hist.SetBinContent(Part3Hist.GetNbinsX()+1, Part1Hist.Integral()+Part2Hist.Integral())
        print("Now set at: "+str(Part3Hist.Integral(0,Part3Hist.GetNbinsX()+1)))
        
        update_hist(Part1Hist, HistFile_other1)
        update_hist(Part2Hist, HistFile_other2)
        update_hist(Part3Hist, HistFile_other3)
        
    else:
        print(histName+" does not exist.")            
    if isNom==True:
        return TotHist, Part1Hist, Part2Hist, Part3Hist
    else:
        pass

#PCA for PDF Sys
def doPCA_PDF_sys(List_PDF_vars,gDir_tot,Nomhist,Nomhist_part1,part):    
    pdf_mat =[]
    pdf_hists =[]
    #for pdf_vars in List_Sys.Sherpa_samples_PDF_sys_dict['PDF_vars']:
    for pdf_vars in List_PDF_vars:
        #print(pdf_vars)
        hist_pdf = gDir_tot.Get(pdf_vars)
        bin_vals = []
        for i in range(1, hist_pdf.GetNbinsX()+1):
            bin_vals.append(hist_pdf.GetBinContent(i))
        pdf_mat.append(bin_vals)
    print(np.shape(pdf_mat))
    Transpose_pdf_mat = np.array(pdf_mat).T
    print(np.shape(Transpose_pdf_mat))
    cov_matrix = (np.cov(Transpose_pdf_mat))
    print(cov_matrix)
    lambdas, vectors = LA.eig(cov_matrix)
    index_max = np.argmax(lambdas)
    print("lambdas")
    print(lambdas)
    print("index max")
    print(index_max)
    print("lambdas[index_max]")
    print(lambdas[index_max+1])
    print("vectors[index_max]")
    print(vectors[index_max+1])
    
    bin_edges  = np.array(np.linspace(0, len(vectors[index_max])+1,len(vectors[index_max])+1), dtype='float64')
    print(bin_edges)
    histName = "PDF_var_"+part

    Pdfhist_parts_up = TH1F(histName+"_1up",   histName+"_up",   len(bin_edges)-1, bin_edges)
    Pdfhist_parts_dn = TH1F(histName+"_1down", histName+"_down", len(bin_edges)-1, bin_edges)
    
    bins = Nomhist.GetNbinsX()
    #print(Nomhist.GetXbins())

    for i in range(1,bins+1):    
        upVar_parts = Nomhist_part1.GetBinContent(i)+ ((vectors[index_max+1][i-1])*np.sqrt(lambdas[index_max+1]))
        dnVar_parts = Nomhist_part1.GetBinContent(i)- ((vectors[index_max+1][i-1])*np.sqrt(lambdas[index_max+1]))
        if Nomhist_part1.GetBinContent(i)>0:
            Pdfhist_parts_up.SetBinContent(i, upVar_parts)
            Pdfhist_parts_dn.SetBinContent(i, dnVar_parts)
        else:
            Pdfhist_parts_up.SetBinContent(i, 0)
            Pdfhist_parts_dn.SetBinContent(i, 0)
        print("bin: ", i)
        if Nomhist_part1.GetBinContent(i)>0:
            
            print("Nominal: ", Nomhist_part1.GetBinContent(i))
            print("Var    : ", (vectors[index_max][i-1])*np.sqrt(lambdas[index_max+1]))
            print("Vector : ", vectors[index_max][i-1])
            print("upVar  : ", upVar_parts)
            print("upVar diff: ", (upVar_parts- Nomhist_part1.GetBinContent(i))/Nomhist_part1.GetBinContent(i))
            print("dnVar: ", dnVar_parts)
            print("dnVar diff: ", (dnVar_parts-Nomhist_part1.GetBinContent(i))/Nomhist_part1.GetBinContent(i))
        else:
            print("Hist Nom = 0")
        
    pdf_hists.append(Pdfhist_parts_up)
    pdf_hists.append(Pdfhist_parts_dn)
    return pdf_hists
            
def main():
    list_systematics=[]
    if not path.isfile(os.path.join(HistPath,ProcName+"_hists.root")):
        print("File "+ProcName+"_hists.root does not exist")
    else:
        HistFile_tot = TFile(os.path.join(HistPath,ProcName+"_hists.root"),"UPDATE")
        print("doing "+ProcName+" Process")
        #Reading the parts
        HistFile_other1 = TFile(os.path.join(HistPath,otherFiles[0]+"_hists.root"),"UPDATE")                        
        HistFile_other2 = TFile(os.path.join(HistPath,otherFiles[1]+"_hists.root"),"UPDATE")                        
        HistFile_other3 = TFile(os.path.join(HistPath,otherFiles[2]+"_hists.root"),"UPDATE")                        
        
        for dirs in List_dirs:
            Nom_hist_files  = []
            Theo_hist_files = []
            Expt_hist_files = []

            directory = HistFile_tot.GetDirectory(dirs)
            if not (directory):
                print(dirs+" does not exist.")
                continue
            HistFile_tot.cd(dirs)
            gDir_tot = gDirectory.GetDirectory(gDirectory.GetPath())
            HistFile_other1.cd(dirs)
            gDir_part1 = gDirectory.GetDirectory(gDirectory.GetPath())
            HistFile_other2.cd(dirs)
            gDir_part2 = gDirectory.GetDirectory(gDirectory.GetPath())
            HistFile_other3.cd(dirs)
            gDir_part3 = gDirectory.GetDirectory(gDirectory.GetPath())
            
            
            if "Nom" in dirs:
                histName="Nom_hist"
                isNom = True
                TotNomHist,Part1NomHist,Part2NomHist,Part3NomHist = add_overflow(histName,gDir_tot,gDir_part1,HistFile_other1,gDir_part2,HistFile_other2,gDir_part3,HistFile_other3,isNom)

            if "Theo" in dirs:
                PDF_PCA_hists_1  = doPCA_PDF_sys(List_Sys.Sherpa_samples_PDF_sys_dict['PDF_vars'],gDir_tot,TotNomHist,Part1NomHist,"Part1")#,Part2NomHist)
                PDF_PCA_hists_2  = doPCA_PDF_sys(List_Sys.Sherpa_samples_PDF_sys_dict['PDF_vars'],gDir_tot,TotNomHist,Part2NomHist,"Part2")
                PDF_PCA_hists_3  = doPCA_PDF_sys(List_Sys.Sherpa_samples_PDF_sys_dict['PDF_vars'],gDir_tot,TotNomHist,Part3NomHist,"Part3")

                PDF_PCA_part1_up = PDF_PCA_hists_1[0]
                PDF_PCA_part1_dn = PDF_PCA_hists_1[1]
                PDF_PCA_part2_up = PDF_PCA_hists_2[0] 
                PDF_PCA_part2_dn = PDF_PCA_hists_2[1]
                PDF_PCA_part3_up = PDF_PCA_hists_3[0] 
                PDF_PCA_part3_dn = PDF_PCA_hists_3[1]

                PDF_PCA_part1_up.SetBinContent(PDF_PCA_part1_up.GetNbinsX()+1, PDF_PCA_part2_up.Integral()+PDF_PCA_part3_up.Integral())
                print("Part1 : PCA PDF upVar Integral: "+str(PDF_PCA_part1_up.Integral()))
                print("Part1 : PCA PDF upVar Integral with overflow: "+str(PDF_PCA_part1_up.Integral(0, PDF_PCA_part1_up.GetNbinsX()+1)))

                PDF_PCA_part1_dn.SetBinContent(PDF_PCA_part1_dn.GetNbinsX()+1, PDF_PCA_part2_dn.Integral()+PDF_PCA_part3_dn.Integral())
                print("Part1 : PCA PDF dnVar Integral: "+str(PDF_PCA_part1_dn.Integral()))
                print("Part1 : PCA PDF dnVar Integral with overflow: "+str(PDF_PCA_part1_dn.Integral(0, PDF_PCA_part1_dn.GetNbinsX()+1)))                

                PDF_PCA_part2_up.SetBinContent(PDF_PCA_part2_up.GetNbinsX()+1, PDF_PCA_part1_up.Integral()+PDF_PCA_part3_up.Integral())
                print("Part2 : PCA PDF upVar Integral: "+str(PDF_PCA_part2_up.Integral()))
                print("Part2 : PCA PDF upVar Integral with overflow: "+str(PDF_PCA_part2_up.Integral(0, PDF_PCA_part2_up.GetNbinsX()+1)))

                PDF_PCA_part2_dn.SetBinContent(PDF_PCA_part2_dn.GetNbinsX()+1, PDF_PCA_part1_dn.Integral()+PDF_PCA_part3_dn.Integral())
                print("Part2 : PCA PDF upVar Integral: "+str(PDF_PCA_part2_dn.Integral()))
                print("Part2 : PCA PDF upVar Integral with overflow: "+str(PDF_PCA_part2_dn.Integral(0, PDF_PCA_part2_dn.GetNbinsX()+1)))

                PDF_PCA_part3_up.SetBinContent(PDF_PCA_part3_up.GetNbinsX()+1, PDF_PCA_part1_up.Integral()+PDF_PCA_part2_up.Integral())
                print("Part3 : PCA PDF upVar Integral: "+str(PDF_PCA_part3_up.Integral()))
                print("Part3 : PCA PDF upVar Integral with overflow: "+str(PDF_PCA_part3_up.Integral(0, PDF_PCA_part3_up.GetNbinsX()+1)))

                PDF_PCA_part3_dn.SetBinContent(PDF_PCA_part3_dn.GetNbinsX()+1, PDF_PCA_part1_dn.Integral()+PDF_PCA_part2_dn.Integral())
                print("Part3 : PCA PDF upVar Integral: "+str(PDF_PCA_part3_dn.Integral()))
                print("Part3 : PCA PDF upVar Integral with overflow: "+str(PDF_PCA_part3_dn.Integral(0, PDF_PCA_part3_up.GetNbinsX()+1)))
                
                update_histList([PDF_PCA_part1_up,PDF_PCA_part1_dn], HistFile_other1, gDir_part1)
                update_histList([PDF_PCA_part2_up,PDF_PCA_part2_dn], HistFile_other2, gDir_part2)
                update_histList([PDF_PCA_part3_up,PDF_PCA_part3_dn], HistFile_other3, gDir_part3)

                for systematics in List_Sys.sys_to_process[ProcName]:
                    for listsys in systematics.keys():
                        if not "PDF" in listsys:
                            if "qqZZ_scale_fac" in listsys:
                                for qcd_ScFac in systematics[listsys]:
                                    add_overflow(qcd_ScFac,gDir_tot,gDir_part1,HistFile_other1,gDir_part2,HistFile_other2,gDir_part3,HistFile_other3,False)
                            else:
                                for sys in systematics[listsys]:
                                    for var in variations:
                                        sys_vars = sys+'__'+var
                                        add_overflow(sys_vars,gDir_tot,gDir_part1,HistFile_other1,gDir_part2,HistFile_other2,gDir_part3,HistFile_other3,False)

            if "Expt" in dirs:
                for Exp_norm_sys in List_Sys.Experimental_sys_Norm['Exp_Norm_sys']:
                    for var in variations:
                        sys_vars = Exp_norm_sys+'__'+var
                        add_overflow(sys_vars,gDir_tot,gDir_part1,HistFile_other1,gDir_part2,HistFile_other2,gDir_part3,HistFile_other3,False)
                for systematics in List_Sys.Systematics_dict['sys']:
                    if systematics != 'NormSystematic':
                        #print(systematics)
                        if "ResoP" in systematics:
                            add_overflow(systematics,gDir_tot,gDir_part1,HistFile_other1,gDir_part2,HistFile_other2,gDir_part3,HistFile_other3,False)

                        else:
                            for var in variations:
                                sys = systematics+'__'+var
                                add_overflow(sys,gDir_tot,gDir_part1,HistFile_other1,gDir_part2,HistFile_other2,gDir_part3,HistFile_other3,False)
                                        

if '__main__' in __name__:
    main()
