from ROOT import *
import sys
sys.path.append('./Systematic_names/')
import os
import os.path
from os import path
from glob import glob
from array import array
import math
import argparse
import List_Sys
from array import array
import numpy as np
import pickle
from numpy import linalg as LA

gROOT.SetBatch(kTRUE)
gStyle.SetOptStat(0)


parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--inpath',  type=str, default='./Merged_hists_code1_finBin', help='Add path to merged histograms')

#Reading in the arguments
args = parser.parse_args()
HistPath = args.inpath

#Getting the list of regions with binning/ observables - output of code 1
#file_regions = open('regions_of_interest.txt','r')
#regions_list = pickle.load(file_regions)
#file_regions.close()


def calc_yield(mu, VBFS, VBFI, VBFB):
    return  mu*VBFS + math.sqrt(mu)*VBFI + VBFB

def calc_yield_ggF(mu, ggS1, ggS2, ggB1, ggB2, ggSBI):
    return mu*ggS1 + (math.sqrt(mu)*(ggSBI + ggS2 + ggB2)) + ggB1

def calc_yield_ggF_unsplit(mu, ggS, ggB, ggSBI):
    return mu*ggS + (math.sqrt(mu)*(ggSBI - ggS - ggB)) + ggB

def main():

    HistFile_VBFB = TFile(os.path.join(HistPath,"VBFB_hists.root"))
    HistFile_VBFB.cd("Nominal")
    VBFB_Nom = gDirectory.Get("Nom_hist")

    HistFile_VBFSBIS = TFile(os.path.join(HistPath,"VBFSBIS_hists.root"))
    HistFile_VBFSBIS.cd("Nominal")
    VBFSBIS_Nom = gDirectory.Get("Nom_hist")

    HistFile_VBFSBI5S = TFile(os.path.join(HistPath,"VBFSBI5S_hists.root"))
    HistFile_VBFSBI5S.cd("Nominal")
    VBFSBI5S_Nom = gDirectory.Get("Nom_hist")

    HistFile_VBFSBI10S = TFile(os.path.join(HistPath,"VBFSBI10S_hists.root"))
    HistFile_VBFSBI10S.cd("Nominal")
    VBFSBI10S_Nom = gDirectory.Get("Nom_hist")

    HistFile_VBFSBII = TFile(os.path.join(HistPath,"VBFSBII_hists.root"))
    HistFile_VBFSBII.cd("Nominal")
    VBFSBII_Nom = gDirectory.Get("Nom_hist")

    HistFile_VBFSBI5I = TFile(os.path.join(HistPath,"VBFSBI5I_hists.root"))
    HistFile_VBFSBI5I.cd("Nominal")
    VBFSBI5I_Nom = gDirectory.Get("Nom_hist")

    HistFile_VBFSBI10I = TFile(os.path.join(HistPath,"VBFSBI10I_hists.root"))
    HistFile_VBFSBI10I.cd("Nominal")
    VBFSBI10I_Nom = gDirectory.Get("Nom_hist")
    
    negative_bins =[]
    negative_mu = []

    print("Doing VBF ....")
    for i in range(1, VBFB_Nom.GetNbinsX()):
        VBFS = VBFSBIS_Nom.GetBinContent(i)+VBFSBI5S_Nom.GetBinContent(i)+VBFSBI10S_Nom.GetBinContent(i)
        VBFI = VBFSBII_Nom.GetBinContent(i)+VBFSBI5I_Nom.GetBinContent(i)+VBFSBI10I_Nom.GetBinContent(i)
        VBFB = VBFB_Nom.GetBinContent(i)
        
        for mu in np.linspace(0,1,100):
            yield_calc = calc_yield(mu, VBFS, VBFI, VBFB)
            if yield_calc<0:
                negative_bins.append(i)
                negative_mu.append(mu)                
                print("Negative yield in bin "+str(i)+ " for mu = %.2f  : %.2f" %(mu, yield_calc))
            #else:
                #print("yield is positive in bin  "+ str(i)+" for mu = "+str(mu)+" :"+str(yield_calc))
    
    print("Bins with negative yields:  "+str(set(negative_bins)))

    HistFile_ggS1 = TFile(os.path.join(HistPath,"ggSNLOS_hists.root"))
    HistFile_ggS1.cd("Nominal")
    ggS1_Nom = gDirectory.Get("Nom_hist")

    HistFile_ggS2 = TFile(os.path.join(HistPath,"ggSNLOI_hists.root"))
    HistFile_ggS2.cd("Nominal")
    ggS2_Nom = gDirectory.Get("Nom_hist")

    HistFile_ggB1 = TFile(os.path.join(HistPath,"ggBNLOB_hists.root"))
    HistFile_ggB1.cd("Nominal")
    ggB1_Nom = gDirectory.Get("Nom_hist")

    HistFile_ggB2 = TFile(os.path.join(HistPath,"ggBNLOI_hists.root"))
    HistFile_ggB2.cd("Nominal")
    ggB2_Nom = gDirectory.Get("Nom_hist")

    HistFile_ggSBI = TFile(os.path.join(HistPath,"ggSBINLOI_hists.root"))
    HistFile_ggSBI.cd("Nominal")
    ggSBI_Nom = gDirectory.Get("Nom_hist")

    HistFile_ggS = TFile(os.path.join(HistPath,"ggSNLO_hists.root"))
    HistFile_ggS.cd("Nominal")
    ggS_Nom = gDirectory.Get("Nom_hist")

    HistFile_ggB = TFile(os.path.join(HistPath,"ggBNLO_hists.root"))
    HistFile_ggB.cd("Nominal")
    ggB_Nom = gDirectory.Get("Nom_hist")


    print("Doing ggF ....")    
    negative_bins =[]
    negative_mu = []

    for i in range(1, VBFB_Nom.GetNbinsX()):
        ggS1  = ggS1_Nom.GetBinContent(i)
        ggS2  = ggS2_Nom.GetBinContent(i)
        ggB1  = ggB1_Nom.GetBinContent(i)
        ggB2  = ggB2_Nom.GetBinContent(i)
        ggSBI = ggSBI_Nom.GetBinContent(i)
        
        ggS   = ggS_Nom.GetBinContent(i)
        ggB   = ggB_Nom.GetBinContent(i)
        
        for mu in np.linspace(0,1,100):
            yield_calc = calc_yield_ggF(mu, ggS1, ggS2, ggB1, ggB2, ggSBI)
            yield_calc_unsplit = calc_yield_ggF_unsplit(mu, ggS, ggB, ggSBI)
            if yield_calc<0.3:
                negative_bins.append(i)
                negative_mu.append(mu)
                print("Low yield in bin "+str(i)+ " for mu = %.2f  : %.2f" %(mu, yield_calc))
                print("Individual yields in bin  "+str(i)+ " for mu = %.2f  :" %(mu))
                print("Split Sample : ")
                print("Signal1: %.2f"%(ggS1))
                print("Signal2: %.2f"%(ggS2))
                print("Bkg1   : %.2f"%(ggB1))
                print("Bkg2   : %.2f"%(ggB2))
                print("SBI    : %.2f"%(ggSBI))
                
                print("Un-split Sample :")
                print("Signal: %.2f"%(ggS))
                print("Bkg   : %.2f"%(ggB))
                print("Yield : %.2f"%(yield_calc_unsplit))
            #else:
                #print("split sample yield is positive in bin  "+ str(i)+" for mu = "+str(mu)+" :"+str(yield_calc))
                #print("un-split sample yield is positive in bin  "+ str(i)+" for mu = "+str(mu)+" :"+str(yield_calc_unsplit))

                #print("Split Sample : ")
                #print("Signal1: %.2f"%(ggS1))
                #print("Signal2: %.2f"%(ggS2))
                #print("Bkg1   : %.2f"%(ggB1))
                #print("Bkg2   : %.2f"%(ggB2))
                #print("SBI    : %.2f"%(ggSBI))
                
                #print("Un-split Sample :")
                #print("Signal: %.2f"%(ggS))
                #print("Bkg   : %.2f"%(ggB))
                #print("Yield : %.2f"%(yield_calc_unsplit))
                

                
                
    print("Bins with negative yields:  "+str(set(negative_bins)))


if '__main__' in __name__:
    main()
