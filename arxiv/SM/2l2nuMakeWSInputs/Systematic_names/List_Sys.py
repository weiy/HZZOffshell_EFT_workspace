import sys
import os
from glob import glob

qqZZ_scale_factors_dict = {'qqZZ_scale_fac':['weight_var_th_MUR0p5_MUF0p5_PDF261000',
                                             'weight_var_th_MUR0p5_MUF1_PDF261000',
                                             'weight_var_th_MUR1_MUF0p5_PDF261000',
                                             'weight_var_th_MUR1_MUF1_PDF261000',
                                             'weight_var_th_MUR1_MUF2_PDF261000',
                                             'weight_var_th_MUR2_MUF1_PDF261000',
                                             'weight_var_th_MUR2_MUF2_PDF261000']}

qqZZ_EW_2l2v = {'qqZZ_EW_2l2v_corr':['weight_qqZZNLO_EW']}
qqZZ_EW_4lep = {'qqZZ_EW_4lep_corr':['weight_EWNLO']}

ggF_KfacSigNLO_sys_dict = {'NLO_QCD_Var':['weight_QCD_ggZZkS']}

ggF_KfacSBINLO_sys_dict = {'NLO_QCD_Var':['weight_QCD_ggZZkSBI']}

ggF_KfacBkgNLO_sys_dict = {'NLO_QCD_Var':['weight_QCD_ggZZkB']}


Systematics_dict={'sys':['NormSystematic',
                         'MET_SoftTrk_ResoPara',
                         'MET_SoftTrk_ResoPerp',
                         "JET_JER_EffectiveNP_6",
                         "JET_JER_EffectiveNP_7",
                         "JET_JER_EffectiveNP_8",
                         "JET_JER_EffectiveNP_9",
                         "PD_JET_JER_DataVsMC_MC16",
                         "PD_JET_JER_EffectiveNP_10",
                         "PD_JET_JER_EffectiveNP_11",
                         "PD_JET_JER_EffectiveNP_1",
                         "PD_JET_JER_EffectiveNP_12restTerm",
                         "PD_JET_JER_EffectiveNP_2",
                         "PD_JET_JER_EffectiveNP_3",
                         "PD_JET_JER_EffectiveNP_4",
                         "PD_JET_JER_EffectiveNP_5",
                         "PD_JET_JER_EffectiveNP_6",
                         "PD_JET_JER_EffectiveNP_7",
                         "PD_JET_JER_EffectiveNP_8",
                         "PD_JET_JER_EffectiveNP_9",
                         'JET_Pileup_OffsetMu',
                         'JET_Pileup_OffsetNPV',
                         'JET_Pileup_PtTerm',
                         'JET_Pileup_RhoTopology',
                         'JET_SingleParticle_HighPt',
                         'MUON_ID',
                         'MUON_MS',
                         'MUON_SAGITTA_RESBIAS',
                         'MUON_SAGITTA_RHO',
                         'EG_SCALE_AF2',
                         'EG_RESOLUTION_ALL',
                         'JET_PunchThrough_MC16',
                         'EG_SCALE_ALL',
                         'JET_BJES_Response',
                         'JET_EtaIntercalibration_Modelling',
                         'JET_EtaIntercalibration_NonClosure_highE',
                         'JET_EtaIntercalibration_NonClosure_negEta',
                         'JET_EtaIntercalibration_NonClosure_posEta',
                         'JET_EtaIntercalibration_TotalStat',
                         'JET_Flavor_Composition',
                         'JET_Flavor_Response',
                         'JET_JER_DataVsMC_MC16',
                         "JET_JER_EffectiveNP_10",
                         "JET_JER_EffectiveNP_11",
                         "JET_JER_EffectiveNP_1",
                         "JET_JER_EffectiveNP_12restTerm",
                         "JET_JER_EffectiveNP_2",
                         "JET_JER_EffectiveNP_3",
                         "JET_JER_EffectiveNP_4",
                         "JET_JER_EffectiveNP_5",
                         'MUON_SCALE']}
#
Experimental_sys_Norm = {'Exp_Norm_sys':['weight_EL_EFF_ID_CorrUncertaintyNP0',
                                         'weight_EL_EFF_ID_CorrUncertaintyNP10',
                                         'weight_EL_EFF_ID_CorrUncertaintyNP11',
                                         'weight_EL_EFF_ID_CorrUncertaintyNP12',
                                         'weight_EL_EFF_ID_CorrUncertaintyNP13',
                                         'weight_EL_EFF_ID_CorrUncertaintyNP14',
                                         'weight_EL_EFF_ID_CorrUncertaintyNP15',
                                         'weight_EL_EFF_ID_CorrUncertaintyNP1',
                                         'weight_EL_EFF_ID_CorrUncertaintyNP2',
                                         'weight_EL_EFF_ID_CorrUncertaintyNP3',
                                         'weight_EL_EFF_ID_CorrUncertaintyNP4',
                                         'weight_EL_EFF_ID_CorrUncertaintyNP5',
                                         'weight_EL_EFF_ID_CorrUncertaintyNP6',
                                         'weight_EL_EFF_ID_CorrUncertaintyNP7',
                                         'weight_EL_EFF_ID_CorrUncertaintyNP8',
                                         'weight_EL_EFF_ID_CorrUncertaintyNP9',
                                         'weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15',
                                         'weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16',
                                         'weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17',
                                         'weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1',
                                         'weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2',
                                         'weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3',
                                         'weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4',
                                         'weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5',
                                         'weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6',
                                         'weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7',
                                         'weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8',
                                         'weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9',
                                         'weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR',
                                         'weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR',
                                         'weight_MUON_EFF_ISO_STAT',
                                         'weight_MUON_EFF_ISO_SYS',
                                         'weight_MUON_EFF_RECO_STAT_LOWPT',
                                         'weight_MUON_EFF_RECO_STAT',
                                         'weight_MUON_EFF_RECO_SYS_LOWPT',
                                         'weight_MUON_EFF_RECO_SYS',
                                         'weight_MUON_EFF_TTVA_STAT',
                                         'weight_MUON_EFF_TTVA_SYS',
                                         "weight_FT_EFF_extrapolation_from_charm",
                                         "weight_FT_EFF_B_systematics",
                                         "weight_FT_EFF_C_systematics",
                                         "weight_FT_EFF_Light_systematics",
                                         "weight_FT_EFF_B_systematics",
                                         "weight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR",
                                         "weight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR",
                                         "weight_MUON_EFF_TrigSystUncertainty",
                                         "weight_MUON_EFF_TrigStatUncertainty"]}

Cat2 = {'resr2' :[]}

sys_to_process = {'ggSNLO'     :[ggF_KfacSigNLO_sys_dict ],
                  'ggBNLO'     :[ggF_KfacBkgNLO_sys_dict ],
                  'ggSBINLOI'  :[ggF_KfacSBINLO_sys_dict],

                  'WZ_3lep_bkg':[],
                  'WZ_3lep_bkg_1':[],
                  'WZ_3lep_bkg_2':[],
                  'WW_emu_bkg_1' :[],
                  'WW_emu_bkg_2' :[],
                  'ttbar_emu_bkg' :[],
                  'Wt_emu_bkg' :[],
                  'Wtbar_emu_bkg' :[],
                  'ttbar_emu_bkg' :[],
                  
                  'Zjets_bkg'  :[],
                  'tot_emu_bkg':[],

                  'qqZZ_bkg'   :[qqZZ_scale_factors_dict],
                  'qqZZ_1_Part0' :[qqZZ_scale_factors_dict, qqZZ_EW_4lep],
                  'qqZZ_1_Part1' :[qqZZ_scale_factors_dict, qqZZ_EW_4lep],
                  'qqZZ_1_Part2' :[qqZZ_scale_factors_dict, qqZZ_EW_4lep],
                  'qqZZ_2_Part0' :[qqZZ_scale_factors_dict, qqZZ_EW_2l2v],
                  'qqZZ_2_Part1' :[qqZZ_scale_factors_dict, qqZZ_EW_2l2v],
                  'qqZZ_2_Part2' :[qqZZ_scale_factors_dict, qqZZ_EW_2l2v],
                  'qqBkg_1'    :[qqZZ_scale_factors_dict, qqZZ_EW_4lep],
                  'qqBkg_2'    :[qqZZ_scale_factors_dict, qqZZ_EW_2l2v],
                  'qqBkg_3'    :[qqZZ_scale_factors_dict],

                  'VBFB'       :[],
                  'VBFS'       :[],
                  'VBFI'       :[],
                  'VBFSBI'     :[],
                  'VBFSBI5'    :[],
                  'VBFSBI10'   :[],
                  'VBFSBIS'    :[],
                  'VBFSBI5S'   :[],
                  'VBFSBI10S'  :[],
                  'VBFSBII'    :[],
                  'VBFSBI5I'   :[],
                  'VBFSBI10I'  :[]}


        #                                
cat = {'test':[]}
