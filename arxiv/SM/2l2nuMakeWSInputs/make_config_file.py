from ROOT import *
import sys
sys.path.append('./Systematic_names/')
import os
from array import array
import math
import argparse
import List_Sys
import numpy as np
import pickle
from datetime import date

today = date.today()

parser = argparse.ArgumentParser(description='Make Config files')
parser.add_argument('--FileName', type=str, default=today.strftime("%d_%m_config_file_3SRs.ini"), help='Name of output config files')
parser.add_argument('--outpath',  type=str, default='/afs/cern.ch/user/s/skrishna/work/HZZ/0604_HZZWS/2l2nu_prel_run/Config_Files/', help='Add path to output config file')
parser.add_argument('--inpath',  type=str, default='/afs/cern.ch/user/s/skrishna/work/HZZ/offshellstatistics/', help='Add path to where all the WS are for the config file')
parser.add_argument('--MCsets', nargs='+',default='ggSNLO,ggBNLO,ggSBINLOI,VBFB,VBFSBI,VBFSBI5,Zjets_bkg,tot_emu_bkg,WZ_3lep_bkg,qqZZ_1_Part0,qqZZ_1_Part1,qqZZ_1_Part2,qqZZ_2_Part0,qqZZ_2_Part1,qqZZ_2_Part2',  help='Procs to study')
#parser.add_argument('--MCsets', nargs='+',default='ggSNLO,ggBNLO,ggSBINLOI,VBFB,VBFSBI,VBFSBI5,Zjets_bkg,tot_emu_bkg,WZ_3lep_bkg,qqZZ_bkg',  help='Procs to study')
#parser.add_argument('--MCsets', nargs='+',default='ggSNLO,ggBNLO,ggSBINLOI,Zjets_bkg,tot_emu_bkg,qqZZ_Part0,qqZZ_Part1,qqZZ_Part2,WZ_3lep_bkg',  help='Procs to study')
parser.add_argument('--doSys', type=bool, default=False, help='to do or not to do sys...' )

#qqZZ_all,
#Reading in the arguments
args = parser.parse_args()
input_path = args.inpath
mcsets = args.MCsets
doSys = args.doSys

config_file_name = os.path.join(args.outpath, args.FileName)
config_file = open(config_file_name,'w')

print("Getting regions of interest")
#Getting the list of regions with binning/ observables - output of code 1
file_regions = open('regions_of_interest.txt','r')
regions_list = pickle.load(file_regions)
file_regions.close()
print(regions_list)

#Looping over the bin regions to get the bin edges/to break down concatenated histograms
bin_regions =[] #upper edges of the regions
bin_regions_names=[] #names (and observable)
region_edges=[]#lower and upper limits
region_obs=[]
region_wgt=[]
region_bins=[]

for regions in regions_list.keys():
    bin_regions.append(len(regions_list[regions][2])-1)
    #print(bin_regions)
    #Needs to be changed depending on what the observable is called
    if "ncl" in (regions_list[regions][1]):
        obs = "ggFNN_MELA"
    elif "VBF" in (regions_list[regions][1]):
        obs = "VBFNN_MELA"
    elif "MCFM" in (regions_list[regions][1]):
        obs = "ME_disc"
    else:
        obs = regions_list[regions][1]

    rgn_name = regions.split("_")
    if "F" in rgn_name[2]:
        bin_regions_names.append((obs+"_"+rgn_name[0]+"_"+rgn_name[1]+"_"+rgn_name[2]))
    else:
        bin_regions_names.append((obs+"_"+rgn_name[0]+"_"+rgn_name[1]))
    region_obs.append(obs)
    region_wgt.append(regions_list[regions][0])
    region_bins.append(regions_list[regions][2])

region_edges.append(1)
for i in range(0, len(bin_regions)):
    region_edges.append(sum(bin_regions[0:i+1])+1)
#print(region_edges)
#print(bin_regions_names)

def main():
    print("Writing Config file...")
    print("[Main]")
    config_file.write("[main]\n\n")
    config_file.write("fileDir = "+str(input_path)+"\n\n")
    config_file.write("NPlist = nuisance.txt\n\n")
    config_file.write("#NPlist = nuisance_noSys.txt\n\n")

    config_file.write("categories = ")
    categories = ""
    for rgn_i in range(0,len(bin_regions_names)):
        #print(rgn_i)
        for bins in range(1, bin_regions[rgn_i]+1):
            #print(bins)
            categories = categories+(bin_regions_names[rgn_i]+"_Incl_bin_"+str(bins)+"_13TeV,")

    config_file.write(categories[:-1])
    config_file.write("\n\nmcsets = "+str(mcsets)+"\n\n")
    print("[Cuts]")
    config_file.write("[cuts]\n\n")
    for rgn_i in range(0,len(bin_regions_names)):
        obs = region_obs[rgn_i]
        wgt = region_wgt[rgn_i]
        i=0
        for bins in range(1, bin_regions[rgn_i]+1):
            #print(bins)
            #print(region_bins[rgn_i][i])
            obs_cut = "("+(str(region_bins[rgn_i][i])+"<"+obs+") && (" +obs+"<"+str(region_bins[rgn_i][i+1])+")")
            print(obs_cut)
            i=i+1
            config_file.write(bin_regions_names[rgn_i]+"_Incl_bin_"+str(bins)+"_13TeV = ("+wgt+"&&("+obs_cut+"))\n")

    print("[Observables]")
    config_file.write("\n[observables]\n\n")
    for rgn_i in range(0,len(bin_regions_names)):
        for bins in range(1, bin_regions[rgn_i]+1):
            config_file.write(bin_regions_names[rgn_i]+"_Incl_bin_"+str(bins)+"_13TeV = ")
            config_file.write(str(region_obs[rgn_i])+":"+str(region_obs[rgn_i])+",1,"+str(region_bins[rgn_i][0])+","+str(region_bins[rgn_i][-1])+"\n")

    print("[Coefficients]")
    config_file.write("\n[coefficients]\n\n")
    mc_sets = mcsets.split(",")
    poi ="none"
    isSignal = False
    for mc in mc_sets:
        print("looking at : "+mc)
        if "Zjets_bkg" in mc:
            poi = "mu_Zjets"
        elif "tot_emu_bkg" in mc:
            poi = "mu_emu"

        elif "Part0" in mc:
            poi = "mu_qqZZ"
        elif "qqZZ_bkg" in mc:
            poi = "mu_qqZZ"
        elif "Part1" in mc:
            poi = "mu_qqZZ*mu_qqZZ_1"
        elif "Part2" in mc:
            poi = "mu_qqZZ*mu_qqZZ_1*mu_qqZZ_2"

        elif "WZ_3lep_bkg" in mc:
            poi = "mu_3lep"

        elif "ggSNLO" == mc:
            poi = "mu*mu_ggF - sqrt(mu*mu_ggF)"
        elif "ggSBINLOI" == mc:
            poi = "sqrt(mu*mu_ggF)"
        elif "ggBNLO" == mc:
            poi = "1 - sqrt(mu*mu_ggF)"
 
        elif "VBFB" == mc:
            poi = "((sqrt(mu*mu_VBF)-1)*(sqrt(mu*mu_VBF)-sqrt(mu5[5.])))/sqrt(mu5[5.])"
        elif "VBFSBI" == mc:
            poi = "(mu5[5.]*sqrt(mu*mu_VBF)-sqrt(mu5[5.])*mu*mu_VBF)/(mu5[5.]-sqrt(mu5[5.]))"
        elif "VBFSBI5" == mc:
            poi = "((mu*mu_VBF)-sqrt(mu*mu_VBF))/(mu5[5.]-sqrt(mu5[5.]))"
        else:
            poi ="none"
        if "qqZZ" in poi: #just a dummy, norm not removed so not needed
            config_file.write(mc+" = factors:n_"+mc+",bgyields139.txt ; sys:"+mc+"_sys.txt; poi:"+poi+" \n")
        elif (("emu" in poi) or ("3lep" in poi) or ("ets" in poi)):
            print("here! ", poi )
            config_file.write(mc+" = factors:n_"+mc+",bgyields139.txt ; sys:"+mc+"_sys.txt; poi:"+poi+" \n")
        elif "none" in poi:
            config_file.write(mc+" = factors:n_"+mc+",bgyields.txt ; sys:"+mc+"_sys.txt; global:ATLAS_LUMI(139.0/0.983/1.017)\n")
        else:
            config_file.write(mc+" = factors:n_"+mc+",bgyields.txt ; sys:"+mc+"_sys.txt; poi:"+poi+"; global:ATLAS_LUMI(139.0/0.983/1.017)\n")
        

    print("[Categories]")
    config_file.write("\n[")
    config_file.write(categories[:-1])
    config_file.write("]\n\n")
    
    for mc in mc_sets:
        if "ggS" in mc:
            isSignal = True
        elif "VBFSBI" in mc:
            isSignal = True
        else:
            isSignal = False

        if isSignal==True:
            config_file.write(mc+" = SampleCount : ATLAS_Signal_"+mc+"\n")
        if isSignal==False:
            config_file.write(mc+" = SampleCount : ATLAS_Bkg_"+mc+"\n")

    
    config_file.write("\n[asimov: asimovData]\n\n")
    config_file.write("mu = 1.0\n")

    config_file.write("mu_ggF = 1.0\n")
    config_file.write("mu_VBF = 1.0\n")

    config_file.write("mu_emu = 1.0\n")
    config_file.write("mu_3lep = 1.0\n")
    config_file.write("mu_Zjets = 1.0\n")    

    config_file.write("mu_qqZZ = 1.0\n")
    config_file.write("mu_qqZZ_1 = 1.0\n")    
    config_file.write("mu_qqZZ_2 = 1.0\n")
                                                                                            
    print(args.outpath)

if '__main__' in __name__:
    main()

