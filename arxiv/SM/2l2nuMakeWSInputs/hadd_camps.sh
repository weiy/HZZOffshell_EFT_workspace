#!/bin/bash
outname="Final_WS_CR"
date="0815"
outputfileName="./outputs_"$date"/Merged_code1_"$outname
rm -rf $outputfileName
mkdir $outputfileName

inputfileName="./outputs_"$date"/hists_code1_"$outname
yield_file="./outputs_"$date"/yields.txt"
outPlotsPath="./outputs_"$date"/Plots/"

#filelist="ggSBINLOI_hists.root ggBNLO_hists.root ggSNLO_hists.root WZ_3lep_bkg_hists.root WW_emu_bkg_hists.root ttbar_emu_bkg_hists.root" 
filelist="ggSBINLOI_hists.root ggBNLO_hists.root ggSNLO_hists.root VBFB_hists.root VBFSBI_hists.root VBFSBI5_hists.root VBFSBI10_hists.root"
#WZ_3lep_bkg_hists.root WW_emu_bkg_hists.root ttbar_emu_bkg_hists.root"

for proc in ${filelist};
do hadd $outputfileName/$proc $inputfileName/mc16*/$proc;
done;

hadd $outputfileName/qqZZ_1_Part0_hists.root $inputfileName/mc16*/qqBkg_1_Part0* 
hadd $outputfileName/qqZZ_1_Part1_hists.root $inputfileName/mc16*/qqBkg_1_Part1* 
hadd $outputfileName/qqZZ_1_Part2_hists.root $inputfileName/mc16*/qqBkg_1_Part2* 

hadd $outputfileName/qqZZ_2_Part0_hists.root  $inputfileName/mc16*/qqBkg_2_Part0*
hadd $outputfileName/qqZZ_2_Part1_hists.root  $inputfileName/mc16*/qqBkg_2_Part1*
hadd $outputfileName/qqZZ_2_Part2_hists.root  $inputfileName/mc16*/qqBkg_2_Part2*

hadd $outputfileName/Zjets_bkg_hists.root $inputfileName/mc16*/Zjets_bkg364*
hadd $outputfileName/tot_emu_bkg_hists.root $inputfileName/mc16*/*emu_bkg*
hadd $outputfileName/WZ_3lep_bkg_hists.root $inputfileName/mc16*/WZ_3lep_bkg*

#hadd $outputfileName/qqZZ_bkg_1_hists.root $outputfileName/qqZZ*

#python qqZZ_norm_overflow.py --inpath $outputfileName
#python3 plotter_code.py --inpath $outputfileName --outpath $outPlotsPath
#python check_yields_mu.py --inpath $outputfileName >> $yield_file
