outname="Final_WS_CR"
date="0815"

if [ ! -d ${outputPath} ]; then mkdir ${outputPath}; fi

rm -rf WS_inputs_$outname
rm -rf NP_names.txt
mkdir WS_inputs_$outname

#HighSoverB=4,11,2
HSOB_Threshold=0
LSOB_Threshold=0

inPath="./outputs_"$date"/Merged_code2_"$outname

echo $inPath
outputPath=WS_inputs_$outname


python make_WSInputs_code3.py --inProc  ggSBINLOI --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath >var_text_files/ggSBINLOI.txt 
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc  ggBNLO --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath >var_text_files/ggBNLO.txt
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc  ggSNLO --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath >var_text_files/ggSNLO.txt
echo "--------------------------------------------------------------------------"

python make_WSInputs_code3.py --inProc  VBFSBI --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath >var_text_files/VBFSBI.txt
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc  VBFB --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath >var_text_files/VBFB.txt
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc  VBFSBI5 --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath >var_text_files/VBFSBI5.txt
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc  VBFSBI10 --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath >var_text_files/VBFSBI10.txt
echo "--------------------------------------------------------------------------"

python make_WSInputs_code3.py --inProc  WZ_3lep_bkg --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath >var_text_files/WZ_3lep_bkg.txt
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc  Zjets_bkg --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath >var_text_files/Zjets_bkg.txt
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc  tot_emu_bkg --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath >var_text_files/tot_emu_bkg.txt
echo "--------------------------------------------------------------------------"
#python make_WSInputs_code3.py --inProc  qqZZ_bkg --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath >var_text_files/.txt
#echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc qqZZ_2_Part0  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath >var_text_files/qqZZ_2_Part0.txt
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc qqZZ_2_Part1  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath >var_text_files/qqZZ_2_Part1.txt
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc qqZZ_2_Part2  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath >var_text_files/qqZZ_2_Part2.txt
echo "--------------------------------------------------------------------------"

python make_WSInputs_code3.py --inProc qqZZ_1_Part0  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath >var_text_files/qqZZ_1_Part0.txt
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc qqZZ_1_Part1  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath >var_text_files/qqZZ_1_Part1.txt
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc qqZZ_1_Part2  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath >var_text_files/qqZZ_1_Part2.txt
echo "--------------------------------------------------------------------------"


echo "Writing NP list"
python make_NPfile.py --outpath $outputPath
echo "Writing Config file"
python make_config_file.py --FileName config_file_3SRs_$outname.ini --inpath $PWD/$outputPath
