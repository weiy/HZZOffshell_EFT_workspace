from ROOT import *
import sys
sys.path.append('./Systematic_names/')
import os
import os.path
from os import path
from glob import glob
from array import array
import math
import argparse
import List_Sys
from array import array
import numpy as np
from math import sqrt
from math import log10
import glob
#import json
import pickle
from collections import OrderedDict

gROOT.SetBatch(kTRUE)
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--tree',    type=str, default='tree_incl_all', help='Name of tree')
parser.add_argument('--inpath',  type=str, default='', help='Add path to the sample directory (only) each campaign accesed separately')
parser.add_argument('--outpath', type=str, default='', help='Add path to output')
parser.add_argument('--sample',  type=str, default='', help='Add a sample you want to read')
parser.add_argument('-c','--campaign',type=str,  nargs='+', default =['mc16a','mc16d','mc16e'],  help='Campaigns needed to run over, example -c mc16a ')
parser.add_argument('--channel', type=str, default='4l', help='is it 4l or 2l2nu' )
#parser.add_argument('-c','--campaign',type=str,  nargs='+', default =['mc16d'],  help='Campaigns needed to run over, example -c mc16a ')
parser.add_argument('--doNormTheoSys', type=bool, default=True, help='Make Norm Theory system. hists' )
parser.add_argument('--doNormExptSys', type=bool, default=True, help='Make Norm Exptem. system. hists' )

args = parser.parse_args()

def region_of_interest(list_regions,name,Cuts,Obs,Obs_name,binning,isSR,isVariableBinning):
    """
    function defines the new regions of interest
    """
    name = name+"_"+str(Obs_name)  
    if isVariableBinning == True:
        bins = binning
        #print(binning)
    else:
        bins = np.linspace(binning[1], binning[2], num=binning[0]+1)
    list_regions.update({name:[Cuts, Obs, bins]})
    
def concat_hists(hist_list, histName):
    """
    Concatenate the historgams from all the regions to make one-long histogram
    easier when removing normalization
    """
    binsContent=[] #list to append bin contents
    binsError  =[] #list to append bin errors
    binsEntry  =0  #gets the entries in the histogram
    outhist_len = 0 #len of the concatenated histogram
    for hist in hist_list:
        outhist_len = outhist_len + hist.GetNbinsX()  #concatenates histograms to get final histogram length      
    #Define the output concatenated histogram
    outhist = TH1F(histName, histName, outhist_len, 0, outhist_len) 
    #Loop over the histograms, each one corresponding to each SR/CR region
    for hist in hist_list:
        binsEntry = binsEntry+(hist.GetEntries()) #get the entries
        for i in range(1,hist.GetNbinsX()+1): #Only the bins, not under and overflow, add overflow to last and underflow to fist bin
            if i ==  1: #add underflow to first bin, do  sum in quadrature for the errors
                binsContent.append(hist.GetBinContent(i)+hist.GetBinContent(0))
                binsError.append(sqrt((hist.GetBinError(i))**2+(hist.GetBinError(0))**2))
            elif i == hist.GetNbinsX(): #add overflow to last coloumn and sum in quadrature for error
                binsContent.append(hist.GetBinContent(i)+hist.GetBinContent(hist.GetNbinsX()+1))
                binsError.append(sqrt((hist.GetBinError(i))**2+(hist.GetBinError(hist.GetNbinsX()+1))**2))
            else: #just get the bin content for other bins
                binsContent.append(hist.GetBinContent(i))
                binsError.append(hist.GetBinError(i))

    #set the information in the concatenated histogram
    outhist.SetEntries(binsEntry)
    for i in range(1,len(binsContent)+1):
        #print( binsContent[i-1])
        outhist.SetBinContent(i, binsContent[i-1])
        outhist.SetBinError(i, binsError[i-1])
    #return the concatenated hist
    #print(sum(binsContent))
    return outhist        

def write_hist(list_regions,weight_Indx,dictHists,Output_Dir,Process,camp,myTree,InSample,Nom_Norm,CRs_Norm):
    """
    function defines the histograms to be written out
    depending on the systematics/ processes etc.
    use the concat_hists function to write out the histogram that has been concatenated in the various regions
    """

    OutFolder = args.outpath #path where hist is stored
    listHists = [] #hist of histograms to write out
    #Fill nominal histograms

    for histToFill in dictHists.keys():
        #print("histtofill")
        #print(histToFill)
        i = 0
        #print(i)
        hist_Rgns =[]
        for regions in list_regions.keys():           
            #print(regions)
            histName = regions+"_"+histToFill #histogram named as Region_variable
            #print("----------------------"+regions+"----------------------")
            #print "Defining...", histName
            bin_edges = np.array(list_regions[regions][2], dtype='float64') #Get the binning/edges
            #print("bin edges")
            #print(bin_edges)
            hist_Rgns.append(TH1F(histName, histName, len(bin_edges)-1, bin_edges)) #append the histograms to a list 
            #print(Nom_Norm)
            #print(CRs_Norm)

            if ("3l" or "emu") in regions:
                fillHist = Form('%s>>%s'%(list_regions[regions][1],histName)) #Fill the histogram with the appropriate variable
                #print(fillHist)
                fillWeight = Form('%s*%s*%s' % (dictHists[histToFill][weight_Indx],list_regions[regions][0],CRs_Norm))#use the appropriate weights
                #print(fillWeight)
                myTree.Draw(fillHist, fillWeight)# draw the histogram
                #print("Integral: "+str(hist_Rgns[i].Integral()))            

            else:
                fillHist = Form('%s>>%s'%(list_regions[regions][1],histName)) #Fill the histogram with the appropriate variable
                #print(fillHist)
                fillWeight = Form('%s*%s*%s' % (dictHists[histToFill][weight_Indx],list_regions[regions][0],Nom_Norm))#use the appropriate weights
                #print(fillWeight)
                myTree.Draw(fillHist, fillWeight)# draw the histogram
                #print("Integral: "+str(hist_Rgns[i].Integral()))
            
            i =i+1
        #append the concatenated histogram
        listHists.append(concat_hists(hist_Rgns, histToFill))

    #print(listHists)
    OutDirCamp = os.path.join(OutFolder,camp)
    if "MAXHTPTV" in InSample:
        OutSample  = Process+(InSample.split(".")[1])+"_hists.root"
        #print(OutSample)
    else:
        OutSample  = Process+"_hists.root"

    if path.isdir(OutDirCamp): #check if the directory exists 
        if path.isfile(os.path.join(OutDirCamp,OutSample)):#check if file exists
            fOut_FromTree = TFile(os.path.join(OutDirCamp,OutSample), "UPDATE")#update the root file with the histograms
            print('root file exits, updating it ...')
        else:#if file doesnt exist then write out a new one
            fOut_FromTree = TFile(os.path.join(OutDirCamp,OutSample), "CREATE")
            print('root file doesnt exit, creating it ..')
    else:#if directory doesnt exist then write out a new one
        print('making a new directory and root file ...')
        os.mkdir(OutDirCamp)
        fOut_FromTree = TFile(os.path.join(OutDirCamp,OutSample), "CREATE")

    directory = fOut_FromTree.GetDirectory(Output_Dir)
    if not (directory):
        #print ">>> created " + Output_Dir 
        directory = fOut_FromTree.mkdir(Output_Dir)
    directory.cd()
    #write out all the histograms
    for hist in listHists:
        print "Writing...", hist
        print(hist.Integral())
        directory.cd()
        hist.Write()
        hist.SetDirectory(0)

    fOut_FromTree.Write("",TFile.kOverwrite)
    fOut_FromTree.Close()

def GetLumi(camp):
    if "a" in camp:
        lumi = 36.207
    elif "d" in camp:
        lumi = 44.307
    elif "e" in camp:
        lumi = 58.250
    else:
        lumi =0
    return lumi

def main():
    """
    Main function.
    """    
    #gStyle.SetOptStat(0)
    TH1.SetDefaultSumw2()
    #reading the arguments provided
    EosDir = args.inpath
    OutDir = args.outpath
    TreeName = args.tree
    InSample = args.sample
    Channel = args.channel
    #The Sample being processed
    print "InSample ", InSample
    
    splitSample = False # Automatically set to False, changed inside the sample
    weight_scl_1 = 1 # set to 1 as default, using %2==0 and %2==1 to split the samples that need splitting 
    weight_scl_2 = 1
    
    Idenf = "" #to keep track of sample names
    Idenf_scl_1 = ""
    Idenf_scl_2 = ""

    campaigns = args.campaign #whichever campaigns need to be run
    variations = ["1up", "1down"]

    ####################
    # Input Root Files #
    ####################

    #qqZZ Procs
    CR_split_num = 3 #3 default
    #sample is split in 3 parts due to 3 CR and already produced in 3 parts x 3 campaigns so there are 27 histograms here
    #bkg1 bkg2 and bkg3 are the 3 minitrees, part1 part2 and part3 are the parts in which we split due to our CR definitions
    
    if "4l" in Channel:
        if "364250" in InSample:
            Process       = "qqBkg_1_4l"
            splitSample   = True
            if CR_split_num == 2:
                weight_scl_1  = "weight*(n_jets<=1)"
                Idenf_scl_1   = "_Part0"
                weight_scl_2  = "weight*(n_jets>=2)"
                Idenf_scl_2   = "_Part2"
                weight_scl    = "weight"
                Idenf         = ""
            
        if "346899" in InSample:
            #if "364251" in InSample:
            Process       = "qqBkg_2_4l"
            splitSample   = True
            if CR_split_num == 2:
                weight_scl_1  = "weight*(n_jets<=1)"
                Idenf_scl_1   = "_Part0"
                weight_scl_2  = "weight*(n_jets>=2)"
                Idenf_scl_2   = "_Part2"
                weight_scl    = "weight"
                Idenf         = ""
            
        if "364252" in InSample:
            Process       = "qqBkg_3_4l"
            splitSample   = True
            if CR_split_num == 2:
                weight_scl_1  = "weight*(n_jets<=1)"
                Idenf_scl_1   = "_Part0"
                weight_scl_2  = "weight*(n_jets>=2)"
                Idenf_scl_2   = "_Part2"
                weight_scl    = "weight"
                Idenf         = ""
    
    if "2l" in Channel:
        if "345666" in InSample:
            Process       = "qqBkg_2"
            splitSample   = True
            if CR_split_num == 2:
                weight_scl_1  = "weight*(n_jets<=1)*(weight_qqZZNLO_EW)"
                Idenf_scl_1   = "_Part0"
                weight_scl_2  = "weight*(n_jets>=2)*(weight_qqZZNLO_EW)"
                Idenf_scl_2   = "_Part2"
                weight_scl    = "weight*(weight_qqZZNLO_EW)"
                Idenf         = ""

            if CR_split_num == 3:
                weight_scl_1  = "weight*(n_jets==0)*(weight_qqZZNLO_EW)"
                Idenf_scl_1   = "_Part0"
                weight_scl_2  = "weight*(n_jets==1)*(weight_qqZZNLO_EW)"
                Idenf_scl_2   = "_Part1"
                weight_scl    = "weight*(n_jets>=2)*(weight_qqZZNLO_EW)"
                Idenf         = "_Part2"
        
        if "364250" in InSample:
            Process       = "qqBkg_1"
            splitSample   = True
            if CR_split_num == 2:
                #weight_scl_1  = "weight*(n_jets<=1)*(tree_incl_all_EWCorr.weight_EWNLO)"
                weight_scl_1  = "weight*(n_jets<=1)"
                Idenf_1       = "_Part0"
                #weight_scl_2  = "weight*(n_jets>=2)*(tree_incl_all_EWCorr.weight_EWNLO)"
                weight_scl_2  = "weight*(n_jets>=2)"
                Idenf_scl_2   = "_Part2"
                #weight_scl    = "weight*(tree_incl_all_EWCorr.weight_EWNLO)"
                weight_scl    = "weight"
                Idenf         = ""

            if CR_split_num == 3:
                #weight_scl_1  = "weight*(n_jets==0)*(tree_incl_all_EWCorr.weight_EWNLO)"
                weight_scl_1  = "weight*(n_jets==0)"
                Idenf_scl_1   = "_Part0"
                #weight_scl_2  = "weight*(n_jets==1)*(tree_incl_all_EWCorr.weight_EWNLO)"
                weight_scl_2  = "weight*(n_jets==1)"
                Idenf_scl_2   = "_Part1"
                #weight_scl    = "weight*(n_jets>=2)*(tree_incl_all_EWCorr.weight_EWNLO)"
                weight_scl    = "weight*(n_jets>=2)"
                Idenf         = "_Part2"

        if "364285" in InSample:
            Process       = "qqBkg_3"
            splitSample   = True
            if CR_split_num == 2:
                weight_scl_1  = "weight*(n_jets<=1)"
                Idenf_1       = "_Part0"
                weight_scl_2  = "weight*(n_jets>=2)"
                Idenf_scl_2   = "_Part2"
                weight_scl    = "weight"
                Idenf         = ""

            if CR_split_num == 3:
                weight_scl_1  = "weight*(n_jets==0)"
                Idenf_scl_1   = "_Part0"
                weight_scl_2  = "weight*(n_jets==1)"
                Idenf_scl_2   = "_Part1"
                weight_scl    = "weight*(n_jets>=2)"
                Idenf         = "_Part2"

        #3l CR: WZ 364253 -> 
        if "364253" in InSample:
            Process     = "WZ_3lep_bkg_1"
            weight_scl  = "weight"
        
        if "364284" in InSample:
            Process     = "WZ_3lep_bkg_2"
            weight_scl  = "weight"
        
        #emu CR: WW and ttbar
        if "361600"  in InSample:
            Process     = "WW_emu_bkg_1"
            weight_scl  = "weight"

        if "345718"  in InSample:
            Process     = "WW_emu_bkg_2"
            weight_scl  = "weight"

        if "410472"  in InSample:
            Process     = "ttbar_emu_bkg"
            weight_scl  = "weight"
        
        if "410646"  in InSample:
            Process     = "Wt_emu_bkg"
            weight_scl  = "weight"

        if "410647"  in InSample:
            Process     = "Wtbar_emu_bkg"
            weight_scl  = "weight"
        
        #Z+jets CR: Z+jets
        if "3641"  in InSample:
            Process     = "Zjets_bkg"
            weight_scl  = "weight"

        #ggF Processes
        if "345726" in InSample:
            Process     = "ggSNLO"
            weight_scl  = "weight*weight_ggZZkS"
            
        if "345723" in InSample:
            Process     = "ggSBINLOI"
            weight_scl  = "weight*weight_ggZZkSBI"

        if "345725" in InSample:
            Process     = "ggBNLO"
            weight_scl  = "weight*weight_ggZZkB"
            
        #VBF Processes
        if "500379" in InSample:
            Process     = "VBFB"
            weight_scl  = "weight"
             
        if "500378" in InSample:
            Process     = "VBFSBI"
            weight_scl  = "weight"
       
        if "500380" in InSample:
            Process     = "VBFSBI5"
            weight_scl  = "weight"

        if "500381" in InSample:
            Process     = "VBFSBI10"
            weight_scl  = "weight"
        
    ################
    # Output Files #
    ################
    
    Dir_Nominal   = "Nominal"
    Dir_TheoSys   = "Theosys"
    Dir_ExptSys   = "Exptsys"
    
    ###########
    # Regions #
    ###########

    list_regions=OrderedDict()

    ###Validation Regions

    #3lep VR1
    ValRgn_name = "VR_3lep"
    ValRgn_cuts = "((lep3rd_mt > 60. && met_signif > 3. && n_bjets == 0)&&(This->GetTreeNumber()==1))" # to be changed when 3 VRs are used 
    ValRgn_bins = [4,0,100] # number of bins, minbin, max bin or Variable bin widths
    ValRgn_Obs  = "met_tst"
    ValRgn_Obs_name = "met_tst"
    isSR = False
    isVarBinning = False
    #region_of_interest(list_regions,ValRgn_name,ValRgn_cuts,ValRgn_Obs,ValRgn_Obs_name,ValRgn_bins,isSR,isVarBinning)

    #emu VR2
    ValRgn_name = "VR_emu"
    ValRgn_cuts = "((met_tst >120. && dLepR<1.8 && dMetZPhi > 2.5  && (dPhiJ100met > 0.4 || dPhiJ100met < 0) && met_signif > 10. && n_bjets == 0)&&(This->GetTreeNumber()==2))" 
    ValRgn_bins = [3,100,200] # number of bins, minbin, max bin or Variable bin widths
    ValRgn_Obs  = "met_tst"
    ValRgn_Obs_name = "met_tst"
    isSR = False
    isVarBinning = False
    #region_of_interest(list_regions,ValRgn_name,ValRgn_cuts,ValRgn_Obs,ValRgn_Obs_name,ValRgn_bins,isSR,isVarBinning)

    #Zjets VR3
    ValRgn_name = "VR_ZJets"
    ValRgn_cuts = "((met_tst >120. && dLepR<1.8 && dMetZPhi > 2.5 && met_signif < 9. && n_bjets == 0)&&(This->GetTreeNumber()==0))" 
    ValRgn_bins = [4,100,200] # number of bins, minbin, max bin or Variable bin widths
    ValRgn_Obs  = "met_tst"
    ValRgn_Obs_name = "met_tst"
    isSR = False
    isVarBinning = False
    #region_of_interest(list_regions,ValRgn_name,ValRgn_cuts,ValRgn_Obs,ValRgn_Obs_name,ValRgn_bins,isSR,isVarBinning)


    #Control region 
    #3lep CR1
    CtrRgn_name = "CR_3lep"
    CtrRgn_cuts = "((lep3rd_mt > 60. && met_signif > 3. && n_bjets == 0)&&(This->GetTreeNumber()==1))" # to be changed when 3 CRs are used 
    CtrRgn_bins = [1,-1,20] # number of bins, minbin, max bin or Variable bin widths
    CtrRgn_Obs  = "n_jets"
    CtrRgn_Obs_name = "n_jets"
    isSR = False
    isVarBinning = False
    region_of_interest(list_regions,CtrRgn_name,CtrRgn_cuts,CtrRgn_Obs,CtrRgn_Obs_name,CtrRgn_bins,isSR,isVarBinning)

    #emu CR2
    CtrRgn_name = "CR_emu"
    CtrRgn_cuts = "((met_tst >120. && dLepR<1.8 && dMetZPhi > 2.5  && (dPhiJ100met > 0.4 || dPhiJ100met < 0) && met_signif > 10. && n_bjets == 0)&&(This->GetTreeNumber()==2))" 
    CtrRgn_bins = [1,0,2000] # number of bins, minbin, max bin or Variable bin widths
    CtrRgn_Obs  = "mT_ZZ"
    CtrRgn_Obs_name = "mT_ZZ"
    isSR = False
    isVarBinning = False
    region_of_interest(list_regions,CtrRgn_name,CtrRgn_cuts,CtrRgn_Obs,CtrRgn_Obs_name,CtrRgn_bins,isSR,isVarBinning)

    CtrRgn_name = "CR_emu_ggF"
    CtrRgn_cuts = "((met_tst >120. && dLepR<1.8 && dMetZPhi > 2.5  && (dPhiJ100met > 0.4 || dPhiJ100met < 0) && met_signif > 10. && n_bjets == 0 && !(n_jets>=2 && TMath::Abs(detajj) > 4.0 ))&&(This->GetTreeNumber()==2))" 
    CtrRgn_bins = [1,0,2000] # number of bins, minbin, max bin or Variable bin widths
    CtrRgn_Obs  = "mT_ZZ"
    CtrRgn_Obs_name = "mT_ZZ"
    isSR = False
    isVarBinning = False
    #region_of_interest(list_regions,CtrRgn_name,CtrRgn_cuts,CtrRgn_Obs,CtrRgn_Obs_name,CtrRgn_bins,isSR,isVarBinning)
    
    CtrRgn_name = "CR_emu_VBF"
    CtrRgn_cuts = "((met_tst >120. && dLepR<1.8 && dMetZPhi > 2.5  && (dPhiJ100met > 0.4 || dPhiJ100met < 0) && met_signif > 10. && n_bjets == 0 && (n_jets>=2 && TMath::Abs(detajj) > 4.0 ))&&(This->GetTreeNumber()==2))" 
    CtrRgn_bins = [1,0,2000] # number of bins, minbin, max bin or Variable bin widths
    CtrRgn_Obs  = "mT_ZZ"
    CtrRgn_Obs_name = "mT_ZZ"
    isSR = False
    isVarBinning = False
    #region_of_interest(list_regions,CtrRgn_name,CtrRgn_cuts,CtrRgn_Obs,CtrRgn_Obs_name,CtrRgn_bins,isSR,isVarBinning)

    CtrRgn_name = "CR_ZJets"
    CtrRgn_cuts = "((met_tst >120. && dLepR<1.8 && dMetZPhi > 2.5 && met_signif < 9. && n_bjets == 0)&&(This->GetTreeNumber()==0))" 
    CtrRgn_bins = [1,0,2000] # number of bins, minbin, max bin or Variable bin widths
    CtrRgn_Obs  = "mT_ZZ"
    CtrRgn_Obs_name = "mT_ZZ"
    isSR = False
    isVarBinning = False
    region_of_interest(list_regions,CtrRgn_name,CtrRgn_cuts,CtrRgn_Obs,CtrRgn_Obs_name,CtrRgn_bins,isSR,isVarBinning)
    
    CtrRgn_name = "CR_ZJets_ggF"
    CtrRgn_cuts = "((met_tst >120. && dLepR<1.8 && dMetZPhi > 2.5 && met_signif < 9. && n_bjets == 0 && !(n_jets>=2 && TMath::Abs(detajj) > 4.0 ))&&(This->GetTreeNumber()==0))" 
    CtrRgn_bins = [1,0,2000] # number of bins, minbin, max bin or Variable bin widths
    CtrRgn_Obs  = "mT_ZZ"
    CtrRgn_Obs_name = "mT_ZZ"
    isSR = False
    isVarBinning = False
    #region_of_interest(list_regions,CtrRgn_name,CtrRgn_cuts,CtrRgn_Obs,CtrRgn_Obs_name,CtrRgn_bins,isSR,isVarBinning)

    CtrRgn_name = "CR_ZJets_VBF"
    CtrRgn_cuts = "((met_tst >120. && dLepR<1.8 && dMetZPhi > 2.5 && met_signif < 9. && n_bjets == 0 && (n_jets>=2 && TMath::Abs(detajj) > 4.0 ))&&(This->GetTreeNumber()==0))" 
    CtrRgn_bins = [1,0,2000] # number of bins, minbin, max bin or Variable bin widths
    CtrRgn_Obs  = "mT_ZZ"
    CtrRgn_Obs_name = "mT_ZZ"
    isSR = False
    isVarBinning = False
    #region_of_interest(list_regions,CtrRgn_name,CtrRgn_cuts,CtrRgn_Obs,CtrRgn_Obs_name,CtrRgn_bins,isSR,isVarBinning)

    #Signal Regions
    #ggF Region
    ggFRgn_name = "SR_ggF_2l2nu"
    ggFRgn_cuts = Form("((met_tst >120. && dLepR<1.8 && dMetZPhi > 2.5  && (dPhiJ100met > 0.4 || dPhiJ100met < 0) && met_signif > 10. && n_bjets == 0 && !(n_jets>=2 && TMath::Abs(detajj) > 4.0 ))&&(This->GetTreeNumber()==0))")
    ggFRgn_Obs  = "mT_ZZ"
    ggFRgn_Obs_name = "mT_ZZ"
    #ggFRgn_bins = [5, 0, 3000]
    ggFRgn_bins = [250., 300., 350., 400., 450., 500., 550., 600., 700., 800., 900., 1000., 1200., 1400., 2000., 3000.]
    isSR = True
    isVarBinning = True
    #region_of_interest(list_regions,ggFRgn_name,ggFRgn_cuts,ggFRgn_Obs,ggFRgn_Obs_name,ggFRgn_bins,isSR,isVarBinning)
    
    #VBF Region - 2l2nu
    VBFRgn_name = "SR_VBF_2l2nu"
    VBFRgn_cuts = Form("((met_tst >120. && dLepR<1.8 && dMetZPhi > 2.5  && (dPhiJ100met > 0.4 || dPhiJ100met < 0) && met_signif > 10. && n_bjets == 0 && (n_jets>=2 && TMath::Abs(detajj) > 4.0 ))&&(This->GetTreeNumber()==0))")
    VBFRgn_Obs  = "mT_ZZ"
    VBFRgn_bins = [100., 420., 540., 820., 1700.] # number of bins, minbin, max bin or Variable bin widths
    VBFRgn_Obs_name  = "mT_ZZ"
    isSR = True
    isVarBinning = True
    #region_of_interest(list_regions,VBFRgn_name,VBFRgn_cuts,VBFRgn_Obs,VBFRgn_Obs_name,VBFRgn_bins,isSR,isVarBinning)

    file_output = open("regions_of_interest.txt",'w')
    pickle.dump(list_regions,file_output)
    file_output.close()

    ####################################
    # Defining dict for the Histograms #
    ####################################

    print(Process)
    #print(weight_scl)

    dictTheoryhists={}
    for systematics in List_Sys.sys_to_process[Process]:
        #print(systematics)
        for sys in systematics.keys():
            #print(sys)
            for var_names in systematics[sys]:
                if "PDF" in var_names:
                    #print(var_names)
                    dictTheoryhists.update({var_names:[Form('(%s)*(%s)'%(weight_scl, var_names)),Form('(%s)*(%s)'%(weight_scl_1,var_names)),Form('(%s)*(%s)'%(weight_scl_2,var_names))],})
                else:
                    for var in variations:
                        #if "QCD_gg" in var_names:
                        #    sys_vars = var_names+'_'+var.replace("1","")
                        #    rem_kfac = var_names.replace("QCD_",'')
                        #    dictTheoryhists.update({sys_vars:[Form('(%s)*(%s)/((weight)*(%s))'%(weight_scl, sys_vars,rem_kfac)),Form('(%s)*(%s)/((weight)*(%s))'%(weight_scl_1,sys_vars,rem_kfac)),Form('(%s)*(%s)/((weight)*(%s))'%(weight_scl_2,sys_vars,rem_kfac))],})
                        if "weight_qqZZNLO_EW" in var_names:
                            sys_vars = var_names+'__'+var
                            dictTheoryhists.update({sys_vars:[Form('(%s)*(%s)/(%s)'%(weight_scl, sys_vars, var_names)),Form('(%s)*(%s)/(%s)'%(weight_scl_1,sys_vars,var_names)),Form('(%s)*(%s)/(%s)'%(weight_scl_2,sys_vars,var_names))],})
                        
                        elif "weight_EWNLO" in var_names:
                            sys_vars = var_names+'__'+var
                            dictTheoryhists.update({sys_vars:[Form('(%s)*(tree_incl_all_EWCorr.%s)'%(weight_scl, sys_vars)),Form('(%s)*(tree_incl_all_EWCorr.%s)'%(weight_scl_1,sys_vars)),Form('(%s)*(tree_incl_all_EWCorr.%s)'%(weight_scl_2,sys_vars))],})

                        else:
                            sys_vars = var_names+'__'+var
                            dictTheoryhists.update({sys_vars:[Form('(%s)*(%s)/(weight)'%(weight_scl, sys_vars)),Form('(%s)*(%s)/(weight)'%(weight_scl_1,sys_vars)),Form('(%s)*(%s)/(weight)'%(weight_scl_2,sys_vars))],})

    dictNominalhists={}
    print(weight_scl)
    dictNominalhists.update({"Nom_hist":[Form('(%s)'%(weight_scl)),Form('(%s)'%(weight_scl_1)),Form('(%s)'%(weight_scl_2))]})

    dictNormExphists ={}#Get Norm Exp sys names
    for Exp_norm_sys in List_Sys.Experimental_sys_Norm['Exp_Norm_sys']:
       for var in variations:
           sys_vars = Exp_norm_sys+'__'+var
           dictNormExphists.update({sys_vars:[Form('(%s)*(%s)/(weight)'%(weight_scl,sys_vars)),Form('(%s)*(%s)/(weight)'%(weight_scl_1,sys_vars)),Form('(%s)*(%s)/(weight)'%(weight_scl_2,sys_vars))]})
    #print("DictTheory")
    #print(dictTheoryhists)
    

    #############################################
    # Accessing file and Writing the histograms #
    #############################################
    
    Nominal_tree = "tree_PFLOW"
    CR1_emu_tree = "tree_emu_PFLOW"
    CR2_3l_tree  = "tree_3l_PFLOW"

    EW_4lep_tree = "tree_PFLOW_emu_4lep"
    EW_2l2v_tree = "tree_PFLOW_emu_2l2v"

    #print(args.doNormTheoSys)
    for camp in campaigns:
        print("doing Campaign " +str(camp)+"...")
        if "ggSBI" in Process:
            InSample_sys = InSample.replace("p4222","p4220")
        else:
            InSample_sys = InSample
        for sys in List_Sys.Systematics_dict['sys']:               
            if sys == 'NormSystematic':               
                #SysDirName_Nominal  ='Nominal'
                SysDirName_Nominal  ='Systematics/weightSyst'
                SysDirName_WgtSys   ='Systematics/weightSyst'
                SysDirName_CRs      ='Systematics_Background/weightSyst' 
                
                chain_Nominal = TChain("2l2nu_Trees_Nom")
                chain_Syst    = TChain("2l2nu_Trees_Sys")
                chain_EW_corr = TChain("2l2nu_Trees_EWCorr")

                #print(InSample)
                if "qqBkg_2" in Process:
                    EosDir_EW="/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hllvv/Run2/minitrees_rel139/qqZZ_EWCorrfix"
                    if "d" in camp:
                        File_Nominal = TFile(os.path.join(EosDir_EW,camp,SysDirName_Nominal,InSample.replace("p4220","p3916")))
                    else:
                        File_Nominal = TFile(os.path.join(EosDir_EW,camp,SysDirName_Nominal,InSample))

                File_Nominal = TFile(os.path.join(EosDir,camp,SysDirName_Nominal,InSample))
                
                if "qqBkg_1" in Process:
                    #tree_Nominal = File_Nominal.Get("tree_PFLOW")
                    #EW_corr_4lep_File = "/eos/user/r/rcoelhol/2l2v_EW_corr_4lqqZZ/"+camp+"/ewcorr_mc16.root"
                    #tree_Nominal.AddFriend("tree_incl_all_EWCorr=tree_PFLOW",EW_corr_4lep_File)
                    chain_EW_corr.Add("/eos/user/r/rcoelhol/2l2v_EW_corr_4lqqZZ/"+camp+"/ewcorr_mc16.root/tree_PFLOW")
                    print("EW Corr Chain -- ")
                    print(chain_EW_corr.GetEntries())


                chain_Nominal.Add(os.path.join(EosDir,camp,SysDirName_Nominal,InSample,Nominal_tree))
                Histo_Nom = TFile(os.path.join(EosDir,camp,SysDirName_Nominal,InSample))
                infoHist_Nom = Histo_Nom.Get("Hist/hInfo_PFlow")
                lumi = GetLumi(camp)
                Nom_Norm = infoHist_Nom.GetBinContent(1)*2.0/infoHist_Nom.GetEntries()*lumi/infoHist_Nom.GetBinContent(2)
                print("------------------------------")
                print(Nom_Norm)
                if args.doNormTheoSys == True:
                    if os.path.isfile(os.path.join(EosDir,camp,SysDirName_WgtSys,InSample_sys)):
                        chain_Syst.Add(os.path.join(EosDir,camp,SysDirName_WgtSys,InSample_sys,Nominal_tree))
                        
                #if os.path.isfile(os.path.join(EosDir,camp,SysDirName_CRs,InSample)):#.replace(".root","_CR.root"))):
                if os.path.isfile(os.path.join(EosDir,camp,SysDirName_CRs,InSample_sys)):#.replace(".root","_CR.root"))):
                    #bkg_sys_CRFile = TFile(os.path.join(EosDir,camp,SysDirName_CRs,InSample))#.replace(".root","_CR.root")))
                    bkg_sys_CRFile = TFile(os.path.join(EosDir,camp,SysDirName_CRs,InSample_sys))#.replace(".root","_CR.root")))
                    if bkg_sys_CRFile.GetListOfKeys().Contains(CR2_3l_tree):
                        print("CR tree found")
                        #chain_Nominal.Add(os.path.join(EosDir,camp,SysDirName_CRs,InSample_sys.replace(".root","_CR.root"),CR2_3l_tree))
                        #chain_Syst.Add(os.path.join(EosDir,camp,SysDirName_CRs,InSample_sys.replace(".root","_CR.root"),CR2_3l_tree))
                        #chain_Nominal.Add(os.path.join(EosDir,camp,SysDirName_CRs,InSample_sys.replace(".root","_CR.root"),CR1_emu_tree))
                        #chain_Syst.Add(os.path.join(EosDir,camp,SysDirName_CRs,InSample_sys.replace(".root","_CR.root"),CR1_emu_tree))
                        #tree_CR2 = bkg_sys_CRFile.Get(CR2_3l_tree)
                        #print(tree_CR2.GetEntries())
                        #tree_CR1 = bkg_sys_CRFile.Get(CR1_emu_tree)
                        #print(tree_CR1.GetEntries())
                        if "qqBkg_1" in Process:
                            #EW_corr_4lep_CR2_File = TFile("/eos/user/r/rcoelhol/2l2v_EW_corr_4lqqZZ/"+camp+"/CR2/ewcorr_mc16.root")
                            #tree_CR2.AddFriend("tree_incl_all_EWCorr=tree_PFLOW",EW_corr_4lep_CR2_File)
                            chain_EW_corr.Add("/eos/user/r/rcoelhol/2l2v_EW_corr_4lqqZZ/"+camp+"/CR2/ewcorr_mc16.root/tree_PFLOW")
                            #EW_corr_4lep_CR1_File = TFile("/eos/user/r/rcoelhol/2l2v_EW_corr_4lqqZZ/"+camp+"/CR1/ewcorr_mc16.root")
                            #tree_CR1.AddFriend("tree_incl_all_EWCorr=tree_PFLOW",EW_corr_4lep_CR1_File)
                            chain_EW_corr.Add("/eos/user/r/rcoelhol/2l2v_EW_corr_4lqqZZ/"+camp+"/CR1/ewcorr_mc16.root/tree_PFLOW")                           
                                        
                        chain_Nominal.Add(os.path.join(EosDir,camp,SysDirName_CRs,InSample_sys,CR2_3l_tree))
                        chain_Syst.Add(os.path.join(EosDir,camp,SysDirName_CRs,InSample_sys,CR2_3l_tree))
                        chain_Nominal.Add(os.path.join(EosDir,camp,SysDirName_CRs,InSample_sys,CR1_emu_tree))
                        chain_Syst.Add(os.path.join(EosDir,camp,SysDirName_CRs,InSample_sys,CR1_emu_tree))

                    Histo_CRs = TFile(os.path.join(EosDir,camp,SysDirName_CRs,InSample_sys))
                    infoHist_CRs = Histo_Nom.Get("Hist/hInfo_PFlow")
                    lumi = GetLumi(camp)
                    CRs_Norm = infoHist_Nom.GetBinContent(1)*2.0/infoHist_Nom.GetEntries()*lumi/infoHist_Nom.GetBinContent(2)               

                    if "qqBkg_1" in Process:
                        chain_Nominal.AddFriend(chain_EW_corr, "tree_incl_all_EWCorr")
                        chain_Syst.AddFriend(chain_EW_corr,"tree_incl_all_EWCorr")

                else:
                    print(InSample+" : CR tree does not exist...")
                    print(os.path.join(EosDir,camp,SysDirName_CRs,InSample))
                    CRs_Norm =1
                
                total = chain_Nominal.GetEntries()
                print(total)
                
                if splitSample==True:
                    print("splitting sample into parts...")
                    weight_Indx=1
                    print "Filling histogram part 1"
                    write_hist(list_regions,weight_Indx,dictNominalhists,Dir_Nominal,Process+Idenf_scl_1,camp,chain_Nominal,InSample,Nom_Norm,CRs_Norm)
                    if args.doNormTheoSys == True:
                        write_hist(list_regions,weight_Indx,dictTheoryhists, Dir_TheoSys,Process+Idenf_scl_1,camp,chain_Syst,InSample_sys,Nom_Norm,CRs_Norm)
                    if args.doNormExptSys == True:
                        write_hist(list_regions,weight_Indx,dictNormExphists,Dir_ExptSys,Process+Idenf_scl_1,camp,chain_Syst,InSample_sys,Nom_Norm,CRs_Norm)
                
                    weight_Indx=2
                    print "Filling histogram part 2"
                    write_hist(list_regions,weight_Indx,dictNominalhists,Dir_Nominal,Process+Idenf_scl_2,camp,chain_Nominal,InSample,Nom_Norm,CRs_Norm)
                    if args.doNormTheoSys == True:
                        write_hist(list_regions,weight_Indx,dictTheoryhists, Dir_TheoSys,Process+Idenf_scl_2,camp,chain_Syst,InSample_sys,Nom_Norm,CRs_Norm)
                    if args.doNormExptSys == True:
                        write_hist(list_regions,weight_Indx,dictNormExphists,Dir_ExptSys,Process+Idenf_scl_2,camp,chain_Syst,InSample_sys,Nom_Norm,CRs_Norm)

                weight_Indx=0
                print "Filling nominal histograms..."
                write_hist(list_regions,weight_Indx,dictNominalhists,Dir_Nominal,Process+Idenf,camp,chain_Nominal,InSample,Nom_Norm,CRs_Norm)
                if args.doNormTheoSys == True:
                    write_hist(list_regions,weight_Indx,dictTheoryhists, Dir_TheoSys,Process+Idenf,camp,chain_Syst,InSample_sys,Nom_Norm,CRs_Norm)
                if args.doNormExptSys == True:
                    write_hist(list_regions,weight_Indx,dictNormExphists,Dir_ExptSys,Process+Idenf,camp,chain_Syst,InSample_sys,Nom_Norm,CRs_Norm)
                print("Now filling other systematics...")

            else:
                if args.doNormExptSys == True:
                    print("Doing "+sys+" Systematic....")
                    if "ResoP" in sys:
                        dictExphists={}
                        dictExphists.update({sys:[Form('(%s)'%(weight_scl)),Form('(%s)'%(weight_scl_1)),Form('(%s)'%(weight_scl_2))]})
                        if os.path.isfile(os.path.join(EosDir,camp,'Systematics',sys,InSample_sys)):
                            myFile = TFile(os.path.join(EosDir,camp,'Systematics',sys,InSample_sys))
                            print("found Tree systematics")
                        else:
                            continue

                        SysDirName_SRs ='Systematics/'+sys
                        SysDirName_CRs ='Systematics_Background/'+sys
                        
                        chain_Syst_Expt = TChain("2l2nu_Trees_ExptSys")
                        if os.path.isfile(os.path.join(EosDir,camp,SysDirName_WgtSys,InSample_sys)):
                            chain_Syst_Expt.Add(os.path.join(EosDir,camp,SysDirName_SRs,InSample_sys,Nominal_tree))
                            Histo_Nom = TFile(os.path.join(EosDir,camp,SysDirName_WgtSys,InSample_sys))
                            infoHist_Nom = Histo_Nom.Get("Hist/hInfo_PFlow")
                            lumi = GetLumi(camp)
                            Nom_Norm = infoHist_Nom.GetBinContent(1)*2.0/infoHist_Nom.GetEntries()*lumi/infoHist_Nom.GetBinContent(2)
                            print("------------------------------")
                            print(Nom_Norm)
                                
                        else:
                            print("Systematic file does not exist")
                        if os.path.isfile(os.path.join(EosDir,camp,SysDirName_CRs,InSample)):
                            Bkg_CR_sys_File = TFile(os.path.join(EosDir,camp,SysDirName_CRs,InSample))
                            if Bkg_CR_sys_File.GetListOfKeys().Contains(CR2_3l_tree):
                                print(chain_Syst_Expt.GetEntries())
                                chain_Syst_Expt.Add(os.path.join(EosDir,camp,SysDirName_CRs,InSample_sys,CR2_3l_tree))
                            else:
                                print("CR2 Sys tree not found")
                            if Bkg_CR_sys_File.GetListOfKeys().Contains(CR2_3l_tree):
                                print(chain_Syst_Expt.GetEntries())
                                chain_Syst_Expt.Add(os.path.join(EosDir,camp,SysDirName_CRs,InSample_sys,CR1_emu_tree))
                                print(chain_Syst_Expt.GetEntries())
                            else:
                                print("CR1 Sys tree not found")

                        else:
                            print("Systematic file for CR does not exist")
                            
                        print(total)                            
                        #Save the histograms
                        if splitSample==True:
                            #print("splitting sample into two parts...")
                            weight_Indx=1
                            #print "Filling Experimental Sys: "+Expt_sys +  " Histogram Part 1"
                            write_hist(list_regions,weight_Indx,dictExphists,Dir_ExptSys,Process+Idenf_scl_1,camp,chain_Syst_Expt,InSample_sys,Nom_Norm,CRs_Norm)
                            weight_Indx=2
                            #print "Filling Experimental Sys: "+Expt_sys +  " Histogram Part 2"
                            write_hist(list_regions,weight_Indx,dictExphists,Dir_ExptSys,Process+Idenf_scl_2,camp,chain_Syst_Expt,InSample_sys,Nom_Norm,CRs_Norm)

                        weight_Indx=0
                        #print "Filling Experimental Sys: "+Expt_sys +  " histograms..."
                        write_hist(list_regions,weight_Indx,dictExphists,Dir_ExptSys,Process+Idenf,camp,chain_Syst_Expt,InSample_sys,Nom_Norm,CRs_Norm)

                    else:
                        for var in variations:
                            dictExphists={}
                            Expt_sys = sys+'__'+var
                            dictExphists.update({Expt_sys:[Form('(%s)'%(weight_scl)),Form('(%s)'%(weight_scl_1)),Form('(%s)'%(weight_scl_2))]})
                            if os.path.isfile(os.path.join(EosDir,camp,'Systematics',Expt_sys,InSample_sys)):
                                myFile = TFile(os.path.join(EosDir,camp,'Systematics',Expt_sys,InSample_sys))
                                print("found Tree systematics")
                            else:
                                continue

                            SysDirName_SRs ='Systematics/'+Expt_sys
                            SysDirName_CRs ='Systematics_Background/'+Expt_sys
                            
                            chain_Syst_Expt = TChain("2l2nu_Trees_ExptSys")
                            if os.path.isfile(os.path.join(EosDir,camp,SysDirName_WgtSys,InSample_sys)):
                                chain_Syst_Expt.Add(os.path.join(EosDir,camp,SysDirName_SRs,InSample_sys,Nominal_tree))
                                Histo_Nom = TFile(os.path.join(EosDir,camp,SysDirName_WgtSys,InSample_sys))
                                infoHist_Nom = Histo_Nom.Get("Hist/hInfo_PFlow")
                                lumi = GetLumi(camp)
                                Nom_Norm = infoHist_Nom.GetBinContent(1)*2.0/infoHist_Nom.GetEntries()*lumi/infoHist_Nom.GetBinContent(2)
                                print("------------------------------")
                                print(Nom_Norm)
                                
                            else:
                                print("Systematic file does not exist")

                            if os.path.isfile(os.path.join(EosDir,camp,SysDirName_CRs,InSample)):
                                Bkg_CR_sys_File = TFile(os.path.join(EosDir,camp,SysDirName_CRs,InSample))
                                if Bkg_CR_sys_File.GetListOfKeys().Contains(CR2_3l_tree):
                                    #print(chain_Syst_Expt.GetEntries())
                                    chain_Syst_Expt.Add(os.path.join(EosDir,camp,SysDirName_CRs,InSample_sys,CR2_3l_tree))
                                else:
                                    print("CR2 Sys tree not found")
                                if Bkg_CR_sys_File.GetListOfKeys().Contains(CR2_3l_tree):
                                    #print(chain_Syst_Expt.GetEntries())
                                    chain_Syst_Expt.Add(os.path.join(EosDir,camp,SysDirName_CRs,InSample_sys,CR1_emu_tree))
                                    #print(chain_Syst_Expt.GetEntries())
                                else:
                                    print("CR1 Sys tree not found")

                            else:
                                print("Systematic file for CR does not exist")
                            #if os.path.isfile(os.path.join(EosDir,camp,SysDirName_CRs,InSample.replace(".root","_CR.root"))):
                            #    chain_Syst.Add(os.path.join(EosDir,camp,SysDirName_CRs,InSample_sys.replace(".root","_CR.root"),CR1_emu_tree))
                            #    chain_Syst.Add(os.path.join(EosDir,camp,SysDirName_CRs,InSample_sys.replace(".root","_CR.root"),CR2_3l_tree))
                            
                            #chain_Nominal.AddFriend(chain_EW_corr, "tree_incl_all_EWCorr")
                            #chain_Syst_Expt.AddFriend(chain_EW_corr,"tree_incl_all_EWCorr")

                            print(total)                            
                            #Save the histograms
                            if splitSample==True:
                                #print("splitting sample into two parts...")
                                weight_Indx=1
                                #print "Filling Experimental Sys: "+Expt_sys +  " Histogram Part 1"
                                write_hist(list_regions,weight_Indx,dictExphists,Dir_ExptSys,Process+Idenf_scl_1,camp,chain_Syst_Expt,InSample_sys,Nom_Norm,CRs_Norm)
                                weight_Indx=2
                                #print "Filling Experimental Sys: "+Expt_sys +  " Histogram Part 2"
                                write_hist(list_regions,weight_Indx,dictExphists,Dir_ExptSys,Process+Idenf_scl_2,camp,chain_Syst_Expt,InSample_sys,Nom_Norm,CRs_Norm)
                            weight_Indx=0
                            #print "Filling Experimental Sys: "+Expt_sys +  " histograms..."
                            write_hist(list_regions,weight_Indx,dictExphists,Dir_ExptSys,Process+Idenf,camp,chain_Syst_Expt,InSample_sys,Nom_Norm,CRs_Norm)

                
if '__main__' in __name__:
    main()

