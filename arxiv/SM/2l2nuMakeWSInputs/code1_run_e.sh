outname="Final_WS_CR"
date="16Mar2022"
outputPath=./outputs_$date/hists_code1_$outname
outPlotsPath=./outputs_$date/Plots/

if [ ! -d outputs_${date} ]; then mkdir outputs_${date}; fi
if [ ! -d ${outputPath} ]; then mkdir ${outputPath}; fi
if [ ! -d ${outPlotsPath} ]; then mkdir ${outPlotsPath}; fi



##ggF Processes##

#pathToFiles=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hllvv/Run2/offshell_rel89/
pathToFiles=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hllvv/Run2/minitrees_rel139/
echo "--------------------------------------------------------------------------"
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_HIGG2D1.e6213_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_01.txt 
#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.345725.Sherpa_222_NNPDF30NNLO_ggllvvZZNoHiggs.deriv.DAOD_HIGG2D1.e6213_s3126_r10724_p4222.root --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_02.txt
#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.345726.Sherpa_222_NNPDF30NNLO_ggllvvZZOnlyHiggs.deriv.DAOD_HIGG2D1.e6213_s3126_r10724_p4222.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_03.txt 



##VBF Processes##

pathToFiles=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hllvv/Run2/minitrees_rel139/
#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.500379.MGPy8EG_A14_NNPDF23LO_VBFH125_bkg_2l2v_MET80.deriv.DAOD_HIGG2D1.e8065_s3126_r10724_p4222.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_04.txt
#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.500378.MGPy8EG_A14_NNPDF23LO_VBFH125_sbi_2l2v_MET80.deriv.DAOD_HIGG2D1.e8065_s3126_r10724_p4222.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_05.txt
#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.500380.MGPy8EG_A14_NNPDF23LO_VBFH125_sbi5_2l2v_MET80.deriv.DAOD_HIGG2D1.e8065_s3126_r10724_p4222.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_06.txt
#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.500381.MGPy8EG_A14_NNPDF23LO_VBFH125_sbi10_2l2v_MET80.deriv.DAOD_HIGG2D1.e8065_s3126_r10724_p4222.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_07.txt




pathToFiles=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hllvv/Run2/minitrees_rel139/
#pathToFiles=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hllvv/Run2/minitrees_rel75/
echo "--------------------------------------------------------------------------"

## WZ ##

#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364253.Sherpa_222_NNPDF30NNLO_lllv.deriv.DAOD_HIGG2D1.e5916_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_08.txt

## qqWW ##

#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.361600.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvlv.deriv.DAOD_HIGG2D1.e4616_s3126_r10724_p4220.root --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_10.txt

## ggWW ##

#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.345718.Sherpa_222_NNPDF30NNLO_ggllvvWW.deriv.DAOD_HIGG2D1.e6525_s3126_r10724_p4220.root --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_11.txt

## ttbar ##

#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_HIGG2D1.e6348_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_12.txt 




echo "--------------------------------------------------------------------------"
##qqZZ
pathToFiles=/afs/cern.ch/user/s/skrishna/eos/2l2v_qqZZ_Files/
#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.deriv.DAOD_HIGG2D1.e5894_s3126_r10724_p4191.root --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_13.txt

#pathToFiles=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hllvv/Run2/minitrees_rel139/qqZZ_EWCorrfix
pathToFiles=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hllvv/Run2/minitrees_rel139/
#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_HIGG2D1.e6240_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_14.txt


echo "--------------------------------------------------------------------------"
##Zjets
pathToFiles=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hllvv/Run2/minitrees_rel139/
:'
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364100.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_HIGG2D1.e5271_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_15.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364101.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_HIGG2D1.e5271_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_16.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364102.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_BFilter.deriv.DAOD_HIGG2D1.e5271_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_17.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_HIGG2D1.e5271_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_18.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_HIGG2D1.e5271_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_19.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364105.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_BFilter.deriv.DAOD_HIGG2D1.e5271_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_20.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364106.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_HIGG2D1.e5271_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_21.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364107.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_HIGG2D1.e5271_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_22.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364108.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_BFilter.deriv.DAOD_HIGG2D1.e5271_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_23.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364109.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_HIGG2D1.e5271_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_24.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_HIGG2D1.e5271_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_25.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364111.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_BFilter.deriv.DAOD_HIGG2D1.e5271_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_26.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364112.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV500_1000.deriv.DAOD_HIGG2D1.e5271_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_27.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364113.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV1000_E_CMS.deriv.DAOD_HIGG2D1.e5271_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_28.txt


python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364114.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_HIGG2D1.e5299_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_29.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364115.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_HIGG2D1.e5299_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_30.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364116.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_BFilter.deriv.DAOD_HIGG2D1.e5299_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_31.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364117.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_HIGG2D1.e5299_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_32.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364118.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_HIGG2D1.e5299_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_33.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364119.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_BFilter.deriv.DAOD_HIGG2D1.e5299_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_34.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364120.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_HIGG2D1.e5299_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_35.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364121.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_HIGG2D1.e5299_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_36.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364122.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_BFilter.deriv.DAOD_HIGG2D1.e5299_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_37.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364123.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_HIGG2D1.e5299_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_38.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364124.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_HIGG2D1.e5299_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_39.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364125.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_BFilter.deriv.DAOD_HIGG2D1.e5299_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_40.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364126.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV500_1000.deriv.DAOD_HIGG2D1.e5299_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_41.txt
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364127.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV1000_E_CMS.deriv.DAOD_HIGG2D1.e5299_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/e_output_42.txt


python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.410646.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_top.deriv.DAOD_HIGG2D1.e6552_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu 
python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.410647.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_antitop.deriv.DAOD_HIGG2D1.e6552_s3126_r10724_p4220.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu 
'






















pathToFiles=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hllvv/Run2/offshell_rel89/
##VBF Processes
#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.500379.MGPy8EG_A14_NNPDF23LO_VBFH125_bkg_2l2v_MET80.deriv.DAOD_HIGG2D1.e8065_s3126_r10724_p3714.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu 
#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.500378.MGPy8EG_A14_NNPDF23LO_VBFH125_sbi_2l2v_MET80.deriv.DAOD_HIGG2D1.e8065_s3126_r10724_p3714.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu 
#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.500380.MGPy8EG_A14_NNPDF23LO_VBFH125_sbi5_2l2v_MET80.deriv.DAOD_HIGG2D1.e8065_s3126_r10724_p3714.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu
#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.500381.MGPy8EG_A14_NNPDF23LO_VBFH125_sbi10_2l2v_MET80.deriv.DAOD_HIGG2D1.e8065_s3126_r10724_p3714.root  --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu

#pathToFiles=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hllvv/Run2/minitrees_rel139/
#python make_all_hists_code1.py --inpath $pathToFiles  --sample mc16_13TeV.364284.Sherpa_222_NNPDF30NNLO_lllvjj_EW6.deriv.DAOD_HIGG2D1.e6055_s3126_r10724_p4220.root --outpath $outputPath --tree tree_PFLOW -c mc16e --channel 2l2nu >txt_code1_files/d_output_09.txt


