from ROOT import *
import sys
sys.path.append('./Systematic_names/')
import os
import os.path
from os import path
from glob import glob
from array import array
import math
import argparse
import List_Sys
from array import array
import numpy as np
import pickle
from numpy import linalg as LA

gROOT.SetBatch(kTRUE)
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--inpath',  type=str, default='./Merged_hists_code1_binnedCR', help='Add path to merged histograms')
parser.add_argument('--outpath', type=str, default='./Merged_hists_code2_binnedCR', help='Add path to output histograms')
parser.add_argument('--inProc',  type=str, default='qqBkg', help='Add the process name you want')
parser.add_argument('--RemoveqqZZNorm', type=bool, default=True, help='remove qqZZ norm uncern. ')

#Reading in the arguments
args = parser.parse_args()
HistPath = args.inpath
ProcName = args.inProc
qqZZNoNorm = args.RemoveqqZZNorm

#Directories in the root files
variations = ["1up", "1down"]
List_dirs=["Nominal", "Theosys","Exptsys",]

#Getting the list of regions with binning/ observables - output of code 1
file_regions = open('regions_of_interest.txt','r')
regions_list = pickle.load(file_regions)
file_regions.close()

#PCA for PDF Sys
def doPCA_PDF_sys(List_PDF_vars, Nomhist, gDirectory):
    pdf_mat =[]
    pdf_hists =[]
    #for pdf_vars in List_Sys.Sherpa_samples_PDF_sys_dict['PDF_vars']:
    for pdf_vars in List_PDF_vars:
        #print(pdf_vars)
        hist_pdf = gDirectory.Get(pdf_vars)
        bin_vals = []
        for i in range(1, hist_pdf.GetNbinsX()+1):
            bin_vals.append(hist_pdf.GetBinContent(i))
        pdf_mat.append(bin_vals)
    print(np.shape(pdf_mat))
    Transpose_pdf_mat = np.array(pdf_mat).T
    print(np.shape(Transpose_pdf_mat))
    cov_matrix = (np.cov(Transpose_pdf_mat))
    print(cov_matrix)
    lambdas, vectors = LA.eig(cov_matrix)
    index_max = np.argmax(lambdas)
    print(index_max)
    print(lambdas[index_max])
    print(vectors[index_max])
    print("lambdas")
    print(lambdas)
    print("index max")
    print(index_max)
    print("lambdas[index_max]")
    print(lambdas[index_max+1])
    print("vectors[index_max]")
    print(vectors[index_max+1])

    histName = "PDF_var_"
    bin_edges  = np.array(np.linspace(0, len(vectors[index_max+1])+1,len(vectors[index_max+1])+1), dtype='float64')
    Pdfhist_up = TH1F(histName+"_1up",   histName+"_up",   len(bin_edges)-1, bin_edges)
    Pdfhist_dn = TH1F(histName+"_1down", histName+"_down", len(bin_edges)-1, bin_edges)
    bins = Nomhist.GetNbinsX()
    for i in range(1,bins+1):
        upVar =Nomhist.GetBinContent(i)+ ((vectors[index_max+1][i-1])*np.sqrt(lambdas[index_max+1]))
        dnVar =Nomhist.GetBinContent(i)- ((vectors[index_max+1][i-1])*np.sqrt(lambdas[index_max+1]))
        print("bin: ", i)
        print("Nominal: ", Nomhist.GetBinContent(i))
        print("upVar: ", upVar)
        print("upVar diff: ", (upVar- Nomhist.GetBinContent(i))/Nomhist.GetBinContent(i))
        print("dnVar: ", dnVar)
        print("dnVar diff: ", (dnVar-Nomhist.GetBinContent(i))/Nomhist.GetBinContent(i))

        Pdfhist_up.SetBinContent(i, upVar)
        Pdfhist_dn.SetBinContent(i, dnVar)
    print("PCA PDF upVar Integral: "+str(Pdfhist_up.Integral()))
    print("PCA PDF dnVar Integral: "+str(Pdfhist_dn.Integral()))
    pdf_hists.append(Pdfhist_up)
    pdf_hists.append(Pdfhist_dn)
    return pdf_hists

def write_hists(ProcName,Output_Dir,listHists):
    OutFolder = args.outpath
    OutSample  = ProcName+"_hists.root"
    if path.isdir(OutFolder):
        if path.isfile(os.path.join(OutFolder,OutSample)):
            fOut_FromTree = TFile(os.path.join(OutFolder,OutSample), "UPDATE")
            print('root file exits, updating it ...')
        else:
            fOut_FromTree = TFile(os.path.join(OutFolder,OutSample), "CREATE")
            print('root file doesnt exits, creating it ..')
    else:
        print('making a new directory and root file ...')
        os.mkdir(OutFolder)
        fOut_FromTree = TFile(os.path.join(OutFolder,OutSample), "CREATE")
    
    directory = fOut_FromTree.GetDirectory(Output_Dir)
    if not (directory):
        #print ">>> created " + Output_Dir
        directory = fOut_FromTree.mkdir(Output_Dir)
    directory.cd()
    for hist in listHists:
        #print "Writing...", hist
        #print(hist.Integral())
        hist.Write()
        hist.SetDirectory(0)
    fOut_FromTree.Write("",TFile.kOverwrite)
    fOut_FromTree.Close()

def main():
    list_systematics=[]
    if not path.isfile(os.path.join(HistPath,ProcName+"_hists.root")):
        print("File "+ProcName+"_hists.root does not exist")
    else:
        if "qq" in ProcName:
            if qqZZNoNorm == True:
                print("Removing qqZZ Norm uncern.")
            
        HistFile = TFile(os.path.join(HistPath,ProcName+"_hists.root"))
        print("doing "+ProcName+" Process")
        for dirs in List_dirs:
            Nom_hist_files  = []
            Theo_hist_files = []
            Expt_hist_files = []

            directory = HistFile.GetDirectory(dirs)
            if not (directory):
                print(dirs+" does not exist.")
                continue
            HistFile.cd(dirs)
                        
            if "Nom" in dirs:
                Nomhist = gDirectory.Get("Nom_hist")
                if Nomhist:
                    Nom_hist_files.append(Nomhist)
                    print("Nominal Histogram Integral : "+str(Nomhist.Integral()))
                    Int_NomHist = Nomhist.Integral(0, Nomhist.GetNbinsX()+1)
                    print("Nominal Histogram Integral (with overflow) : "+str(Int_NomHist))

                else:
                    print(Nomhist+" does not exist.")            
                write_hists(ProcName, dirs, Nom_hist_files)

            if "Theo" in dirs:

                if "VBF" in ProcName:
                    PDF_PCA_hists = doPCA_PDF_sys(List_Sys.MadGraph_samples_PDF_sys_dict['PDF_vars'], Nomhist, gDirectory)
                    list_variations=[]
                    for PDFhist in PDF_PCA_hists:
                        Theo_hist_files.append(PDFhist)
                    
                if "gg" in ProcName:
                    PDF_PCA_hists = doPCA_PDF_sys(List_Sys.Sherpa_samples_PDF_sys_dict['PDF_vars'], Nomhist, gDirectory)
                    list_variations=[]
                    for PDFhist in PDF_PCA_hists:
                        Theo_hist_files.append(PDFhist)

                if "qq" in ProcName:
                    part = (ProcName.split("_"))[1]
                    print("doing: "+part)
                    pdf_hist_up = gDirectory.Get("PDF_var_"+part+"_1up")
                    print("PDF up Hist integral (with overflow): "+str(pdf_hist_up.Integral(1,pdf_hist_up.GetNbinsX()+1)))
                    pdf_hist_up.SetName("PDF_var__1up")
                    pdf_hist_dn = gDirectory.Get("PDF_var_"+part+"_1down")
                    print("PDF dn Hist integral (with overflow): "+str(pdf_hist_dn.Integral(1,pdf_hist_dn.GetNbinsX()+1)))
                    pdf_hist_dn.SetName("PDF_var__1down")
                    if qqZZNoNorm== True:
                        pdf_hist_up.Scale(Int_NomHist/(pdf_hist_up.Integral(1, pdf_hist_up.GetNbinsX()+1)))
                        print("Now renormed to: "+str(pdf_hist_up.Integral()))
                        print("Now renormed to (with overflow): "+str(pdf_hist_up.Integral(1,pdf_hist_up.GetNbinsX()+1)))
                        pdf_hist_dn.Scale(Int_NomHist/(pdf_hist_dn.Integral(1, pdf_hist_dn.GetNbinsX()+1)))
                        print("Now renormed to: "+str(pdf_hist_dn.Integral()))
                        print("Now renormed to (with overflow): "+str(pdf_hist_dn.Integral(1,pdf_hist_dn.GetNbinsX()+1)))

                    Theo_hist_files.append(pdf_hist_up)
                    Theo_hist_files.append(pdf_hist_dn)

                for systematics in List_Sys.sys_to_process[ProcName]:
                    for listsys in systematics.keys():
                        print(listsys)
                        if "PDF" not in listsys:
                            if "qq" in ProcName:
                                part = (ProcName.split("_"))[1]
                                print("doing: "+part)
                                qcd_ScFac_Histos = []
                                qcd_ScFac_Intgrals =[]
                                if "qqZZ_scale_fac" in listsys:
                                    for qcd_ScFac in systematics[listsys]:
                                        ScFac_hist = gDirectory.Get(qcd_ScFac)
                                        qcd_ScFac_Histos.append(ScFac_hist)
                                        qcd_ScFac_Intgrals.append(ScFac_hist.Integral())
                                    max_hist = np.argmax(qcd_ScFac_Intgrals)
                                    qcd_ScFac_Histos[max_hist].SetName('weight_var_HOQCD_syst_'+part+'__1up')
                                    print(str(qcd_ScFac_Histos[max_hist].GetName())+ " Integral: "+str(qcd_ScFac_Histos[max_hist].Integral()))
                                    print(str(qcd_ScFac_Histos[max_hist].GetName())+ " overflow: "+str(qcd_ScFac_Histos[max_hist].GetBinContent(qcd_ScFac_Histos[max_hist].GetNbinsX()+1)))
                                    print(str(qcd_ScFac_Histos[max_hist].GetName())+ " underflow: "+str(qcd_ScFac_Histos[max_hist].GetBinContent(0)))

                                    print(str(qcd_ScFac_Histos[max_hist].GetName())+ " Integral (with overflow): "+str(qcd_ScFac_Histos[max_hist].Integral(1, qcd_ScFac_Histos[max_hist].GetNbinsX()+1)))
                                    min_hist = np.argmin(qcd_ScFac_Intgrals)
                                    qcd_ScFac_Histos[min_hist].SetName('weight_var_HOQCD_syst_'+part+'__1down')
                                    print(str(qcd_ScFac_Histos[min_hist].GetName())+ " Integral: "+str(qcd_ScFac_Histos[min_hist].Integral()))
                                    print(str(qcd_ScFac_Histos[min_hist].GetName())+ " overflow: "+str(qcd_ScFac_Histos[min_hist].GetBinContent(qcd_ScFac_Histos[min_hist].GetNbinsX()+1)))
                                    print(str(qcd_ScFac_Histos[max_hist].GetName())+ " underflow: "+str(qcd_ScFac_Histos[min_hist].GetBinContent(0)))
                                    print(str(qcd_ScFac_Histos[min_hist].GetName())+ " Integral (with overflow): "+str(qcd_ScFac_Histos[min_hist].Integral(1,qcd_ScFac_Histos[min_hist].GetNbinsX()+1)))

                                    if qqZZNoNorm== True:
                                        qcd_ScFac_Histos[max_hist].Scale(Int_NomHist/(qcd_ScFac_Histos[max_hist].Integral(1, qcd_ScFac_Histos[max_hist].GetNbinsX()+1)))
                                        print("Now renormed to: "+str(qcd_ScFac_Histos[max_hist].Integral()))
                                        print("overflow: "+str(qcd_ScFac_Histos[max_hist].Integral(qcd_ScFac_Histos[max_hist].GetNbinsX(), qcd_ScFac_Histos[max_hist].GetNbinsX()+1)))
                                        print("Now renormed to (with overflow): "+str(qcd_ScFac_Histos[max_hist].Integral(1, qcd_ScFac_Histos[max_hist].GetNbinsX()+1)))
                                        
                                        qcd_ScFac_Histos[min_hist].Scale(Int_NomHist/(qcd_ScFac_Histos[min_hist].Integral(1, qcd_ScFac_Histos[min_hist].GetNbinsX()+1)))
                                        print("Now renormed to: "+str(qcd_ScFac_Histos[min_hist].Integral()))
                                        print("overflow: "+str(qcd_ScFac_Histos[min_hist].Integral(qcd_ScFac_Histos[min_hist].GetNbinsX(), qcd_ScFac_Histos[min_hist].GetNbinsX()+1)))
                                        print("Now renormed to (with overflow): "+str(qcd_ScFac_Histos[min_hist].Integral(1, qcd_ScFac_Histos[min_hist].GetNbinsX()+1)))
                                    Theo_hist_files.append(qcd_ScFac_Histos[max_hist]) 
                                    Theo_hist_files.append(qcd_ScFac_Histos[min_hist])
                                
                                elif "qqZZ_NLO_corr" in listsys:
                                    for HOcorr in systematics[listsys]:
                                        for var in variations:
                                            print("-------------------------------------------------------------------------------")
                                            HOcorr_var = HOcorr+'__'+var 
                                            print(HOcorr_var)
                                            HOcorr_hist = gDirectory.Get(HOcorr_var)
                                            sys_name = ((HOcorr_hist.GetName()).split('__'))[0]
                                            print(HOcorr_hist.Integral())
                                            print(HOcorr_hist.Integral(0, HOcorr_hist.GetNbinsX()+1))
                                            if "up" in var:
                                                HOcorr_hist.SetName(sys_name+'_'+part+'__1up')
                                            else:
                                                HOcorr_hist.SetName(sys_name+'_'+part+'__1down')
                                            Theo_hist_files.append(HOcorr_hist)
                                            
                                else:
                                    for sys in systematics[listsys]:
                                        list_variations=[]
                                        for var in variations:
                                            sys_vars = sys+'__'+var
                                            print(sys_vars)
                                            print("-------------------------------------------------------------------------------------")
                                            Theohist = gDirectory.Get(sys_vars)
                                            if not (Theohist):
                                                print(sys_vars+"does not exist.")
                                                continue
                                            #print(sys_vars+ " Integral: "+str(Theohist.Integral()))
                                            if "qq" in ProcName:
                                                if qqZZNoNorm== True:
                                                    Theohist.Scale(Int_NomHist/(Theohist.Integral(1, Theohist.GetNbinsX()+1)))
                                                    print("Now renormed to: "+str(Theohist.Integral()))
                                                    print("Now renormed to (with overflow): "+str(Theohist.Integral(1, Theohist.GetNbinsX()+1)))
                                            Theo_hist_files.append(Theohist)
                                
                            else:
                                for sys in systematics[listsys]:
                                    list_variations=[]
                                    for var in variations:
                                        sys_vars = sys+'__'+var
                                        print(sys_vars)
                                        print("-------------------------------------------------------------------------------------")
                                        Theohist = gDirectory.Get(sys_vars)
                                        if not (Theohist):
                                            print(sys_vars+"does not exist.")
                                            continue
                                        #print(sys_vars+ " Integral: "+str(Theohist.Integral()))
                                        if "qq" in ProcName:
                                            if qqZZNoNorm== True:
                                                Theohist.Scale(Int_NomHist/(Theohist.Integral(1, Theohist.GetNbinsX()+1)))
                                                print("Now renormed to: "+str(Theohist.Integral()))
                                                print("Now renormed to (with overflow): "+str(Theohist.Integral(1, Theohist.GetNbinsX()+1)))
                                        Theo_hist_files.append(Theohist)
                                
                write_hists(ProcName, dirs, Theo_hist_files)
                

            if "Expt" in dirs:
                for Exp_norm_sys in List_Sys.Experimental_sys_Norm['Exp_Norm_sys']:
                    #print()
                    list_variations=[]
                    for var in variations:
                        sys_vars = Exp_norm_sys+'__'+var
                        Expthist = gDirectory.Get(sys_vars)
                        if not (Expthist):
                            print(sys_vars+" does not exist.")
                            continue
                        #print(sys_vars+ " Integral: "+str(Expthist.Integral()))
                        if qqZZNoNorm== True:
                            if "qq" in ProcName:
                                Expthist.Scale(Int_NomHist/(Expthist.Integral(1, Expthist.GetNbinsX()+1)))
                                print("Now renormed to: "+str(Expthist.Integral()))
                                print("Now renormed to (with overflow) : "+str(Expthist.Integral(1, Expthist.GetNbinsX()+1)))
                        Expt_hist_files.append(Expthist)                                
                
                for systematics in List_Sys.Systematics_dict['sys']:
                    if systematics != 'NormSystematic':
                        #print(systematics)
                        if "ResoP" in systematics:
                            Expthist = gDirectory.Get(systematics)
                            if not (Expthist):
                                print(systematics+" does not exist.")
                                continue
                            #print(systematics+ " Integral: "+str(Expthist.Integral()))
                            if "qq" in ProcName:
                                if qqZZNoNorm== True:
                                    Expthist.Scale(Int_NomHist/(Expthist.Integral(1, Expthist.GetNbinsX()+1)))
                                    print("Now renormed to: "+str(Expthist.Integral()))
                                    print("Now renormed to(with overflow): "+str(Expthist.Integral(1, Expthist.GetNbinsX()+1)))
                            Expt_hist_files.append(Expthist)

                        else:
                            list_variations=[]
                            for var in variations:
                                Expthist = gDirectory.Get(systematics+'__'+var)
                                if not (Expthist):
                                    print(systematics+'__'+var+" does not exist.")
                                    continue
                                #print(systematics+'__'+var+ " Integral: "+str(Expthist.Integral()))
                                if "qq" in ProcName:
                                    if qqZZNoNorm== True:
                                        Expthist.Scale(Int_NomHist/(Expthist.Integral(1, Expthist.GetNbinsX()+1)))
                                        print("Now renormed to: "+str(Expthist.Integral()))
                                        print("Now renormed to (with overflow): "+str(Expthist.Integral(1, Expthist.GetNbinsX()+1)))
                                Expt_hist_files.append(Expthist)

                write_hists(ProcName, dirs, Expt_hist_files)

if '__main__' in __name__:
    main()
