
#outname="Bin_last_p14_cp9"
#outname="Bin_last_p15_cp9"
#outname="Bin_last_p16_cp9"
outname="NomTrees"
outname="SysTrees"
outname="METrees"
#outname="Bin_last_p40_cp9"

if [ ! -d Merged_code2_$outname ]; then mkdir Merged_code2_$outname; fi
#if [ ! -d ${outPlotsPath} ]; then mkdir ${outPlotsPath}; fi

#rm -rf Merged_code2_$outname
#mkdir Merged_code2_$outname
#mkdir Merged_hists_code2/WS_inputs
#--outpath $outputPath
inPath="./Merged_code1_"$outname
outputPath="./Merged_code2_"$outname

python make_sys_hists_code2.py --inProc ggSNLO --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc ggSNLOS --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc ggSNLOI --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc ggINLO --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc ggSBINLOI --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc ggBNLO --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc ggBNLOB --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc ggBNLOI --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc qqZZ_Part1 --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc qqZZ_Part2 --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc qqZZ_Part3 --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc VBFB --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc VBFI --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc VBFS --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc VBFSBI --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc VBFSBI5 --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc VBFSBI10 --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc VBFSBIS --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc VBFSBI5S --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc VBFSBI10S --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc VBFSBII --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc VBFSBI5I --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_sys_hists_code2.py --inProc VBFSBI10I --inpath $inPath --outpath $outputPath
#echo "Code to determine high S/B bins for pruning"
#python3 plotter_code.py
