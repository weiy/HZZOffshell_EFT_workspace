#outname="Bin_last_p14_cp9"
#outname="Bin_last_p15_cp9"
#outname="Bin_last_p16_cp9"
#outname="NomTrees"
outname="SysTrees"
#outname="METrees"
#outname="Bin_last_p40_cp9"

rm -rf WS_inputs_$outname
rm -rf NP_names.txt
mkdir WS_inputs_$outname

#HighSoverB=4,11,12
HSOB_Threshold=1
LSOB_Threshold=8         

inPath="./Merged_code2_"$outname
echo $inPath
outputPath=WS_inputs_$outname

#python make_WSInputs_code3.py --inProc ggSNLO  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold
#echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc ggSNLOS  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc ggSNLOI  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc ggSBINLOI  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc ggBNLOI  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc ggBNLOB  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc ggBNLOI  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc qqZZ_Part1  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc qqZZ_Part2  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc qqZZ_Part3  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc VBFB  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc VBFI  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc VBFS  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc VBFSBI  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc VBFSBI5  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc VBFSBI10  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc VBFSBIS  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc VBFSBI5S  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc VBFSBI10S  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc VBFSBII  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc VBFSBI5I  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath
echo "--------------------------------------------------------------------------"
python make_WSInputs_code3.py --inProc VBFSBI10I  --Threshold_HSOB $HSOB_Threshold --Threshold_LSOB $LSOB_Threshold --inpath $inPath --outpath $outputPath

echo "Writing NP list"
python make_NPfile.py --outpath $outputPath
echo "Writing Config file"
python make_config_file.py --FileName config_file_3SRs_$outname.ini --inpath /afs/cern.ch/user/s/skrishna/work/HZZ/offshellstatistics/PP_Prep_3SR/$outputPath
