import sys
sys.path.append('./Systematic_names/')
import os
import os.path
from os import path
from glob import glob
from array import array
import math
import argparse
import List_Sys
from array import array
import numpy as np
import pickle

parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--outpath', type=str, default='./Workspace_inputs_binnedCR', help='Add path to output histograms')
args = parser.parse_args()
outFolder = args.outpath


NP_names  = outFolder+"/"+"nuisance.txt"
NP_noSys  = outFolder+"/"+"nuisance_noSys.txt"

NP_names_file = open(NP_names,"w")
NP_names_file.write("#global systematics:\n")
NP_names_file.write("ATLAS_LUMI\n")
NP_names_outFile = ("./NP_names.txt")
with open (NP_names_outFile, 'rb') as NP:
    NP_names = pickle.load(NP)

NP_names_set = set(NP_names)

for name in NP_names_set:
    NP_names_file.write((str(name)).replace('weight_var_','')+"\n")

NP_noSys_file = open(NP_noSys,"w")
NP_noSys_file.write("#global systematics:\n")
NP_noSys_file.write("ATLAS_LUMI\n\n")


