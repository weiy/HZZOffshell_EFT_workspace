#python3 needed
import uproot3 as uproot 
import os

merged_dir = '/home/weiy/Desktop/HZZOffshell_EFT_workspace/2l2nu_WS_Inputs_EFT/outputs_16Mar2022/Merged_code1_2l2v_EFT_WS_v1/'

samples = ['OpgSM_hists.root',
           'OtpSM_hists.root',
           'OptSM_hists.root',
           'OpgSqrd_hists.root',
           'OtpSqrd_hists.root',
           'OptSqrd_hists.root',
           'OpgOtp_hists.root',
           'qqZZ_hists.root',
           'ggZZ_hists.root',
           'tot_emu_bkg_hists.root',
           'WZ_3lep_bkg_hists.root',
           'Zjets_bkg_hists.root']

for sample in samples:
    f_merged = uproot.open(merged_dir + sample)
    Nom_hist = f_merged['Nominal']['Nom_hist']
    print('n_'+sample.replace('_hists.root',''),end='')
    for i in range(4, len(Nom_hist)-1):
        print(' & ' + "%.6f" % (Nom_hist[i]/139.0), end='')
    print()

print()
