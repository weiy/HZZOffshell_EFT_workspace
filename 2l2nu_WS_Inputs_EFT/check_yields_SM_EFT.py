import numpy as np

f=open('/afs/cern.ch/user/m/mveen/public/Workspaces_llvv/WS_15_08_2022/WS_inputs_Final_WS_22_08_15/bgyields139.txt')
samples=['n_qqZZ_1_Part0','n_qqZZ_1_Part1','n_qqZZ_1_Part2','n_qqZZ_2_Part0','n_qqZZ_2_Part1','n_qqZZ_2_Part2','n_ggSBINLOI','n_ggBNLO','n_ggSNLO','n_VBFSBI','n_WZ_3lep_Part0','n_WZ_3lep_Part1','n_WZ_3lep_Part2','n_Zjets_bkg','n_tot_emu_bkg','n_Other_bkg']

def add_lists(lists):
    added_lists = [0]*len(lists[0])
    for j in range(len(lists[0])):
        for list_i in lists:
            added_lists[j] += list_i[j]
    return added_lists

def get_CR(list0):
    return list0[0]+list0[1]+list0[2],list0[3],list0[4]

def get_SR(list0):
    return list0[5:]
n = {}
for line in f:
    line=line.rstrip()
    for sample in samples:
        if sample == line.split()[0]:
            n[sample]=[float(a) for a in line.split('&')[1:]]
            print(sample+": "+str(n[sample]))

print('')

n['n_qqZZ_4l']=add_lists([n['n_qqZZ_1_Part0'],
                          n['n_qqZZ_1_Part1'],
                          n['n_qqZZ_1_Part2']])

n['n_qqZZ_2l2v']=add_lists([n['n_qqZZ_2_Part0'],
                            n['n_qqZZ_2_Part1'],
                            n['n_qqZZ_2_Part2']])

n['n_ggZZ']=n['n_ggSBINLOI']

n['n_WZ_3lep']=add_lists([n['n_WZ_3lep_Part0'],
                          n['n_WZ_3lep_Part1'],
                          n['n_WZ_3lep_Part2']])

#qqZZ_4l is done!!
#print(n['n_qqZZ_4l'])
#print(get_CR(n['n_qqZZ_4l']))
#print(get_SR(n['n_qqZZ_4l']))

#qqZZ_2l2v is done!!
#print(n['n_qqZZ_2l2v'])
#print(get_CR(n['n_qqZZ_2l2v']))
#print(get_SR(n['n_qqZZ_2l2v']))

#ggZZ is done!! but need to apply 1.1
#!!!!!!!!!!!!!!!!!!!!!!important!!!!!!!!!!!!!!!!!!!!!!!#
#print(n['n_ggZZ'])
#print( np.array(get_CR(n['n_ggZZ']))/1.1 )
#print( list(np.array(get_SR(n['n_ggZZ']))/1.1) )


#VBF is done!!
#print(n['n_VBFSBI'])
#print('VBF')
#print( get_CR(n['n_VBFSBI']) )
#print( get_SR(n['n_VBFSBI']) )

#WZ_3lep is done!!
#print(n['n_WZ_3lep'])
#print('WZ_3lep')
#print( get_CR(n['n_WZ_3lep']) )
#print( get_SR(n['n_WZ_3lep']) )


#Zjets is done!!
#print('Z+jets')
#print( get_CR(n['n_Zjets_bkg']) )
#print( get_SR(n['n_Zjets_bkg']) )

#n_tot_emu_bkg is done!!
#print('n_tot_emu_bkg')
#print( get_CR(n['n_tot_emu_bkg']) )
#print( get_SR(n['n_tot_emu_bkg']) )

#n_others_bkg is done!!
print('n_Other_bkg')
print( get_CR(n['n_Other_bkg']) )
print( get_SR(n['n_Other_bkg']) )
