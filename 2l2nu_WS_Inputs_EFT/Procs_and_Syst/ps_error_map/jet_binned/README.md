using: https://gitlab.cern.ch/HZZ/HZZSoftware/HZZOffshell/-/tree/2l2v_partonshower/2l2v_partonshower/ps_error_map_Xs_normalized

The "normalized" name is misleading. The actual ps error files are not normalized. These files are used in the standard analysis.