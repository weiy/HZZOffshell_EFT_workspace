import sys
import os
import List_Procs

var_suffix = ["__1up","__1down"]
def updown(sys_list):
    return [ i+j for i in sys_list for j in var_suffix]

################Theory uncertainties###############
#ggZZ
ggF_KfactorNLO_sys = updown(['weight_QCD_ggZZkSBI'])

#!!!check and rename
ggZZ_Shower_sys = updown(['llvv_PS_ggZZ_CKKW',
                          'llvv_PS_ggZZ_CSSKIN',
                          'llvv_PS_ggZZ_QSF'])

#qqZZ
#QCD systematics
Sherpa_QCD_scale_factors = ['weight_var_th_MUR0p5_MUF0p5_PDF261000',
                            'weight_var_th_MUR0p5_MUF1_PDF261000',
                            'weight_var_th_MUR1_MUF0p5_PDF261000',
                            'weight_var_th_MUR1_MUF1_PDF261000',
                            'weight_var_th_MUR1_MUF2_PDF261000',
                            'weight_var_th_MUR2_MUF1_PDF261000',
                            'weight_var_th_MUR2_MUF2_PDF261000']

qqZZ_EW_2l2v=updown(['weight_qqZZNLO_EW']) #check!!!

#in 2l2v channel, we only have HOEW_QCD mixed syst. Actuall in both 4l and 2l2v channels, HOEW only syst is very small, the variation is about 0.07% which can be neglected for sure
#qqZZ_NLO_corr = updown(['weight_var_HOEW_QCD_syst',
#                        'weight_var_HOEW_syst'])

qqZZ_Shower_sys = updown(['llvv_PS_qqZZ_CKKW',
                          'llvv_PS_qqZZ_CSSKIN',
                          'llvv_PS_qqZZ_QSF'])

#weight_var_th_MUR1_MUF1_PDF261001-100
Sherpa_PDF_replica = ["weight_var_th_MUR1_MUF1_PDF261{0:03}".format(j) for j in range(0,100+1)]

Powheg_QCD_scale_factors = ['weight_var_th_nominal', # nominal first
                            'weight_var_th_muR0p5_muF0p5',
                            'weight_var_th_muR0p5_muF1p0',
                            'weight_var_th_muR1p0_muF0p5',
                            'weight_var_th_muR1p0_muF2p0',
                            'weight_var_th_muR2p0_muF1p0',
                            'weight_var_th_muR2p0_muF2p0']


VBF_PDF_replica=['weight_var_th_MUR1p0_MUF1p0_PDF260{:03d}'.format(j) for j in range(0,100+1)]

VBF_QCD_scale_factors = ['weight_var_th_MUR1p0_MUF1p0_PDF260000', # nominal first
                         'weight_var_th_MUR0p5_MUF0p5_PDF260000',
                         'weight_var_th_MUR0p5_MUF1p0_PDF260000',
                         'weight_var_th_MUR1p0_MUF0p5_PDF260000',
                         'weight_var_th_MUR1p0_MUF2p0_PDF260000',
                         'weight_var_th_MUR2p0_MUF1p0_PDF260000',
                         'weight_var_th_MUR2p0_MUF2p0_PDF260000']

VBF_Shower_sys = updown(['llvv_PS_VBF_EIG',
                         'llvv_PS_VBF_HAD',
                         'llvv_PS_VBF_PDF',
                         'llvv_PS_VBF_RE'])

EFT_scale_factors_MUF_MUR = [#MUF and MUR
                     "weight_var_th_MUR05_MUF05_PDF261000",
                     "weight_var_th_MUR05_MUF10_PDF261000",
                     "weight_var_th_MUR05_MUF20_PDF261000",
                     "weight_var_th_MUR10_MUF05_PDF261000",
                     "weight_var_th_MUR10_MUF10_PDF261000",
                     "weight_var_th_MUR10_MUF20_PDF261000",
                     "weight_var_th_MUR20_MUF05_PDF261000",
                     "weight_var_th_MUR20_MUF10_PDF261000",
                     "weight_var_th_MUR20_MUF20_PDF261000",
                     ]

EFT_scale_factors_PDF_var = [#PDFset variations
                     "weight_var_th_MUR10_MUF10_PDF13000",#CT14nnlo
                     "weight_var_th_MUR10_MUF10_PDF25300",#MMHT2014nnlo68cl
                     "weight_var_th_MUR10_MUF10_PDF91400",#PDF4LHC15_nnlo_30_pdfas
                     ]

EFT_scale_factors_alphaS = [#alpha_S
                     "weight_var_th_MUR10_MUF10_PDF269000",#NNPDF30_nnlo_as_0117
                     "weight_var_th_MUR10_MUF10_PDF270000",#NNPDF30_nnlo_as_0119
                     ]

#EFT
EFT_scale_factors = EFT_scale_factors_MUF_MUR + EFT_scale_factors_PDF_var + EFT_scale_factors_alphaS


#weight_var_th_MUR10_MUF10_PDF261001-100
EFT_PDF_replica=["weight_var_th_MUR10_MUF10_PDF261{0:03}".format(j) for j in range(0,100+1)]

EFT_Shower_sys_muf_muR = ["weight_var_th_isrmuRfac05_fsrmuRfac05",
                          "weight_var_th_isrmuRfac05_fsrmuRfac10",
                          "weight_var_th_isrmuRfac05_fsrmuRfac20",
                          "weight_var_th_isrmuRfac0625_fsrmuRfac10",
                          "weight_var_th_isrmuRfac075_fsrmuRfac10",
                          "weight_var_th_isrmuRfac0875_fsrmuRfac10",
                          "weight_var_th_isrmuRfac10_fsrmuRfac05",
                          "weight_var_th_isrmuRfac10_fsrmuRfac0625",
                          "weight_var_th_isrmuRfac10_fsrmuRfac075",
                          "weight_var_th_isrmuRfac10_fsrmuRfac0875",
                          "weight_var_th_isrmuRfac10_fsrmuRfac125",
                          "weight_var_th_isrmuRfac10_fsrmuRfac15",
                          "weight_var_th_isrmuRfac10_fsrmuRfac175",
                          "weight_var_th_isrmuRfac10_fsrmuRfac20",
                          "weight_var_th_isrmuRfac125_fsrmuRfac10",
                          "weight_var_th_isrmuRfac15_fsrmuRfac10",
                          "weight_var_th_isrmuRfac175_fsrmuRfac10",
                          "weight_var_th_isrmuRfac20_fsrmuRfac05",
                          "weight_var_th_isrmuRfac20_fsrmuRfac10",
                          "weight_var_th_isrmuRfac20_fsrmuRfac20"]

EFT_Shower_sys_others = ["weight_var_th_Var3cDown",
                         "weight_var_th_Var3cUp",
                         "weight_var_th_hardHi",
                         "weight_var_th_hardLo",
                         "weight_var_th_isrPDFminus",
                         "weight_var_th_isrPDFplus"]

EFT_Shower_sys = EFT_Shower_sys_others + EFT_Shower_sys_muf_muR

####################################################

#############Experimental uncertainties#############
NormExp_sys = updown(["weight_EL_EFF_ID_CorrUncertaintyNP"+str(j) for j in range(0,15+1)] \
                   + ["weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP"+str(j) for j in range(0,17+1)] \
                   + ["weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR",
                      "weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR",
                      "weight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR",
                      "weight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR",
                      #"weight_FT_EFF_B_systematics",
                      #"weight_FT_EFF_C_systematics",
                      #"weight_FT_EFF_Light_systematics",
                      #"weight_FT_EFF_extrapolation",
                      #"weight_FT_EFF_extrapolation_from_charm",
                      "weight_JET_JvtEfficiency",
                      "weight_MUON_EFF_ISO_STAT",
                      "weight_MUON_EFF_ISO_SYS",
                      "weight_MUON_EFF_RECO_STAT_LOWPT",
                      "weight_MUON_EFF_RECO_STAT",
                      "weight_MUON_EFF_RECO_SYS_LOWPT",
                      "weight_MUON_EFF_RECO_SYS",
                      "weight_MUON_EFF_TTVA_STAT",
                      "weight_MUON_EFF_TTVA_SYS",
                      "weight_JET_fJvtEfficiency",
                      "weight_MUON_EFF_TrigStatUncertainty",
                      "weight_MUON_EFF_TrigSystUncertainty",
                      "weight_PRW_DATASF",

                      "weight_FT_EFF_Eigen_B_0",
                      "weight_FT_EFF_Eigen_B_1",
                      "weight_FT_EFF_Eigen_B_2",
                      "weight_FT_EFF_Eigen_B_3",
                      "weight_FT_EFF_Eigen_B_4",
                      "weight_FT_EFF_Eigen_B_5",
                      "weight_FT_EFF_Eigen_B_6",
                      "weight_FT_EFF_Eigen_B_7",
                      "weight_FT_EFF_Eigen_B_8",
                      "weight_FT_EFF_Eigen_C_0",
                      "weight_FT_EFF_Eigen_C_1",
                      "weight_FT_EFF_Eigen_C_2",
                      "weight_FT_EFF_Eigen_C_3",
                      "weight_FT_EFF_Eigen_Light_0",
                      "weight_FT_EFF_Eigen_Light_1",
                      "weight_FT_EFF_Eigen_Light_2",
                      "weight_FT_EFF_Eigen_Light_3",
                      "weight_FT_EFF_extrapolation",
                      "weight_FT_EFF_extrapolation_from_charm"])


ExtraExp_sys = updown(["EG_RESOLUTION_ALL",
                       "EG_SCALE_AF2",
                       "EG_SCALE_ALL",
                       "JET_BJES_Response",
                       "JET_EffectiveNP_Detector1",
                       "JET_EffectiveNP_Detector2",
                       "JET_EffectiveNP_Mixed1",
                       "JET_EffectiveNP_Mixed2",
                       "JET_EffectiveNP_Mixed3",
                       "JET_EffectiveNP_Modelling1",
                       "JET_EffectiveNP_Modelling2",
                       "JET_EffectiveNP_Modelling3",
                       "JET_EffectiveNP_Modelling4",
                       "JET_EffectiveNP_Statistical1",
                       "JET_EffectiveNP_Statistical2",
                       "JET_EffectiveNP_Statistical3",
                       "JET_EffectiveNP_Statistical4",
                       "JET_EffectiveNP_Statistical5",
                       "JET_EffectiveNP_Statistical6",
                       "JET_EtaIntercalibration_Modelling",
                       "JET_EtaIntercalibration_NonClosure_2018data",
                       "JET_EtaIntercalibration_NonClosure_highE",
                       "JET_EtaIntercalibration_NonClosure_negEta",
                       "JET_EtaIntercalibration_NonClosure_posEta",
                       "JET_EtaIntercalibration_TotalStat",
                       "JET_Flavor_Composition",
                       "JET_Flavor_Response",
                       "JET_JER_DataVsMC_MC16",
                       "JET_JER_EffectiveNP_10",
                       "JET_JER_EffectiveNP_11",
                       "JET_JER_EffectiveNP_1",
                       "JET_JER_EffectiveNP_12restTerm",
                       "JET_JER_EffectiveNP_2",
                       "JET_JER_EffectiveNP_3",
                       "JET_JER_EffectiveNP_4",
                       "JET_JER_EffectiveNP_5",
                       "JET_JER_EffectiveNP_6",
                       "JET_JER_EffectiveNP_7",
                       "JET_JER_EffectiveNP_8",
                       "JET_JER_EffectiveNP_9",
                       "JET_Pileup_OffsetMu",
                       "JET_Pileup_OffsetNPV",
                       "JET_Pileup_PtTerm",
                       "JET_Pileup_RhoTopology",
                       "JET_PunchThrough_MC16",
                       "JET_SingleParticle_HighPt",
                       "MUON_ID",
                       "MUON_MS",
                       "MUON_SAGITTA_RESBIAS",
                       "MUON_SAGITTA_RHO",
                       "MUON_SCALE",
                       "PD_JET_JER_DataVsMC_MC16",
                       "PD_JET_JER_EffectiveNP_10",
                       "PD_JET_JER_EffectiveNP_11",
                       "PD_JET_JER_EffectiveNP_1",
                       "PD_JET_JER_EffectiveNP_12restTerm",
                       "PD_JET_JER_EffectiveNP_2",
                       "PD_JET_JER_EffectiveNP_3",
                       "PD_JET_JER_EffectiveNP_4",
                       "PD_JET_JER_EffectiveNP_5",
                       "PD_JET_JER_EffectiveNP_6",
                       "PD_JET_JER_EffectiveNP_7",
                       "PD_JET_JER_EffectiveNP_8",
                       "PD_JET_JER_EffectiveNP_9",
                       "MET_SoftTrk_Scale"])


ExtraExp_sys.append("MET_SoftTrk_ResoPara")
ExtraExp_sys.append("MET_SoftTrk_ResoPerp")
#do not consider the MET in 4l analysis

#######################################################

#Systematics_list=["NormSystematic"]+ExtraExp_sys
Systematics_list=["weightSyst"]+ExtraExp_sys #weightSyst (2l2v) is equivalent to NormSystematic (4l)

#should change to theoretical uncertainties
TheoSyst_dict = {'qqZZ_4l_0jet'   : Sherpa_QCD_scale_factors,
                 'qqZZ_4l_1jet'   : Sherpa_QCD_scale_factors,
                 'qqZZ_4l_2jet'   : Sherpa_QCD_scale_factors,
                 'qqZZ_2l2v_0jet' : Sherpa_QCD_scale_factors + qqZZ_EW_2l2v + Sherpa_PDF_replica + qqZZ_Shower_sys,
                 'qqZZ_2l2v_1jet' : Sherpa_QCD_scale_factors + qqZZ_EW_2l2v + Sherpa_PDF_replica + qqZZ_Shower_sys,
                 'qqZZ_2l2v_2jet' : Sherpa_QCD_scale_factors + qqZZ_EW_2l2v + Sherpa_PDF_replica + qqZZ_Shower_sys,
                 'ggZZ'      : ggF_KfactorNLO_sys + Sherpa_PDF_replica + ggZZ_Shower_sys,
                 'VBF'       : VBF_PDF_replica + VBF_QCD_scale_factors + VBF_Shower_sys,

                 'WZ_3lep_0jet'  : Sherpa_QCD_scale_factors + Sherpa_PDF_replica,
                 'WZ_3lep_1jet'  : Sherpa_QCD_scale_factors + Sherpa_PDF_replica,
                 'WZ_3lep_2jet'  : Sherpa_QCD_scale_factors + Sherpa_PDF_replica,
                 'WW_emu_bkg_1'  : [],
                 'WW_emu_bkg_2'  : [],
                 'ttbar_emu_bkg' : [],
                 'Wt_emu_bkg'    : [],
                 'Wtbar_emu_bkg' : [],


                 #for code2 merge process
                 'tot_emu_bkg'   : [], #safely ignore WW_emu_bkg_2 as this is very very small
                 'Zjets_bkg'     : Sherpa_QCD_scale_factors,
                 'others_bkg'    : [],
                 }

for j in range(0,27+1):
    TheoSyst_dict['Zjets3641{0:02}'.format(j)] = Sherpa_QCD_scale_factors

for proc in List_Procs.others_2l2v:
    TheoSyst_dict[proc] = []

for proc in ['OtpSM','OpgSM','OptSM',
             'OpgSqrd','OtpSqrd','OptSqrd',
             'OpgOtp','OptOtp']:
    TheoSyst_dict[proc]= EFT_scale_factors + EFT_PDF_replica + EFT_Shower_sys
