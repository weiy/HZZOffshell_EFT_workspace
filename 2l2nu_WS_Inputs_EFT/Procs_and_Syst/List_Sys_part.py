import sys
import os
import List_Procs

var_suffix = ["__1up","__1down"]
def updown(sys_list):
    return [ i+j for i in sys_list for j in var_suffix]

################Theory uncertainties###############
#ggZZ
ggF_KfactorNLO_sys = updown(['weight_QCD_ggZZkSBI'])

#!!!check and rename
ggZZ_Shower_sys = updown(['llvv_PS_ggZZ_CKKW',
                          'llvv_PS_ggZZ_CSSKIN',
                          'llvv_PS_ggZZ_QSF'])

#qqZZ
#QCD systematics
Sherpa_QCD_scale_factors = []

qqZZ_EW_2l2v=updown(['weight_qqZZNLO_EW']) #check!!!

#in 2l2v channel, we only have HOEW_QCD mixed syst. Actuall in both 4l and 2l2v channels, HOEW only syst is very small, the variation is about 0.07% which can be neglected for sure
#qqZZ_NLO_corr = updown(['weight_var_HOEW_QCD_syst',
#                        'weight_var_HOEW_syst'])

qqZZ_Shower_sys = updown([])

#weight_var_th_MUR1_MUF1_PDF261001-100
Sherpa_PDF_replica = []

Powheg_QCD_scale_factors = ['weight_var_th_nominal', # nominal first
                            'weight_var_th_muR0p5_muF0p5',
                            'weight_var_th_muR0p5_muF1p0',
                            'weight_var_th_muR1p0_muF0p5',
                            'weight_var_th_muR1p0_muF2p0',
                            'weight_var_th_muR2p0_muF1p0',
                            'weight_var_th_muR2p0_muF2p0']


VBF_PDF_replica=['weight_var_th_MUR1p0_MUF1p0_PDF260{:03d}'.format(j) for j in range(0,100+1)]

VBF_QCD_scale_factors = ['weight_var_th_MUR1p0_MUF1p0_PDF260000', # nominal first
                         'weight_var_th_MUR0p5_MUF0p5_PDF260000',
                         'weight_var_th_MUR0p5_MUF1p0_PDF260000',
                         'weight_var_th_MUR1p0_MUF0p5_PDF260000',
                         'weight_var_th_MUR1p0_MUF2p0_PDF260000',
                         'weight_var_th_MUR2p0_MUF1p0_PDF260000',
                         'weight_var_th_MUR2p0_MUF2p0_PDF260000']

VBF_Shower_sys = updown(['llvv_PS_VBF_EIG',
                         'llvv_PS_VBF_HAD',
                         'llvv_PS_VBF_PDF',
                         'llvv_PS_VBF_RE'])

EFT_scale_factors_MUF_MUR = [#MUF and MUR
                     "weight_var_th_MUR05_MUF05_PDF261000",
                     "weight_var_th_MUR05_MUF10_PDF261000",
                     "weight_var_th_MUR05_MUF20_PDF261000",
                     "weight_var_th_MUR10_MUF05_PDF261000",
                     "weight_var_th_MUR10_MUF10_PDF261000",
                     "weight_var_th_MUR10_MUF20_PDF261000",
                     "weight_var_th_MUR20_MUF05_PDF261000",
                     "weight_var_th_MUR20_MUF10_PDF261000",
                     "weight_var_th_MUR20_MUF20_PDF261000",
                     ]

EFT_scale_factors_PDF_var = [#PDFset variations
                     "weight_var_th_MUR10_MUF10_PDF13000",#CT14nnlo
                     "weight_var_th_MUR10_MUF10_PDF25300",#MMHT2014nnlo68cl
                     "weight_var_th_MUR10_MUF10_PDF91400",#PDF4LHC15_nnlo_30_pdfas
                     ]

EFT_scale_factors_alphaS = [#alpha_S
                     "weight_var_th_MUR10_MUF10_PDF269000",#NNPDF30_nnlo_as_0117
                     "weight_var_th_MUR10_MUF10_PDF270000",#NNPDF30_nnlo_as_0119
                     ]

#EFT
EFT_scale_factors = EFT_scale_factors_MUF_MUR + EFT_scale_factors_PDF_var + EFT_scale_factors_alphaS


#weight_var_th_MUR10_MUF10_PDF261001-100
EFT_PDF_replica=["weight_var_th_MUR10_MUF10_PDF261{0:03}".format(j) for j in range(0,100+1)]

EFT_Shower_sys_muf_muR = ["weight_var_th_isrmuRfac05_fsrmuRfac05",
                          "weight_var_th_isrmuRfac05_fsrmuRfac10",
                          "weight_var_th_isrmuRfac05_fsrmuRfac20",
                          "weight_var_th_isrmuRfac0625_fsrmuRfac10",
                          "weight_var_th_isrmuRfac075_fsrmuRfac10",
                          "weight_var_th_isrmuRfac0875_fsrmuRfac10",
                          "weight_var_th_isrmuRfac10_fsrmuRfac05",
                          "weight_var_th_isrmuRfac10_fsrmuRfac0625",
                          "weight_var_th_isrmuRfac10_fsrmuRfac075",
                          "weight_var_th_isrmuRfac10_fsrmuRfac0875",
                          "weight_var_th_isrmuRfac10_fsrmuRfac125",
                          "weight_var_th_isrmuRfac10_fsrmuRfac15",
                          "weight_var_th_isrmuRfac10_fsrmuRfac175",
                          "weight_var_th_isrmuRfac10_fsrmuRfac20",
                          "weight_var_th_isrmuRfac125_fsrmuRfac10",
                          "weight_var_th_isrmuRfac15_fsrmuRfac10",
                          "weight_var_th_isrmuRfac175_fsrmuRfac10",
                          "weight_var_th_isrmuRfac20_fsrmuRfac05",
                          "weight_var_th_isrmuRfac20_fsrmuRfac10",
                          "weight_var_th_isrmuRfac20_fsrmuRfac20"]

EFT_Shower_sys_others = ["weight_var_th_Var3cDown",
                         "weight_var_th_Var3cUp",
                         "weight_var_th_hardHi",
                         "weight_var_th_hardLo",
                         "weight_var_th_isrPDFminus",
                         "weight_var_th_isrPDFplus"]

EFT_Shower_sys = EFT_Shower_sys_others + EFT_Shower_sys_muf_muR

####################################################

#############Experimental uncertainties#############
NormExp_sys = updown(["weight_FT_EFF_Eigen_B_0",
                      "weight_FT_EFF_Eigen_B_1",
                      "weight_FT_EFF_Eigen_B_2",
                      "weight_FT_EFF_Eigen_B_3",
                      "weight_FT_EFF_Eigen_B_4",
                      "weight_FT_EFF_Eigen_B_5",
                      "weight_FT_EFF_Eigen_B_6",
                      "weight_FT_EFF_Eigen_B_7",
                      "weight_FT_EFF_Eigen_B_8",
                      "weight_FT_EFF_Eigen_C_0",
                      "weight_FT_EFF_Eigen_C_1",
                      "weight_FT_EFF_Eigen_C_2",
                      "weight_FT_EFF_Eigen_C_3",
                      "weight_FT_EFF_Eigen_Light_0",
                      "weight_FT_EFF_Eigen_Light_1",
                      "weight_FT_EFF_Eigen_Light_2",
                      "weight_FT_EFF_Eigen_Light_3",
                      "weight_FT_EFF_extrapolation",
                      "weight_FT_EFF_extrapolation_from_charm"])


ExtraExp_sys = updown(["EG_RESOLUTION_ALL",
                       "MUON_MS",
                       "MUON_SAGITTA_RESBIAS",
                       "MUON_SAGITTA_RHO",
                       "MUON_SCALE",
                       "PD_JET_JER_DataVsMC_MC16",
                       "PD_JET_JER_EffectiveNP_10",
                       "MET_SoftTrk_Scale"])


ExtraExp_sys.append("MET_SoftTrk_ResoPara")
ExtraExp_sys.append("MET_SoftTrk_ResoPerp")
#do not consider the MET in 4l analysis

#######################################################

#Systematics_list=["NormSystematic"]+ExtraExp_sys
Systematics_list=["weightSyst"]+ExtraExp_sys #weightSyst (2l2v) is equivalent to NormSystematic (4l)

#should change to theoretical uncertainties
TheoSyst_dict = {'qqZZ_4l'   : Sherpa_QCD_scale_factors,
                 #'qqZZ_2l2v' : Sherpa_QCD_scale_factors + qqZZ_EW_2l2v + Sherpa_PDF_replica + qqZZ_Shower_sys,
                 'qqZZ_2l2v' : [],
                 'ggZZ'      : ggF_KfactorNLO_sys + Sherpa_PDF_replica + ggZZ_Shower_sys,
                 'VBF'       : VBF_PDF_replica + VBF_QCD_scale_factors + VBF_Shower_sys,

                 'WZ_3lep'   : Sherpa_QCD_scale_factors + Sherpa_PDF_replica,
                 'WW_emu_bkg_1'  : Powheg_QCD_scale_factors,
                 'WW_emu_bkg_2'  : Sherpa_QCD_scale_factors,
                 'ttbar_emu_bkg' : Powheg_QCD_scale_factors,
                 'Wt_emu_bkg'    : Powheg_QCD_scale_factors,
                 'Wtbar_emu_bkg' : Powheg_QCD_scale_factors,


                 #for code2 merge process
                 'tot_emu_bkg'   : Powheg_QCD_scale_factors, #safely ignore WW_emu_bkg_2 as this is very very small
                 'Zjets_bkg'     : Sherpa_QCD_scale_factors,
                 'others_bkg'    : [],
                 }

for j in range(0,27+1):
    TheoSyst_dict['Zjets3641{0:02}'.format(j)] = Sherpa_QCD_scale_factors

for proc in List_Procs.others_2l2v:
    TheoSyst_dict[proc] = []

for proc in ['OtpSM','OpgSM','OptSM',
             'OpgSqrd','OtpSqrd','OptSqrd',
             'OpgOtp','OptOtp']:
    TheoSyst_dict[proc]= EFT_scale_factors + EFT_PDF_replica + EFT_Shower_sys
