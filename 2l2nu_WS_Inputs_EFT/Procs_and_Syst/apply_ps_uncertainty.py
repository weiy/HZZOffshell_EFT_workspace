import os
import re
import ROOT
from ROOT import gROOT
from termcolor import colored

gROOT.SetBatch()

# helper function to rewrite a cut from c++ to python
# used for python loops over minitree entry's (make sure to name the entries 'entry')
def cut_string_for_python_loop(cut_string):
        print("converting cut string for python loop")
        # modify cut_string, needs entry. in from of every variable, special case needed for var names with numbers in them!
        cut_string = re.sub("([a-z,_]+3rd([a-z,_]+|)|event_.CR|dPhiJ100met|[a-z,A-Z,_]+)", "entry.\\1", cut_string)
        # replace 'abs' operation
        cut_string = cut_string.replace('entry.TMath::entry.Abs', 'abs')
        # replace boolian operations
        cut_string = cut_string.replace('&&', 'and')
        cut_string = cut_string.replace('||', 'or')
        cut_string = cut_string.replace('!', 'not ')
        cut_string = cut_string.replace('entry.This->entry.GetTreeNumber()', 'entry.GetTreeNumber()')
        return cut_string


def buildMaps(variation):

        # get the dir the inputs files are stored in
        txtpath = os.path.dirname(__file__)
        #txtpath = os.path.realpath(txtpath + '/ps_error_map/inclusive/')
        txtpath = os.path.realpath(txtpath + '/ps_error_map/jet_binned/')

        maps = {}
        # read the input files
        print('inside buildMaps')
        for f in os.listdir(txtpath):
                print('f: '+str(f))
                print('variation: '+str(variation))

                if "ReadMe" in f or "._" in f or "README" in f:
                        continue

                if variation not in f:
                        continue

                print("Using file: ", f)

                fin = os.path.join(txtpath, f)
                tmap = []
                paras = ((f.split('.txt'))[0]).split('_')

                with open(fin) as txt:
                        for l in txt:
                                blocks = l.split(' ')
                                tmap.append( (float(blocks[0]), float(blocks[1]), float(blocks[2]), float(blocks[3])) )

                tag = paras[1]
                tags =[]
                if tag == 'ggZZ':
                        tags = ['ggZZ']
                if tag == 'VBFbkg':
                        tags = ['VBF']
                if tag == 'qqZZ':
                        tags = ['qqZZ_2l2v_0jet','qqZZ_2l2v_1jet','qqZZ_2l2v_2jet']

                njet = paras[3]
                for t in tags:
                        # print "Made map: ", t+njet
                        maps[t+njet] = tmap

        return maps

def getweight(t_mT, nj, mctp, maps) :
        if nj == 0 :
                njet = '0j'
        elif nj == 1:
                njet = '1j'
        elif nj == 2:
                njet = '2j'
        else:
                print 'no matching jet number'
                return (1., 1.)

        if (mctp+njet) not in maps:
                return (1., 1.)

        t = maps[mctp+njet]
        # print t
        for (low, high, up, down) in t:
                # print t_mT
                if t_mT > low and t_mT < high:
                        return (up, down)
        return (1., 1.)

def fill_PS_hist(tree, ini_hist, var, weight, process, cut_name, cut_string, variation):
        print('tree, ini_hist, var, weight, process, cut_name, cut_string, variation')
        print(tree, ini_hist, var, weight, process, cut_name, cut_string, variation)
        #an example: (<cppyy.gbl.TTree object at 0x6027ae0>, <cppyy.gbl.TH1F object at 0x689f010>, 'mT_ZZ', '(weight*(weight_qqZZNLO_EW))*(llvv_PS_qqZZ_CKKW__1up)/(weight)*0.0749392402788', 'qqZZ_2', 'SR_2l2nu_binwidth1GeV_mT_ZZ', '((met_tst > 80) && (This->GetTreeNumber()==0))/2', 'llvv_PS_qqZZ_CKKW__1up')
        
        # take out the 'ps weight'
        print("before weight sub: "+str(weight))
        weight = re.sub("\*\("+variation+"\)/\(weight\)","", weight)
        print("after weight sub: "+str(weight)+"\n")

        cut_string = weight+"*"+cut_string
        print('before cut_string: '+cut_string)
        cut_string = cut_string_for_python_loop(cut_string)
        print("after cut_string: "+ cut_string+"\n")

        do_down = "down" in variation

        if ("inclusive" in variation):
                maps = buildMaps_inclusive()
                variation = ""
        else:
                if "CKKW" in variation:
                        variation = "CKKW"
                elif "CSSKIN" in variation:
                        variation = "CSSKIN"
                elif "QSF" in variation:
                        variation = "QSF"
                elif "EIG" in variation:
                        variation = "EIG"
                elif "HAD" in variation:
                        variation = "HAD"
                elif "PDF" in variation or "pdf" in variation:
                        variation = "PDF"
                elif "RE" in variation:
                        variation = "RE"
                else:
                        print(colored("llvv Parton Shower: variation "+ variation+  " not recognized!",'red'))
                        exit()

                # print "variation: ", variation

                maps = buildMaps(variation)

        uncorr_hist = ini_hist.Clone()

        weight = re.sub("weight","entry.weight", weight)
        weight = re.sub("event","entry.event", weight)
        weight = re.sub("n_jets","entry.n_jets", weight)
        # weight = re.sub("n_jets(>|=)=[0,1,2]","1", weight)
        print("final weight: ", weight)

        njets = 0
        if "0j" in cut_name or "ggF" in cut_name:
                njets = 0
        elif '1j' in cut_name or "Mixed" in cut_name:
                njets = 1
        elif '2j' in cut_name or "VBF" in cut_name:
                njets = 2
        else:
                # what to use for CRs?
                njets = 0

        print("using PS with ", njets, " jets for region: ", cut_name)

        if (process+str(njets)+"j") not in maps:
                print(colored("PS uncertainties requested but not implemented for "+ process+str(njets)+"j",'red'))
                print(maps.keys())
                exit()

        for entry in tree:
                # apply cuts
                if not eval(cut_string): continue

                # for VBF, we do not have truth MTZZ
                if "VBF" in process:
                        (val_ps_reweight_up, val_ps_reweight_down )= getweight(entry.mT_ZZ, njets, process, maps)
                else:
                        (val_ps_reweight_up, val_ps_reweight_down )= getweight(entry.mT_ZZ_Truth, njets, process, maps)

                corr_weight = val_ps_reweight_down*eval(weight) if do_down else val_ps_reweight_up*eval(weight)
                # print eval(weight), corr_weight

                # fill into corrected hist
                ini_hist.Fill(eval('entry.'+var), corr_weight)
                uncorr_hist.Fill(eval('entry.'+var), eval(weight))

        print("Integral without PS uncertainty: ", uncorr_hist.Integral())
        print("Integral with PS uncertainty: ", ini_hist.Integral())
        return ini_hist
