class Process:
    def __init__(self,name,DSID,weight):
        self.name = name
        self.DSID = DSID
        self.weight = weight

HNNLO_kfactor=1.1

#procs_4l
procs={
    ##EFT
    "OtpSM"  : Process(name="OtpSM",DSID="507656",weight="weight"),
    "OpgSM"  : Process("OpgSM"  ,"507656","weight*weight_OpgSM"),
    "OptSM"  : Process("OptSM"  ,"507650","weight*(weight>-0.01)*(weight<0.01)"),
    "OpgSqrd": Process("OpgSqrd","507599","weight"),
    "OpgOtp" : Process("OpgOtp" ,"507599","weight*weight_OpgOtp"),
    "OtpSqrd": Process("OtpSqrd","507652","weight"),
    "OptSqrd": Process("OptSqrd","507654","weight"),
    "OptOtp" : Process("OptOtp" ,"507648","weight"),

    ##SM
    "qqZZ_1": Process("qqZZ_1" ,"364250","weight"),
    "qqZZ_2": Process("qqZZ_2" ,"346899","weight"),
    "qqZZ_3": Process("qqZZ_3" ,"364252","weight"),
    "ggZZ"  : Process("ggZZ"   ,"345706","weight*w_ggZZkSBI*"+str(HNNLO_kfactor)),
}

procs_2l2v={
    ##EFT
    "OtpSM"  : Process(name="OtpSM",DSID="507655",weight="weight"),
    "OpgSM"  : Process("OpgSM"  ,"507655","weight*weight_OpgSM"),
    "OptSM"  : Process("OptSM"  ,"507649","weight*(weight>-0.01)*(weight<0.01)"),
    "OpgSqrd": Process("OpgSqrd","507600","weight"),
    "OpgOtp" : Process("OpgOtp" ,"507600","weight*weight_OpgOtp"),
    "OtpSqrd": Process("OtpSqrd","507651","weight"),
    "OptSqrd": Process("OptSqrd","507653","weight"),
    "OptOtp" : Process("OptOtp" ,"507647","weight"),

    ##SM
    #qqZZ
    "qqZZ_4l_0jet": Process("qqZZ_4l_0jet" ,"364250","weight*(n_jets==0)"),
    "qqZZ_4l_1jet": Process("qqZZ_4l_1jet" ,"364250","weight*(n_jets==1)"),
    "qqZZ_4l_2jet": Process("qqZZ_4l_2jet" ,"364250","weight*(n_jets>=2)"),

    "qqZZ_2l2v_0jet": Process("qqZZ_2l2v_0jet" ,"345666","weight*(weight_qqZZNLO_EW)*(n_jets==0)"),
    "qqZZ_2l2v_1jet": Process("qqZZ_2l2v_1jet" ,"345666","weight*(weight_qqZZNLO_EW)*(n_jets==1)"),
    "qqZZ_2l2v_2jet": Process("qqZZ_2l2v_2jet" ,"345666","weight*(weight_qqZZNLO_EW)*(n_jets>=2)"),

    #ggZZ
    "ggZZ"  : Process("ggZZ"   ,"345723","weight*weight_QCD_ggZZkSBI*"+str(HNNLO_kfactor)), #rel189
    #"ggZZ"  : Process("ggZZ"   ,"345723","weight*weight_ggZZkSBI"), #rel139

    #VBF
    "VBF"   : Process("VBF"    ,"500378","weight"),

    #WZ+emu
    "WZ_3lep_0jet"  : Process("WZ_3lep_0jet",  "364253","weight*(n_jets==0)"),
    "WZ_3lep_1jet"  : Process("WZ_3lep_1jet",  "364253","weight*(n_jets==1)"),
    "WZ_3lep_2jet"  : Process("WZ_3lep_2jet",  "364253","weight*(n_jets>=2)"),

    "WW_emu_bkg_1"  : Process("WW_emu_bkg_1",  "361600","weight"),
    "WW_emu_bkg_2"  : Process("WW_emu_bkg_2",  "345718","weight"),
    "ttbar_emu_bkg" : Process("ttbar_emu_bkg", "410472","weight"),
    "Wt_emu_bkg"    : Process("Wt_emu_bkg",    "410646","weight"),
    "Wtbar_emu_bkg" : Process("Wtbar_emu_bkg", "410647","weight"),

    #Z+jets: need to define separately, see after this declaration

    #Others: VVV and tt etc, see after this declaration
}

others_2l2v = {"others_WWW_3l3v_EW6":"364242",
               "others_WWZ_4l2v_EW6":"364243",
               "others_WWZ_2l4v_EW6":"364244",
               "others_WZZ_5l1v_EW6":"364245",
               "others_WZZ_3l3v_EW6":"364246",
               "others_ZZZ_6l0v_EW6":"364247",
               "others_ZZZ_4l2v_EW6":"364248",
               "others_ZZZ_2l4v_EW6":"364249",
               "others_ttbarWW":"410081",
               "others_ttW":"410155",
               "others_ttZnunu":"410156",
               "others_ttZqq":"410157",
               "others_ttee":"410218",
               "others_ttmumu":"410219",
               "others_ggllll_130M4l":"345706",
               "others_ZqqZll":"363356",
               "others_WqqZll":"363358",
               "others_lllljj_EW6":"364283",
               "others_lllvjj_EW6":"364284"}
#Z+jets
for j in range(0,27+1):    
    DSID="3641{0:02}".format(j)
    procs_2l2v["Zjets"+DSID] = Process("Zjets"+DSID, DSID, "weight")

#others
for proc in others_2l2v:
    procs_2l2v[proc] = Process(proc, others_2l2v[proc], "weight")

#README
# (1) OptSM : the mc16a DAOD has abnormal weights for 2l2v only
# (2) qqZZ_3: very small, can be neglected.
