outdir="outputs_ws_EFT_2l2v"
datetag="16Mar2022_noSys_wMCSTAT_EFT_2l2v"

## scan poi
tag=$datetag

#input="${outdir}/WS_OffShell_${tag}.root"
input="${outdir}/WS_OffShell_${tag}_noCR.root"

wsName="combined"
mcName="ModelConfig"
#dataName="obsData"
#dataName="asimovData_0_0"
dataName="asimovData"

POIName="cpG"

#out_name="${outdir}/${POIName}_scan_${tag}.root"
out_name="${outdir}/${POIName}_scan_${tag}_noCR.root"

scan_poi  ${input} ${out_name} ${wsName} ${POIName} ${dataName} ${POIName}:300:-0.3:0.3 cpGsqrd:0.0,ctp:0.0,ctpsqrd:0.0,cpt:0.0,cptsqrd:0.0,cpGctp:0.0 2>&1 | tee ${outdir}/log_scan_offshell_${POIName}_${tag}
