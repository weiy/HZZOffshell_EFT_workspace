import os

outname="2l2v_EFT_WS_wCR"
date="23Jan2023"

inPath="./outputs_"+date+"/Merged_code2_"+outname

print(inPath)
outputPath="WS_inputs_"+date+"_"+outname

os.system('mkdir -p ' + outputPath)
os.system('rm -rf ' + outputPath)
os.system('rm -rf NP_names.txt')
os.system('mkdir -p ' + outputPath)

#HighSoverB=4,11,2
HSOB_Threshold=0
LSOB_Threshold=0


ProcNames = ["qqZZ_4l_0jet","qqZZ_4l_1jet","qqZZ_4l_2jet",
             "qqZZ_2l2v_0jet","qqZZ_2l2v_1jet","qqZZ_2l2v_2jet",
             "ggZZ","VBF",
             "WZ_3lep_0jet","WZ_3lep_1jet","WZ_3lep_2jet",
             "Zjets_bkg",
             "tot_emu_bkg","others_bkg",
             "OtpSM","OpgSM",
             "OpgSqrd","OtpSqrd",
             "OpgOtp"]

#ProcNames = ["OtpSM"]
# ProcNames = ["qqZZ_4l_0jet"]

for ProcName in ProcNames:
    os.system('python make_WSInputs_code3.py --inProc '+ProcName+' --inpath '+inPath+' --outpath '+outputPath)

os.system('python make_NPfile.py --outpath '+outputPath)
#python make_NPfile.py --outpath WS_inputs_07Aug2022_2l2v_EFT_WS_wCR
