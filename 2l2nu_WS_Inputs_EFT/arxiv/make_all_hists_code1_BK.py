from ROOT import TFile, gROOT, gStyle, TH1, Form, TH1F
import sys
import os
sys.path.append('./Systematic_names/')
import os.path
from os import path
from glob import glob
from array import array
import math
import argparse
import List_Sys
import List_Procs
from array import array
import numpy as np
from math import sqrt
from math import log10
import glob
import yaml
import json
import pickle
from collections import OrderedDict

gROOT.SetBatch(1)
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--tree',    type=str, default='tree_PFLOW', help='Name of tree')
parser.add_argument('--inpath',  type=str, default='', help='Add path to the sample directory (only) each campaign accesed separately')
parser.add_argument('--outpath', type=str, default='', help='Add path to output')
parser.add_argument('--proc',  type=str, default='', help='which process to deal with')
parser.add_argument('--sample',  type=str, default='', help='Add a sample you want to read')
parser.add_argument('-c','--campaign',type=str,  nargs='+', default =['mc16a','mc16d','mc16e'],  help='Campaigns needed to run over, example -c mc16a ')

args = parser.parse_args()

def region_of_interest(list_regions,ROI_names,all_regions):
    #ROI: region of interest
    """
    function defines the new regions of interest
    """
    for ROI_name in ROI_names:
        if ROI_name in all_regions:
            ROI = all_regions[ROI_name]
            Cuts, Obs, isVarBin, binning = ROI[0], ROI[1], ROI[3], ROI[4]
            name = ROI_name+"_"+str(Obs)
            if isVarBin == True:
                bins = binning
            else:
                bins = np.linspace(binning[1], binning[2], num=binning[0]+1).tolist()
        list_regions.update({name:[Cuts, Obs, bins]})


def concat_hists(hist_list, histName):
    """
    Concatenate the historgams from all the regions to make one-long histogram
    easier when removing normalization
    """
    binsContent=[] #list to append bin contents
    binsError  =[] #list to append bin errors
    binsEntry  =0  #gets the entries in the histogram
    outhist_len = 0 #len of the concatenated histogram
    for hist in hist_list:
        outhist_len = outhist_len + hist.GetNbinsX()  #concatenates histograms to get final histogram length
    #Define the output concatenated histogram
    outhist = TH1F(histName, histName, outhist_len, 0, outhist_len)
    #Loop over the histograms, each one corresponding to each SR/CR region
    for hist in hist_list:
        binsEntry = binsEntry+(hist.GetEntries()) #get the entries
        for i in range(1,hist.GetNbinsX()+1): #Only the bins, not under and overflow, add overflow to last and underflow to fist bin
            if i ==  1: #add underflow to first bin, do  sum in quadrature for the errors
                binsContent.append(hist.GetBinContent(i)+hist.GetBinContent(0))
                binsError.append(sqrt((hist.GetBinError(i))**2+(hist.GetBinError(0))**2))
            elif i == hist.GetNbinsX(): #add overflow to last coloumn and sum in quadrature for error
                binsContent.append(hist.GetBinContent(i)+hist.GetBinContent(hist.GetNbinsX()+1))
                binsError.append(sqrt((hist.GetBinError(i))**2+(hist.GetBinError(hist.GetNbinsX()+1))**2))
            else: #just get the bin content for other bins
                binsContent.append(hist.GetBinContent(i))
                binsError.append(hist.GetBinError(i))

    #set the information in the concatenated histogram
    outhist.SetEntries(binsEntry)
    for i in range(1,len(binsContent)+1):
        #print( binsContent[i-1])
        outhist.SetBinContent(i, binsContent[i-1])
        outhist.SetBinError(i, binsError[i-1])
    #return the concatenated hist
    #print(sum(binsContent))
    return outhist

def write_hist(list_regions,weight,Output_Dir,Process,camp,myTree,InSample,Nom_Norm,CRs_Norm):
    """
    function defines the histograms to be written out
    depending on the systematics/ processes etc.
    use the concat_hists function to write out the histogram that has been concatenated in the various regions
    """

    OutFolder = args.outpath #path where hist is stored
    listHists = [] #list of histograms to write out
    #Fill nominal histograms

    hist_Rgns =[]
    for regions in list_regions.keys():
        histName = regions+'_Nom_hist' #histogram named as Region_variable
        bin_edges = np.array(list_regions[regions][2], dtype='float64') #Get the binning/edges
        hist_Rgns.append(TH1F(histName, histName, len(bin_edges)-1, bin_edges)) #append the histograms to a list

        if ("3l" or "emu") in regions:
            Norm = CRs_Norm
        else:
            Norm = Nom_Norm

        fillHist = Form('%s>>%s'%(list_regions[regions][1],histName)) #Fill the histogram with the appropriate variable
        fillWeight = Form('%s*%s*%s' % (weight,list_regions[regions][0],Norm))#use the appropriate weights
        myTree.Draw(fillHist, fillWeight)# draw the histogram

    #append the concatenated histogram
    listHists.append(concat_hists(hist_Rgns, "Nom_hist"))

    OutDirCamp = os.path.join(OutFolder,camp)
    OutSample  = Process+"_hists.root"

    os.system("mkdir -p " + OutDirCamp)
    fOut_FromTree = TFile(os.path.join(OutDirCamp,OutSample), "RECREATE")

    directory = fOut_FromTree.GetDirectory(Output_Dir)
    if not (directory):
        directory = fOut_FromTree.mkdir(Output_Dir)
    directory.cd()
    for hist in listHists:
        print("Writing..." + str(hist))
        print(hist.Integral())
        directory.cd()
        hist.Write()
        hist.SetDirectory(0)

    fOut_FromTree.Write("",TFile.kOverwrite)
    fOut_FromTree.Close()

def GetLumi(camp):
    if   "a" in camp: lumi = 36.207
    elif "d" in camp: lumi = 44.307
    elif "e" in camp: lumi = 58.250
    else:             lumi = 0
    return lumi

def main():
    """
    Main function.
    """
    #gStyle.SetOptStat(0)
    TH1.SetDefaultSumw2()

    EosDir = args.inpath
    OutDir = args.outpath
    TreeName = args.tree
    InSample = args.sample
    #The Sample being processed
    print("InSample " + InSample)

    campaigns = args.campaign #whichever campaigns need to be run

    ##############
    # Get weight #
    ##############
    process = args.proc
    weight = List_Procs.procs_2l2v[args.proc].weight
    print(process)
    print(weight)

    print(len(weight))
    ###########
    # Regions #
    ###########

    list_regions = OrderedDict()
    file_all_regions = open('all_regions.yaml','r')
    yaml_all_regions = yaml.safe_load(file_all_regions)

    # list of region of interest
    #ROI_list = ['CR_3lep','CR_emu','CR_ZJets','SR_2l2nu']
    ROI_list = ['SR_2l2nu_binwidth1GeV']
    region_of_interest(list_regions,ROI_list,yaml_all_regions)

    file_region_interest = open('regions_of_interest.yaml','w')
    yaml.dump(list_regions, file_region_interest, default_flow_style=False)
    file_region_interest.close()
    #print(list_regions)
    print(yaml.dump(list_regions, default_flow_style=False))

    #############################################
    # Accessing file and Writing the histograms #
    #############################################

    th_dir_Nominal = 'Nominal'
    th_dir_TheoSys = 'TheoSys'
    th_dir_ExptSys = 'ExptSys'

    Nominal_tree = args.tree

    for camp in campaigns:
        print("doing Campaign " +str(camp)+"...")
        Histo_Nom = TFile(os.path.join(EosDir,camp,'Systematics',InSample))
        infoHist_Nom = Histo_Nom.Get("Hist/hInfo_PFlow")
        lumi = GetLumi(camp)
        Nom_Norm = infoHist_Nom.GetBinContent(1)*2.0/infoHist_Nom.GetEntries()*lumi/infoHist_Nom.GetBinContent(2)
        CRs_Norm = 1
        myTree = Histo_Nom.Get(Nominal_tree)
        print('Filling nominal histograms ' + process + '...')
        write_hist(list_regions,weight,th_dir_Nominal,process,camp,myTree,InSample,Nom_Norm,CRs_Norm)

if '__main__' in __name__:
    main()
