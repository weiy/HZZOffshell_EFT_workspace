from ROOT import *
import sys
sys.path.append('./Procs_and_Syst/')
sys.dont_write_bytecode = True
import os
import re
from glob import glob
from array import array
import math
import argparse
import List_Sys
from array import array
import numpy as np
import pickle
from numpy import linalg as LA

gROOT.SetBatch(kTRUE)
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--inpath',  type=str, default='./Merged_hists_code1_binnedCR', help='Add path to merged histograms')
parser.add_argument('--outpath', type=str, default='./Merged_hists_code2_binnedCR', help='Add path to output histograms')
parser.add_argument('--inProc',  type=str, default='qqZZ', help='Add the process name you want')

#Reading in the arguments
args = parser.parse_args()
HistPath = args.inpath
ProcName = args.inProc

#PCA for PDF Sys
def doPCA_PDF_sys(proc, List_PDF_vars, Nomhist, gDirectory):
    pdf_mat =[]
    pdf_hists =[]
    #for pdf_vars in List_Sys.Sherpa_samples_PDF_sys_dict['PDF_vars']:
    for pdf_vars in List_PDF_vars:
        #print(pdf_vars)
        hist_pdf = gDirectory.Get(pdf_vars)
        #hist_pdf.Print("All")
        bin_vals = []
        for i in range(1, hist_pdf.GetNbinsX()+1):
            # if hist_pdf.GetBinContent(i)!=0: #removing this since we found some empty bins in VBF
            bin_vals.append(hist_pdf.GetBinContent(i))
        pdf_mat.append(bin_vals)


    print(len(pdf_mat))
    Transpose_pdf_mat = np.array(pdf_mat).T
    cov_matrix = (np.cov(Transpose_pdf_mat))

    lambdas, vectors = LA.eig(cov_matrix)
    index_max = np.argmax(lambdas)
    X = np.matmul(cov_matrix,vectors.T[index_max])

    # print index_max
    print(len(vectors))

    histName = proc+"_PDF_replica_"
    bin_edges  = np.array(np.linspace(0, len(vectors[index_max+1])+1,len(vectors[index_max+1])+1), dtype='float64')
    Pdfhist_up = TH1F(histName+"_1up",   histName+"_up",   len(bin_edges)-1, bin_edges)
    Pdfhist_dn = TH1F(histName+"_1down", histName+"_down", len(bin_edges)-1, bin_edges)
    bins = Nomhist.GetNbinsX()
    print(bins)
    for i in range(1,bins+1):
        upVar =Nomhist.GetBinContent(i)+ ((vectors.T[index_max][i-1])*np.sqrt(lambdas[index_max]))
        dnVar = Nomhist.GetBinContent(i)- ((vectors.T[index_max][i-1])*np.sqrt(lambdas[index_max]))

        #???
        if abs(Nomhist.GetBinContent(i))>0.0:
            Pdfhist_up.SetBinContent(i, upVar)
            Pdfhist_dn.SetBinContent(i, dnVar)

        else:
            Pdfhist_up.SetBinContent(i, Nomhist.GetBinContent(i))
            Pdfhist_dn.SetBinContent(i, Nomhist.GetBinContent(i))

    print("PCA PDF upVar Integral: "+str(Pdfhist_up.Integral()))
    print("PCA PDF dnVar Integral: "+str(Pdfhist_dn.Integral()))
    pdf_hists.append(Pdfhist_up)
    pdf_hists.append(Pdfhist_dn)
    return(pdf_hists)


def write_hists(ProcName,Output_Dir,listHists):
    OutFolder = args.outpath
    OutSample  = ProcName+"_hists.root"
    if os.path.isdir(OutFolder):
        if os.path.isfile(os.path.join(OutFolder,OutSample)):
            fOut_FromTree = TFile(os.path.join(OutFolder,OutSample), "UPDATE")
            print('root file exits, updating it ...')
        else:
            fOut_FromTree = TFile(os.path.join(OutFolder,OutSample), "CREATE")
            print('root file doesnt exits, creating it ..')
    else:
        print('making a new directory and root file ...')
        os.mkdir(OutFolder)
        fOut_FromTree = TFile(os.path.join(OutFolder,OutSample), "CREATE")

    directory = fOut_FromTree.GetDirectory(Output_Dir)
    if not (directory):
        #print ">>> created " + Output_Dir
        directory = fOut_FromTree.mkdir(Output_Dir)

    #return if no lists
    if len(listHists)==0: return

    print("weiy listHists: "+str(listHists[-1].GetName()))
    if 'Nom_hist' != listHists[-1].GetName():
        print("weiy listHists:"+str(listHists[0].GetName()))
        Nom_hist=fOut_FromTree.Get('Nominal/Nom_hist')
        print(Nom_hist)
        print("weiy: Nom:"+str(Nom_hist.Integral()))
    directory.cd()
    for hist in listHists:
        print "\nWriting...",hist
        print(hist.Integral())
        hist.Write()
        hist.SetDirectory(0)
        if 'Nom_hist' != listHists[-1].GetName():
            hist_clone=hist.Clone()
            print('ratio to nominal: '+str(hist_clone.Integral()/Nom_hist.Integral()))
    fOut_FromTree.Write("",TFile.kOverwrite)
    fOut_FromTree.Close()

def main():
    list_systematics=[]
    try:
        HistFile = TFile(os.path.join(HistPath,ProcName+"_hists.root"))
    except:
        print("file "+HistPath+'/'+ProcName+"_hists.root doen't exist.")
        exit()
    print("\ndoing "+ProcName+" Process")
    for dirs in ["Nominal", "TheoSys","ExptSys"]:
        print(dirs)
        Nom_hist_files  = []
        Theo_hist_files = []
        Exp_hist_files = []

        directory = HistFile.GetDirectory(dirs)
        HistFile.cd(dirs)
        if "Nom" in dirs:
            Nomhist = gDirectory.Get("Nom_hist")
            Nom_hist_files.append(Nomhist)
            print("Nominal Histogram Integral : "+str(Nomhist.Integral()))
            Int_NomHist = Nomhist.Integral(0, Nomhist.GetNbinsX()+1)
            print("Nominal Histogram Integral (with overflow) : "+str(Int_NomHist))
            write_hists(ProcName, dirs, Nom_hist_files)

        ###########done############
        if "Theo" in dirs:
            ##PDF replica
            List_PDF_vars = []
            if "ggZZ" in ProcName or "qqZZ_2l2v" in ProcName or "WZ_3lep" in ProcName :
                List_PDF_vars = List_Sys.Sherpa_PDF_replica
            elif "VBF" in ProcName:
                List_PDF_vars = List_Sys.VBF_PDF_replica
            elif "O" in ProcName: #'O': EFT operator
                List_PDF_vars = List_Sys.EFT_PDF_replica

            if List_PDF_vars:
                PDF_PCA_hists = doPCA_PDF_sys(ProcName, List_PDF_vars, Nomhist, gDirectory)
                for PDFhist in PDF_PCA_hists:
                    Theo_hist_files.append(PDFhist)

            ##QCD scale factor
            ##no need to emu
            if "qqZZ" in ProcName or "WZ_3lep" in ProcName or "Zjets" in ProcName or "VBF" in ProcName:
                qcd_ScFac_Histos = []
                qcd_ScFac_Intgrals =[]
                QCD_scale_factors = []
                if "VBF" in ProcName:
                    QCD_scale_factors = List_Sys.VBF_QCD_scale_factors
                else:
                    QCD_scale_factors = List_Sys.Sherpa_QCD_scale_factors
                for TheoSys in QCD_scale_factors:
                    ScFac_hist = gDirectory.Get(TheoSys)
                    qcd_ScFac_Histos.append(ScFac_hist)
                    qcd_ScFac_Intgrals.append(ScFac_hist.Integral())
                max_hist_index = np.argmax(qcd_ScFac_Intgrals)
                max_hist = qcd_ScFac_Histos[max_hist_index].Clone()
                #need to check the Zjets,qqZZ_1_0jet, qqZZ_2_0jet and WZ_3lep_0jet ProcName,
                max_hist.SetName('weight_var_HOQCD_syst_'+re.sub("_[0,1,2]jet","",ProcName)+'__1up')
                print(str(max_hist.GetName())+ " Integral: "+str(max_hist.Integral()))
                print(str(max_hist.GetName())+ " Integral (with overflow): "+str(max_hist.Integral(1, max_hist.GetNbinsX()+1)))
                min_hist_index = np.argmin(qcd_ScFac_Intgrals)
                min_hist = qcd_ScFac_Histos[min_hist_index].Clone()
                min_hist.SetName('weight_var_HOQCD_syst_'+re.sub("_[0,1,2]jet","",ProcName)+'__1down')
                print(str(min_hist.GetName())+ " Integral: "+str(min_hist.Integral()))
                print(str(min_hist.GetName())+ " Integral (with overflow): "+str(min_hist.Integral(1,min_hist.GetNbinsX()+1)))

                Theo_hist_files.append(max_hist)
                Theo_hist_files.append(min_hist)

            if 'O' in ProcName: #O: EFT operator
                #PS scale
                PS_Histos=[]
                PS_Intgrals=[]
                for PS_sys in List_Sys.EFT_Shower_sys_muf_muR:
                    PS_hist = gDirectory.Get(PS_sys)
                    PS_Histos.append(PS_hist)
                    PS_Intgrals.append(PS_hist.Integral())
                max_hist_index = np.argmax(PS_Intgrals)
                max_hist = PS_Histos[max_hist_index].Clone()
                max_hist.SetName('weight_var_EFToperator_PS_muF_muR__1up')
                print(str(max_hist.GetName())+ " Integral: "+str(max_hist.Integral()))
                print(str(max_hist.GetName())+ " Integral (with overflow): "+str(max_hist.Integral(1, max_hist.GetNbinsX()+1)))
                min_hist_index = np.argmin(PS_Intgrals)
                min_hist = PS_Histos[min_hist_index].Clone()
                min_hist.SetName('weight_var_EFToperator_PS_muF_muR__1down')
                print(str(min_hist.GetName())+ " Integral: "+str(min_hist.Integral()))
                print(str(min_hist.GetName())+ " Integral (with overflow): "+str(min_hist.Integral(1,min_hist.GetNbinsX()+1)))
                Theo_hist_files.append(max_hist)
                Theo_hist_files.append(min_hist)

                #add muF muR
                PDF_Histos=[]
                PDF_Intgrals=[]
                for PDF_sys in List_Sys.EFT_scale_factors_MUF_MUR:
                    PDF_hist = gDirectory.Get(PDF_sys)
                    PDF_Histos.append(PDF_hist)
                    PDF_Intgrals.append(PDF_hist.Integral())
                max_hist_index = np.argmax(PDF_Intgrals)
                max_hist = PDF_Histos[max_hist_index].Clone()
                min_hist_index = np.argmin(PDF_Intgrals)
                min_hist = PDF_Histos[min_hist_index].Clone()
                if max_hist.Integral() > 0:
                    max_hist.SetName('weight_var_EFToperator_PDF_MUF_MUR__1up')
                    min_hist.SetName('weight_var_EFToperator_PDF_MUF_MUR__1down')
                else:
                    max_hist.SetName('weight_var_EFToperator_PDF_MUF_MUR__1down')
                    min_hist.SetName('weight_var_EFToperator_PDF_MUF_MUR__1up')

                print(str(max_hist.GetName())+ " Integral: "+str(max_hist.Integral()))
                print(str(max_hist.GetName())+ " Integral (with overflow): "+str(max_hist.Integral(1, max_hist.GetNbinsX()+1)))
                print(str(min_hist.GetName())+ " Integral: "+str(min_hist.Integral()))
                print(str(min_hist.GetName())+ " Integral (with overflow): "+str(min_hist.Integral(1,min_hist.GetNbinsX()+1)))
                Theo_hist_files.append(max_hist)
                Theo_hist_files.append(min_hist)

                #add PDF var
                PDF_var_Histos=[]
                PDF_var_Intgrals=[]
                for PDF_var_sys in List_Sys.EFT_scale_factors_PDF_var:
                    PDF_var_hist = gDirectory.Get(PDF_var_sys)
                    PDF_var_Histos.append(PDF_var_hist)
                    PDF_var_Intgrals.append(PDF_var_hist.Integral())

                max_hist_index = np.argmax(PDF_var_Intgrals)
                max_hist = PDF_var_Histos[max_hist_index].Clone()
                max_hist.SetName('weight_var_EFToperator_PDF_var__1up')

                min_hist_index = np.argmin(PDF_var_Intgrals)
                min_hist = PDF_var_Histos[min_hist_index].Clone()
                min_hist.SetName('weight_var_EFToperator_PDF_var__1down')
                Theo_hist_files.append(max_hist)
                Theo_hist_files.append(min_hist)

                #add alpha_S
                max_hist = gDirectory.Get('weight_var_th_MUR10_MUF10_PDF270000').Clone()
                max_hist.SetName('weight_var_EFToperator_alphaS__1up')
                min_hist = gDirectory.Get('weight_var_th_MUR10_MUF10_PDF269000').Clone()
                min_hist.SetName('weight_var_EFToperator_alphaS__1down')
                Theo_hist_files.append(max_hist)
                Theo_hist_files.append(min_hist)

                #add weight_var_th_Var3c
                max_hist = gDirectory.Get('weight_var_th_Var3cUp').Clone()
                max_hist.SetName('weight_var_EFToperator_Var3c__1up')
                min_hist = gDirectory.Get('weight_var_th_Var3cDown').Clone()
                min_hist.SetName('weight_var_EFToperator_Var3c__1down')
                Theo_hist_files.append(max_hist)
                Theo_hist_files.append(min_hist)

                #add Hardronization
                max_hist = gDirectory.Get('weight_var_th_hardHi').Clone()
                max_hist.SetName('weight_var_EFToperator_hard__1up')
                min_hist = gDirectory.Get('weight_var_th_hardLo').Clone()
                min_hist.SetName('weight_var_EFToperator_hard__1down')
                Theo_hist_files.append(max_hist)
                Theo_hist_files.append(min_hist)

                #add isrPDF
                max_hist = gDirectory.Get('weight_var_th_isrPDFplus').Clone()
                max_hist.SetName('weight_var_EFToperator_isrPDF__1up')
                min_hist = gDirectory.Get('weight_var_th_isrPDFminus').Clone()
                min_hist.SetName('weight_var_EFToperator_isrPDF__1down')
                Theo_hist_files.append(max_hist)
                Theo_hist_files.append(min_hist)

            for Theo_Sys in List_Sys.TheoSyst_dict[ProcName]:
                if Theo_Sys not in List_PDF_vars + List_Sys.Sherpa_QCD_scale_factors + List_Sys.EFT_Shower_sys + List_Sys.EFT_scale_factors:
                    Theo_hist = gDirectory.Get(Theo_Sys)
                    Theo_hist_files.append(Theo_hist)

            write_hists(ProcName, dirs, Theo_hist_files)
            
        if "Exp" in dirs:
            for Exp_sys in List_Sys.NormExp_sys + List_Sys.ExtraExp_sys:
                Exp_hist = gDirectory.Get(Exp_sys)
                if not Exp_hist:
                    print(str(Exp_sys)+" does not exist.")
                    continue
                Exp_hist_files.append(Exp_hist)
            write_hists(ProcName, dirs, Exp_hist_files)

if '__main__' in __name__:
    main()
