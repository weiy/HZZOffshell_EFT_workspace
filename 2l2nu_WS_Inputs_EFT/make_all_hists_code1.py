from ROOT import TFile, gROOT, gStyle, TH1, Form, TH1F, TChain
import sys
sys.dont_write_bytecode = True
sys.path.append('./Procs_and_Syst/')
import os
from glob import glob
from array import array
import math
import argparse
import List_Sys
import List_Procs
#import List_Sys_part as List_Sys
from array import array
import numpy as np
from math import sqrt
import glob
import yaml
import pickle
from collections import OrderedDict

gROOT.SetBatch(1)
gStyle.SetOptStat(0)

from apply_ps_uncertainty import fill_PS_hist

parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--tree',    type=str, default='tree_incl_all', help='Name of tree')
parser.add_argument('--inpath',  type=str, default='', help='Add path to the sample directory (only) each campaign accessed separately')
parser.add_argument('--outpath', type=str, default='', help='Add path to output')
parser.add_argument('--sample',  type=str, default='', help='Add a sample you want to read')
parser.add_argument('-c','--campaign',type=str,  nargs='+', default =['mc16a','mc16d','mc16e'],  help='Campaigns needed to run over, example -c mc16a ')
parser.add_argument('--proc',  type=str, default='', help='which process to deal with')
parser.add_argument('--doTheoSys', type=bool, default=True, help='Make Theory system. hists' )
parser.add_argument('--doNormExptSys', type=bool, default=True, help='Make Norm Exptem. system. hists' )
parser.add_argument('--doExtraExptSys', type=bool, default=True, help='Make Extra Experimental Systematic hists')
parser.add_argument('--binCR', type=bool, default=False, help='Turn on binning for the control regions' )

args = parser.parse_args()

def region_of_interest(list_regions,ROI_names,all_regions):
    #ROI: region of interest
    """
    function defines the new regions of interest
    """
    for ROI_name in ROI_names:
        if ROI_name in all_regions:
            ROI = all_regions[ROI_name]
            Cuts, Obs, Obs_name, isVarBin, binning = ROI[0], ROI[1], ROI[2], ROI[4], ROI[5]
            name = ROI_name+"_"+str(Obs_name)
            if isVarBin == True:
                bins = binning
            else:
                bins = np.linspace(binning[1], binning[2], num=binning[0]+1).tolist()
        else:
            print("ERROR: region "+ROI_name+" is not defined!")
        list_regions.update({name:[Cuts, Obs, bins]})

def concat_hists(hist_list, histName):
    """
    Concatenate the historgams from all the regions to make one-long histogram
    easier when removing normalization
    """
    binsContent=[] #list to append bin contents
    binsError  =[] #list to append bin errors
    binsEntry  =0  #gets the entries in the histogram
    outhist_len = 0 #len of the concatenated histogram
    for hist in hist_list:
        outhist_len = outhist_len + hist.GetNbinsX()  #concatenates histograms to get final histogram length
    #Define the output concatenated histogram
    outhist = TH1F(histName, histName, outhist_len, 0, outhist_len)
    #Loop over the histograms, each one corresponding to each SR/CR region
    for hist in hist_list:
        binsEntry = binsEntry+(hist.GetEntries()) #get the entries
        for i in range(1,hist.GetNbinsX()+1): #Only the bins, not under and overflow, add overflow to last and underflow to fist bin
            if i ==  1: #add underflow to first bin, do sum in quadrature for the errors
                binsContent.append(hist.GetBinContent(i)+hist.GetBinContent(0))
                binsError.append(sqrt((hist.GetBinError(i))**2+(hist.GetBinError(0))**2))
            elif i == hist.GetNbinsX(): #add overflow to last coloumn and sum in quadrature for error
                binsContent.append(hist.GetBinContent(i)+hist.GetBinContent(hist.GetNbinsX()+1))
                binsError.append(sqrt((hist.GetBinError(i))**2+(hist.GetBinError(hist.GetNbinsX()+1))**2))
            else: #just get the bin content for other bins
                binsContent.append(hist.GetBinContent(i))
                binsError.append(hist.GetBinError(i))

    #set the information in the concatenated histogram
    outhist.SetEntries(binsEntry)
    for i in range(1,len(binsContent)+1):
        #print( binsContent[i-1])
        outhist.SetBinContent(i, binsContent[i-1])
        outhist.SetBinError(i, binsError[i-1])
    #return the concatenated hist
    #print(sum(binsContent))
    return outhist

def write_hist(list_regions,dictHists,Output_Dir,process,camp,myTree,Nom_Norm,CRs_Norm):
    """
    function defines the histograms to be written out
    depending on the systematics/ processes etc.
    use the concat_hists function to write out the histogram that has been concatenated in the various regions
    """

    OutFolder = args.outpath #path where hist is stored
    listHists = [] #list of histograms to write out
    #Fill nominal histograms

    for histToFill in dictHists.keys():
        #print("histtofill: "+str(histToFill))
        i = 0
        #print(i)
        hist_Rgns =[]
        for regions in list_regions.keys():
            #print(regions)
            histName = regions+"_"+histToFill #histogram named as Region_variable
            #print("\n----------------------"+regions+"----------------------")
            # print "Defining...", histName
            bin_edges = np.array(list_regions[regions][2], dtype='float64') #Get the binning/edges
            #print("bin edges")
            #print(bin_edges)
            hist_Rgns.append(TH1F(histName, histName, len(bin_edges)-1, bin_edges)) #append the histograms to a list
            # print("norm nom:", Nom_Norm)
            # print("CR norm", CRs_Norm)
            Norm = CRs_Norm if ("3l" in regions or "emu" in regions) else Nom_Norm
            norm_to_one = 1.

            # PS systematics: special case, read syst from input files
            if "llvv_PS" in dictHists[histToFill]:
                fillWeight = Form('%s*%s' % (dictHists[histToFill],norm_to_one))#use the appropriate weights (but keep norm separate)
                hist_Rgns[i] = fill_PS_hist(myTree, hist_Rgns[i], list_regions[regions][1], fillWeight, process, regions, list_regions[regions][0], histToFill)
            # special case for release 189: qqZZ does not have FT syst for the CR (and its contribution is tiny), use the nominal weight to set the variation to 1
            elif  ("FT" in dictHists[histToFill] and "qqZZ" in process and ("3l" in regions or "emu" in regions)):
                fillHist = Form('%s>>%s'%(list_regions[regions][1],histName)) #Fill the histogram with the appropriate variable
                fillWeight = Form('%s*%s*%s' % ("weight*(weight_qqZZNLO_EW)",list_regions[regions][0],norm_to_one))#use the nominal weights
                # print(fillHist + ", " + fillWeight)
                myTree.Draw(fillHist, fillWeight)# draw the histogram

            # we need WW FT hists for the complete emu set, but 189 does not have these minitrees
            elif ("FT" in dictHists[histToFill] and "WW" in process):
                fillHist = Form('%s>>%s'%(list_regions[regions][1],histName)) #Fill the histogram with the appropriate variable
                fillWeight = Form('%s*%s*%s' % ("weight",list_regions[regions][0],norm_to_one))#use the nominal weights
                # print(fillHist + ", " + fillWeight)
                myTree.Draw(fillHist, fillWeight)# draw the histogram
            else:
                fillHist = Form('%s>>%s'%(list_regions[regions][1],histName)) #Fill the histogram with the appropriate variable
                fillWeight = Form('%s*%s*%s' % (dictHists[histToFill],list_regions[regions][0],norm_to_one))#use the appropriate weights
                #print(fillHist + ", " + fillWeight)
                myTree.Draw(fillHist, fillWeight)# draw the histogram

            hist_Rgns[i].Scale(Norm)
            #print("Integral: "+str(hist_Rgns[i].Integral()))
            i =i+1
        #append the concatenated histogram
        listHists.append(concat_hists(hist_Rgns, histToFill))

    #print(listHists)
    OutDirCamp = os.path.join(OutFolder,camp)
    OutSample  = process+"_hists.root"

    os.system("mkdir -p " + OutDirCamp)
    OutFile = os.path.join(OutDirCamp,OutSample)
    if 'Nom_hist' in dictHists.keys():
        os.system("rm -f " + OutFile) # remove older file
    fOut_FromTree = TFile(OutFile, "UPDATE")

    directory = fOut_FromTree.GetDirectory(Output_Dir)
    if not (directory):
        directory = fOut_FromTree.mkdir(Output_Dir)
    directory.cd()
    #write out all the histograms
    for hist in listHists:
        print("\nWriting..." + str(hist))
        print(hist.Integral())
        directory.cd()
        hist.Write()
        hist.SetDirectory(0)

    fOut_FromTree.Write("",TFile.kOverwrite)
    fOut_FromTree.Close()

    os.system("mkdir -p " + OutDirCamp+'/ratio/')
    OutFile_ratio = os.path.join(OutDirCamp+'/ratio/',OutSample.replace('.root','_ratio.root'))
    if 'Nom_hist' in dictHists.keys():
        os.system("rm -f " + OutFile_ratio) # remove older file
    fOut_FromTree_ratio = TFile(OutFile_ratio, "UPDATE")

    directory = fOut_FromTree_ratio.GetDirectory(Output_Dir)
    if not (directory):
        directory = fOut_FromTree_ratio.mkdir(Output_Dir)
    directory.cd()
    if 'Nom_hist' not in dictHists.keys():
        Nom_hist=fOut_FromTree_ratio.Get('Nominal/Nom_hist')
    for hist in listHists:
        if 'Nom_hist' not in dictHists.keys():
            hist.Divide(Nom_hist)
        hist.Write()
        hist.SetDirectory(0)

    fOut_FromTree_ratio.Write("",TFile.kOverwrite)
    fOut_FromTree_ratio.Close()

def GetLumi(camp):
    if   "a" in camp: lumi = 36.207
    elif "d" in camp: lumi = 44.307
    elif "e" in camp: lumi = 58.250
    else:             lumi = 0
    return lumi

def main():
    """
    Main function.
    """
    #gStyle.SetOptStat(0)
    TH1.SetDefaultSumw2()
    #reading the arguments provided
    EosDir = args.inpath
    OutDir = args.outpath
    TreeName = args.tree
    InSample = args.sample

    #The Sample being processed
    print("InSample "+InSample)

    campaigns = args.campaign #whichever campaigns need to be run

    ##############
    # Get weight #
    ##############
    process = args.proc
    weight = List_Procs.procs_2l2v[args.proc].weight
    print(process)
    print(weight)

    ###########
    # Regions #
    ###########

    list_regions=OrderedDict()
    file_all_regions = open('all_regions.yaml','r')
    yaml_all_regions = yaml.safe_load(file_all_regions)

    # list of region of interest
    ROI_list = ['CR_3lep_0jet','CR_3lep_1jet','CR_3lep_2jet','CR_emu','CR_ZJets','SR_ggF_2l2nu','SR_Mixed_2l2nu','SR_VBF_2l2nu']
    #ROI_list = ['SR_2l2nu_binwidth1GeV'] #check truth sample
    #ROI_list = ['SR_2l2nu']
    region_of_interest(list_regions,ROI_list,yaml_all_regions)

    file_region_interest = open('regions_of_interest.yaml','w')
    yaml.dump(list_regions, file_region_interest, default_flow_style=False)
    file_region_interest.close()
    #print(list_regions)
    print(yaml.dump(list_regions, default_flow_style=False))

    ####################################
    # Defining dict for the Histograms #
    ####################################

    Nominal_tree = args.tree

    th_dir_Nominal = 'Nominal'
    th_dir_TheoSys = 'TheoSys'
    th_dir_ExptSys = 'ExptSys'

    dictNominalhists=OrderedDict()
    dictNominalhists.update({"Nom_hist":Form('(%s)'%(weight))})
    print("dictNominalhists: "+str(dictNominalhists))

    dictTheoryhists=OrderedDict()
    for var_name in List_Sys.TheoSyst_dict[process]:
        print("dictTheoryhists var_names: "+str(var_name))
        #EFT sample?
        if "var" in var_name:
            dictTheoryhists.update({var_name:Form('(%s)*(%s)'%(weight,var_name))})

        elif ("weight_qqZZNLO_EW" in var_name) or ("QCD_ggZZk" in var_name):
            dictTheoryhists.update({var_name:Form('(%s)*(%s)/(%s)'%(weight,var_name,var_name.split('__')[0]))})
        else:
            dictTheoryhists.update({var_name:Form('(%s)*(%s)/(weight)'%(weight,var_name))})
    print("dictTheoryhists: "+str(dictTheoryhists))

    dictNormExphists=OrderedDict()
    for NormExp_sys in List_Sys.NormExp_sys:
        print("dictNormExphists sys_vars: "+str(NormExp_sys))
        if ('FT' in NormExp_sys and "189" in EosDir):
            # FT is produced for qqZZ, WZ, ttbar, Wt and Wtbar, also adding WW here to complete emu, which is handled in the writehist function
            if ("345666" in InSample or "364253" in InSample or "emu" in process):
                    dictNormExphists.update({NormExp_sys:Form('(%s)*(%s)/(weight_MUON_EFF_RECO_STAT_LOWPT__1up)'%(weight,NormExp_sys))})
            else:
                print("Skipping FT for this sample, since we did not produce it")
        else:
            # to get rid of shift in weightsyst, normalize to weight_FT_EFF_Light_systematics__1up, which should have a variation of 0 for all samples (validated on ttbar)
            # this fixes the bug that was fixed in the minitree code here: https://gitlab.cern.ch/HZZ/HZZSoftware/HZZCutCode/-/commit/d1f74e32
            dictNormExphists.update({NormExp_sys:Form('(%s)*(%s)/(weight_MUON_EFF_RECO_STAT_LOWPT__1up)'%(weight,NormExp_sys))})
    print("dictNormExphists: "+str(dictNormExphists))

    #############################################
    # Accessing file and Writing the histograms #
    #############################################

    for camp in campaigns:
        print("doing Campaign " +str(camp)+"...")
        if List_Sys.Systematics_list[0] != 'weightSyst': #weightSyst (2l2v) is equivalent to NormSystematic (4l)
            print('make sure NormSystematics is firstly processed as we need '
                  'get the division of others to Nom_hist. The ratio is more useful.')
            exit()
        InSample_CR = InSample.replace(".root","_CR.root")
        # for these samples (qqZZ, WZ, ttbar, Wt, Wtbar) , we use the trees that have the correct FT syst
        lumi = GetLumi(camp)
        print("lumi:", lumi)
        n=0
        for sys in List_Sys.Systematics_list:
            if sys == 'weightSyst':
                EosDir_backup = EosDir
                if (("345666" in InSample or "364253" in InSample or "410472" in InSample or "410646" in InSample or "410647" in InSample)  and "189" in EosDir):
                    EosDir = EosDir + "FTfix/"
                chain_Syst = TChain("2l2nu_Trees_Sys")
                print("weiy: sys: "+str(sys))
                myFile_path=os.path.join(EosDir,camp,'Systematics',sys,InSample)
                chain_Syst.Add(os.path.join(EosDir,camp,'Systematics',sys,InSample,'tree_PFLOW'))
                # qqZZ has new FT trees, but only for the SR
                if ("345666" in InSample):
                    EosDir = EosDir_backup
                chain_Syst.Add(os.path.join(EosDir,camp,'Systematics_Background',sys,InSample_CR,'tree_3l_PFLOW'))
                chain_Syst.Add(os.path.join(EosDir,camp,'Systematics_Background',sys,InSample_CR,'tree_emu_PFLOW'))
                try:
                    myFile = TFile(myFile_path)
                except:
                    print('Failed to open ' + myFile_path)
                    continue
                myTree = myFile.Get(TreeName)
                total = myTree.GetEntries()
                infoHist = myFile.Get("Hist/hInfo_PFlow")
                Nom_Norm = infoHist.GetBinContent(1)*2.0/infoHist.GetEntries()*lumi/infoHist.GetBinContent(2)
                if n==0:
                    print("\n------------------------------")
                    print("xsec: ", infoHist.GetBinContent(1)*2.0/infoHist.GetEntries())
                    print("sow: ", infoHist.GetBinContent(2))
                    print("Nom norm", Nom_Norm)
                    n+=1
                    chain_Syst.Print("")
                Histo_CRs = TFile(os.path.join(EosDir,camp,'Systematics_Background',sys,InSample_CR))
                infoHist_CRs = Histo_CRs.Get("Hist/hInfo_PFlow")
                CRs_Norm = infoHist_CRs.GetBinContent(1)*2.0/infoHist_CRs.GetEntries()*lumi/infoHist_CRs.GetBinContent(2)
                #CRs_Norm =1
                if n==0:
                    print("CR norm: "+str(CRs_Norm))

                # reset EosDir for FT trees
                EosDir = EosDir_backup

                print("Filling nominal histograms...")
                write_hist(list_regions,dictNominalhists,th_dir_Nominal,process,camp,chain_Syst,Nom_Norm,CRs_Norm)
                if args.doTheoSys == True:
                    write_hist(list_regions,dictTheoryhists,th_dir_TheoSys,process,camp,chain_Syst,Nom_Norm,CRs_Norm)
                if args.doNormExptSys == True:
                    write_hist(list_regions,dictNormExphists,th_dir_ExptSys,process,camp,chain_Syst,Nom_Norm,CRs_Norm)
                print("Now filling other systematics...")

            else:
                if args.doExtraExptSys == True:
                    chain_Syst_Expt = TChain("2l2nu_Trees_ExptSys")
                    chain_Syst_Expt.Add(os.path.join(EosDir,camp,'Systematics',sys,InSample,'tree_PFLOW'))
                    Histo_Nom = TFile(os.path.join(EosDir,camp,'Systematics',sys,InSample))
                    infoHist_Nom = Histo_Nom.Get("Hist/hInfo_PFlow")
                    Nom_Norm = infoHist_Nom.GetBinContent(1)*2.0/infoHist_Nom.GetEntries()*lumi/infoHist_Nom.GetBinContent(2)

                    chain_Syst_Expt.Add(os.path.join(EosDir,camp,'Systematics_Background',sys,InSample_CR,'tree_3l_PFLOW'))
                    chain_Syst_Expt.Add(os.path.join(EosDir,camp,'Systematics_Background',sys,InSample_CR,'tree_emu_PFLOW'))
                    Histo_Nom_CR = TFile(os.path.join(EosDir,camp,'Systematics_Background',sys,InSample_CR))
                    infoHist_Nom_CR = Histo_Nom_CR.Get("Hist/hInfo_PFlow")
                    CRs_Norm = infoHist_Nom_CR.GetBinContent(1)*2.0/infoHist_Nom.GetEntries()*lumi/infoHist_Nom.GetBinContent(2)
                    dictExtraExphists=OrderedDict()
                    dictExtraExphists.update({sys:Form('(%s)'%(weight))})
                    write_hist(list_regions,dictExtraExphists,th_dir_ExptSys,process,camp,chain_Syst_Expt,Nom_Norm,CRs_Norm)

if '__main__' in __name__:
    main()
