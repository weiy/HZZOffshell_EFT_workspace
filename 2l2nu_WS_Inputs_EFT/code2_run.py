import os

outname="2l2v_EFT_WS_wCR"
date="23Jan2023"

inPath="./outputs_"+date+"/Merged_code1_"+outname
outputPath="./outputs_"+date+"/Merged_code2_"+outname

os.system('mkdir -p ' + outputPath)
os.system('rm -rf ' + outputPath+'/*')

ProcNames = ["qqZZ_4l_0jet","qqZZ_4l_1jet","qqZZ_4l_2jet",
             "qqZZ_2l2v_0jet","qqZZ_2l2v_1jet","qqZZ_2l2v_2jet",
             "ggZZ","VBF",
             "WZ_3lep_0jet","WZ_3lep_1jet","WZ_3lep_2jet",
             "Zjets_bkg",
             "tot_emu_bkg","others_bkg",
             "OtpSM","OpgSM",
             "OpgSqrd","OtpSqrd",
             "OpgOtp"]

# ProcNames = ["qqZZ_4l_0jet","qqZZ_4l_1jet","qqZZ_4l_2jet"]

for ProcName in ProcNames:
    os.system('python make_sys_hists_code2.py --inProc '+ProcName+' --inpath '+inPath+' --outpath '+outputPath)
    print('--------------------------------')
