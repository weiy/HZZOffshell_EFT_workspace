import os

SM_dir='WS_inputs_SM_only'
EFT_pre_dir='WS_inputs_23Jan2023_2l2v_EFT_WS_wCR'
EFT_fin_dir='WS_inputs_23Jan2023_2l2v_EFT_WS_wCR_final'

def append(lumi):

    f_SM = open(SM_dir+'/bgyields'+lumi+'.txt','r')
    f_EFT_pre=open(EFT_pre_dir+'/bgyields'+lumi+'.txt','r')  #preliminary
    f_EFT_fin=open(EFT_fin_dir+'/bgyields'+lumi+'.txt','w') #final

    for line in f_SM:
        f_EFT_fin.write(line)

    f_EFT_fin.write('\n')
    for line in f_EFT_pre:
        if 'n_O' in line:
            f_EFT_fin.write(line)

    print(EFT_fin_dir+'/bgyields'+lumi+'.txt finished!')
    f_SM.close()
    f_EFT_pre.close()
    f_EFT_fin.close()

def nuisance():

    f_SM = open(SM_dir+'/nuisance.txt','r')
    f_EFT_pre=open(EFT_pre_dir+'/nuisance.txt','r')  #preliminary
    f_EFT_fin=open(EFT_fin_dir+'/nuisance.txt','w') #final

    for line in f_SM:
        #remove _HM_CSSKIN_ as this is not important and not included in SM analysis
        # MCSTAT will make workspace crash
        if 'HOQCD_syst_qqZZ_1_' not in line and 'MCSTAT' not in line:
            f_EFT_fin.write(line)
    #
    # f_EFT_fin.write('#global systematics:\n')

    # nuisance_SM=[]
    # for line in f_SM:
    #     if '#' not in line and line !='\n':
    #         nuisance_SM.append(line.rstrip())

    # nuisance_EFT_pre=[]
    # for line in f_EFT_pre:
    #     if '#' not in line and line !='\n':
    #         nuisance_EFT_pre.append(line.rstrip())

    # nuisance_EFT_fin=sorted(list(set(nuisance_SM+nuisance_EFT_pre)))
    # for item in nuisance_EFT_fin:
    #     f_EFT_fin.write(item+'\n')

if '__main__' in __name__:
    append('')
    append('139')

    Procs_SM=['qqZZ_1_Part0','qqZZ_1_Part1','qqZZ_1_Part2',
              'qqZZ_2_Part0','qqZZ_2_Part1','qqZZ_2_Part2',
              'ggSNLO','ggSBINLOI','ggBNLO',
              'VBFSBI','VBFSBI5','VBFSBI10','VBFB',
              'WZ_3lep_Part0','WZ_3lep_Part1','WZ_3lep_Part2',
              'tot_emu_bkg','Zjets_bkg','Other_bkg']

    Procs_EFT=['OtpSM','OpgSM','OpgSqrd','OtpSqrd','OpgOtp']

    for Proc in Procs_SM:
        os.system('cp '+SM_dir+'/'+Proc+'_sys.txt '+EFT_fin_dir+'/')

    for Proc in Procs_EFT:
        os.system('cp '+EFT_pre_dir+'/'+Proc+'_sys.txt '+EFT_fin_dir+'/')

    nuisance()
