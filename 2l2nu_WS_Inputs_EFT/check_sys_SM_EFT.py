import os
import json
from collections import OrderedDict
import numpy as np
from glob import glob

SM_dir='/afs/cern.ch/user/m/mveen/public/Workspaces_llvv/WS_15_08_2022/WS_inputs_Final_WS_22_08_15/'
EFT_dir='./WS_inputs_07Aug2022_2l2v_EFT_WS_wCR/'
f=open('/afs/cern.ch/user/m/mveen/public/Workspaces_llvv/WS_15_08_2022/WS_inputs_Final_WS_22_08_15/bgyields139.txt')
samples=['n_qqZZ_1_Part0','n_qqZZ_1_Part1','n_qqZZ_1_Part2','n_qqZZ_2_Part0','n_qqZZ_2_Part1','n_qqZZ_2_Part2','n_ggSBINLOI','n_ggBNLO','n_ggSNLO','n_VBFSBI','n_WZ_3lep_Part0','n_WZ_3lep_Part1','n_WZ_3lep_Part2','n_Zjets_bkg','n_tot_emu_bkg','n_Other_bkg']

def read_sys(sys_dir):
    sys = OrderedDict()
    for sys_file in glob(sys_dir+'./*sys.txt'):
        sys_channel = os.path.basename(sys_file).replace('_sys.txt','')
        sys[sys_channel] = OrderedDict()
        for line in open(sys_file):
            line=line.rstrip()
            if not line.lstrip().startswith('#'):
                if line.lstrip().startswith('['):
                    category = line.replace('[','').replace(']','')
                    sys[sys_channel][category] = OrderedDict()
                else:
                    var_string = line.split()
                    var_name = var_string[0].replace('weight_','')
                    var_up = float(var_string[2])
                    var_down = float(var_string[3])
                    sys[sys_channel][category][var_name] = OrderedDict()
                    sys[sys_channel][category][var_name]['up'] = var_up
                    sys[sys_channel][category][var_name]['down'] = var_down
    return sys

#structure
#sys[sys_channel][category][var_name]['up']
def get_channels(sys):
    channels = []
    for channel in sys:
        channels.append(channel)
    return channels

def get_categories(sys):
    categories = []
    for channel in sys:
        for category in sys[channel]:
            categories.append(category)
        return categories

def get_var_names(sys):
    var_names = []
    for channel in sys:
        for category in sys[channel]:
            for var_name in sys[channel][category]:
                var_names.append(var_name)
    return list(sorted(set(var_names),reverse=True))
        
sys_SM = read_sys(SM_dir)
sys_EFT = read_sys(EFT_dir)

#print(json.dumps(sys_SM,sort_keys=True, indent=4))
#print(json.dumps(sys_EFT,sort_keys=True, indent=4))
                    
#######################channel start######################
#                    SM_channel:EFT_channel 
sys_channel_pairs = ['qqZZ_2_Part0:qqZZ_2l2v',
                     'qqZZ_1_Part0:qqZZ_4l',
                     'ggSBINLOI:ggZZ',
                     'VBFSBI:VBF',
                     'tot_emu_bkg:tot_emu_bkg',
                     'WZ_3lep_Part0:WZ_3lep',
                     'Zjets_bkg:Zjets_bkg',
                     'Other_bkg:others_bkg']
#######################channel end#######################

#######################category start######################
categories_SM = get_categories(sys_SM)
categories_EFT = get_categories(sys_EFT)

#                SM_category:EFT_category
category_pairs = []
for i in range(len(categories_EFT)):
    #print(categories_SM[i])
    #print(categories_EFT[i])
    category_pairs.append(categories_SM[i if i < 1 else i+2]+':'+categories_EFT[i])
#######################category end######################

#######################var_names start######################
var_names = get_var_names(sys_SM) #only need to get var_names in SM
#######################var_names end######################

#for var_name in var_names:
#    print(var_name)

#now let's compare
#and then check
#sys[sys_channel][category][var_name]['up']

##loop and check one by one##
#channel
#category
#var_name


'''
sys_channel_pairs = ['qqZZ_2_Part0:qqZZ_2l2v',
                     'qqZZ_1_Part0:qqZZ_4l',
                     'ggSBINLOI:ggZZ',
                     'VBFSBI:VBF',
                     'tot_emu_bkg:tot_emu_bkg',
                     'WZ_3lep_Part0:WZ_3lep',
                     'Zjets_bkg:Zjets_bkg',
                     'Other_bkg:others_bkg']
'''
#check one by one
channel_to_check=['qqZZ_2l2v']


#channel
for sys_channel_pair in sys_channel_pairs:
    sys_channel_SM = sys_channel_pair.split(':')[0]
    sys_channel_EFT = sys_channel_pair.split(':')[1]
    if sys_channel_EFT not in channel_to_check:
        continue
    #category
    for category_pair in category_pairs:
        category_SM = category_pair.split(':')[0]
        category_EFT = category_pair.split(':')[1]
        for var_name in var_names:
            #SM_up = sys_SM[sys_channel_SM][category_SM][var_name]['up']
            #EFT_up = sys_EFT[sys_channel_EFT][category_EFT][var_name]['up']
            #SM_down = sys_SM[sys_channel_SM][category_SM][var_name]['down']
            #EFT_down = sys_EFT[sys_channel_EFT][category_EFT][var_name]['down']
            try:
                SM_up = sys_SM[sys_channel_SM][category_SM][var_name]['up']
                SM_down = sys_SM[sys_channel_SM][category_SM][var_name]['down']
            except:
                print('no '+var_name+':'+category_SM+':'+sys_channel_SM+' in sys_SM')
                continue
            try:
                EFT_up = sys_EFT[sys_channel_EFT][category_EFT][var_name]['up']
                EFT_down = sys_EFT[sys_channel_EFT][category_EFT][var_name]['down']
            except:
                print('no '+var_name+':'+category_EFT+':'+sys_channel_EFT+' in sys_EFT')
                continue

            #print(SM_up,EFT_up,SM_down,EFT_down)
            #print(sys_SM[sys_channel_SM][category_SM][var_names],)
            #print(sys_channel_SM,sys_channel_EFT)
