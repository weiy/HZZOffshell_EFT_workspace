from ROOT import *
import sys
sys.dont_write_bytecode = True
sys.path.append('./Procs_and_Syst/')
import os
from array import array
import math
import argparse
import List_Sys
import numpy as np
import pickle
import yaml

parser = argparse.ArgumentParser(description='Make Config files')
parser.add_argument('--FileName', type=str, default='23Jan2023_EFT_2l2v_config_file_wCR_wSys_wMCSTAT_mT_ZZ.ini', help='Name of output config files')
parser.add_argument('--outpath',  type=str, default='./WS_inputs_config', help='Add path to output config file')
parser.add_argument('--inpath',  type=str, default='./WS_inputs_23Jan2023_2l2v_EFT_WS_wCR', help='Add path to where all the WS are for the config file')
parser.add_argument('--MCsets', nargs='+',default='OpgSM,OtpSM,OpgSqrd,OtpSqrd,OpgOtp,qqZZ_2l2v_0jet,qqZZ_2l2v_1jet,qqZZ_2l2v_2jet,qqZZ_4l_0jet,qqZZ_4l_1jet,qqZZ_4l_2jet,ggZZ,VBF,WZ_3lep_0jet,WZ_3lep_1jet,WZ_3lep_2jet,tot_emu_bkg,Zjets_bkg,others_bkg', help='Procs to study')

#Reading in the arguments
args = parser.parse_args()
mcsets = args.MCsets
input_path = args.inpath

config_file_name = os.path.join(args.outpath, args.FileName)
config_file = open(config_file_name,'w')

print("Getting regions of interest")
#Getting the list of regions with binning/ observables - output of code 1
file_regions = open('regions_of_interest.yaml','r')
regions_list = yaml.load(file_regions)
file_regions.close()
print(regions_list)

#Looping over the bin regions to get the bin edges/to break down concatenated histograms
bin_regions =[] #upper edges of the regions
bin_regions_names=[] #names (and observable)
region_edges=[]#lower and upper limits
region_obs=[]
region_wgt=[]
region_bins=[]

for regions in regions_list.keys():
    bin_regions.append(len(regions_list[regions][2])-1)
    #print(bin_regions)
    #Needs to be changed depending on what the observable is called
    if "ncl" in (regions_list[regions][1]):
        obs = "ggFNN_MELA"
    elif "VBF" in (regions_list[regions][1]):
        obs = "VBFNN_MELA"
    elif "MCFM" in (regions_list[regions][1]):
        obs = "ME_disc"
    else:
        obs = regions_list[regions][1]

    rgn_name = regions.split("_")
    if "F" in rgn_name[2] or "3lep" in rgn_name[1] :
        bin_regions_names.append((obs+"_"+rgn_name[0]+"_"+rgn_name[1]+"_"+rgn_name[2]))
    else:
        bin_regions_names.append((obs+"_"+rgn_name[0]+"_"+rgn_name[1]))
    region_obs.append(obs)
    region_wgt.append(regions_list[regions][0])
    region_bins.append(regions_list[regions][2])

region_edges.append(1)
for i in range(0, len(bin_regions)):
    region_edges.append(sum(bin_regions[0:i+1])+1)
#print(region_edges)
#print(bin_regions_names)

def main():
    print("Writing Config file...")
    print("[Main]")
    config_file.write("[main]\n\n")
    config_file.write("fileDir = "+str(input_path)+"\n\n")
    config_file.write("data = /eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hllvv/Run2/minitrees_rel189/r189_llvv_minitrees/skimmed_data/skimmed_data_minitree_SR_plus_CR_renameTTree.root\n\n")
    config_file.write("NPlist = nuisance.txt\n\n")
    config_file.write("#NPlist = nuisance_noSys.txt\n\n")

    config_file.write("categories = ")
    categories = ""
    for rgn_i in range(0,len(bin_regions_names)):
        #print(rgn_i)
        for bins in range(1, bin_regions[rgn_i]+1):
            #print(bins)
            categories = categories+(bin_regions_names[rgn_i]+"_Incl_bin_"+str(bins)+"_13TeV,")

    config_file.write(categories[:-1])
    config_file.write("\n\nmcsets = "+str(mcsets)+"\n\n")
    print("[Cuts]")
    config_file.write("[cuts]\n\n")
    for rgn_i in range(0,len(bin_regions_names)):
        obs = region_obs[rgn_i]
        wgt = region_wgt[rgn_i]
        i=0
        for bins in range(1, bin_regions[rgn_i]+1):
            #print(bins)
            #print(region_bins[rgn_i][i])
            obs_cut = "("+(str(region_bins[rgn_i][i])+"<"+obs+") && (" +obs+"<"+str(region_bins[rgn_i][i+1])+")")


            # fix the cuts for data: remove lower bound for ggF
            obs_cut = obs_cut.replace("(250.0<mT_ZZ) && (mT_ZZ<300.0)", "(mT_ZZ<300.0)")
            obs_cut = obs_cut.replace("(250<mT_ZZ) && (mT_ZZ<300)", "(mT_ZZ<300)")
            # remove upper bound for ggF
            obs_cut = obs_cut.replace("(2000.0<mT_ZZ) && (mT_ZZ<3000.0)", "(2000.0<mT_ZZ)")
            obs_cut = obs_cut.replace("(2000<mT_ZZ) && (mT_ZZ<3000)", "(2000<mT_ZZ)")
            # remove lower bound for VBF/Mixed
            obs_cut = obs_cut.replace("(100.0<mT_ZZ) && (mT_ZZ<420.0)", "(mT_ZZ<420.0)")
            obs_cut = obs_cut.replace("(100<mT_ZZ) && (mT_ZZ<420)", "(mT_ZZ<420)")
            # remove upper bound for VBF/Mixed
            obs_cut = obs_cut.replace("(820.0<mT_ZZ) && (mT_ZZ<1700.0)", "(820.0<mT_ZZ)")
            obs_cut = obs_cut.replace("(820<mT_ZZ) && (mT_ZZ<1700)", "(820<mT_ZZ)")

            wgt = wgt.replace("This->GetTreeNumber()==0", "event_type != 2 && event_3CR == 0") #SR
            wgt = wgt.replace("This->GetTreeNumber()==1", "event_3CR != 0") #3lCR
            wgt = wgt.replace("This->GetTreeNumber()==2", "event_type == 2 && event_3CR == 0") #emuCR
            print (wgt)
            print(obs_cut)
            i=i+1
            # no need for observable boundaries when there is only one bin
            if (bin_regions[rgn_i] == 1):
                config_file.write(bin_regions_names[rgn_i]+"_Incl_bin_"+str(bins)+"_13TeV = ("+wgt+")\n")
            else:
                config_file.write(bin_regions_names[rgn_i]+"_Incl_bin_"+str(bins)+"_13TeV = ("+wgt+"&&"+obs_cut+")\n")

    print("[Observables]")
    config_file.write("\n[observables]\n\n")
    for rgn_i in range(0,len(bin_regions_names)):
        for bins in range(1, bin_regions[rgn_i]+1):
            config_file.write(bin_regions_names[rgn_i]+"_Incl_bin_"+str(bins)+"_13TeV = ")
            config_file.write(str(region_obs[rgn_i])+":"+str(region_obs[rgn_i])+",1,"+str(region_bins[rgn_i][0])+","+str(region_bins[rgn_i][-1])+"\n")

    print("[Coefficients]")
    config_file.write("\n[coefficients]\n\n")
    mc_sets = mcsets.split(",")
    poi ="none"
    isSignal = False
    for mc in mc_sets:
        print("looking at : "+mc)

        if "Zjets_bkg" in mc: poi = "mu_Zjets"
        elif "tot_emu_bkg" in mc: poi = "mu_emu"

        elif "0jet" in mc and "qqZZ" in mc: poi = "mu_qqZZ"
        elif "1jet" in mc and "qqZZ" in mc: poi = "mu_qqZZ*mu_qqZZ_1"
        elif "2jet" in mc and "qqZZ" in mc: poi = "mu_qqZZ*mu_qqZZ_1*mu_qqZZ_2"

        elif "WZ_3lep_0jet" in mc: poi = "mu_3lep"
        elif "WZ_3lep_1jet" in mc: poi = "mu_3lep*mu_3lep_1"
        elif "WZ_3lep_2jet" in mc: poi = "mu_3lep*mu_3lep_1*mu_3lep_2"

        elif "OpgSM" in mc: poi = "cpG"
        elif "OtpSM" in mc: poi = "ctp"
        elif "OpgSqrd" in mc: poi = "cpGsqrd*cpG*cpG"
        elif "OtpSqrd" in mc: poi = "ctpsqrd*ctp*ctp"
        elif "OpgOtp" in mc: poi = "cpGctp*cpG*ctp"

        else: poi ="none"

        if "qqZZ" in poi: #just a dummy, norm not removed so not needed
            config_file.write(mc+" = factors:n_"+mc+",bgyields139.txt ; sys:"+mc+"_sys.txt; poi:"+poi+" \n")
        elif (("emu" in poi) or ("3lep" in poi) or ("ets" in poi)):
            config_file.write(mc+" = factors:n_"+mc+",bgyields139.txt ; sys:"+mc+"_sys.txt; poi:"+poi+" \n")
        elif "none" in poi:
            config_file.write(mc+" = factors:n_"+mc+",bgyields.txt ; sys:"+mc+"_sys.txt; global:ATLAS_LUMI(139.0/0.983/1.017)\n")
        else:
            config_file.write(mc+" = factors:n_"+mc+",bgyields.txt ; sys:"+mc+"_sys.txt; poi:"+poi+"; global:ATLAS_LUMI(139.0/0.983/1.017)\n")


    print("[Categories]")
    config_file.write("\n[")
    config_file.write(categories[:-1])
    config_file.write("]\n\n")

    for mc in mc_sets:
        if "O" in mc:
            isSignal = True
        else:
            isSignal = False

        if isSignal==True:
            config_file.write(mc+" = SampleCount : ATLAS_Signal_"+mc+"\n")
        if isSignal==False:
            config_file.write(mc+" = SampleCount : ATLAS_Bkg_"+mc+"\n")


    config_file.write("\n[asimov: asimovData]\n\n")

    config_file.write("cpG = 0.0\n")
    config_file.write("ctp = 0.0\n")
    config_file.write("cpGsqrd = 0.0\n")
    config_file.write("ctpsqrd = 0.0\n")
    config_file.write("cpGctp = 0.0\n")

    config_file.write("mu_qqZZ = 1.0\n")
    config_file.write("mu_qqZZ_1 = 1.0\n")
    config_file.write("mu_qqZZ_2 = 1.0\n")

    config_file.write("mu_emu = 1.0\n")
    config_file.write("mu_Zjets = 1.0\n")

    config_file.write("mu_3lep = 1.0\n")
    config_file.write("mu_3lep_1 = 1.0\n")
    config_file.write("mu_3lep_2 = 1.0\n")

    print('\nFinished!!\nSee '+str(config_file_name))

if '__main__' in __name__:
    main()
