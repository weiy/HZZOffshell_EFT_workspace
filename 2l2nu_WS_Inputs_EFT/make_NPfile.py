import sys
sys.path.append('./Procs_and_Syst/')
sys.dont_write_bytecode = True
import os
import os.path
from os import path
from glob import glob
from array import array
import math
import argparse
import List_Sys
from array import array
import numpy as np
import pickle

parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--outpath', type=str, default='./Workspace_inputs_binnedCR', help='Add path to output histograms')
args = parser.parse_args()
outFolder = args.outpath


NP_names  = outFolder+"/"+"nuisance.txt"
NP_noSys  = outFolder+"/"+"nuisance_noSys.txt"

NP_names_file = open(NP_names,"w")
NP_names_file.write("#global systematics:\n")
NP_names_file.write("ATLAS_LUMI\n")
NP_names_outFile = ("./NP_names.txt")
with open (NP_names_outFile, 'rb') as NP:
    NP_names = pickle.load(NP)

NP_names_set = set(NP_names)

for name in NP_names_set:
    #remove EFT related theoretical Uncertainties as we don't know these parts
    #including removing EFT_MUR_MUF, EFT_PDF_var, isrPDF, alphaS
    if "EFToperator" in name:
        continue
    #MCSTAT will make workspace built failed. To be fixed
    if "MCSTAT" in name:
        continue
    if "llvv_PS" in name:
        NP_names_file.write((str(name)).replace('weight_var_','').replace('weight_','')+"_shape\n")
        NP_names_file.write((str(name)).replace('weight_var_','').replace('weight_','')+"_norm\n")
    else:
        NP_names_file.write((str(name)).replace('weight_var_','').replace('weight_','')+"\n")

#NP_names_file.write("redBkgNorm"+"\n")
#NP_names_file.write("redBkgShape"+"\n")
NP_names_file.write("JET_Flavor_Composition_VBF"+"\n")
NP_names_file.write("JET_Flavor_Response_VBF"+"\n")
NP_names_file.write("JET_Flavor_Composition_qq"+"\n")
NP_names_file.write("JET_Flavor_Response_qq"+"\n")
NP_names_file.write("JET_Flavor_Composition_gg"+"\n")
NP_names_file.write("JET_Flavor_Response_gg"+"\n")


NP_names_file.write(""+"\n")

NP_noSys_file = open(NP_noSys,"w")
NP_noSys_file.write("#global systematics:\n")
NP_noSys_file.write("ATLAS_LUMI\n\n")
for name in NP_names_set:
    if "MCSTAT" in str(name):
        NP_noSys_file.write((str(name)).replace('weight_var_','')+"\n")

# Copy yields for reducible bkg file
'''
with open('/afs/cern.ch/work/w/wleight/public/Higgs_2018_bkg/redBkg_val.txt','r') as RedBkgYieldsFile, open(outFolder+"/bgyields.txt","a+") as bkgyields_file:
    for line in RedBkgYieldsFile:
        bkgyields_file.write("\n"+line)

'''
