#python3
import glob
#import root_numpy
import uproot
import pandas as pd
from pandas import HDFStore
from datetime import datetime
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import math
import csv
import argparse
import pickle
import os
import multiprocessing as mp

def plot_hist(sample):
    print(sample+'\n')
    #labels=[str(i*10+220) for i in range(0,178)]
    #labels=['0','200','450','700','950','1200','1450','1700','1950']
    x_values_virtual = [-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
    x_values = [0,250,300,350,400,450,500,550,600,650,700,800,900,1000,1200,1400,2000,3000]
    labels= [str(i) for i in x_values]
    for sys in ['Nominal','ExptSys','TheoSys']:
        i=0
        for term in dict(sorted(hists_f[sample][sys].items())).keys():
            print('plotting ' + str(term))
            fig1,ax = plt.subplots(figsize=(12,9))
            if sys != 'Nominal':
                print(sample)
                #print(type(hists_f[sample]['Nominal']['Nom_hist'].values()))
                #print(hists_f[sample]['Nominal']['Nom_hist'].values())
                ax.step(1+np.asarray(range(-1,len(hists_f[sample][sys][term].values()))), np.concatenate(([1.0],hists_f[sample][sys][term].values()/(hists_f[sample]['Nominal']['Nom_hist'].values()+10e-10))), label=sample+': #'+str(i)+': '+str(term.split(';')[0]),linestyle='solid',**kwargs)
            else:
                ax.step(1+np.asarray(range(-1,len(hists_f[sample][sys][term].values()))),np.concatenate(([0.0],hists_f[sample][sys][term].values())), label=str(i)+': '+sample+': '+str(term),linestyle='solid',**kwargs)
                print(np.concatenate(([0.0],hists_f[sample][sys][term].values())))
            plt.legend(loc='upper right',fontsize=15)
            plt.ylabel("Cross-section distribution", fontsize=25)
            #ax.set_xlabel(r'$m_{4\ell}$ [GeV]', fontsize=25)
            ax.set_xlabel('mT_ZZ', fontsize=25)
            plt.xticks(fontsize=15)
            plt.yticks(fontsize=25)
            #print(hists_f[sample][sys][term].values())
            #print(len(hists_f[sample][sys][term].values()))
            ax.set_xticks(x_values_virtual)
            ax.set_xticklabels(labels)
            #plt.yscale('symlog', linthresh=1)
            if sys != 'Nominal':
                plt.ylim(0.7, 1.3)
                plt.ylabel("Ratio to Nominal", fontsize=25)
                plt.axhline(y=1, color='black', linestyle='--')
            outdir=OutFolder+"/"+sample+'/'+sys+'/'
            os.system('mkdir -p '+outdir)
            plt.savefig(outdir+'/'+str(i)+'_'+str(term.split(';')[0])+".png",dpi=200)
            plt.close()
            i+=1


parser = argparse.ArgumentParser(description='Read a tree')
parser.add_argument('--inpath',  type=str, default='', help='Add path to the sample directory (only) each campaign accesed separately')
parser.add_argument('--outpath', type=str, default='', help='Add path to output')

args = parser.parse_args()
OutFolder = args.outpath
input_file_loc = args.inpath

OutFolder = 'outputs_22Jul2022/outputs_22Jul2022_plot_mT_ZZ/'
#input_file_loc = 'outputs_04June2022/Merged_code1_Final_WS/'
input_file_loc = 'outputs_22Jul2022/Merged_code1_2l2v_EFT_WS_v2/'

os.system('mkdir -p '+ OutFolder)
procs=['qqZZ_4l','qqZZ_2l2v','ggZZ','VBF','WZ_3lep','Zjets_bkg','tot_emu_bkg','others_bkg',
       'OpgSM','OtpSM','OptSM','OpgSqrd','OtpSqrd','OptSqrd','OpgOtp']

procs=['qqZZ_2l2v']
hists_f = {}
for proc in procs:
    hists_f[proc] = uproot.open(input_file_loc+"/"+proc+"_hists.root")

#print(hists_f['ggZZ']['ExptSys'].keys())
#print(hists_f['ggZZ']['ExptSys'].values())
kwargs = dict(linewidth=3)


pool = mp.Pool(mp.cpu_count())

results = pool.map(plot_hist, procs)

pool.close()
