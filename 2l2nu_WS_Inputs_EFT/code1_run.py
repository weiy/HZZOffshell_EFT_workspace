import os
import sys
from glob import glob
sys.path.append('./Procs_and_Syst/')
sys.dont_write_bytecode = True
import List_Procs

mc_campaigns = ['mc16a','mc16d','mc16e']
# mc_campaigns = ['mc16a']

outname = '2l2v_EFT_WS_wCR'
#outname = '2l2v_EFT_WS_binwidth1GeV'
date = '23Jan2023'
outputPath = './outputs_'+date+'/hists_code1_'+outname
outPlotsPath = './outputs_'+date+'/Plots/'
logPath = './txt_code1_files/'+outname+'/'

pathToFiles = '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hllvv/Run2/minitrees_rel189/r189_llvv_minitrees/'

os.system('mkdir -p outputs_'+date + ' '+outputPath+' '+outPlotsPath)
for mc_campaign in mc_campaigns:
    os.system('mkdir -p '+logPath+'/'+mc_campaign)

SM_procs_1=['qqZZ_4l_0jet','qqZZ_4l_1jet','qqZZ_4l_2jet',
            'qqZZ_2l2v_0jet','qqZZ_2l2v_1jet','qqZZ_2l2v_2jet',
            'ggZZ','VBF']

SM_procs_2=['WZ_3lep_0jet','WZ_3lep_1jet','WZ_3lep_2jet','WW_emu_bkg_1','WW_emu_bkg_2',
            'ttbar_emu_bkg','Wt_emu_bkg','Wtbar_emu_bkg']

SM_procs_3=['Zjets3641{0:02}'.format(j) for j in range(0,27+1)]

SM_procs_4=[proc for proc in List_Procs.others_2l2v]

SM_procs = SM_procs_1 + SM_procs_2 + SM_procs_3 + SM_procs_4

EFT_procs=['OtpSM','OpgSM','OptSM',
           'OpgSqrd','OpgOtp','OtpSqrd',
           'OptSqrd','OptOtp']

# all_procs = EFT_procs + SM_procs
# all_procs = ['qqZZ_2l2v','VBF','ggZZ']
# all_procs = ['OtpSM','OpgSM']
# all_procs = ['qqZZ_2l2v_0jet','qqZZ_2l2v_1jet','qqZZ_2l2v_2jet']
# all_procs = ['qqZZ_4l_0jet','qqZZ_4l_1jet','qqZZ_4l_2jet']
# all_procs = ['ggZZ']
# all_procs = SM_procs_3
# all_procs = EFT_procs
# all_procs = ['WW_emu_bkg_1','WW_emu_bkg_2','ttbar_emu_bkg','Wt_emu_bkg','Wtbar_emu_bkg']
# all_procs = ['WW_emu_bkg_1']
# all_procs = ['ttbar_emu_bkg']
# all_procs = ['qqZZ_2l2v','qqZZ_4l']

# all_procs = SM_procs_1 to 4
all_procs = EFT_procs

backgroup_process = True
output_log = ''
for mc_campaign in mc_campaigns:
    for proc in all_procs:
        sample = os.path.basename(glob(pathToFiles + mc_campaign + '/Systematics/weightSyst/*'+ List_Procs.procs_2l2v[proc].DSID+'*')[0])
        print('Proceed ' + pathToFiles + mc_campaign + '/Systematics/weightSyst/'+ sample +'\n')
        if backgroup_process: 
            output_log = ' &> '+ logPath + '/'+ mc_campaign + '/'+ sample.split('.deriv')[0]+'__Proc_'+proc+'.txt &\n'
        os.system('python make_all_hists_code1.py --inpath '+pathToFiles+' --sample '+sample+' --outpath '+outputPath+' --proc '+proc+' --tree tree_PFLOW -c ' + mc_campaign + output_log)

if backgroup_process: 
    print('\nAll jobs have been running in the background. Please ps to check!')
    print('For output files, please go to folder '+ logPath +' to check!')
