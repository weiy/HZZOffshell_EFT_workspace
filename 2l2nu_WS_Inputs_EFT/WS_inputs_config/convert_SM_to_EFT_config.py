from collections import OrderedDict

date='23Jan2023'

f_in=open('config_file_3SRs_Final_WS_22_09_05.ini','r')

f_out_name=date+'_EFT_2l2v_config_file_wCR_wSys_wMCSTAT_mT_ZZ_data.ini'
f_out=open(f_out_name,'w')

Procs=OrderedDict([('OpgSM','cpG'),
                   ('OtpSM','ctp'),
                   ('OpgSqrd','cpGsqrd*cpG*cpG'),
                   ('OtpSqrd','ctpsqrd*ctp*ctp'),
                   ('OpgOtp','cpGctp*cpG*ctp')])

for line in f_in:
    if 'fileDir' in line:
        f_out.write('fileDir = ./WS_inputs_'+date+'_2l2v_EFT_WS_wCR_final\n')
    elif 'mcsets' in line:
        f_out.write(line.rstrip()+',OpgSM,OtpSM,OpgSqrd,OtpSqrd,OpgOtp\n')
    elif 'qqZZ_2_Part2 = factors' in line:
        f_out.write(line)
        for Proc in Procs:
            f_out.write(Proc+' = poi:'+Procs[Proc]+'; factors:n_'+Proc+',bgyields.txt; sys:'+Proc+'_sys.txt ; global:ATLAS_LUMI(139.0/0.983/1.017)\n')
    elif '_Signal_' in line:
        f_out.write(line.replace('Signal','Bkg'))
    elif 'qqZZ_2_Part2 = SampleCount' in line:
        f_out.write(line)
        for Proc in Procs:
            f_out.write(Proc+' = SampleCount : ATLAS_Signal_'+Proc+'\n')
    elif 'mu_qqZZ_2 = 1.0' in line:
        f_out.write(line+"\n")
        f_out.write('cpG = 0.0\n')
        f_out.write('ctp = 0.0\n')
        f_out.write('cpGsqrd = 0.0\n')
        f_out.write('ctpsqrd = 0.0\n')
        f_out.write('cpGctp = 0.0\n')
    else:
        f_out.write(line)

print("\nEFT config file '"+f_out_name+"' done!")
