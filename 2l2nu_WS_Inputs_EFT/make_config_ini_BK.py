#python3 needed
import numpy as np
import os

print(f'generating config file... (If SyntaxError, please use python3)')

#range_x = [-2.0,1.7]
range_x = [250.0,3000.0]
#range_x = [-5,1]

#binning = 14 #total bins
#binning = 10 #total bins
#binning = 178
#binning = np.linspace(range_x[0],range_x[1],binning+1)

#binning = [-2.0, -1.72, -1.43, -1.15, -0.86, -0.58, -0.29, -0.01, 0.28, 0.56, 1.7]

binning = [250,300,350,400,450,500,550,600,700,800,900,1000,1200,1400,2000,3000]

#f=open('22Jul2022_EFT_2l2v_noCR_wSys_noMCSTAT_mT_ZZ.ini','w')
config_filename='WS_inputs_config/23Jan2023_EFT_2l2v_config_file_wCR_wSys_wMCSTAT_mT_ZZ.ini'
f=open(config_filename,'w')

f.write('[main]\n\n')
f.write('fileDir = ./WS_inputs_23Jan2023_2l2v_EFT_WS_wCR\n\n')
f.write('NPlist = nuisance.txt\n\n')
f.write('data = /eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hllvv/Run2/minitrees_rel189/r189_llvv_minitrees/skimmed_data/skimmed_data_minitree_SR_plus_CR_renameTTree.root\n\n')
obs='mT_ZZ'
#obs='ME_dis'
#obs='ggFNN_MELA'

f.write('categories = ')
# f.write('n_jets_CR_3lep_0jet_Incl_bin_1_13TeV,n_jets_CR_3lep_1jet_Incl_bin_1_13TeV,n_jets_CR_3lep_2jet_Incl_bin_1_13TeV,mT_ZZ_CR_emu_Incl_bin_1_13TeV,mT_ZZ_CR_ZJets_Incl_bin_1_13TeV,')
# for i in range(len(binning)-1):
#     if i != (len(binning)-2):
#         f.write(f'{obs}_SR_2l2nu_Incl_bin_{i+1}_13TeV,')
#     else:
#         f.write(f'{obs}_SR_2l2nu_Incl_bin_{i+1}_13TeV\n\n')

f.write('n_jets_CR_3lep_0jet_Incl_bin_1_13TeV,n_jets_CR_3lep_1jet_Incl_bin_1_13TeV,n_jets_CR_3lep_2jet_Incl_bin_1_13TeV,mT_ZZ_CR_emu_Incl_bin_1_13TeV,mT_ZZ_CR_ZJets_Incl_bin_1_13TeV,mT_ZZ_SR_ggF_Incl_bin_1_13TeV,mT_ZZ_SR_ggF_Incl_bin_2_13TeV,mT_ZZ_SR_ggF_Incl_bin_3_13TeV,mT_ZZ_SR_ggF_Incl_bin_4_13TeV,mT_ZZ_SR_ggF_Incl_bin_5_13TeV,mT_ZZ_SR_ggF_Incl_bin_6_13TeV,mT_ZZ_SR_ggF_Incl_bin_7_13TeV,mT_ZZ_SR_ggF_Incl_bin_8_13TeV,mT_ZZ_SR_ggF_Incl_bin_9_13TeV,mT_ZZ_SR_ggF_Incl_bin_10_13TeV,mT_ZZ_SR_ggF_Incl_bin_11_13TeV,mT_ZZ_SR_ggF_Incl_bin_12_13TeV,mT_ZZ_SR_ggF_Incl_bin_13_13TeV,mT_ZZ_SR_ggF_Incl_bin_14_13TeV,mT_ZZ_SR_ggF_Incl_bin_15_13TeV,mT_ZZ_SR_Mixed_Incl_bin_1_13TeV,mT_ZZ_SR_Mixed_Incl_bin_2_13TeV,mT_ZZ_SR_Mixed_Incl_bin_3_13TeV,mT_ZZ_SR_Mixed_Incl_bin_4_13TeV,mT_ZZ_SR_VBF_Incl_bin_1_13TeV,mT_ZZ_SR_VBF_Incl_bin_2_13TeV,mT_ZZ_SR_VBF_Incl_bin_3_13TeV,mT_ZZ_SR_VBF_Incl_bin_4_13TeV')

#f.write('mcsets = OpgSM,OtpSM,OptSM,OpgSqrd,OtpSqrd,OptSqrd,OpgOtp,OptOtp,qqZZ,ggZZ')
f.write('mcsets = OpgSM,OtpSM,OpgSqrd,OtpSqrd,OpgOtp,qqZZ_2l2v_0jet,qqZZ_2l2v_1jet,qqZZ_2l2v_2jet,qqZZ_4l_0jet,qqZZ_4l_1jet,qqZZ_4l_2jet,ggZZ,VBF,WZ_3lep_0jet,WZ_3lep_1jet,WZ_3lep_2jet,tot_emu_bkg,Zjets_bkg,others_bkg\n\n')

f.write('\n[cuts]\n\n')

f.write('n_jets_CR_3lep_0jet_Incl_bin_1_13TeV = ((lep3rd_mt > 60. && met_signif > 3. && n_bjets == 0 && && n_jets == 0)&&(event_3CR != 0))\n')
f.write('n_jets_CR_3lep_1jet_Incl_bin_1_13TeV = ((lep3rd_mt > 60. && met_signif > 3. && n_bjets == 0 && && n_jets == 1)&&(event_3CR != 0))\n')
f.write('n_jets_CR_3lep_2jet_Incl_bin_1_13TeV = ((lep3rd_mt > 60. && met_signif > 3. && n_bjets == 0 && && n_jets >= 2)&&(event_3CR != 0))\n')
f.write('mT_ZZ_CR_emu_Incl_bin_1_13TeV = (((met_tst >120. && dLepR<1.8 && dMetZPhi > 2.5  && (dPhiJ100met > 0.4 || dPhiJ100met < 0) && met_signif > 10. && n_bjets == 0)&&(event_type == 2 && event_3CR == 0)))\n')
f.write('mT_ZZ_CR_ZJets_Incl_bin_1_13TeV = (((met_tst >120. && dLepR<1.8 && dMetZPhi > 2.5 && met_signif < 9. && n_bjets == 0)&&(event_type != 2 && event_3CR == 0)))\n')


ggF_binning = [250,300,350,400,450,500,550,600,700,800,900,1000,1200,1400,2000,3000]
Mxd_binning = []
VBF_binning = [250,300,350,400,450,500,550,600,700,800,900,1000,1200,1400,2000,3000]
for i in range(len(ggF_binning)-1):
    f.write(f'{obs}_SR_ggF_Incl_bin_{i+1}_13TeV = (((met_tst >120. && dLepR<1.8 && dMetZPhi > 2.5  && (dPhiJ100met > 0.4 || dPhiJ100met < 0) && met_signif > 10. && n_bjets == 0 )&& (event_type != 2 && event_3CR == 0) )&&(({ggF_binning[i]:.2f}<{obs})&&({obs}<{ggF_binning[i+1]:.2f})))\n')

f.write('\n\n[observables]\n\n')
f.write('n_jets_CR_3lep_0jet_Incl_bin_1_13TeV = n_jets:n_jets,1,-1.0,3.0\n')
f.write('n_jets_CR_3lep_1jet_Incl_bin_1_13TeV = n_jets:n_jets,1,-1.0,3.0\n')
f.write('n_jets_CR_3lep_2jet_Incl_bin_1_13TeV = n_jets:n_jets,1,0,100.0\n')
f.write('mT_ZZ_CR_emu_Incl_bin_1_13TeV = mT_ZZ:mT_ZZ,1,0.0,3000.0\n')
f.write('mT_ZZ_CR_ZJets_Incl_bin_1_13TeV = mT_ZZ:mT_ZZ,1,0.0,3000.0\n')
for i in range(len(ggF_binning)-1):
    f.write(f'{obs}_SR_ggF_Incl_bin_{i+1}_13TeV = {obs}:{obs},1,250,3000\n')

f.write('\n\n[coefficients]\n\n')

Procs={'OpgSM':'cpG',
       'OtpSM':'ctp',
       'OpgSqrd':'cpGsqrd*cpG*cpG',
       'OtpSqrd':'ctpsqrd*ctp*ctp',
       'OpgOtp':'cpGctp*cpG*ctp',
       'ggZZ':'',
       'VBF':'',
       'qqZZ_4l_0jet':'mu_qqZZ',
       'qqZZ_4l_1jet':'mu_qqZZ_1',
       'qqZZ_4l_2jet':'mu_qqZZ_2',
       'qqZZ_2l2v_0jet':'mu_qqZZ',
       'qqZZ_2l2v_1jet':'mu_qqZZ_1',
       'qqZZ_2l2v_2jet':'mu_qqZZ_2',
       'WZ_3lep_0jet':'mu_3lep',
       'WZ_3lep_1jet':'mu_3lep_1',
       'WZ_3lep_2jet':'mu_3lep_2',
       'tot_emu_bkg':'mu_emu',
       'Zjets_bkg':'mu_Zjets',
       'others_bkg':''}

for Proc in Procs:
    poi=''
    if Procs[Proc]:
        poi='poi:'+Procs[Proc]+'; '
    if 'mu' not in Procs[Proc]:
        f.write(Proc+' = '+poi+'factors:n_'+Proc+',bgyields.txt; sys:'+Proc+'_sys.txt ; global:ATLAS_LUMI(139.0/0.983/1.017)\n')
    else:
        f.write(Proc+' = '+poi+'factors:n_'+Proc+',bgyields139.txt; sys:'+Proc+'_sys.txt\n')
        

# f.write('\n\n[')
# f.write('n_jets_CR_3lep_Incl_bin_1_13TeV,mT_ZZ_CR_emu_Incl_bin_1_13TeV,mT_ZZ_CR_ZJets_Incl_bin_1_13TeV,')
# for i in range(len(binning)-1):
#     if i != (len(binning)-2):
#         f.write(f'{obs}_SR_2l2nu_Incl_bin_{i+1}_13TeV,')
#     else:
#         f.write(f'{obs}_SR_2l2nu_Incl_bin_{i+1}_13TeV]\n\n')

f.write('[n_jets_CR_3lep_0jet_Incl_bin_1_13TeV,n_jets_CR_3lep_1jet_Incl_bin_1_13TeV,n_jets_CR_3lep_2jet_Incl_bin_1_13TeV,mT_ZZ_CR_emu_Incl_bin_1_13TeV,mT_ZZ_CR_ZJets_Incl_bin_1_13TeV,mT_ZZ_SR_ggF_Incl_bin_1_13TeV,mT_ZZ_SR_ggF_Incl_bin_2_13TeV,mT_ZZ_SR_ggF_Incl_bin_3_13TeV,mT_ZZ_SR_ggF_Incl_bin_4_13TeV,mT_ZZ_SR_ggF_Incl_bin_5_13TeV,mT_ZZ_SR_ggF_Incl_bin_6_13TeV,mT_ZZ_SR_ggF_Incl_bin_7_13TeV,mT_ZZ_SR_ggF_Incl_bin_8_13TeV,mT_ZZ_SR_ggF_Incl_bin_9_13TeV,mT_ZZ_SR_ggF_Incl_bin_10_13TeV,mT_ZZ_SR_ggF_Incl_bin_11_13TeV,mT_ZZ_SR_ggF_Incl_bin_12_13TeV,mT_ZZ_SR_ggF_Incl_bin_13_13TeV,mT_ZZ_SR_ggF_Incl_bin_14_13TeV,mT_ZZ_SR_ggF_Incl_bin_15_13TeV,mT_ZZ_SR_Mixed_Incl_bin_1_13TeV,mT_ZZ_SR_Mixed_Incl_bin_2_13TeV,mT_ZZ_SR_Mixed_Incl_bin_3_13TeV,mT_ZZ_SR_Mixed_Incl_bin_4_13TeV,mT_ZZ_SR_VBF_Incl_bin_1_13TeV,mT_ZZ_SR_VBF_Incl_bin_2_13TeV,mT_ZZ_SR_VBF_Incl_bin_3_13TeV,mT_ZZ_SR_VBF_Incl_bin_4_13TeV]')

for Proc in Procs:
    SB = 'Bkg'
    if 'O' in Proc:
        SB = 'Signal'
    f.write(Proc+' = SampleCount : ATLAS_'+SB+'_'+Proc+'\n')

f.write('\n\n[asimov: asimovData]\n\n')

f.write('cpG = 0.0\n')
f.write('ctp = 0.0\n')
f.write('cpGsqrd = 0.0\n')
f.write('ctpsqrd = 0.0\n')
f.write('cpGctp = 0.0\n')
f.write('mu_qqZZ = 1.0\n')
f.write('mu_qqZZ_1 = 1.0\n')
f.write('mu_qqZZ_2 = 1.0\n')
f.write('mu_emu = 1.0\n')
f.write('mu_3lep = 1.0\n')
f.write('mu_3lep_1 = 1.0\n')
f.write('mu_3lep_2 = 1.0\n')
f.write('mu_Zjets = 1.0\n')

print('Finished!!\nSee '+config_filename)
