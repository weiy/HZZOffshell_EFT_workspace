#!/bin/bash
outname="2l2v_EFT_WS_wCR"
#outname="2l2v_EFT_WS_binwidth1GeV"
# date="07Aug2022"
date="23Jan2023"
outputfileName="./outputs_"$date"/Merged_code1_"$outname
rm -rf $outputfileName
mkdir -p $outputfileName

inputfileName="./outputs_"$date"/hists_code1_"$outname
yield_file="./outputs_"$date"/yields.txt"
outPlotsPath="./outputs_"$date"/Plots/"

filelist="qqZZ_4l_0jet_hists.root qqZZ_4l_1jet_hists.root qqZZ_4l_2jet_hists.root qqZZ_2l2v_0jet_hists.root qqZZ_2l2v_1jet_hists.root qqZZ_2l2v_2jet_hists.root WZ_3lep_0jet_hists.root WZ_3lep_1jet_hists.root WZ_3lep_2jet_hists.root ggZZ_hists.root VBF_hists.root OpgSM_hists.root OtpSM_hists.root OptSM_hists.root OpgSqrd_hists.root OtpSqrd_hists.root OptSqrd_hists.root OpgOtp_hists.root"

for proc in ${filelist};
do hadd -f $outputfileName/$proc $inputfileName/mc16*/$proc;
done;

hadd -f $outputfileName/Zjets_bkg_hists.root $inputfileName/mc16*/Zjets364*
hadd -f $outputfileName/tot_emu_bkg_hists.root $inputfileName/mc16*/*emu_bkg*
hadd -f $outputfileName/others_bkg_hists.root $inputfileName/mc16*/others_*
#python qqZZ_norm_overflow.py --inpath $outputfileName
#python3 plotter_code.py --inpath $outputfileName --outpath $outPlotsPath
#python check_yields_mu.py --inpath $outputfileName >> $yield_file
