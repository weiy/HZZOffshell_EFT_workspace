#run this script by: . build_ws.sh
if [ -z $SOURCEHZZ ];
then
    export PATH=/afs/cern.ch/user/w/weiy/offshell/HZZWorkspace/source/HZZWorkspace/scripts:${PATH}
    export HZZWSCODEDIR=/afs/cern.ch/user/w/weiy/offshell/HZZWorkspace/source/HZZWorkspace/
    cd /afs/cern.ch/user/w/weiy/offshell/HZZWorkspace/build
    source command.txt
    cd -
    SOURCEHZZ=1
fi


outdir="WS_outputs_SM_2l2v"
mkdir -p ${outdir}

wstags=(
"config_file_3SRs_Final_WS_22_09_05_CROnly"
)

for wstag in "${wstags[@]}"
do
    mainCombiner  WS_inputs_Michiel_rel189_22_09_05/config_22_09_05/${wstag}.ini ${outdir}/WS_OffShell_${wstag}.root 2>&1 | tee ${outdir}/log_offshell_${wstag}.log
done

cp ${outdir}/WS_OffShell_${wstag}.root /afs/cern.ch/user/w/weiy/offshell/offshellWS_fit/ws/2l2v/SM/ 
echo "copy workspace to fit directory done"
