#run this script by: . build_ws.sh
if [ -z $SOURCEHZZ ];
then
    export PATH=/afs/cern.ch/user/w/weiy/offshell/HZZWorkspace/source/HZZWorkspace/scripts:${PATH}
    export HZZWSCODEDIR=/afs/cern.ch/user/w/weiy/offshell/HZZWorkspace/source/HZZWorkspace/
    cd /afs/cern.ch/user/w/weiy/offshell/HZZWorkspace/build
    source command.txt
    cd -
    SOURCEHZZ=1
fi


outdir="WS_outputs_EFT_2l2v"
mkdir -p ${outdir}

wstags=(
#"22Jul2022_EFT_2l2v_noCR_wSys_noMCSTAT_mT_ZZ"
#"22Jul2022_EFT_2l2v_noCR_wSys_noMCSTAT_mT_ZZ_cpG0p06"
#"22Jul2022_EFT_2l2v_noCR_wSys_noMCSTAT_mT_ZZ_cpG0p031"
#"22Jul2022_EFT_2l2v_noCR_wSys_noMCSTAT_mT_ZZ_SystCut0p01"
#"22Jul2022_EFT_2l2v_noCR_wSys_noMCSTAT_mT_ZZ_SystCut0p05"
#"22Jul2022_EFT_2l2v_noCR_wSys_noMCSTAT_mT_ZZ_SysOnlyImportant"
#"22Jul2022_EFT_2l2v_noCR_wSys_noMCSTAT_mT_ZZ_SystCut0p05_cpG0p03"
#"07Aug2022_EFT_2l2v_config_file_wCR_wSys_noMCSTAT_mT_ZZ_cpGlin0p06_HOQCDqqZZbin1orderchange"
# "07Aug2022_EFT_2l2v_config_file_wCR_wSys_noMCSTAT_mT_ZZ_CROnly_wData"
# "07Aug2022_EFT_2l2v_config_file_wCR_wSys_noMCSTAT_mT_ZZ"
# "07Aug2022_EFT_2l2v_config_file_wCR_wSys_noMCSTAT_mT_ZZ_cpGlin0p06"
# "07Aug2022_EFT_2l2v_config_file_wCR_wSys_noMCSTAT_mT_ZZ_cpGlinq0p036"
# "07Aug2022_EFT_2l2v_config_file_wCR_wSys_noMCSTAT_mT_ZZ_ctplin16p7"
# "07Aug2022_EFT_2l2v_config_file_wCR_wSys_noMCSTAT_mT_ZZ_ctplinq17p4"
"23Jan2023_EFT_2l2v_config_file_wCR_wSys_wMCSTAT_mT_ZZ_data"
"23Jan2023_EFT_2l2v_config_file_wCR_wSys_wMCSTAT_mT_ZZ_data_cpGlin0p05"
"23Jan2023_EFT_2l2v_config_file_wCR_wSys_wMCSTAT_mT_ZZ_data_cpGlinq0p01"
"23Jan2023_EFT_2l2v_config_file_wCR_wSys_wMCSTAT_mT_ZZ_data_ctplinm18p8"
"23Jan2023_EFT_2l2v_config_file_wCR_wSys_wMCSTAT_mT_ZZ_data_ctplinqm9p1"
)

for wstag in "${wstags[@]}"
do
    mainCombiner  WS_inputs_config/${wstag}.ini ${outdir}/WS_OffShell_${wstag}.root 2>&1 | tee ${outdir}/log_offshell_${wstag}.log
    cp --preserve=timestamps ${outdir}/WS_OffShell_${wstag}.root /afs/cern.ch/user/w/weiy/offshell/offshellWS_fit/ws/2l2v/EFT/
done

bash /afs/cern.ch/user/w/weiy/offshell/offshellWS_fit/ws/upload_to_ox.sh
echo "copy workspace to fit directory done"
