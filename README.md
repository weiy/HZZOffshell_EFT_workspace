# Statistical interpretation of the H4l off-shell EFT analysis 

Clone the repository: `git clone https://:@gitlab.cern.ch:8443/weiy/HZZOffshell_EFT_workspace.git`.

To access the __SM__ branch: `git checkout SM`.

The __master__ branch is about __EFT__.

## Description

This project summarizes all the scripts for preparing input files for the statistical interpretation. The worskpace will be built using `HZZWorkspace`.

## 4l channel

See folder __4lep_WS_inputs_EFT__.

Detailed instructions are work-in-process.

## 2l2v channel

See folder __2l2nu_WS_Inputs_EFT__.

Detailed instructions are work-in-process.