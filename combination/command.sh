# note that all the followings need to be corrected.
if [ -z $SOURCEWSCOMBINER ];
then
    thisdir_weiy=`pwd`
    cd /afs/cern.ch/user/w/weiy/workspaceCombiner
    source setup_lxplus.sh
    cd ${thisdir_weiy}
    SOURCEWSCOMBINER=1
fi

# manager -w combine -x Combination_config_xml/Combination_WS_OffShellHZZ_07Aug2022_EFT_4l2l2v_wCR_wSys_noMC_4lMCFM_2l2vmTZZ.xml

manager -w combine -x Combination_config_xml/Combination_WS_OffShellHZZ_23Jan2023_EFT_4l2l2v_wCR_wSys_wMC_4lNNSM_2l2vmTZZ.xml

mv workspaces/*raw* workspaces/additional_info/
mv workspaces/*tmp* workspaces/additional_info/
mv workspaces/*txt workspaces/additional_info/

cp workspaces/* /afs/cern.ch/user/w/weiy/offshell/offshellWS_fit/ws/combination/EFT/

echo "copy to afs done"

cd /afs/cern.ch/user/w/weiy/offshell/offshellWS_fit/ws
bash upload_to_ox.sh
cd -
